import React from "react";
import {
 Text,
 View,
 Image,
 StyleSheet,
 TouchableOpacity,
 Platform,
 Alert,
 Button,
 SafeAreaView, Dimensions
} from "react-native";
import {
 createBottomTabNavigator,
 createStackNavigator,
 createAppContainer,
 createDrawerNavigator,
 createSwitchNavigator,
 NavigationEvents
} from 'react-navigation';
import MapScreen from './src/screen/MapScreen';
import FavoriteScreen from './src/screen/FavoriteScreen';
import ListScreen from './src/screen/ListScreen';
import FilterScreen from './src/screen/FilterScreen';
import PendingPayment from './src/screen/PendingPayment';
import PaymentHistory from './src/screen/PaymentHistory';
import TenantApplication from './src/screen/TenantApplication';
import MyProperties from './src/screen/MyProperties';
import MyCreditReport from './src/screen/MyCreditReport';
import MyTenants from './src/screen/MyTenants';
import ChangePassword from './src/screen/ChangePassword';
import SideBarDrawer from './src/screen/SideBarDrawer';
import Login from './src/screen/Login';
import PropertyInfo from './src/screen/PropertyInfo';
import LandlordPaymentHistory from './src/screen/LandlordPaymentHistory';
import InwardPendingPayment from './src/screen/InwardPendingPayment';
import OutwardPendingPayment from './src/screen/OutwardPendingPayment';
import ApplyForRent from './src/screen/ApplyForRent';
import Registration from './src/screen/Registration';
import ContactDetails from './src/screen/ContactDetails';
import MyProfile from './src/screen/MyProfile';
import Feedback from './src/screen/Feedback';
import AddCreditReport from './src/screen/AddCreditReport';
import AddNewProperty from './src/screen/AddNewProperty';
import PendingDetails from './src/screen/PendingDetails';
import AddCreditFilter from './src/screen/AddCreditFilter';
import MakeAnOffer from './src/screen/MakeAnOffer';
import LocationSearchScreen from './src/screen/LocationSearchScreen';
import ViewCreditRoportScreen from './src/screen/ViewCreditRoportScreen';
import TenantApplicationView from './src/screen/TenantApplicationView'
import AsyncStorage from '@react-native-community/async-storage';
import EditProperty from './src/screen/EditProperty';
import ViewApplicant from './src/screen/ViewApplicant';

const { width } = Dimensions.get("window");
class AuthLoadingScreen extends React.Component {

  constructor(props) {
    super(props);
   this.state = {
   token:'',
   allData:'',
   }
  }
  componentDidMount() {
    this._bootstrapAsync();
   // this.getCountry();
  }
  async getData(){
    try {
      const value = await AsyncStorage.getItem('Token');
      if(value !== null) {
       console.log('valueeeeeee',value);
      }
    } catch(e) {
      // error reading value
    }
  }


  _bootstrapAsync = async () => {
    const userToken = await AsyncStorage.getItem('Token');
    await AsyncStorage.setItem('Search', 'false');
    await AsyncStorage.setItem('ListSearch', 'false');
    // This will switch to the App screen or Auth screen and this loading
    // screen will be unmounted and thrown away.
    this.props.navigation.navigate(userToken ? 'AppStack' : 'AuthStack');
  };
  render() {

    return (
      <View style={{flex:1}}></View>
    );
    }
}



    
const App = createStackNavigator({
 //Constant which holds all the screens like index of any book
   MapScreen: { screen: MapScreen },
   //First entry by default be our first screen if we do not define initialRouteName
   ListScreen: { screen: ListScreen },
   FavoriteScreen: { screen: FavoriteScreen },
   FilterScreen: { screen: FilterScreen },
   PropertyInfo: { screen: PropertyInfo, },
   ApplyForRent: {screen: ApplyForRent,},
   LocationSearchScreen: {screen: LocationSearchScreen,},
   ViewCreditRoportScreen: {screen: ViewCreditRoportScreen,},
   TenantApplicationView:{screen:TenantApplicationView},
 },
 {
   headerMode: 'none',
   navigationOptions: {
   headerVisible: false,
 }
 }
);

const AuthStack = createStackNavigator(
  {
   Login:{screen: Login,
    navigationOptions: {
      title: 'Home',
      header: null //this will hide the header
    },
  } ,
   Registration:{screen: Registration,
    navigationOptions: {
      title: 'Home',
      header: null //this will hide the header
    },
  } ,
  },

);
const MyDrawerNavigator = createDrawerNavigator({
  Home_Menu_Label: {

    screen: App,
 
  },
  ViewApplicant: {

    screen: ViewApplicant,
 
  },
  PendingPayment: {

   screen: PendingPayment,

 },
 PaymentHistory: {

   screen: PaymentHistory,

 },
  TenantApplication: {

   screen: TenantApplication,

 },
 MyProperties: {

   screen: MyProperties,

 },
  MyCreditReport: {

   screen: MyCreditReport,

 },
  MyTenants: {

   screen: MyTenants,

 },
  ChangePassword: {

   screen: ChangePassword,

 },
  SideBarDrawer: {

   screen: SideBarDrawer,

 },
  LandlordPaymentHistory: {

   screen: LandlordPaymentHistory,

 },
 InwardPendingPayment: {

   screen: InwardPendingPayment,

 },
 OutwardPendingPayment: {

   screen: OutwardPendingPayment,

 },
 ContactDetails: {

   screen: ContactDetails,
 },
 MyProfile: {

   screen: MyProfile,
 },
 Feedback: {

   screen: Feedback,
 },
 AddCreditReport: {

   screen: AddCreditReport,
 },
  AddNewProperty: {

   screen: AddNewProperty,
 },
 PendingDetails: {

  screen: PendingDetails,
},
AddCreditFilter: {

  screen: AddCreditFilter,
},
MakeAnOffer: {

  screen: MakeAnOffer,
},
EditProperty:{

  screen:EditProperty

}
},
{
 unmountInactiveRoutes:true,
 drawerPosition:'left',
// initialRouteName:'FilterScreen',
 contentComponent :SideBarDrawer,
// drawerBackgroundColor:'#fff',
 drawerWidth:2*width/3
}
);
export default createAppContainer(
  createSwitchNavigator(
    {
      
      AuthLoading: AuthLoadingScreen,
      AppStack: MyDrawerNavigator,
      AuthStack: AuthStack,
     //  postLogin: postLogin
    },
    {
      initialRouteName: "AuthLoading"
      //  initialRouteName: "Example"
    }
  )
);