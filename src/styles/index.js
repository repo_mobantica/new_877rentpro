import {StyleSheet} from "react-native";
import {widthPercentageToDP as wp, heightPercentageToDP as hp} from 'react-native-responsive-screen';

// function mt(value) {
//     return {marginTop:value}
// }

const styles = StyleSheet.create({
   
    errorMessage:{
        fontSize:11,
        color:'red',
        marginLeft:'10%'
      },
      titleView:{
        height:hp('8%'), 
        justifyContent:'center',
         alignItems:'center'
     },
     titleText:{
       fontSize:16,
        fontWeight:'bold'
   },
   requiredField:{
    flexDirection:'row',
     alignItems:'center'
  },
  requiredDot:{
    height:6,
     width:6,
      borderRadius:3,
       backgroundColor:'red',
         marginStart:wp('8%'),
         marginTop:hp('2%'), 
       },
       textinputBorder:{
        marginStart:wp('5%'), 
        marginEnd:wp('5%'), 
         backgroundColor:'gray',
          height:1,
           width:'85%',
           marginTop:-hp('1%')
           },
           profileTextBorder:{
            backgroundColor:'gray',
             height:1,
              width:'100%',
              marginBottom:hp('3%'),
              marginTop:-hp('2%')
              },
           nameStyle:{
            marginTop:hp('2%'), 
             marginStart:wp('10%'),
          // backgroundColor:'yellow', 
         },
         nameStyleWithDot:{
             marginTop:hp('2%'), 
             marginStart:wp('2%'),
         },
         textTitle:{
            marginTop:hp('1%'), 
          // backgroundColor:'yellow', 
         },
         valueStyle:{
            marginTop:hp('1%'), 
         marginBottom:hp('3%'),
         //color:'#0d3494',
         //fontSize:16
         },

});

export default styles;
