import React, { Component } from 'react';
import { Text, View, SafeAreaView, Image, TouchableOpacity, StyleSheet, Dimensions, Alert,
Button, FlatList, TextInput, ScrollView } from 'react-native';
import {widthPercentageToDP as wp, heightPercentageToDP as hp} from 'react-native-responsive-screen';
import CheckBox from 'react-native-check-box'
import commonStyles from "../styles/index";
import RNPickerSelect from 'react-native-picker-select';
import ToolbarWithNavAndHome from '../component/ToolbarWithNavAndHome';
export default class MakeAnOffer extends Component {
    constructor(props) {
    super(props);

    this.state = {
      country:''
    };   
  }
  render() {
    
    return (
       <SafeAreaView style={styles.container}>
      <View style={styles.container}>
         <View style={styles.mainView}>  
            <View style={styles.titleView}>
              <Text style={{fontWeight:'bold'}}>Payment Details</Text>
              <Image
                     style={{height:20, width:20, margin:'4%'}}
                     source={require("../image/cancel.png")}
                   />
            </View>
            <View style={styles.totalOuterView}>
              <View style={styles.usdView}>
              <Text style={styles.usdText}>Total : 39 USD</Text>
              </View>
            </View>

            <Text style={styles.nameStyle}>Card Number</Text>
            <View style={styles.textView}>
            <Text>Valid Card Number</Text>
              <Image
                     style={styles.iconStyle}
                     source={require("../image/WalletBlue.png")}
                   />
            </View>
            <View style={commonStyles.textinputBorder}></View>

            <Text style={styles.nameStyle}>Card Holder's Name</Text>
            <View style={styles.textView}>
            <Text >Name</Text>
              <Image
                     style={styles.iconStyle}
                     source={require("../image/user.png")}
                   />
            </View>
            <View style={commonStyles.textinputBorder}></View>
            
            <View style={styles.cwMainView}>
            <Text style={styles.nameStyle}>Expiration Date</Text>
            <Text style={[styles.nameStyle, { marginRight:'15%'}]}>CVV</Text>
            </View>
            <View style={{flexDirection:'row'}}>
             <View style={styles.pickerView}>  
            <RNPickerSelect
            onValueChange={(value) => console.log(value)}
            items={[
                { label: '01', value: '01' },
                { label: '02', value: '02' },
                { label: '03', value: '03' },
            ]}
        />
             </View> 
             <View style={styles.pickerView}>  
            <RNPickerSelect
            onValueChange={(value) => console.log(value)}
            items={[
                { label: '2019', value: '2019' },
                { label: '2018', value: '2018' },
                { label: '2017', value: '2017' },
            ]}
        />
             </View> 
             <View style={styles.cwView}> 
             <Text>CVV</Text>
             </View>
             </View>
            <View style={commonStyles.textinputBorder}></View>

            <Text style={styles.nameStyle}>Zip Code</Text>
            <View style={styles.textView}>
            <Text >413102</Text>
            
            </View>
            <View style={commonStyles.textinputBorder}></View>

            <TouchableOpacity style={styles.lastButton}
      onPress={this.toggleModal}>
      <Text style={styles.sendText}> Pay Now</Text>
     </TouchableOpacity>

        </View>
          </View>
           </SafeAreaView>
    );
  }
}
const styles = StyleSheet.create({
container: {
    flex: 1,
   backgroundColor: "#ededed"
  },
  mainView:{
    height:'82%',
     width:'86%',
     margin:'7%', 
     backgroundColor:'white',
      borderRadius:10, 
     
},
lastButton:{
  width:'100%',
     height:'10%', 
     borderBottomLeftRadius:10,
    borderBottomRightRadius:10, 
     backgroundColor:'#034d94', 
     justifyContent:'center', 
     alignItems:'center',
     marginTop:'5%'
},
   titleText:{
     color:'#034d94',
      fontWeight:'bold',
      fontSize:15
   },
   sendText:{
    color:'white',
     fontWeight:'bold'
  },
 titleView:{
  flexDirection:'row',
   alignItems:"center", 
   justifyContent:'space-between',
    paddingStart:'7%',
    paddingEnd:'7%',
     height:'10%',
      width:'100%',
      marginBottom:'2%',
     backgroundColor:'#e0e0e0',
     borderTopRightRadius: 11,
     borderTopLeftRadius: 11,
 } ,
 totalOuterView:{
  flexDirection:'row',
   alignItems:"center", 
   justifyContent:'space-between',
     height:'10%',
      width:'86%',
      marginBottom:'2%',
     backgroundColor:'gray',
     marginTop:'2%', backgroundColor: 'white'
 } ,
 nameStyle:{
  marginTop:'4%', 
   marginStart:'7%',
// backgroundColor:'yellow', 
},
textView:{
  flexDirection:'row',
   alignItems:"center", 
   justifyContent:'space-between',
    paddingStart:'7%',
    paddingEnd:'7%',
     height:'10%',
      width:'100%',
      marginBottom:'2%',
   
},
usdView:{
  width:'50%',
   height:'70%',
    backgroundColor:'#034d94', 
   justifyContent:'center',
    alignItems:'center',
     borderWidth:1,
      borderRadius:10,
       marginStart:'55%'
},
usdText:{
  fontWeight:'bold', color:'white'
},
iconStyle:{
  height:hp('4%'), width:hp('4%'), 
},
cwMainView:{
  height:'8%',
   flexDirection:'row',
    alignItems:'center', 
    justifyContent:'space-between'
},
pickerView:{
  width:'32%', margin:'3%'
},
cwView:{
  width:'20%',
   margin:'3%',
    justifyContent:'center', 
}
});

