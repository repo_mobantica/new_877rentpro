import React, { Component } from 'react';
import { Text, View, SafeAreaView, Image, TouchableOpacity, StyleSheet, Dimensions, Alert, FlatList, TextInput,
Button } from 'react-native';
import {widthPercentageToDP as wp, heightPercentageToDP as hp} from 'react-native-responsive-screen';
import Toolbar from '../component/Toolbar';
import { Searchbar } from 'react-native-paper';
import { ScrollView } from 'react-native-gesture-handler';
import Communications from 'react-native-communications';
import {NavigationEvents} from 'react-navigation';
 import AsyncStorage from '@react-native-community/async-storage';
  import Geolocation from 'react-native-geolocation-service';
  import Geocoder from 'react-native-geocoder';
  import Api from "../Api/Api";
import { 
  ProgressDialog, 
} from "react-native-simple-dialogs";
  var token ='';
export default class List extends Component {
  state = {
    search: '', 
    flatlistData:[],
    check:false,
    isFavorite:false,
    showProgress: false,
    currentLat:'',
    currentLong:'',
    connection_Status : ""

  }

  async componentDidMount(){
    
   location = await AsyncStorage.getItem('LocationPermission');
     token = await AsyncStorage.getItem('Token');
    console.log(token);
   this.getCurrentLocation();
}


getCurrentLocation(){
  if(location != ''){
      Geolocation.getCurrentPosition(
          (position) => {
              console.log(position);
              this.setState({
                currentLat:position.coords.latitude,
                currentLong : position.coords.longitude
              }, function(){
                console.log('this.state.currentLat',this.state.currentLat);
                console.log('this.state.currentLong',this.state.currentLong);
                 this.fetchData();
              }
              )
          },
          (error) => {
              // See error code charts below.
              console.log(error.code, error.message);
          },
          { enableHighAccuracy: true, timeout: 15000, maximumAge: 10000 }
      );
  }  
}
  
  fetchData(){
    console.log('inside fetch');
    console.log('llat',this.state.currentLat);
    console.log('loong',this.state.currentLong);
    this.setState({
      showProgress: true
    });
    Api("property_list", {
      userid:token,
      lat:this.state.currentLat,
      long:this.state.currentLong
    })
      .then((responseJson) => {
      
    if(responseJson.status==1){
      this.setState({
        showProgress: false
      });
       let data = responseJson.response.data;
       this.setState({
         flatlistData:data
       })
       console.log('inside sucess',responseJson);
      }
      else if(responseJson==false)
      {
        this.setState({
          showProgress: false,
          flatlistData:[],
        });
        alert("No internet");
      }
    else{
      Alert.alert(responseJson.message);
      console.log('inside else fall',responseJson.message);
      this.setState({
        showProgress: false,
         flatlistData:[],
      });
    }
  }).catch((error) => {
console.error(error);
this.setState({
  showProgress: false
});
});
  }

  addRemoveFavorite(status, index, id){
    let flatlistData= this.state.flatlistData;
     let isFavorite = status;
     isFavorite =!isFavorite;
     flatlistData[index].isfavorite = ! flatlistData[index].isfavorite;
     this.setState({
      flatlistData:flatlistData
     })
     //"userid":"91","propertyid":"863", "addremovetype":"false
     this.setState({
      showProgress: true
    });
     
    Api("add_remove_favorites", {
      userid: token,
        propertyid: id,
        addremovetype: isFavorite
    })
      .then((responseJson) => {
        this.setState({
          showProgress: false
        });
    if(responseJson.status==1){
      console.log(responseJson.message);
      }
      else if(responseJson==false)
      {
        this.setState({
          showProgress: false
        });
        alert("No internet");
      }
    else{
      Alert.alert(responseJson.message);
    }
  }).catch((error) => {
console.error(error);
this.setState({
  showProgress: false
});
});
  }
  componentWillUnmount(){
    AsyncStorage.setItem('ListSearch', 'false');
  }
   searchLocation(value){
      Geocoder.geocodeAddress(value).then(res => {
        console.log('Serch  data');
        console.log('lng',res[0].position.lng,'lat',res[0].position.lat );
    
        this.setState({
             currentLat : res[0].position.lat,
             currentLong : res[0].position.lng,
        }, function(){
           console.log('sear curent data');
            console.log(this.state.currentLat,  this.state.currentLong);
             this.fetchData();
        });
      })
      .catch(err => console.log('in serch error ...////',err))
    }

  _renderItem = ({item, index}) => {
   let halfbaths =item.halfbaths;
   if(halfbaths == 0){
    halfbaths ='';
   }
   else{
    halfbaths ='1/2';
   }
    return(
    <View style={styles.flatListView}>
      <View style={styles.firstRow}>
    {/* <Text style={[styles.address,{width:'82%',}]} numberOfLines={1}>{item.address}</Text> */}
    <Text style={[styles.address,{width:'82%',}]} numberOfLines={1}>{item.id}</Text>
    <TouchableOpacity onPress={()=> this.addRemoveFavorite(item.isfavorite, index, item.id)}>
       {
        item.isfavorite ?
        <Image
        style={styles.icon}
         source={require('../image/heart.png')}
       />
       :
       <Image
       style={styles.icon}
        source={require('../image/Like.png')}
      />
      }
   
         </TouchableOpacity>  
    </View>
  <TouchableOpacity onPress={()=> this.props.navigation.navigate('PropertyInfo', {Property_id:item.id})}>
   <Image
          style={styles.displayImage}
          source={{uri: item.image_url}}
       />
    
    <Text style={styles.userName}>{item.contact_details.firstname} {item.contact_details.lastname}</Text>
    <Text style={styles.noOfItemtext}>{item.bedrooms} Bedrooms | {item.fullbaths} {halfbaths} Bathrooms </Text>
     {/* <Text style={styles.address} numberOfLines={2}>{item.display_address}</Text> */}
     <View style={styles.dateRow}>
     <Text style={styles.address}>${item.minrent} - {item.maxrent}</Text>
       <View style={{flexDirection:'row'}}>
       <Image
           style={styles.icon}
          source={require('../image/clock.png')}
       />
    <Text style={{marginLeft:'2%'}}>new</Text>
    </View>
    </View>
    </TouchableOpacity>
     
    <View style={styles.offerOuterView}>
                  <TouchableOpacity style={styles.rentApplyView}
                 onPress={() => Communications.phonecall(item.contact_details.mobile, true)}>
                  <Image
          style={styles.icon}
          source={require('../image/Phone.png')}
       />
       <Text style={{color:'white'}}> Phone</Text>
               </TouchableOpacity> 
               <TouchableOpacity style={styles.makeOfferView}
                onPress={() => Communications.email([item.contact_details.email],null,null,
                  'Enquiry Mail','')}>
               <Image
          style={styles.icon}
          source={require('../image/Email.png')}
       />
       <Text style={{color:'white'}}> Email</Text>
               </TouchableOpacity> 
               </View>

    </View>
  );
  }
  render() {
    return (
       <SafeAreaView style={styles.container}>
      <View style={styles.container}>

      <Toolbar  title={"Address "} />
      <View style={{height:'78%'}}>
        <ScrollView>
     
      <TouchableOpacity
          activeOpacity={0.9}
          onPress={()=>this.props.navigation.navigate("LocationSearchScreen",{returnScreen:'List'})}
          style={[
            styles.searchBox,
            {
              // justifyContent: "center",
              // alignItems: "center",
              flexDirection:'row'
            }
          ]}
          >
       {/* <Searchbar
        placeholder="Current Location"
        onChangeText={query => { this.setState({ search: query }, function(){
          this.searchLocation();
        }); }}
        value={this.state.search}
        placeholderTextColor= '#034d94' 
        icon ={require("../image/my_location.png")} 
        // style={{backgroundColor:'transparent', height:hp('6.8%'), borderRadius:35, width:'100%'}}
        style={{
          height: hp("6.8%"),
          borderRadius: 35,
          width: "100%",
          backgroundColor: "white"
        }}
      /> */}
      <View style={{width:'20%', justifyContent: "center",alignItems: "center",}}>
          <Image
        style={{height:hp('4%'), width:hp('4%'), }}
         source={require("../image/my_location.png")} 
       />
       </View>
       {this.state.search !='' ?
        <Text style={{width:'80%', alignSelf:'center', fontSize:15}}>{this.state.search}</Text>
        :
        <Text style={{width:'80%', alignSelf:'center', fontSize:15}}>Current Location</Text>
       }
      
       {/* <TextInput
          style={{width:'80%', backgroundColor:'red'}}
           underlineColorAndroid = "transparent"
          placeholder="Current Location"
          onChangeText={(username) => this.setState({search:username,
          })}
          value={this.state.search}
        /> */}
      </TouchableOpacity>
        
        <NavigationEvents onDidFocus={() => {
             
             AsyncStorage.getItem('ListSearch').then((value) =>
             {
               if(value=='false'){
                 this.getCurrentLocation()
              
               }
               else{
              this.setState({
                search:value
              },function(){
               this.searchLocation(value);
              })
               }
             }
             )
     }} />

             <FlatList
              data={this.state.flatlistData}
              extraData={this.state}
            keyExtractor={(item, index) => index.toString()}
              renderItem={this._renderItem}
            />
            {/* </View> */}
         <View style={{height:hp('1%'), width:'100%'}}></View>
         </ScrollView>
         </View>
          
          <View style={styles.tabView}>
          <TouchableOpacity onPress={() => this.props.navigation.navigate('MapScreen')}>
          <View style={[styles.subTab,{backgroundColor:'#034d94'}]}>
          <Image  style={styles.menuIcon}
           source={require("../image/map.png")}/>
          <Text style={[styles.fontStyle,{color:'white'}]}>MAP</Text>
          </View>
          </TouchableOpacity>
          <View style={styles.subTab}>
          <Image  style={styles.menuIcon}
           source={require("../image/map.png")}/>
          <Text style={styles.fontStyle}>LIST</Text>
          </View>
          
          <TouchableOpacity onPress={() => this.props.navigation.navigate('FavoriteScreen')}>
          <View style={[styles.subTab,{width:wp('25.3%'),backgroundColor:'#034d94'}]}>
          <Image  style={styles.menuIcon}
           source={require("../image/map.png")}/>
          <Text style={[styles.fontStyle,{color:'white'}]}>FAVORITE</Text>
          </View>
          </TouchableOpacity>
          <ProgressDialog
          // title="Progress Dialog"
          activityIndicatorColor="red"
          activityIndicatorSize="large"
          animationType="fade"
          message="Please, wait..."
          visible={this.state.showProgress}
        />
          </View >
          </View>
           </SafeAreaView>
    );
  }
}
const styles = StyleSheet.create({
container: {
    flex: 1,
   backgroundColor: "#ededed"
  },
  menuIcon: {
    height: "40%",
    resizeMode: "contain",
    width: 30,
    marginStart: 5
  },
  tabView : {
    position: 'absolute',
    height: hp('9%'),
   margin: wp('10%'),
   marginBottom:hp('5%'),
   marginTop:hp('85%'),
    //justifyContent: "flex-end",
    backgroundColor: '#034d94',
    borderRadius:35,
    flexDirection:'row'
  },
  subTab: {
  width:wp('23.1%'),
  margin:wp('1.5%'),
  justifyContent:'center',
  alignItems:'center',
  backgroundColor:'white',
  borderRadius:33
  },
  fontStyle:{
    fontSize:12
  },
  map: {
    marginTop:hp('9%'),
   position: 'absolute',
   left: 0,
   right: 0,
  height:hp('72%')
 },
 flatListView:{
   height:hp('35.8%'),
   margin:hp('3%'),
   marginTop:hp('3%'),
   marginBottom:hp('0%'),
   borderRadius:11,
   backgroundColor:'white',
     
},
   dateRow:{
  flexDirection:'row', 
 height:hp('4'),
   alignItems:'center',
   justifyContent:'space-between',
   paddingRight:'6%'
  },
  displayImage:{
    width: '100%',
    height: hp('10%'), 
   },
   userName:
   {color:'#034d94',
    marginStart:wp('3.7%'), 
    marginTop:wp('3.7%'), 
    fontWeight:'bold'
    },
  address:{
    color:'black',
     marginStart:wp('3.7%'),
      fontWeight:'bold',
      marginEnd:wp('3.7%')
  },
       icon:{
         height:hp('3%'),
         width:hp('3%'),
         marginEnd:wp('2%'),
        
     },
       rentApplyView:{
        width:'50%',
         backgroundColor:'#034d94', 
        borderBottomLeftRadius: 11,
         borderEndWidth:1,
         justifyContent:'center', 
        alignItems:'center',
         borderColor:'white',
         flexDirection:'row', 
      },
      makeOfferView:{
        justifyContent:'center',
         alignItems:'center',
        borderBottomRightRadius: 11,
         width:'50%',
          backgroundColor:'#034d94',
           borderColor:'white',
           flexDirection:'row', 
      },
      offerOuterView:{
        height:hp('7%'), 
        marginTop:hp('1%'),
         flexDirection:'row'
      },
      firstRow:{
        height:hp('7%'),
          width:'100%',
          borderTopLeftRadius: 11, 
    borderTopRightRadius: 11,
     paddingRight:wp('3.7%'),
      flexDirection:'row', 
    //backgroundColor:'#008f2c',
     alignItems:'center', 
     justifyContent:'space-between'
      },
      searchBox : {
        height: hp('7%'),
       marginStart: wp('10%'),
       marginEnd: wp('10%'),
       marginBottom:hp('1%'),
       marginTop:hp('2%'),
        backgroundColor: 'white',
        borderRadius:35,
        flexDirection:'row',
        width:'80%'
      },
      noOfItemtext:{
        color:'gray',
         marginStart:wp('3.7%'),
          fontSize:11.5
          },
});
 