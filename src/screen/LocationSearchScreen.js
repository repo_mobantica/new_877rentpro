import { GooglePlacesAutocomplete } from 'react-native-google-places-autocomplete';
import React, { Component } from 'react';

const homePlace = {description: 'Home', geometry: { location: { lat: 48.8152937, lng: 2.4597668 } }};
const workPlace = {description: 'Work', geometry: { location: { lat: 48.8496818, lng: 2.2940881 } }};

import AsyncStorage from "@react-native-community/async-storage";
var pageName;
export default class AddCreditFilter extends Component {
     componentDidMount(){
          pageName = this.props.navigation.getParam('returnScreen');
          console.log('pageName pageName');
          console.log(pageName);
     }
    render(){
    return(
    <GooglePlacesAutocomplete
    placeholder="Search"
    minLength={2} // minimum length of text to search
    autoFocus={true}
    returnKeyType={'search'} // Can be left out for default return key https://facebook.github.io/react-native/docs/textinput.html#returnkeytype
    listViewDisplayed="auto" // true/false/undefined
    fetchDetails={true}
    renderDescription={row => row.description} // custom description render
    onPress={(data, details = null) => {
        var that = this;

     if(pageName ==='Map'){
        AsyncStorage.setItem('Search', data.description);
       }
       else if(pageName ==='List'){
        AsyncStorage.setItem('ListSearch', data.description);
       }
     setTimeout(function(){
       if(pageName ==='Map'){
        that.props.navigation.navigate("MapScreen");
       }
       else if(pageName ==='List'){
        that.props.navigation.navigate("ListScreen");
       }
      }, 3000);
   
    }}
    getDefaultValue={() => {
      return ''; // text input default value
    }}
    query={{
      // available options: https://developers.google.com/places/web-service/autocomplete
      key: 'AIzaSyAGF8cAOPFPIKCZYqxuibF9xx5XD4JBb84',
      language: 'en', // language of the results
      //types: '(cities)', // default: 'geocode'
    }}
    styles={{
      description: {
        fontWeight: 'bold',
      },
      predefinedPlacesDescription: {
        color: '#1faadb',
      },
      textInputContainer: {
        height: 55,
      },
      textInput: {
        //marginLeft: 0,
       // marginRight: 0,
        height: 40,
        color: '#5d5d5d',
        fontSize: 16,
        borderRadius:40/2
      },
    }}
    //currentLocation={true} // Will add a 'Current location' button at the top of the predefined places list
    //currentLocationLabel="Current location"
    nearbyPlacesAPI="GooglePlacesSearch" // Which API to use: GoogleReverseGeocoding or GooglePlacesSearch
    GoogleReverseGeocodingQuery={{
      // available options for GoogleReverseGeocoding API : https://developers.google.com/maps/documentation/geocoding/intro
    }}
    GooglePlacesSearchQuery={{
      // available options for GooglePlacesSearch API : https://developers.google.com/places/web-service/search
      rankby: 'distance',
      types: 'food',
    }}
    filterReverseGeocodingByTypes={[
      'locality',
      'administrative_area_level_3',
    ]} // filter the reverse geocoding results by types - ['locality', 'administrative_area_level_3'] if you want to display only cities
   // predefinedPlaces={[homePlace, workPlace]}
    debounce={200}
  />
);
    }
}

{/* <GooglePlacesAutocomplete
    placeholder="Search"
    minLength={2} // minimum length of text to search
    autoFocus={false}
    returnKeyType={'search'} // Can be left out for default return key https://facebook.github.io/react-native/docs/textinput.html#returnkeytype
    listViewDisplayed="auto" // true/false/undefined
    fetchDetails={true}
    renderDescription={row => row.description} // custom description render
    onPress={(data, details = null) => {
        var that = this;

     AsyncStorage.setItem('Search', data.description);
     setTimeout(function(){
 
        that.props.navigation.navigate("MapScreen")
   
      }, 3000);
   
    }}
    getDefaultValue={() => {
      return ''; // text input default value
    }}
    query={{
      // available options: https://developers.google.com/places/web-service/autocomplete
      key: 'AIzaSyAGF8cAOPFPIKCZYqxuibF9xx5XD4JBb84',
      language: 'en', // language of the results
      //types: '(cities)', // default: 'geocode'
    }}
    styles={{
      description: {
        fontWeight: 'bold',
      },
      predefinedPlacesDescription: {
        color: '#1faadb',
      },
    }}
    //currentLocation={true} // Will add a 'Current location' button at the top of the predefined places list
    //currentLocationLabel="Current location"
    nearbyPlacesAPI="GooglePlacesSearch" // Which API to use: GoogleReverseGeocoding or GooglePlacesSearch
    GoogleReverseGeocodingQuery={{
      // available options for GoogleReverseGeocoding API : https://developers.google.com/maps/documentation/geocoding/intro
    }}
    GooglePlacesSearchQuery={{
      // available options for GooglePlacesSearch API : https://developers.google.com/places/web-service/search
      rankby: 'distance',
      types: 'food',
    }}
    filterReverseGeocodingByTypes={[
      'locality',
      'administrative_area_level_3',
    ]} // filter the reverse geocoding results by types - ['locality', 'administrative_area_level_3'] if you want to display only cities
   // predefinedPlaces={[homePlace, workPlace]}
    debounce={200}
  /> */}