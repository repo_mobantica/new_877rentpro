import React, { Component } from 'react'
import { AppRegistry, StyleSheet, Text, View, Button, TouchableOpacity, Image, Alert } from 'react-native'
import TenantApplicationOne from '../component/TenantApplicationOne';
import TenantApplicationTwo from '../component/TenantApplicationTwo';
 
import TenentApplicationThree from '../component/TenentApplicationThree';
import TenentApplicationFour from '../component/TenentApplicationFour';
import Swiper from 'react-native-swiper';
import ToolbarWithNavigation from '../component/ToolbarWithNavigation';
import AsyncStorage from '@react-native-community/async-storage';
import {widthPercentageToDP as wp, heightPercentageToDP as hp} from 'react-native-responsive-screen';
import Api from "../Api/Api"
import Toast from 'react-native-simple-toast';
import { 
  ProgressDialog, 
} from "react-native-simple-dialogs";

var token= '';
var Property_id ='';
var idUrlUpload ='';
var incomeUrlUpload = '';
var data=null;
var Edit=null;
const renderPagination = (index, total, context) => {

  return (
    <View >
      <Text style={{ color: 'grey' }}>
        <Text >{index + 1}</Text>/{total}
      </Text>
    </View>
  )
}

export default class TenantapplicationView extends Component {

  constructor(props){
    super(props);
    this.onPressNext = this.onPressNext.bind(this);
    this.onPressPrev = this.onPressPrev.bind(this);
    this.state = {
      idxActive: 0,
      applyOne:'',
      applyTwo:'',
      applyThree:'',
      applyFour:'',
      applyFive:'',
      applySix:'',
      applySeven:'',
      applyEight:'',
      showProgress:false,
      set:false
    }
 }

 
 onPressPrev = () => {
  const {idxActive} = this.state;
  if (idxActive > 0) {
    this.refs.swiper.scrollBy(-1)
  }
  else{
    //  this.props.navigation.navigate('PropertyInfo');
   }
}

componentWillMount(){
  console.log('9999999');
   this.getData();
   
}
 async getData(){
    try {
      const value = await AsyncStorage.getItem('Token');
      this.setState({
        showProgress: true
      });
      token=value;
      Property_id =  this.props.navigation.getParam('id');
      Edit =  this.props.navigation.getParam('edit');
   
      Api("view_application", {
        application_id:Property_id,
      
      })
        .then(responseJson => {
          if (responseJson.status == 1) {
           data=responseJson.response.data;
           this.setState({
            showProgress: false,
            set:true
          });
          }
         else if(responseJson==false)
          {
            alert("No internet");
            this.setState({
              showProgress: false,
              set:false,
            });
          }
           else {
            Alert.alert(responseJson.message);
            this.setState({
              showProgress: false,
              set:false,
            });
          }
    })
    } catch(e) {
      // error reading value
    }
  }

onSubmitPress = () => {
  this.refs.swiper.scrollBy(1);
  const {idxActive} = this.state;
  console.log('active pageeee');
  console.log(this.state.idxActive);
 // Probably best set as a constant somewhere vs a hardcoded 5
 let eight = this.refs.tenantViewFour.nextPress();
 //alert(JSON.stringify(eight))
    if(eight.isNext){
     this.setState({
       applyFour: eight
     },function(){
       
      for(let i =0; i<2; i++){
        console.log('iiiii', i);
        if(i== 0){
         this.convertImage('idUrl');
        }
        else{
         this.convertImage('incomeUrl');
        console.log('incomeUrl');
        }
      }
    })
    }
  }
 
  convertImage(item){
          var baseImage;
          console.log('item', item);
            if(item === 'idUrl'){
              baseImage = this.state.applyFour.idUrl;
            }
            else if(item === 'incomeUrl'){
              baseImage = this.state.applyFour.incomeUrl;
            }
          
    //         fetch('https://1800rentpro.com/rentpro_api/api/upload_image', {
    //         method: 'POST',
    //         headers: {
    //                 'Accept': 'application/json',
    //                 'Content-Type': 'application/json',
    //                   },
    //         body: JSON.stringify({
    //           image: baseImage,
    //           type:"property"
    //               })
    //       }).then((response) => response.json())
    //         .then((responseJson) => {
            
    //       if(responseJson.status==1){
    //         this.setState({
    //           showProgress: false
    //       });
    //       console.log('mouse');
    //     console.log(responseJson);
            
    //     if(item === 'idUrl'){
    //      idUrlUpload = responseJson.response.data;
    //      console.log('idUrl');
    //      console.log(idUrlUpload);
    //     }
    //     else if(item === 'incomeUrl'){
    //       incomeUrlUpload = responseJson.response.data;
    //       console.log('idUrl');
    //       console.log(incomeUrlUpload);
        this.saveData();
    //     }
    //         }
    //       else{
    //         Alert.alert(responseJson.message);
    //         this.setState({
    //           showProgress: false
    //         });                  
    //       }
    //     }).catch((error) => {              
    //   console.error(error);
    //   this.setState({
    //     showProgress: false
    //   });
    // });
  
       
  }
    saveData(){
     
    idUrlUpload = this.state.applyFour.idUrl;
     incomeUrlUpload = this.state.applyFour.incomeUrl;
     propertyid= this.props.navigation.getParam('propertyid');
      this.setState({
        showProgress: true
        });
         if(idUrlUpload != '' && incomeUrlUpload != ''){
          //  alert(token+"\n"+JSON.stringify(this.state.applyOne)
          //  +"\n"+JSON.stringify(this.state.applyTwo)+"\n"+JSON.stringify(this.state.applyThree)+"\n"+JSON.stringify(this.state.applyFour))
      fetch('https://1800rentpro.com/rentpro_api/api/update_application', {
            method: 'POST',
            headers: {
                    'Accept': 'application/json',
                    'Content-Type': 'application/json',
                      },
            body: JSON.stringify({
              application_id:Property_id, 
               userid: token, propertyid:propertyid,  
              application_type: "property", 
               firstname: this.state.applyOne.firstName,  
              lastname: this.state.applyOne.lastName,
              email: this.state.applyOne.email,
              mobile:this.state.applyOne.mobileNo,
              dateofbirth:this.state.applyOne.birthday,
              work_phone: this.state.applyOne.workPhone,
              home_phone: "",
              guarantor: this.state.applyOne.guarantor,
                ssn: this.state.applyOne.SSN,
                id_type:this.state.applyOne.idType, 
               id_number: this.state.applyOne.idNumber, 
                present_address: this.state.applyOne.address,
              present_state: this.state.applyOne.state,
              present_city: this.state.applyOne.city,
              present_zip: this.state.applyOne.zip,
              present_manager_name:this.state.applyOne.managerName,
              present_manager_phone: this.state.applyOne.managerPhone,
              present_moving_reason: "abc",
              present_rent_per_month: "432543",
              present_date_in: "2019-09-30",
              present_date_out: "2019-10-30",
              bank_name: this.state.applyTwo.bankName,
              acc_type: this.state.applyTwo.accountType,
              acc_no: this.state.applyTwo.accountNumber,
              avg_monthly_bal: this.state.applyTwo.avgBalance,
               fin_obl_creditor_name_1: this.state.applyTwo.financeCreditorName,
              fin_obl_address_1: this.state.applyTwo.financeAddress,
              fin_obl_phone_1: this.state.applyTwo.financePhone,
              fin_obl_monthly_payment_1: this.state.applyTwo.finanacePayment,
              fin_obl_creditor_name_2: "",
              fin_obl_address_2: "",
              fin_obl_phone_2: "",
              fin_obl_monthly_payment_2: "",
              emergencycontact_name:this.state.applyTwo.emergencyName,
              emergencycontact_address:this.state.applyTwo.emergencyAddress,
              emergencycontact_relationship: this.state.applyTwo.emergencyRelationship,
              emergencycontact_phone:this.state.applyTwo.emergencyPhone,
               cats: this.state.applyFour.cat,
              dogs:this.state.applyFour.dog,
              tenant_emp_status: this.state.applyFour.tenanatIs,
              occupation: this.state.applyFour.occupation,
              income: this.state.applyFour.income,
              frequency_1: this.state.applyFour.frequency,
              other_sources: this.state.applyFour.otherSource,
              amount: "",
              frequency_2: "1",
              occupants: "1",
              lease_period: "1",
              move_in_date: "2019-10-30",
              employer_name: this.state.applyFour.employerName,
              employer_address:this.state.applyFour.employerAdress,
              start_date: this.state.applyFour.dateFrom,
              end_date: this.state.applyFour.dateTo,
                proposed_occupants_name_1: this.state.applyThree.occupant[0].name,
              proposed_occupants_age_1:this.state.applyThree.occupant[0].age,
              proposed_occupants_dob_1: this.state.applyThree.occupant[0].birthday,
              proposed_occupants_mobile_no_1: this.state.applyThree.occupant[0].mobileNo,
              proposed_occupants_name_2: this.state.applyThree.occupant[1].name,
              proposed_occupants_age_2: this.state.applyThree.occupant[1].age,
              proposed_occupants_dob_2:this.state.applyThree.occupant[1].birthday,
              proposed_occupants_mobile_no_2: this.state.applyThree.occupant[1].mobileNo,
              proposed_occupants_name_3: this.state.applyThree.occupant[2].name,
              proposed_occupants_age_3: this.state.applyThree.occupant[2].age,
              proposed_occupants_dob_3: this.state.applyThree.occupant[2].birthday,
              proposed_occupants_mobile_no_3:this.state.applyThree.occupant[2].mobileNo,
              proposed_occupants_name_4: this.state.applyThree.occupant[3].name,
              proposed_occupants_age_4: this.state.applyThree.occupant[3].age,
              proposed_occupants_dob_4: this.state.applyThree.occupant[3].birthday,
              proposed_occupants_mobile_no_4:this.state.applyThree.occupant[3].mobileNo,
              proposed_occupants_name_5: this.state.applyThree.occupant[4].name,
              proposed_occupants_age_5: this.state.applyThree.occupant[4].age,
              proposed_occupants_dob_5  :this.state.applyThree.occupant[4].birthday,
              proposed_occupants_mobile_no_5:this.state.applyThree.occupant[4].mobileNo,
              proposed_occupants_name_6: this.state.applyThree.occupant[5].name,
              proposed_occupants_age_6: this.state.applyThree.occupant[5].age,
              proposed_occupants_dob_6: this.state.applyThree.occupant[5].birthday,
              proposed_occupants_mobile_no_6:this.state.applyThree.occupant[5].mobileNo,
              contact_first_name: this.state.applyThree.referanceContact[0].name,
              contact_first:this.state.applyThree.referanceContact[0].mobileNo,
              contact_second_name: this.state.applyThree.referanceContact[1].name,
              contact_second:this.state.applyThree.referanceContact[1].mobileNo,
              supervisor_contact_name: this.state.applyThree.supevisorName,
              supervisor_contact: this.state.applyThree.supervisorMobile,
              vehicle_make:this.state.applyThree.vehicalMake,
              vehicle_model: this.state.applyThree.vehicalModel,
              vehicle_year: this.state.applyThree.VehicalYear,
              vehicle_lic: this.state.applyThree.vehicalLisence,
              is_evicted: this.state.applyFour.radioOne,
              is_bankrupt: this.state.applyFour.radioThree,
              is_convicted: this.state.applyFour.radioThree,
              primary_bank: "",
              id_proof_path_first: idUrlUpload,
              id_proof_path_second: incomeUrlUpload,
              signature: this.state.applyFour.signUrl,
              
                  })
          }).then((response) => response.json())
            .then((responseJson) => {
            
           // alert(JSON.stringify(responseJson))
           console.log("responseJson",responseJson)
          if(responseJson.status==1){
            this.setState({
              showProgress: false
            });
            console.log('22222');
            console.log(responseJson.message);
            Toast.show(responseJson.message);
           this.setState({
            showProgress: false
          });
            }
          else{
            Alert.alert(responseJson.message);
            this.setState({
              showProgress: false
            });
            
          }
        }).catch((error) => {
   alert("e"+error);
      this.setState({
        showProgress: false
      });
    });
    }
    else{
      console.log('imagw is not available');
     
      if(idUrlUpload != ''){
        Alert.alert('id proof upload failed!!');
      }
      else if(incomeUrlUpload != ''){
        Alert.alert('income proof upload failed!!');
      }
      this.setState({
        showProgress: false
      });
    }
  }
onPressNext = () => {
  // alert('done')
  const {idxActive} = this.state;
//  let eight = this.refs.applyRentEight.nextPress();
//  console.log('eight',eight);
 
 if (idxActive < 4) {
    switch (this.state.idxActive) {
      case 0:
        let one = this.refs.tenantViewOne.nextPress();
        // console.log('oneeee', one);
        // alert("one")
        if(one.isNext){
          this.setState({
            applyOne:one
          })
          this.refs.swiper.scrollBy(1);
        }
        break;
        case 1:
          let two = this.refs.tenantViewTwo.nextPress();
          if(two.isNext){
            this.setState({
              applyTwo:two
            })
            this.refs.swiper.scrollBy(1);
          }
          break;
          case 2:
            let three = this.refs.tenantViewThree.nextPress();
          if(three.isNext){
            this.setState({
              applyThree:three
            })
            this.refs.swiper.scrollBy(1);
          }
          break;
          // case 3:
          //   let four = this.refs.tenantViewFour.nextPress();
          
          //   if(four.isNext){
          //   this.setState({
          //     applyFour:four
          //   })
          //   this.refs.swiper.scrollBy(1);
          // }
          // break;
          // case 4:
          //   let five = this.refs.applyRentFive.nextPress();
          // if(five.isNext){
          //   this.setState({
          //     applyFive:five
          //   },function(){
          //   //   console.log('this.state.applyFive');
          //   // console.log(this.state.applyFive);
          //   })
          //   this.refs.swiper.scrollBy(1);
          // }
          // break;
         
          
      //  default:
      //     //  this.refs.swiper.scrollBy(1);
     }
  }
}
  render() {
    return (

      <View style={{flex:1}}>
        {/* <ToolbarWithNavigation title="Apply For Rent" parentMethod={this.onPressNext}/> */}
        <View style={{height:'9%', backgroundColor:'white', paddingLeft:'7%', paddingRight:'7%',
         flexDirection:'row', justifyContent:'space-between', alignItems:'center'}}>
          <TouchableOpacity 
          
          onPress={this.onPressPrev}>
          <Image source={require("../image/left.png")} style={{width:25,height:20 }} />
          </TouchableOpacity>
          <Text style={{textAlign:"center", color:'#034d94',  fontWeight:'bold', fontSize:18}}>Details</Text>
          <View style={{height:10, width:10, backgroundColor:'white'}}></View>
        </View>

        <Swiper
          style={styles.wrapper}
          renderPagination={renderPagination}
          showsButtons={false}
           showsPagination={false}
          scrollEnabled={false}
          loop={false}
          ref={'swiper'}
          dot={true}
          onIndexChanged={idxActive => this.setState({idxActive},function(){
           // console.log('nextttt', this.state.idxActive);
          }) }
        >
          {/* <View style={styles.slide}> */}
        
            {/* <ApplyForRentOne   ref="applyRentOne"/> */}
          {/* </View> */}
           {/* <ApplyForRentTwo ref="applyRentTwo" />
           <ApplyForRentThree ref="applyRentThree" />
           <ApplyForRentFour ref="applyRentFour" />
           <ApplyForRentFive ref="applyRentFive" /> */}
           {/* <ApplyForRentSix ref="applyRentSix"/> */}

            <TenantApplicationOne ref="tenantViewOne" data={this.state.set?data:null}/>
         
           <TenantApplicationTwo ref="tenantViewTwo"  data={this.state.set?data:null} />
           <TenentApplicationThree ref="tenantViewThree" data={this.state.set?data:null}/>
           <TenentApplicationFour ref="tenantViewFour" data={this.state.set?data:null}/>


           {/* <ApplyForRentSeven ref="applyRentSeven"/>
           <ApplyForRentEight ref="applyRentEight"/> */}
            
           </Swiper>  

           <View style={styles.footer}>
          <View style={{flexDirection:'row',}}>
          <View style={styles.indicator}></View>
           <View style={this.state.idxActive >=1 ? styles.indicator : styles.inactiveIndicator}></View>
            <View style={this.state.idxActive >=2 ? styles.indicator : styles.inactiveIndicator}></View>
             <View style={this.state.idxActive >=3 ? styles.indicator : styles.inactiveIndicator}></View>
            {/* <View style={this.state.idxActive >=4 ? styles.indicator : styles.inactiveIndicator}></View> */}
            {/* <View style={this.state.idxActive >=5 ? styles.indicator : styles.inactiveIndicator}></View>
            <View style={this.state.idxActive >=6 ? styles.indicator : styles.inactiveIndicator}></View>
            <View style={this.state.idxActive >=7 ? styles.indicator : styles.inactiveIndicator}></View> */}
         </View>
           
      {this.state.idxActive >=3 ?
      Edit?
           <TouchableOpacity style={[styles.nextBtn,{  backgroundColor:'#008f2c',}]}
       onPress={this.onSubmitPress} >
       <Text style={styles.nextText}>Edit</Text>
       </TouchableOpacity>:null
      :
       <TouchableOpacity style={styles.nextBtn}
       onPress={this.onPressNext} >
         
       <Text style={styles.nextText}>Next</Text>
       </TouchableOpacity> 
      }
       <ProgressDialog
          // title="Progress Dialog"
          activityIndicatorColor="red"
          activityIndicatorSize="large"
          animationType="fade"
          message="Please, wait..."
          visible={this.state.showProgress}
        />
      </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  wrapper: {},
   indicator:{ 
         height:12,
          width:12,
          borderRadius:6,
          backgroundColor:'#008f2c',
         margin:wp('1%'),
       },
       inactiveIndicator:{ 
         height:12,
          width:12,
          borderRadius:6,
          backgroundColor:'white',
          borderWidth:0.5,
         margin:wp('1%'),
       },
       nextBtn:{
         height:hp('7%'),
       backgroundColor:'#034d94',
       width:wp('25%'),
      borderRadius:10,
     justifyContent:'center',
       alignItems:'center',
       flexDirection:'row', 
       paddingStart:wp('2%'),
        paddingEnd:wp('2%')
       },
       nextText:{
         fontSize:13,
          fontWeight:'bold',
           color:'white'
           },
          footer:{
            backgroundColor:'white',
             height:'11%',
              justifyContent:'space-between', 
              flexDirection:'row',
            alignItems:'center',
            paddingStart:'7%',
           paddingEnd:'7%'
       },
         signUpBtn:{
   height:hp('7%'),
    backgroundColor:'#008f2c',
     width:'46%',
      marginStart:'4%', 
     marginEnd:'2%',
   borderRadius:10,
    justifyContent:'center',
     alignItems:'center'
     },
})