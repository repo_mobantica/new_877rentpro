import React, { Component } from "react";
import {
  Text,
  View,
  SafeAreaView,
  Image,
  TouchableOpacity,
  StyleSheet,
  Dimensions,
  Alert,Toast,
  AsyncStorage,
  Button,
  FlatList
} from "react-native";
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp
} from "react-native-responsive-screen";
import ToolbarWithLeftIcon from "../component/ToolbarWithLeftIcon";
import { ProgressDialog } from "react-native-simple-dialogs";
import Moment from 'react-moment';
export default class LandlordPaymentHistory extends Component {
  constructor(props) {
    super(props);

    this.state = {
      dataSource: "",
      showProgress: false,
      data: ""
    };
    
  }
  format(){
    const theDate1 = moment("2019 Tue Apr 09 13:30", FORMAT).format('LL')
  }

  _renderItem = ({ item }) => (
    <TouchableOpacity  onPress={()=>{ this.props.navigation.navigate('PendingDetails', {
      id: item.id
    }) }} >
    <View style={styles.flatListView}>
      <Image
        style={styles.displayImage}
        
        source={{ uri:item.image_url}}
      />
      <View style={styles.firstRow}>
  <Text style={styles.priceLable}>{item.totalamount}{""}{item.id}</Text>
        <Image style={styles.eyeIcon} source={require("../image/eye.png")} />
      </View>
      <Text style={styles.addressText}>{item.property_address}</Text>
      <View style={styles.lastRow}>
        <Image
          style={{ width: 10.5, height: 10.5 }}
          source={require("../image/calendar.png")}
        />
        {/* <Text style={styles.datetext}>{Moment(item.payment_date).format('MM/DD/YYYY')}</Text> */}
  <Text style={styles.datetext}>  {item.payment_date} </Text>
       
        <View style={styles.paidView}>
          <Text style={styles.paidText}>{item.payment_status==1 ? 'Paid' : 'UnPaid' }</Text>
        </View>

      </View>
    </View>
    </TouchableOpacity>
  );
  
  async componentDidMount() {
    const value = await AsyncStorage.getItem("Token");
    console.log("valueeeeeee", value);
    token = value;

    this.landlordPaymentHistory();
  }

  landlordPaymentHistory = () => {
    this.setState({
      showProgress: true
    });
    fetch(
      "https://1800rentpro.com/rentpro_api/api/landlord_payment_history_list",
      {
        method: "POST",
        headers: {
          Accept: "application/json",
          "Content-Type": "application/json"
        },
        body: JSON.stringify({
          userid: token
        })
      }
    )
      .then(response => response.json())
      .then(responseJson => {
        // Alert.alert(
        //   'Alert',
        //   'Save Successfully',
        //   [
        //     {
        //       text: 'Cancel',
        //       onPress: () => console.log('Cancel Pressed'),
        //       style: 'cancel',
        //     },
        //     {text: 'OK', onPress: () => console.log('OK Pressed')},
        //   ],
        //   {cancelable: false},
        // );

        //alert("d"+JSON.stringify(responseJson.response))
       
        console.log("responseJsonfgdfgh", responseJson.response);
        console.log("responseJson", responseJson.message);
        this.setState({
          dataSource: responseJson.response.data
        });
        console.log(
          "responseJson.responseresponseJson.response",
          this.state.dataSource
        );
       // console.log("this.state.dataSourcethis.state.dataSource", this.state.dataSource.image_url);

        if (responseJson.status == 1) {
          this.setState({
            showProgress: false
          });
        //  console.log("22222");
          console.log(responseJson.message);         
          this.setState({
            showProgress: false
          });
        } else {
          Alert.alert(responseJson.message);
          this.setState({
            showProgress: false
          });
        }
      })
      .catch(error => {
       // alert("e" + error);
       // console.log("error", error);
        this.setState({
          showProgress: false
        });
      });
  };

  render() {
    return (
      <SafeAreaView style={[styles.container, { backgroundColor: "red" }]}>
        <View style={styles.container}>
          <ToolbarWithLeftIcon title="Landlord Payment History" />
          <FlatList
            data={this.state.dataSource}
            keyExtractor={(item, index) => index.toString()}
            renderItem={this._renderItem}
          />
 <View style={{ height: hp("1%"), width: "100%" }}></View>
          <ProgressDialog
            // title="Progress Dialog"
            activityIndicatorColor="red"
            activityIndicatorSize="large"
            animationType="fade"
            message="Please, wait..."
            visible={this.state.showProgress}
          />
      
        
        </View>
      </SafeAreaView>
    );
  }
}
const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "#ededed"
  },
  menuIcon: {
    height: "40%",
    resizeMode: "contain",
    width: 30,
    marginStart: 5
  },
  flatListView: {
    height: hp("24%"),
    margin: hp("3%"),
    marginTop: hp("3%"),
    marginBottom: hp("0%"),
    borderRadius: 11,
    backgroundColor: "white"
  },
  firstRow: {
    flexDirection: "row",
    marginStart: wp("3.7%"),
    marginTop: wp("3.7%"),
    marginEnd: wp("3.7%"),
    justifyContent: "space-between"
  },
  lastRow: {
    flexDirection: "row",
    marginStart: wp("3.7%"),
    marginTop: -6,
    marginEnd: wp("3.7%"),
    alignItems: "center",
    flex: 1
  },
  displayImage: {
    width: "100%",
    height: hp("10%"),
    borderTopLeftRadius: 11,
    borderTopRightRadius: 11
  },
  priceLable: {
    color: "#034d94",
    fontWeight: "bold"
  },
  eyeIcon: {
    width: 21,
    height: 21
  },
  addressText: {
    color: "black",
    marginStart: wp("3.7%"),
    fontWeight: "bold"
  },
  datetext: {
    color: "#939393",
    marginStart: wp("1.6%"),
    fontSize: 12,
    // marginStart: wp("3.7%"),
  },
  paidView: {
    justifyContent: "center",
    alignItems: "center",
    backgroundColor: "#cbe8d8",
    borderRadius: 15,
    height: hp("3%"),
    borderColor: "green",
    borderWidth: 2,
    marginStart: "41.5%",
    width: 65
  },
  paidText: {
    fontSize: 13
  }
});
