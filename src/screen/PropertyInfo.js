import React from 'react';
 import { View, StyleSheet, Text, Alert, Image, Button, ScrollView, TextInput, TouchableOpacity, FlatList } from 'react-native';
import {widthPercentageToDP as wp, heightPercentageToDP as hp} from 'react-native-responsive-screen';
 import Toolbar from '../component/Toolbar'
 import Slideshow from 'react-native-slideshow';
 import Communications from 'react-native-communications';
 import openMap from 'react-native-open-maps';
 import Modal from "react-native-modal";
 import Api from "../Api/Api";
 import { 
  ProgressDialog, 
} from "react-native-simple-dialogs";
import Moment from 'moment';
 var latitude ;
 var longitude ;
 var halfBath ;
 var Property_id = '';

export default class PropertyInfo extends React.Component {
  constructor() {
 
    super();
 
    this.state = {
      position: 1,
      interval: null,
      flatlistData:[],
      isModalVisible: false,
      amenities:[],
      appliances:[],
      propertyphotos:[],
      property_types:[],
      dataArray:[],
      dataSource: [],
       showProgress: false,
       userType:''
    };
 
  }
 
 async componentWillMount() {
    Property_id =  this.props.navigation.getParam('Property_id');
    let userType = await AsyncStorage.getItem("UserType");
     console.log('Property_id');
     console.log(Property_id);
     if (userType != undefined ) {
      // We have data!!
     
      this.setState({
        userType:userType
      },function(){
        console.log('userType state');
         console.log(this.state.userType);
      })
    }
    this.setState({
      interval: setInterval(() => {
        this.setState({
          position: this.state.position === this.state.dataSource.length ? 0 : this.state.position + 1
        });
      }, 5000)
    });
    this.fetchData();
   }
 
  toggleModal = () => {
    this.setState({ isModalVisible: !this.state.isModalVisible });
  };

  fetchData(){
     this.setState({
      showProgress: true
    });
   
    Api("property_details", {
      propertyid:Property_id
    })
      .then((responseJson) => {
        this.setState({
          showProgress: false
        });
    if(responseJson.status==1){
       console.log('responseJson.response');
        console.log(responseJson.response);
        
       let amenities = responseJson.response.data.amenities;
       let property_types = responseJson.response.data.property_types;
       let appliances = responseJson.response.data.appliances;
       let propertyphotos = responseJson.response.data.propertyphotos;
       latitude  = Number(responseJson.response.data.latitude);
       longitude = Number(responseJson.response.data.longitude);
       let a  = responseJson.response.data.latitude;
       let b = responseJson.response.data.longitude;
       let data = responseJson.response.data;
       halfBath = responseJson.response.data.halfbaths;
       if(halfBath == '1'){
         halfBath= 1/2;
       }
       else{
         halfBath='';
       }
       console.log('lat long');
       console.log(latitude, longitude);
       let photoArray=[];
     
       propertyphotos.map((item, index)=>{
         photoArray.push({url:item.imageurl})
       })
       console.log('photoArray', photoArray);
       this.setState({
        amenities:amenities,
        property_types:property_types,
        appliances:appliances,
        dataSource: photoArray,
         dataArray: data
         }, function(){
             
           console.log(this.state.dataArray.description);
       })
      }
      else if(responseJson==false){
        alert('No internet');
      }
    else{
      Alert.alert(responseJson.message);
    }
  }).catch((error) => {
     this.setState({
      showProgress: false
    });
console.error(error);
});
  }
  
  componentWillUnmount() {
    clearInterval(this.state.interval);
  }

  _goToYosemite() {
   // console.log('lat long');
  //  console.log(latitude , longitude);
      if(latitude != '' && longitude != ''){
        openMap({ latitude: latitude, longitude: longitude});
      }
   
  }
 
  

dateFormater = (date) => {
  Moment.locale('en');
 
  return Moment(date).format('MM/DD/YYYY');
}
  render() {
    //Image Slider
    return (
        <View style={styles.container}> 
         <Toolbar title={"Property Information"} />
        
           <View style={styles.mainView}>
             <View style={{height:'90%'}}>
        <ScrollView>
       <Slideshow
          dataSource={this.state.dataSource}
          position={this.state.position}
          onPositionChanged={position => this.setState({ position })}
          indicatorColor={'transparent'}
          indicatorSelectedColor={'transparent'}
          height={hp('25%')}
           />
           <View style={{ flexDirection:'row', alignItems:'center',}}>
              <View style={styles.leftSideText}>
           
            <Text style={styles.availabilityText}>{this.state.dataArray.bedrooms} Beds | {this.state.dataArray.fullbaths} {halfBath} Bath | {this.state.dataArray.minsquarefeet} sq ft</Text>
            <Text style={styles.addresstext} numberOfLines={1}>{this.state.dataArray.address}</Text>
              <View style={{flexDirection:'row', marginTop:hp('1%')}}>
               <Image style={styles.clockImg} source={require('../image/clock.png')}/>
               <Text style={styles.daysText}>Posted By{" "}
               {this.dateFormater(this.state.dataArray.created)}</Text>
              </View>
             </View>
               <TouchableOpacity style={styles.imageView}
                onPress={this._goToYosemite}>
                <Image style={styles.flotingImage} source={require('../image/mapCopy.png')}/>
                 </TouchableOpacity>
                   </View>
            
            
           <View style={styles.priceOuterView}>
           <Text style={styles.priceText}>${this.state.dataArray.minrent}</Text>
           <View style={styles.viewButton}> 
      <Text style={{fontSize:11}}>AVAILABLE  NOW</Text>
     </View>
           </View>

        <View style={styles.desciptionView}>
        <Text style={{fontSize:13, color:'gray'}}>{this.state.dataArray.description}</Text>
        </View>
              <Text style={styles.titleText}>Amenities</Text>
        <FlatList
          data={this.state.amenities}
          renderItem={({ item }) => (
            <View style={{ flexDirection: 'row', margin: 1,height:40, width:'50%',  paddingLeft:'3%', paddingRight:'3%'}}>
               <Image style={styles.flotingImage} source={require('../image/filter.png')}/>
              <Text style={{fontSize:12, color:'black', paddingLeft:'3%'}} numberOfLines={1}>{item.amenity_name}</Text>
            </View>
          )}
          //Setting the number of column
          numColumns={2}
          keyExtractor={(item, index) => index.toString()}
        />
       
       <Text style={styles.titleText}>Appliances</Text>
        <FlatList
          data={this.state.appliances}
          renderItem={({ item }) => (
            <View style={{ flexDirection: 'row', margin: 1,height:40, width:'50%',  paddingLeft:'3%',
             paddingRight:'3%'}}>
               <Image style={styles.flotingImage} source={require('../image/filter.png')}/>
              <Text style={{fontSize:12, color:'black', paddingLeft:'3%'}} numberOfLines={1}>{item.appliance_name}</Text>
            </View>
          )}
          //Setting the number of column
          numColumns={2}
          keyExtractor={(item, index) => index.toString()}
        />

     <Text style={styles.titleText}>Property_types</Text>
        <FlatList
          data={this.state.property_types}
          renderItem={({ item }) => (
            <View style={{ flexDirection: 'row', margin: 1,height:40, width:'50%',  paddingLeft:'3%',
             paddingRight:'3%'}}>
               <Image style={styles.flotingImage} source={require('../image/filter.png')}/>
              <Text style={{fontSize:12, color:'black', paddingLeft:'3%'}} numberOfLines={1}>{item.type}</Text>
            </View>
          )}
          //Setting the number of column
          numColumns={2}
          keyExtractor={(item, index) => index.toString()}
        />
        
        <View style={styles.floatingView}>
           <TouchableOpacity style={styles.imageView}
             onPress={() => Communications.email([this.state.dataArray.contact_details.email],null,null,
             'Demo Subject','Demo Content for the mail')}
           >
                <Image style={styles.flotingImage} source={require('../image/Email123.png')}/>
                 </TouchableOpacity>
                 <TouchableOpacity style={styles.imageView}
                  onPress={() => Communications.phonecall(this.state.dataArray.contact_details.mobile, true)}>
                <Image style={styles.flotingImage} source={require('../image/Phone.png')}/>
                 </TouchableOpacity>
                 </View>
                
                 </ScrollView>
                 </View>
                 <Modal isVisible={this.state.isModalVisible}
                onSwipeComplete={() => this.setState({ isVisible: false })}
                swipeDirection="left">
                 
                 <View style={styles.modelView}>  
                 <View style={{height:'10%', alignItems:'flex-end'}}>
                   <TouchableOpacity onPress={ this.toggleModal}>
                 <Image
                     style={{height:20, width:20, margin:'4%'}}
                     source={require("../image/cancel.png")}
                   />
                   </TouchableOpacity>
                 </View>
             <View style={styles.titleView}> 
             <Text style={styles.titleMakeOfferText}> Make an Offer</Text>
           </View>
           <View style={styles.upView}> 
             <Text > On a 3475 S Bentley Ave</Text>
           </View>

           <View style={styles.downView}> 
             <Text >Lowest Price Online : $4579</Text>
           </View>
           <View style={styles.middleView}>
           <Text style={{width:'20%'}} numberOfLines={2} >Enter Your price</Text>
           <Image
                     style={{height:30, width:30}}
                     source={require("../image/selectedLocation.png")}
                   />
                   <View style={styles.textView}>
             <TextInput  placeholder="Enter Email" 
             style={{marginBottom:-5}}/> 
             </View>
           </View>
      <TouchableOpacity style={styles.lastButton}
      onPress={this.toggleModal}>
      <Text style={styles.sendText}> Send</Text>
     </TouchableOpacity>
     
        </View>
      
        </Modal>
        {this.state.userType=="2"?null:
               <View style={styles.offerOuterView}>
                  <TouchableOpacity style={styles.rentApplyView}
                   onPress={()=> this.props.navigation.navigate('ApplyForRent', {Property_id:Property_id})}>
                <Text style={styles.whiteText}>Apply for Rent</Text>
               </TouchableOpacity> 
               <TouchableOpacity style={styles.makeOfferView}
               onPress={this.toggleModal}>
              <Text style={styles.whiteText}>Make an Offer</Text>
               </TouchableOpacity> 
             </View> }
               <ProgressDialog
          // title="Progress Dialog"
          activityIndicatorColor="red"
          activityIndicatorSize="large"
          animationType="fade"
          message="Please, wait..."
          visible={this.state.showProgress}
        />
         </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
container: {
    flex: 1,
   backgroundColor: "#ededed"
  },
  ScrollViewStyle:{
    width:wp('92%'), 
    height:hp('87%'),
    backgroundColor: "#ededed", 
    margin:wp('4%')
    },
     mainView:{
    height:'83%',
     width:'90%',
     margin:wp('5%'), 
     backgroundColor:'white',
   // backgroundColor:'#f1f1f1',
      borderRadius:10, 
     // flex:1
},
    detailTextStyle:{
     fontSize:12.8,
      marginStart:wp('4%')
    },
      applyRent:{ 
        width:wp('40%'),
         alignItems:'center', 
         marginStart:wp('2%'), 
          justifyContent:'center'
          },
    //  lastButton:{
    //         height:hp('8%'),
    //          width:'90%',
    //           flexDirection:'row',
    //           backgroundColor: '#034d94',
    //           borderRadius:50,
    //      alignItems:'center',
    //       borderColor:'black',
    //        borderWidth:0.5, margin:wp('5%')
    //   },
      flotingImage:{
        height:hp('4%'),
         width:hp('4%'),
        },
         imageView:{
 backgroundColor:'#034d94', 
 //marginEnd:hp('2%'),
 alignItems:'center',
 justifyContent:'center',
 borderRadius:hp('3.5%'), 
 height:hp('7%'),
 width:hp('7%')
 },
  viewButton: {
        justifyContent:'center', 
        alignItems:'center',
         backgroundColor:'#cbe8d8',
          borderRadius:15,
           height:hp('3.8%'),
      borderColor:'green',
       borderWidth:2,
        marginStart:wp('12%'),
         width:wp('30%')
         },
   whiteText:{
      color:'white'
    },
rentApplyView:{
  width:'50%',
   backgroundColor:'#034d94', 
  borderBottomLeftRadius: 11,
   borderEndWidth:1,
   justifyContent:'center', 
  alignItems:'center',
   borderColor:'white'
},
makeOfferView:{
  justifyContent:'center',
   alignItems:'center',
  borderBottomRightRadius: 11,
   width:'50%',
    backgroundColor:'#008f2c',
     borderColor:'white'
},
offerOuterView:{
  height:'10%', 
  marginTop:hp('2.3%'),
   flexDirection:'row'
},
floatingView:{
  height:hp('7%'),
   width:'100%', 
  flexDirection:'row',
  justifyContent: "space-between",
    paddingStart:wp('20%'),
     paddingEnd:wp('20%') 
},
desciptionView:{
  padding:wp('5%'), 
},
availabilityText:{
   marginTop:hp('4%'), 
   marginBottom:hp('1%'),
    fontSize:12,
    color:'gray'
},
addresstext:{
  fontWeight:"bold", fontSize:15
},
titleText:{
  fontWeight:"bold",
   fontSize:15,
   margin:'3%'
},
leftSideText:{
  height:hp('16.8'),
   width:'65%',
    marginStart:wp('6%'), 
},
clockImg:{
  height:hp('3%'),
   width:hp('3%'),
    marginEnd:wp('2%') 
},
priceOuterView:{
  height:hp('10%'), 
   width:'88%',
    marginStart:wp('6%'),
     marginEnd:wp('6%'),
      borderTopWidth:0.5, 
      borderBottomWidth:0.5,
        justifyContent: "space-between",
         flexDirection:'row',
          alignItems:'center'
},
priceText:{
  fontWeight:'bold',
   fontSize:20,
    color:'#034d94', 
},
daysText:{
  fontSize:12, color:'gray'
},
modelView:{
  height:'65%',
   width:'100%',
  // margin:'7%', 
   backgroundColor:'white',
    borderRadius:10, 
    position:'absolute'
},
 lastButton:{
   width:'100%',
      height:'12%', 
      borderBottomLeftRadius:10,
     borderBottomRightRadius:10, 
      backgroundColor:'#034d94', 
      justifyContent:'center', 
      alignItems:'center'
 },
 titleMakeOfferText:{
   color:'#034d94',
    fontWeight:'bold',
    fontSize:15
 },
 sendText:{
  color:'white',
   fontWeight:'bold'
},
titleView:{
  height:'18%',
   justifyContent:'center',
    alignItems:'center' 
},
upView:{
  height:'15%',
   justifyContent:'center',
   alignItems:'center',
    borderBottomWidth:1, 
    borderBottomColor:'gray',
     marginLeft:'10%',
      marginRight:'10%'
},
downView:{
  height:'15%',
   justifyContent:'center',
    alignItems:'center', 
     marginLeft:'10%',
      marginRight:'10%'
},
middleView:{
  flexDirection:'row',
   justifyContent:'space-around',
    alignItems:'center',
     height:'30%',
},
textView:{
  borderColor:'black',
   borderRadius:10,
    borderWidth:1, 
    height:'40%',
     width:'25%',
}

});

