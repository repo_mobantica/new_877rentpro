import React, { Component } from 'react';
import { Text, View, SafeAreaView, Image, TouchableOpacity, StyleSheet, Dimensions, Alert, NetInfo,
FlatList} from 'react-native';
import {widthPercentageToDP as wp, heightPercentageToDP as hp} from 'react-native-responsive-screen';
import Toolbar from '../component/Toolbar';
import {NavigationEvents} from 'react-navigation';
import Communications from 'react-native-communications';
import Api from "../Api/Api";
import { 
  ProgressDialog, 
} from "react-native-simple-dialogs";
 import AsyncStorage from '@react-native-community/async-storage';
 
   var token;
//import Geocoder from 'react-native-geocoding';
//import Geolocation from '@react-native-community/geolocation';
export default class Favorite extends Component {
  state = {
    search: '', 
    flatlistData:[],
    check:false,
    isFavorite:false,
    showProgress: false,
    connection_Status : ""

  }
  
   async componentDidMount() {
   
  token = await AsyncStorage.getItem('Token');
 
}

  fetchData(){
    this.setState({
      showProgress: true
    });
    
    Api("favorite_list", {
      userid:token,
    })
      .then((responseJson) => {
        this.setState({
          showProgress: false
        });
    if(responseJson.status==1){
      let data = responseJson.response.data;
       this.setState({
         flatlistData:data
       })
      }
      else if(responseJson==false)
      {
        this.setState({
          showProgress: false
        });
        alert("No internet");
      }
    else{
      Alert.alert(responseJson.message);
    }
  }).catch((error) => {
console.error(error);
this.setState({
  showProgress: false
});
});
  }

  addRemoveFavorite(index, id){
    let flatlistData= this.state.flatlistData;
    flatlistData.splice(index, 1);
     
     this.setState({
      flatlistData:flatlistData
     })
     this.setState({
      showProgress: true
    });

    Api("add_remove_favorites", {
      userid: token,
      propertyid: id,
      addremovetype: false
    })
      .then((responseJson) => {
        this.setState({
          showProgress: false
        });
    if(responseJson.status==1){
      console.log(responseJson.message);
      }
      else if(responseJson==false)
      {
        this.setState({
          showProgress: false
        });
        alert("No internet");
      }
    else{
      Alert.alert(responseJson.message);
    }
  }).catch((error) => {
console.error(error);
this.setState({
  showProgress: false
});
});
  }

  _renderItem = ({item, index}) => {
    let halfbaths =item.halfbaths;
    if(halfbaths == 0){
     halfbaths ='';
    }
    else{
     halfbaths ='1/2';
    }
    return(
    <View style={styles.flatListView}>
      <View style={styles.firstRow}>
    {/* <Text style={[styles.address,{width:'82%',}]} numberOfLines={1}>{item.address}</Text> */}
    <Text style={[styles.address,{width:'82%',}]} numberOfLines={1}>{item.id}</Text>
    <TouchableOpacity onPress={()=> this.addRemoveFavorite( index, item.id)}>
        <Image
        style={styles.icon}
         source={require('../image/heart.png')}
       />
       </TouchableOpacity>  
    </View>
  
   <Image
          style={styles.displayImage}
          source={{uri: item.image_url}}
         // source={require('../image/room.png')}
       />
     <TouchableOpacity  onPress={()=> this.props.navigation.navigate('PropertyInfo', {Property_id:item.id})}>
    <Text style={styles.userName}>{item.contact_details.firstname} {item.contact_details.lastname}</Text>
    <Text style={styles.noOfItemtext}>{item.bedrooms} Bedrooms | {item.fullbaths} {halfbaths} Bathrooms </Text>
     {/* <Text style={styles.address} numberOfLines={2}>{item.display_address}</Text> */}
     <View style={styles.dateRow}>
     <Text style={styles.address}>${item.minrent} - {item.maxrent}</Text>
       <View style={{flexDirection:'row'}}>
       <Image
           style={styles.icon}
          source={require('../image/clock.png')}
       />
    <Text style={{marginLeft:'2%'}}>new</Text>
    </View>
    </View>
    </TouchableOpacity>
     
    <View style={styles.offerOuterView}>
                  <TouchableOpacity style={styles.rentApplyView}
                 onPress={() => Communications.phonecall(item.contact_details.mobile, true)}>
                  <Image
          style={styles.icon}
          source={require('../image/Phone.png')}
       />
       <Text style={{color:'white'}}> Phone</Text>
               </TouchableOpacity> 
               <TouchableOpacity style={styles.makeOfferView}
                onPress={() => Communications.email([item.contact_details.email],null,null,
                  'Enquiry Mail','')}>
               <Image
          style={styles.icon}
          source={require('../image/Email.png')}
       />
       <Text style={{color:'white'}}> Email</Text>
               </TouchableOpacity> 
               </View>

    </View>
  );
  }

  render() {
    return (
       <SafeAreaView style={styles.container}>
      <View style={styles.container}>
      <Toolbar  title={"Address "} />
      <View style={{height:'78%'}}>
      <NavigationEvents onDidFocus={() =>  this.fetchData()} />
      <FlatList
              data={this.state.flatlistData}
              extraData={this.state}
            keyExtractor={(item, index) => index.toString()}
              renderItem={this._renderItem}
            />
              <View style={{height:hp('1%'), width:'100%'}}></View>
        </View>
          
          <View style={styles.tabView}>
          <TouchableOpacity onPress={() => this.props.navigation.navigate('MapScreen')}>
          <View style={[styles.subTab,{backgroundColor:'#034d94'}]}>
          <Image  style={styles.menuIcon}
           source={require("../image/map12.png")}/>
          <Text style={styles.fontStyle}>MAP</Text>
          </View>
          </TouchableOpacity>
          <TouchableOpacity onPress={() => this.props.navigation.navigate('ListScreen')}>
          <View style={[styles.subTab,{backgroundColor:'#034d94'}]}>
          <Image  style={styles.menuIcon}
           source={require("../image/List.png")}/>
          <Text style={[styles.fontStyle,{color:'white'}]}>LIST</Text>
          </View>
          </TouchableOpacity>
          <View style={[styles.subTab,{width:wp('25.3%')}]}>
          <Image  style={styles.menuIcon}
           source={require("../image/map.png")}/>
          <Text style={styles.fontStyle}>FAVORITE</Text>
          </View>
          <ProgressDialog
          // title="Progress Dialog"
          activityIndicatorColor="red"
          activityIndicatorSize="large"
          animationType="fade"
          message="Please, wait..."
          visible={this.state.showProgress}
        />
          </View >
          </View>
           </SafeAreaView>
    );
  }
}
const styles = StyleSheet.create({
  container: {
      flex: 1,
     backgroundColor: "#ededed"
    },
    menuIcon: {
      height: "40%",
      resizeMode: "contain",
      width: 30,
      marginStart: 5
    },
    tabView : {
      position: 'absolute',
      height: hp('9%'),
     margin: wp('10%'),
     marginBottom:hp('5%'),
     marginTop:hp('85%'),
      //justifyContent: "flex-end",
      backgroundColor: '#034d94',
      borderRadius:35,
      flexDirection:'row'
    },
    subTab: {
    width:wp('23.1%'),
    margin:wp('1.5%'),
    justifyContent:'center',
    alignItems:'center',
    backgroundColor:'white',
    borderRadius:33
    },
    fontStyle:{
      fontSize:12
    },
    map: {
      marginTop:hp('9%'),
     position: 'absolute',
     left: 0,
     right: 0,
    height:hp('72%')
   },
   flatListView:{
     height:hp('35.8%'),
     margin:hp('3%'),
     marginTop:hp('3%'),
     marginBottom:hp('0%'),
     borderRadius:11,
     backgroundColor:'white',
       
  },
     dateRow:{
    flexDirection:'row', 
   height:hp('4'),
     alignItems:'center',
     justifyContent:'space-between',
     paddingRight:'6%'
    },
    displayImage:{
      width: '100%',
      height: hp('10%'), 
     },
     userName:
     {color:'#034d94',
      marginStart:wp('3.7%'), 
      marginTop:wp('3.7%'), 
      fontWeight:'bold'
      },
    address:{
      color:'black',
       marginStart:wp('3.7%'),
        fontWeight:'bold',
        marginEnd:wp('3.7%')
    },
         icon:{
           height:hp('3%'),
           width:hp('3%'),
           marginEnd:wp('2%'),
          
       },
         rentApplyView:{
          width:'50%',
           backgroundColor:'#034d94', 
          borderBottomLeftRadius: 11,
           borderEndWidth:1,
           justifyContent:'center', 
          alignItems:'center',
           borderColor:'white',
           flexDirection:'row', 
        },
        makeOfferView:{
          justifyContent:'center',
           alignItems:'center',
          borderBottomRightRadius: 11,
           width:'50%',
            backgroundColor:'#034d94',
             borderColor:'white',
             flexDirection:'row', 
        },
        offerOuterView:{
          height:hp('7%'), 
          marginTop:hp('1%'),
           flexDirection:'row'
        },
        firstRow:{
          height:hp('7%'),
            width:'100%',
            borderTopLeftRadius: 11, 
      borderTopRightRadius: 11,
       paddingRight:wp('3.7%'),
        flexDirection:'row', 
      //backgroundColor:'#008f2c',
       alignItems:'center', 
       justifyContent:'space-between'
        },
        searchBox : {
          height: hp('7%'),
         marginStart: wp('10%'),
         marginEnd: wp('10%'),
         marginBottom:hp('1%'),
         marginTop:hp('2%'),
          backgroundColor: 'white',
          borderRadius:35,
          flexDirection:'row',
          width:'80%'
        },
        noOfItemtext:{
          color:'gray',
           marginStart:wp('3.7%'),
            fontSize:11.5
            },
  });
  

// const styles = StyleSheet.create({
// container: {
//     flex: 1,
//    backgroundColor: "#ededed"
//   },
//   menuIcon: {
//     height: "40%",
//     resizeMode: "contain",
//     width: 30,
//     marginStart: 5
//   },
//    menuIcon: {
//     height: "40%",
//     resizeMode: "contain",
//     width: 30,
//     marginStart: 5
//   },
//   headerView: {
//       position: 'absolute',
//     flexDirection: "row",
//     backgroundColor: "white",
//     justifyContent: "space-between",
//     paddingHorizontal: 10,
//     alignItems: "center",
//     height:  hp('9%'),
//     width:'100%'
//   },
//   tabView : {
//     position: 'absolute',
//     height: hp('9%'),
//    margin: wp('10%'),
//    marginBottom:hp('5%'),
//    marginTop:hp('85%'),
//     //justifyContent: "flex-end",
//     backgroundColor: '#034d94',
//     borderRadius:35,
//     flexDirection:'row'
//   },
//   searchBox : {
//     position: 'absolute',
//     height: hp('7%'),
//    margin: wp('10%'),
//    marginBottom:hp('5%'),
//    marginTop:hp('11.5%'),
//     //justifyContent: "flex-end",
//     backgroundColor: 'white',
//     borderRadius:35,
//     flexDirection:'row',
//     width:'80%'
//   },
//   subTab: {
//   width:wp('23.1%'),
//   margin:wp('1.5%'),
//   justifyContent:'center',
//   alignItems:'center',
//   backgroundColor:'white',
//   borderRadius:33
//   },
//   tapImage:{
//     height:'20%',
//   },
//   fontStyle:{
//     fontSize:12
//   },
//   map: {
//     marginTop:hp('9%'),
//    position: 'absolute',
//    left: 0,
//    right: 0,
//   height:hp('72%')
//  },
// });