import React, { Component } from 'react';
import { Text, View, SafeAreaView, Image, TouchableOpacity, StyleSheet, Dimensions, Alert,
Button, FlatList, TextInput, ScrollView } from 'react-native';
import {widthPercentageToDP as wp, heightPercentageToDP as hp} from 'react-native-responsive-screen';
import CheckBox from 'react-native-check-box'
import commonStyles from "../styles/index";
import ToolbarWithNavAndHome from '../component/ToolbarWithNavAndHome';
import Api from "../Api/Api";
import { 
  ProgressDialog, 
} from "react-native-simple-dialogs";
export default class Feedback extends Component {
    constructor(props) {
    super(props);

    this.state = {
      tenant:false,
      manager:false,
      landlord:false,
      broker:false,
      firstName:'',
      lastName:'',
      address:'',
      email:'',
      phone:'',
      message:'',
      firstNameError:false,
      lastNameError:false,
      emailError:false,
      phoneError:false,
      messageError:false,
      checkBoxError:false,
      showProgress: false,
    };   
  }
  //{"input_type":"Landlord, Prospective Tenant","property_address":"Pune","name":"Rohit","email":"rohitkadam653@gmail.com","mobileno":"9423405541","message":"Test"}
  
  namevalidate(text) {    
    const reg =  /^[A-Za-z\b]+$/;
     if(text!=''){
       if (reg.test(text) === false) {
         return false;
       }
       else {
       this.setState({firstName:text,
         firstNameError:false})
       }
       }
       else{
       this.setState({firstName:'',
       firstNameError:true})
       }
    }
  
    lastNamevalidate(text) {    
      const reg =  /^[A-Za-z\b]+$/;
      //const passReg = /^(?=.*\d)(?=.*[a-z])(?=.*[A-Z])[0-9a-zA-Z]{8,}$/;
       if(text!=''){
         if (reg.test(text) === false) {
           return false;
         }
         else {
         this.setState({lastName:text,
        lastNameError:false})
         }
         }
         else{
         this.setState({lastName:'',
        lastNameError:true})
         }
      }
  
      mobilevalidate = (text3) => {
        const reg = /^[0-9\b]+$/;
        
        if(text3!=''){
        if (reg.test(text3) === false) {
          return false;
        } 
        else {
        this.setState({phone:text3,
        phoneError:false})
        }
        }
        else{
        this.setState({phone:'',
        phoneError:true})
        }
      }
     
      messageValidate(text) {    
        // const reg = /^(?=.*[a-z])[0-9a-zA-Z ]*$/;
        const reg =  /^[A-Za-z 0-9., \b]+$/;
         if(text!=''){
           if (reg.test(text) === false) {
             return false;
           }
           else {
           this.setState({message:text,
          messageError:false})
           }
           }
           else{
           this.setState({message:'',
           messageError:true})
           }
        }

  onSubmit = (e) => {
    console.log('in submit');
    let count = 0;
    let phone = this.state.phone;
     if(this.state.firstName== "" ){
           this.setState({
                 firstNameError:true
           }) 
      count++;
          }
           if(this.state.lastName== ""){
              this.setState({
                lastNameError:true
              })
        count++;
             }
         if(this.state.email== ""){
                this.setState({
                  emailError:true
                })
          count++;
               }
           if(this.state.phone== "" || phone.length < 10){
                  this.setState({
                    phoneError:true
                  })
            count++;
                 }
             if(this.state.message== "" || this.state.message== " "){
                    this.setState({
                      messageError:true
                    })
              count++;
                   }
              if(this.state.tenant === false && this.state.manager === false &&  this.state.landlord === false &&  this.state.broker === false ){
                      this.setState({
                        checkBoxError:true
                      })
                count++;
                     }
                else{
                  this.setState({
                    checkBoxError:false
                  })
                }
                if(count === 0){
                 
                  let input_type ='';
                  if(this.state.tenant){
                    input_type = input_type.concat('Prospective Tenant, ');
                  }
                  if(this.state.landlord){
                    input_type = input_type.concat('Landlord, ');
                  }
                  if(this.state.broker){
                    input_type = input_type.concat('Broker Agent, ');
                  }
                  if(this.state.manager){
                    input_type = input_type.concat('Property Manager ');
                  }
                   console.log('input_type', input_type);

                  
                  this.setState({
                    showProgress: true,
                  });
                  Api("contactus_send_enquiry_form", {
                    input_type: input_type,
                    property_address: this.state.address,
                    name: this.state.firstName,
                    email: this.state.email,
                    mobileno: this.state.phone,
                    message: this.state.message,
                  })
                    .then((responseJson) => {
                      this.setState({
                        showProgress: false,
                      });
                  if(responseJson.status==1){
                    console.log('22222');
                    console.log(responseJson.message);
                    Alert.alert(responseJson.message);
                    }
                    else if(responseJson==false){
                      alert('No internet');
                    }
                  else{
                    Alert.alert(responseJson.message);
                  }
                }).catch((error) => {
              console.error(error);
              Alert.alert(error);
              this.setState({
                showProgress: false,
              });
            });
                  }
  }
 
  render() {
    
    return (
       <SafeAreaView style={styles.container}>
      <View style={styles.container}>
           <ToolbarWithNavAndHome title="Feedback " />    
           <View style={styles.mainView}>
           <ScrollView >
         
          <View style={[styles.iconRow,{marginTop:hp('3%')}]}>
           <CheckBox
            style={{ paddingRight: 5}}
             checkBoxColor={'#034d94'}
                    checkedCheckBoxColor={'#034d94'}
            onClick={()=>{
           this.setState({
              tenant:!this.state.tenant,
             
           })
               }}
           isChecked={this.state.tenant}
          />
             <Text style={styles.checkboxText}>Prospective Tenant</Text>
            <CheckBox
            style={{ paddingRight: 5}}
             checkBoxColor={'#034d94'}
                    checkedCheckBoxColor={'#034d94'}
            onClick={()=>{
           this.setState({
              manager:!this.state.manager
           })
               }}
          isChecked={this.state.manager}
          />
               <Text style={styles.checkboxText}>Property Manager</Text>
          </View>

           <View style={[styles.iconRow,{marginBottom:hp('3%')}]}>
           <CheckBox
            style={{ paddingRight: 5}}
             checkBoxColor={'#034d94'}
                    checkedCheckBoxColor={'#034d94'}
            onClick={()=>{
           this.setState({
              landlord:!this.state.landlord
           })
               }}
          isChecked={this.state.landlord}
          />
            <Text style={styles.checkboxText}>Landlord</Text>
            <CheckBox
            style={{ paddingRight: 5}}
             checkBoxColor={'#034d94'}
                    checkedCheckBoxColor={'#034d94'}
            onClick={()=>{
           this.setState({
              broker:!this.state.broker
           })
               }}
          isChecked={this.state.broker}
          />
               <Text style={styles.checkboxText}>Broker Agent</Text>
          </View>
          { this.state.checkBoxError == true ? (
             <Text style={commonStyles.errorMessage}>
                 * Please select one .
             </Text>
            ) : null  }

            <Text style={styles.nameStyle}>Address</Text>
             <View style={styles.textInputView}>
            <TextInput
         style={styles.textInput}
           underlineColorAndroid = "transparent"
             style = {{ flex: 1, textAlignVertical:'top' }}
            numberOfLines={3}
          placeholder="Address here"
          onChangeText={(address) => this.setState({address})}
          value={this.state.address}
          multiline={true}
          returnKeyType = {"next"}
          autoFocus = {true}
        onSubmitEditing={(event) => { 
           this.refs.FirstN.focus(); 
         }}
        />
        </View>
        <View style={styles.textinputBorder}></View>

           <Text style={styles.nameStyle}>First Name</Text>
            <TextInput
          style={styles.textInput}
           underlineColorAndroid = "transparent"
          placeholder="Enter First Name"
          onChangeText={text => this.namevalidate(text)}  
          value={this.state.firstName}
          ref='FirstN'
           returnKeyType = {"next"}
           onSubmitEditing={(event) => { 
            this.refs.LastN.focus(); 
          }}
        />
        <View style={styles.textinputBorder}></View>
        { this.state.firstNameError == true ? (
             <Text style={commonStyles.errorMessage}>
                 * Please enter correct input to proceed.
             </Text>
            ) : null  }

         <Text style={styles.nameStyle}>Last Name</Text>
            <TextInput
          style={styles.textInput}
           underlineColorAndroid = "transparent"
          placeholder="Enter Last Name"
          onChangeText={text => this.lastNamevalidate(text)}  
          value={this.state.lastName}
          ref='LastN'
           returnKeyType = {"next"}
           onSubmitEditing={(event) => { 
            this.refs.EmailN.focus(); 
          }}
        />
        <View style={styles.textinputBorder}></View>
        { this.state.lastNameError == true ? (
             <Text style={commonStyles.errorMessage}>
                 * Please enter correct input to proceed.
             </Text>
            ) : null  }
       
        <Text style={styles.nameStyle}>Email</Text>
            <TextInput
          style={styles.textInput}
           underlineColorAndroid = "transparent"
          placeholder="Enter Email here"
          onChangeText={(email) => this.setState({email:email, 
            emailError:false})}
          value={this.state.email}
          ref='EmailN'
          returnKeyType = {"next"}
          onSubmitEditing={(event) => { 
           this.refs.PhoneN.focus(); 
         }}
        />
        <View style={styles.textinputBorder}></View>
        { this.state.emailError == true ? (
             <Text style={commonStyles.errorMessage}>
                 * Please enter correct input to proceed.
             </Text>
            ) : null  }
       
        <Text style={styles.nameStyle}>Phone</Text>
            <TextInput
          style={styles.textInput}
           underlineColorAndroid = "transparent"
          placeholder="Enter your Phone number here"
          onChangeText={text => this.mobilevalidate(text)}  
          value={this.state.phone}
          keyboardType={'numeric'}
          maxLength={10}
          ref='PhoneN'
          returnKeyType = {"next"}
          onSubmitEditing={(event) => { 
           this.refs.MessageN.focus(); 
         }}
        />
        <View style={styles.textinputBorder}></View>
        { this.state.phoneError == true ? (
             <Text style={commonStyles.errorMessage}>
                 * Please enter correct input to proceed.
             </Text>
            ) : null  }

         <Text style={styles.nameStyle}>Message</Text>
             <View style={styles.textInputView}>
            <TextInput
         style={styles.textInput}
           underlineColorAndroid = "transparent"
             style = {{ flex: 1, textAlignVertical:'top' }}
            numberOfLines={3}
          placeholder="write here message"
          onChangeText={text => this.messageValidate(text)}  
          value={this.state.message}
          multiline={true}
          ref='MessageN'
        />
        </View>
        <View style={styles.textinputBorder}></View>
        { this.state.messageError == true ? (
             <Text style={commonStyles.errorMessage}>
                 * Please enter correct input to proceed.
             </Text>
            ) : null  }
         
       </ScrollView>
      <TouchableOpacity style={styles.lastButton}
      onPress={ this.onSubmit } >
      <Text style={styles.sendText}> Send</Text>
     </TouchableOpacity>
     <ProgressDialog
          // title="Progress Dialog"
          activityIndicatorColor="red"
          activityIndicatorSize="large"
          animationType="fade"
          message="Please, wait..."
          visible={this.state.showProgress}
        />
        </View>
          </View>
           </SafeAreaView>
    );
  }
}
const styles = StyleSheet.create({
container: {
    flex: 1,
   backgroundColor: "#ededed"
  },
  mainView:{
    height:'100%',
     width:'90%',
     margin:wp('5%'), 
     backgroundColor:'white',
      borderRadius:10, 
      flex:1
},

iconRow:{
  height:hp('5%'),
   marginTop:hp('1%'), 
    width:'100%',
     flexDirection:'row', 
     alignItems:'center',
      //backgroundColor:'red',
     paddingStart:'5%', paddingEnd:'5%'
     },
    checkboxText:{
      width:'42%',
      fontSize:13,
       color:'gray'
    },
    nameStyle:{
    marginTop:hp('2%'), 
     marginStart:wp('10%'),
  // backgroundColor:'yellow', 
 },
 textInput:{
    marginStart:wp('10%'), 
     marginTop:-hp('1.3%'),
     //backgroundColor:'yellow', 
    // height:hp('5%')
 },
 textinputBorder:{
   marginStart:wp('5%'), 
   marginEnd:wp('5%'), 
    backgroundColor:'gray',
     height:1,
      width:'85%',
      marginTop:-hp('1%')
      },
   textInputView:{
           height:hp('13%'),
            marginStart:'10%',
             marginEnd:'10%',
             justifyContent: 'flex-start',
   },
   lastButton:{
     width:'100%',
       marginTop:hp('2%'),
        height:'8%', 
        borderBottomLeftRadius:10,
       borderBottomRightRadius:10, 
        backgroundColor:'#034d94', 
        justifyContent:'center', 
        alignItems:'center'
   },
   sendText:{
     color:'white',
      fontWeight:'bold'
   }
});

