import React, { Component } from 'react';
import { Text, View, SafeAreaView, Image, TouchableOpacity, StyleSheet, Dimensions, Alert,
Button, FlatList } from 'react-native';
import {widthPercentageToDP as wp, heightPercentageToDP as hp} from 'react-native-responsive-screen';
import ToolbarHomeImg from '../component/ToolbarHomeImg';
import AsyncStorage from '@react-native-community/async-storage';
var halfbath = '';
var token ='';
const monthNames = [" ", "January", "February", "March", "April", "May", "June",
  "July", "August", "September", "October", "November", "December"
];
export default class MyProperties extends Component {
  state = {
    search: '', 
    flatlistData:[],
      date:[]
  }
  async componentDidMount(){
    token = await AsyncStorage.getItem('Token');
    console.log('token', token);
    this.fetchData();
   }
 

 fetchData(){
  fetch('https://1800rentpro.com/rentpro_api/api/landlord_my_properties', {
    method: 'POST',
    headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json',
              },
    body: JSON.stringify({
      usertype: '2',
      userid: token
          })
  }).then((response) => response.json())
    .then((responseJson) => {
    
  if(responseJson.status==1){
    let dataArray =  responseJson.response.data;
             var date = dataArray.map(function(item){
               if(item.availabledate != null){
                let splitDate = item.availabledate.split('-');
                return splitDate;
               }
               else{
                return null;
               }
                 
             })
            console.log('date', date);
    
    this.setState({
      flatlistData:responseJson.response.data,
      date:date,
      showProgress: false
    },function(){
   // console.log(responseJson.response);
    })
  
    }
  else{
    Alert.alert(responseJson.message);
    this.setState({
      showProgress: false
    });
  }
}).catch((error) => {
console.error(error);
this.setState({
showProgress: false
});
});
 }

  _renderItem = ({item, index}) => {
   
    if(item.halfbaths==1){
      halfbath= '1/2';
    }
    return(
    <View style={styles.flatListView}>
   <Image
          style={styles.displayImage}
          source={{uri: item.image_url}}
       />
         <TouchableOpacity  onPress={()=> this.props.navigation.navigate('PropertyInfo', {Property_id:item.id})}>
        <View style={styles.firstRow}>
        <View style={{flexDirection:'row'}}>
    <Text style={styles.prizeText} numberOfLines={1}>${item.minrent} - {item.maxrent}</Text>
    {
      item.approvalstatus === '0'?
      <View style={styles.viewButton}> 
      <Text style={{fontSize:10}}>Pending</Text>
     </View>
     :
     null
    }
    {
      item.approvalstatus === '1'?
      <View style={styles.paidView}> 
      <Text style={{fontSize:10}}>Approved</Text>
     </View>
     :
     null
    }
    {
      item.approvalstatus === '2'?
      <View style={styles.unpaidView}> 
      <Text style={{fontSize:10}}>Rejected</Text>
     </View>
     :
     null
    }
    {
      item.approvalstatus === '3'?
      <View style={styles.viewButton}> 
      <Text style={{fontSize:10}}>Deleted</Text>
     </View>
     :
     null
    }
     {
      item.approvalstatus === null?
      null
     :
     null
    }
   
     </View>
    <TouchableOpacity   onPress={()=>  this.props.navigation.navigate('EditProperty', {
      id: item.id
    })}>
    <Image
          style={styles.imageStyle}
          source={require('../image/edit.png')}
       />
       </TouchableOpacity>
    </View>
     <Text style={styles.noOfItemtext}>{item.bedrooms} Bedrooms | {item.fullbaths}  {halfbath} Bathrooms</Text>
      <Text style={styles.location} numberOfLines={1}>{item.address} </Text>
       <View style={styles.lastRow}>
       <Image
           style={styles.calenderIcon}
          source={require('../image/calendar.png')}
       />
        {this.state.date[index] != null ?
    <Text style={styles.availableText}>Available From, {monthNames[Number(this.state.date[index][1])]} {Number(this.state.date[index][2])}</Text>
    :
    <Text style={styles.availableText}>Available From, </Text>
        }
     <View style={styles.viewButton}> 
      <Text style={{fontSize:10}}>{item.application_count} Applications</Text>
     </View>
    </View>
    </TouchableOpacity>
     <TouchableOpacity style={styles.viewStyle}
      onPress={()=>  this.props.navigation.navigate('ViewApplicant',{Property_id:item.id})}>
       <Image
          style={styles.imageStyle}
          source={require('../image/users.png')}
       />
     <Text style={{color:'white'}}> View Applicants</Text>
     </TouchableOpacity>
    </View>
  );
  }
  render() {
    
    return (
       <SafeAreaView style={styles.container}>
      <View style={styles.container}>
     <ToolbarHomeImg title="My Properties" />

            <FlatList
        data={this.state.flatlistData}
      extraData={this.state}
      keyExtractor={(item, index) => index.toString()}
        renderItem={this._renderItem}
      />
    {/* <View style={{height:hp('1%'), width:'100%', marginTop:hp('1%')}}></View> */}
          </View>
           </SafeAreaView>
    );
  }
}
const styles = StyleSheet.create({
container: {
    flex: 1,
   backgroundColor: "#ededed"
  },
  menuIcon: {
    height: "40%",
    resizeMode: "contain",
    width: 30,
    marginStart: 5
  },
   flatListView:{
     height:hp('33.5%'),
     marginStart:hp('3%'),
     marginEnd:hp('3%'),
     marginTop:hp('1.5%'),
     marginBottom:hp('1.5%'),
     borderRadius:13,
     backgroundColor:'white',
       
  },
  firstRow:{
    flexDirection:'row', 
   marginStart:wp('3.7%'),
   marginTop:wp('3.7%'),
    marginEnd:wp('3.7%'),
    justifyContent: "space-between",
    },
    lastRow:{
    flexDirection:'row', 
   marginStart:wp('3.7%'),
   height:hp('4.5'),
   marginTop:hp('1%'),
   // backgroundColor:'red',
     alignItems:'center',
     marginBottom:hp('1%')
    },
   imageStyle:{ 
     width: hp('3%'), 
     height:hp('3%'),
      },
      viewStyle:{
        justifyContent:'center', 
        alignItems:'center', 
        height:hp('6.5%'),
      backgroundColor:'#034d94', 
      borderBottomLeftRadius:11,
       borderBottomRightRadius:11, 
      flexDirection:'row'
      },
      viewButton: {
        justifyContent:'center', 
        alignItems:'center',
         backgroundColor:'#d3e6ef',
          borderRadius:15, height:hp('3%'),
      borderColor:'#005294',
       borderWidth:2,
        marginStart:'9%',
         width:wp('25%')
         },
         displayImage:{
            borderTopLeftRadius: 11, 
        borderTopRightRadius: 11,
        width: '100%', 
        height: hp('10%')
        },
        location:{
          color:'black',
           marginStart:wp('3.7%'),
            fontWeight:'bold'
        },
        noOfItemtext:{
          color:'gray',
           marginStart:wp('3.7%'),
            fontSize:11.5,
            marginTop:hp('2%')
            },
            prizeText:{
              color:'#034d94',
               fontWeight:'bold', 
               width:'30%'
    },
    availableText:{
      marginStart:wp('2%'),
       color:'gray', 
       fontSize:11.5, 
       width:'51%'
    },
    calenderIcon:{
      width: hp('2%'),
       height:hp('2%'), 
    },
    unpaidView:{
      justifyContent:'center',
       alignItems:'center',
        backgroundColor:'#ffcccd', 
        borderRadius:15, height:hp('3%'),
         borderColor:'#f0b0b2',
      borderWidth:2,
      marginStart:'13%',
      width:wp('25%')
    },
    paidView:{
      justifyContent:'center',
       alignItems:'center',
        backgroundColor:'#cbe8d8',
         borderRadius:15, 
         height:hp('3%'),
          borderColor:'green',
      borderWidth:2,
      marginStart:'13%',
      width:wp('25%')
    },
});