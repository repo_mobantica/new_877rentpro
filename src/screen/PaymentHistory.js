import React, { Component } from 'react';
import { Text, View, SafeAreaView, Image, TouchableOpacity, StyleSheet, Dimensions, Alert,
Button, FlatList } from 'react-native';
import {widthPercentageToDP as wp, heightPercentageToDP as hp} from 'react-native-responsive-screen';
import ToolbarHomeImg from '../component/ToolbarHomeImg';
import AsyncStorage from '@react-native-community/async-storage';
import Api from "../Api/Api";
import { identifier } from '@babel/types';
import { 
  ProgressDialog, 
} from "react-native-simple-dialogs";
 var token ='';
const monthNames = [" ", "January", "February", "March", "April", "May", "June",
  "July", "August", "September", "October", "November", "December"
];
export default class PaymentHistory extends Component {

    state = {
     token:'',
     showProgress: false,
     flatlistData:[],
     date:[],
  }

   async componentDidMount(){
   token = await AsyncStorage.getItem('Token');
   console.log('hasss permission');
    console.log(token);
   this.fetchData();
}

     fetchData(){
      
          this.setState({
            showProgress: true
          });
          Api("tenent_payment_history_list", {
            userid: token,
          })
            .then((responseJson) => {
              this.setState({
                showProgress: false
              });
          if(responseJson.status==1){
            console.log('22222');
            console.log(responseJson.message);
            //  this.storeData(responseJson.response);
            console.log(responseJson.response);
            let dataArray =  responseJson.response.data;
             var date = dataArray.map(function(item){
                  let day = item.created.split(' ');
                  let splitDate = day[0].split('-');
              return splitDate;
             })
            console.log('date', date);
           
           this.setState({
            showProgress: false,
            date: date,
            flatlistData: responseJson.response.data
          }, function(){
            console.log('@@@@@@', this.state.date);
          });
            }
            else if(responseJson==false)
              {
             alert("No internet");
            }
          else{
            Alert.alert(responseJson.message);
            
          }
        }).catch((error) => {
      console.error(error);
      this.setState({
        showProgress: false
      });
    });
     
    }

  _renderItem = ({item, index}) => (
    <View style={styles.flatListView}>
   <Image
          style={styles.displayImage}
           source={{uri: item.image_url}}
       />
        <View style={styles.firstRow}>
    <Text style={styles.priceText}>{item.id}</Text>
    <Image
          style={styles.eyeIcon}
          source={require('../image/eye.png')}
       />
    </View>
     <Text style={styles.addressText} numberOfLines={1}>{item.property_address}</Text>
     <View style={styles.lastRow}>
     <Image
          style={{width: 10.5, height:10.5}}
          source={require('../image/calendar.png')}
       />
       {this.state.date[index] != null ?
     <Text style={styles.dateText}>{monthNames[Number(this.state.date[index][1])]}, {Number(this.state.date[index][2])} {this.state.date[index][0]} </Text>
       :null}
     {item.status === '1'?
     <View style={styles.paidView}> 
        <Text style={styles.paidText}>PAID</Text>
       </View>
       :
       <View style={styles.unpaidView}> 
      <Text style={styles.paidText}>UNPAID</Text>
     </View>}
     
     </View>
    </View>
  );
  render() {
    
    return (
       <SafeAreaView style={styles.container}>
      <View style={styles.container}>
     <ToolbarHomeImg title="Payment History" />
            <FlatList
        data={this.state.flatlistData}
        extraData={this.state}
       keyExtractor={(item, index) => index.toString()}
        renderItem={this._renderItem}
      />
         <ProgressDialog
          // title="Progress Dialog"
          activityIndicatorColor="red"
          activityIndicatorSize="large"
          animationType="fade"
          message="Please, wait..."
          visible={this.state.showProgress}
        />
          </View>
           </SafeAreaView>
    );
  }
}
const styles = StyleSheet.create({
container: {
    flex: 1,
   backgroundColor: "#ededed"
  },
  menuIcon: {
    height: "40%",
    resizeMode: "contain",
    width: 30,
    marginStart: 5
  },
   flatListView:{
     height:hp('24%'),
     margin:hp('3%'),
     marginTop:hp('3%'),
     marginBottom:hp('0%'),
     borderRadius:11,
     backgroundColor:'white',
       
  },
  firstRow:{
    flexDirection:'row', 
   marginStart:wp('3.7%'),
   marginTop:wp('3.7%'),
    marginEnd:wp('3.7%'),
    justifyContent: "space-between",
    },
    lastRow:{
      flexDirection:'row', 
   marginStart:wp('3.7%'),
   marginTop:-6,
    marginEnd:wp('3.7%'),
   alignItems:'center',
   flex:1
    },
    unpaidView:{
      justifyContent:'center',
       alignItems:'center', 
       backgroundColor:'#ffcccd', 
       borderRadius:15, 
       height:hp('3%'),
        borderColor:'#f0b0b2',
   borderWidth:2,
 //   marginStart:'37%',
     width:65
    },
    paidView:{
      justifyContent:'center',
       alignItems:'center', 
       backgroundColor:'#cbe8d8',
        borderRadius:15,
         height:hp('3%'),
          borderColor:'green',
     borderWidth:2,
     //  marginStart:'37%',
        width:65
    },
    dateText:{
      color:'#939393',
       marginStart:wp('1.6%'), 
       fontSize:11.5 ,
       width:'72%'
    },
    paidText:{
      fontSize:13
    },
    addressText:{
      color:'black',
       marginStart:wp('3.7%'), 
       fontWeight:'bold',
    },
    priceText:{
      color:'#034d94',
       fontWeight:'bold'
    },
    displayImage:{
      width: '100%', 
      height: hp('10%'),
        borderTopLeftRadius: 11, 
        borderTopRightRadius: 11,
    },
    eyeIcon:{
      width: 21, height:21
    }
});