import React, { Component } from 'react';
import { Text, View, SafeAreaView, Image, TouchableOpacity, StyleSheet, Dimensions, Alert,
Button, FlatList, TextInput, ImageBackground, PermissionsAndroid } from 'react-native';
import {widthPercentageToDP as wp, heightPercentageToDP as hp} from 'react-native-responsive-screen';
import DialogInput from 'react-native-dialog-input';
import ToolbarHomeImg from '../component/ToolbarHomeImg';
import AsyncStorage from '@react-native-community/async-storage';
import Api from "../Api/Api";
import { 
  ProgressDialog, 
} from "react-native-simple-dialogs";
import {
  LoginButton,
  AccessToken,
  GraphRequest,
  GraphRequestManager,
} from 'react-native-fbsdk';
var email='',first_name='',middle_name='',last_name='';
import { LoginManager } from 'react-native-fbsdk';
var countryArray =[];
var stateArray =[];
var cityArray =[];
var countryList = [];
import {
  GoogleSignin,
  GoogleSigninButton,
  statusCodes,
} from 'react-native-google-signin';
export default class Login extends Component {
    constructor(props) {
    super(props);

    this.state = {
      password:'',
     username:'',
      userInfo: null,
      gettingLoginStatus: true,
      user_name: '',
      token: '',
      profile_pic: '',
      passError:false,
      userNameError: false,
      isDialogVisible:false,
      token:'',
      isVisible:true,
      allData:'',
      showProgress: false,
      countryList:[]
    };   
  }
 
  componentDidMount() {
    //initial configuration
    this.requestLocationPermission();
    GoogleSignin.configure({
      //It is mandatory to call this method before attempting to call signIn()
      //scopes: ['https://www.googleapis.com/auth/drive.readonly'],
      // Repleace with your webClientId generated from Firebase console
      webClientId: '119193189347-nulslg5qqcdc6ebe8m4jbqja1j3lohit.apps.googleusercontent.com',
    });
    //Check if user is already signed in
   // this._isSignedIn();
   // this.getCountry();
  }

   async  requestLocationPermission(){
    try {
      const granted = await PermissionsAndroid.request(
        PermissionsAndroid.PERMISSIONS.ACCESS_FINE_LOCATION,
        {
          'title': 'Example App',
          'message': 'Example App access to your location '
        }
      )
      if (granted === PermissionsAndroid.RESULTS.GRANTED) {
       
         await AsyncStorage.setItem('LocationPermission', 'allow');
        console.log("You can use the location")
      //  alert("You can use the location");
      } else {
        console.log("location permission denied")
      //  alert("Location permission denied");
      }
    } catch (err) {
      console.warn(err)
    }
  }
 
  _isSignedIn = async () => {
    const isSignedIn = await GoogleSignin.isSignedIn();
    if (isSignedIn) {
      alert('User is already signed in');
      //Get the User details as user is already signed in
      this._getCurrentUserInfo();
    } else {
      //alert("Please Login");
      console.log('Please Login');
    }
    this.setState({ gettingLoginStatus: false });
  };
    resetProgress(){
      
    this._toRegistration();
    }
    getCountry(type){
      this.setState({
       showProgress: true
      });
    fetch('https://1800rentpro.com/rentpro_api/api/country_state_city_list', {
       method: 'POST',
       headers: {
               'Accept': 'application/json',
               'Content-Type': 'application/json',
                 },
     }).then((response) => response.json())
       .then((responseJson) => {
       
       if(responseJson.status==1){
        countryArray = responseJson.response.country_list;
        stateArray = responseJson.response.state_list;
        cityArray = responseJson.response.city_list;
         this.setState({
           allData: responseJson.response,
          })
          this.setState({
           showProgress: false
         });
       
 
         this.props.navigation.navigate("Registration",{countryList:responseJson.response.country_list,state_list: responseJson.response.state_list,city_list: responseJson.response.city_list,first_name:first_name,last_name:last_name,middle_name:middle_name,email:email}); 
       }
     else{
       Alert.alert(responseJson.message);  
       this.setState({
         showProgress: false
       });
     }
   }).catch((error) => {
 console.error(error);
 this.setState({
   showProgress: false
 });
 });
   }
 
  _getCurrentUserInfo = async () => {
    try {
      const userInfo = await GoogleSignin.signInSilently();
      console.log('User Info --> ', userInfo);
      this.setState({ userInfo: userInfo });
    } catch (error) {
      if (error.code === statusCodes.SIGN_IN_REQUIRED) {
        alert('User has not signed in yet');
        console.log('User has not signed in yet');
      } else {
        alert("Something went wrong. Unable to get user's info");
        console.log("Something went wrong. Unable to get user's info");
      }
    }
  };
 
  _signIn = async () => {
    //Prompts a modal to let the user sign in into your application.
    try {
     
      await GoogleSignin.hasPlayServices({
        //Check if device has Google Play Services installed.
        //Always resolves to true on iOS.
        showPlayServicesUpdateDialog: true,
      });
      const userInfo = await GoogleSignin.signIn();
      var word = userInfo.user.name
      
        email=userInfo.user.email,
        first_name= word.split(" ")[0],
        last_name='',
        middle_name=''
        var that = this;
     // this.setState({ userInfo: userInfo });
     // this.setToken('google');
       that.getCountry("G")
    } catch (error) {
      
      console.log('Message', error.message);
      if (error.code === statusCodes.SIGN_IN_CANCELLED) {
        console.log('User Cancelled the Login Flow');
      } else if (error.code === statusCodes.IN_PROGRESS) {
        console.log('Signing In');
      } else if (error.code === statusCodes.PLAY_SERVICES_NOT_AVAILABLE) {
        console.log('Play Services Not Available or Outdated');
      } else {
        console.log('Some Other Error Happened');
      }
    }
  };
 
  _signOut = async () => {
    //Remove user session from the device.
    try {
      await GoogleSignin.revokeAccess();
      await GoogleSignin.signOut();
      this.setState({ userInfo: null },function(){
        console.log('successfully logout');
      }); // Remove the user from your app's state as well
    } catch (error) {
      console.error(error);
    }
  };


  _fbAuth() {
    var that = this;
     LoginManager.logInWithPermissions(['public_profile', 'email', 'user_friends']).then(
    function(result) {
     if (result.isCancelled) {
        console.log("Login cancelled");
      } else {
      
        AccessToken.getCurrentAccessToken().then(data => {
          fetch('https://graph.facebook.com/v2.5/me?fields=email,name,first_name,middle_name,last_name&access_token=' + data.accessToken.toString())
          .then((response) => response.json())
          .then((json) => {
            // Some user object has been set up somewhere, build that user here   
         
              email=json.email,
              first_name=json.first_name,
              last_name=json.last_name,
              middle_name=json.middle_name
              that.getCountry("F")
          
        
          })
          .catch((err) => {
           alert('error'+err)
          })
  
        })
      
       // this.setToken('facebook');
      }
    },
    function(error) {
      console.log("Login fail with error: " + error);
    }
  );
  }
     setToken(item){
        this.setState({
             token:item
        })
   }

  async storeData(item){
    console.log('jjjjjj');
    let name = item.firstname;
    name = name.concat(' ', item.lastname);
  
    try {
      await AsyncStorage.setItem('Token', item.id);
      await AsyncStorage.setItem('UserName', name);
      await AsyncStorage.setItem('UserType', item.user_type_id);
      console.log('in side aync');
    } catch (e) {
      // saving error
    }
  }


  onLogout = () => {
    //Clear the state after logout
    this.setState({ user_name: null, token: null, profile_pic: null });
  };

  goToPage(item){
    this.props.navigation.navigate(item);
  }
 
 onSubmit(){

    let count = 0;
     if(this.state.username== "" ){
           this.setState({
                 userNameError:true
           }) 
      count++;
          }
           if(this.state.password== ""){
              this.setState({
                passError:true
              })
        count++;
             }
      
        if(count === 0){
          this.setState({
            showProgress: true
          });     
                Api("login", {
                  email: this.state.username,
                 password: this.state.password
                })
            .then((responseJson) => {
              
          if(responseJson.status==1){
            console.log('22222');
            console.log(responseJson.message);
              this.storeData(responseJson.response.userData);
              var that = this;
           setTimeout(function(){
            that.props.navigation.navigate('MapScreen');
            that.setState({
              showProgress: false
            });
           }, 3000);
           
            }
            else if(responseJson==false){
              alert('No internet');
              this.setState({
                showProgress: false
              });
            }
          else{
            Alert.alert(responseJson.message);
            this.setState({
              showProgress: false
            });
          }
        }).catch((error) => {
      console.error(error);
      this.setState({
        showProgress: false
      });
    });
          }
  }

  _toRegistration = () => {
   // this.getCountry();   this.props.navigation.navigate('AddAbsentNote', { date: this.state.selectDays[i], value: value });
   console.log('listtttt');
   console.log(countryList)
   let a= 'hello';
    this.props.navigation.navigate("Registration",{countryList:countryList});
  };
  showDialog(item){
    this.setState({
      isDialogVisible:item
    })
  }

  render() {
    
    return (
       <SafeAreaView style={styles.container}>
      <View style={styles.container}>
      <ImageBackground
          resizeMode={'stretch'} // or cover
          style={{flex: 1}} // must be passed from the parent, the number may vary depending upon your screen size
          source={require('../image/background.jpg')}
        >
    
           <View style={styles.mainView}>
           <View style={{ height:hp('72%')}}>

           <View style={styles.logoStyle}>
            <Image
          style={styles.logo}
          source={require('../image/logo.png')}
          />
          </View>

          <View style={styles.titleView}>
          <Text style={styles.titleText}>Sign In</Text>
          </View>

              <View style={styles.requiredField}>
            <View style={styles.requiredDot}></View>
             <Text style={styles.nameStyleWithDot}>Username</Text>
             </View>
            <TextInput
          style={styles.textInput}
           underlineColorAndroid = "transparent"
          //placeholder="Enter Email"
          onChangeText={(username) => this.setState({username:username,
          userNameError:false})}
          value={this.state.username}
        />
        <View style={styles.textinputBorder}></View>
        { this.state.userNameError == true ? (
             <Text style={styles.errorMessage}>
                * Please enter correct input to proceed.
             </Text>
            ) : null  }
      
        <View style={styles.requiredField}>
            <View style={styles.requiredDot}></View>
             <Text style={styles.nameStyleWithDot}>Password</Text>
             </View>
              <View style={[styles.requiredField,{justifyContent:'space-between', height:50, width:'100%', }]}>
             
         <TextInput
          style={styles.textInput}
           underlineColorAndroid = "transparent"
          //placeholder="Enter Email"
          onChangeText={(password) => this.setState({password:password,
          passError:false})}
          value={this.state.password}
          secureTextEntry={this.state.isVisible}
        />
        <TouchableOpacity onPress={()=> {this.setState({
          isVisible : !this.state.isVisible
        })}}>
                 <Image style={styles.hideShow} source={require('../image/eye.png')}/>
                 </TouchableOpacity>
              </View>
            <View style={styles.textinputBorder}></View>
            { this.state.passError == true ? (
             <Text style={styles.errorMessage}>
               * Please enter correct input to proceed.
             </Text>
            ) : null  }

        <TouchableOpacity style={styles.forgotPass} 
         onPress={ () => {this.showDialog(true)}}>
        <Text style={{color:'#034d94', fontWeight:'bold'}}> Forgot password?</Text>
        </TouchableOpacity>

        <TouchableOpacity style={styles.signUpBtn}
        onPress={() => this.onSubmit()}> 
      
        <Text style={styles.signInText}> SIGN IN</Text>
        </TouchableOpacity>

       <View style={styles.socialView}>
       <TouchableOpacity style={styles.facebookView}
           onPress={this._fbAuth.bind(this)}
       >
        <Image
          style={styles.fbLogo}
          source={require('../image/facebook.png')}
          />
       <Text style={{fontSize:13, fontWeight:'bold', color:'white'}}>Facebook</Text>
       </TouchableOpacity>

     <TouchableOpacity style={[styles.facebookView,{backgroundColor:'#f43c47'}]}
      onPress={this._signIn}>
        <Image
          style={styles.fbLogo}
          source={require('../image/google.png')}
          />
       <Text style={{fontSize:13, fontWeight:'bold', color:'white'}}>Google</Text>
       </TouchableOpacity>
        </View>

 {/* <TouchableOpacity style={styles.button} onPress={this._signOut}>
              <Text>Logout</Text>
            </TouchableOpacity> */}

     <View style={styles.lastRow}>
     <Text > Not a member? </Text>
        {/* <TouchableOpacity onPress={this._toRegistration} > */}
        <TouchableOpacity onPress={() => this.getCountry("R") }>
          <Text style={{color:'green'}}>Sign Up</Text>
                </TouchableOpacity>
                </View>
                <DialogInput isDialogVisible={this.state.isDialogVisible}
            title={"Forgot Password"}
            message={"Please enter email to create new password"}
            hintInput ={"Enter Email"}
           // hintColor={'gray'}
           // submitInput={ (inputText) => {this.sendInput(inputText)} }
            submitInput={ () => {this.showDialog(false)}}
            closeDialog={ () => {this.showDialog(false)}}>
           </DialogInput>
           <ProgressDialog
          // title="Progress Dialog"
          activityIndicatorColor="red"
          activityIndicatorSize="large"
          animationType="fade"
          message="Please, wait..."
          visible={this.state.showProgress}
        />
        </View>
       </View>
       </ImageBackground>
          </View>
           </SafeAreaView>
    );
  }
}
const styles = StyleSheet.create({
container: {
    flex: 1,
   backgroundColor: "#ededed"
  },
  mainView:{
    height:hp('84%'),
     width:'86%',
     margin:'7%',
     marginTop:hp('6%'),
     marginBottom:hp('6%'), 
     backgroundColor:'white',
      borderRadius:10, 
      flex:1},
      logoStyle:{ 
           width:'100%', 
           height: hp('12%'),
            justifyContent:'center',
             alignItems:'center'
             },
 nameStyle:{
    marginTop:hp('5%'), 
     marginStart:wp('10%')
 },
 textInput:{
     marginTop:-hp('1.3%'),
      marginStart:wp('10%'),
     // backgroundColor:'red',
      width:'70%',
 },
 textinputBorder:{
   marginStart:wp('5%'), 
   marginEnd:wp('5%'), 
    backgroundColor:'gray',
     height:1,
      width:'85%',
      marginTop:-hp('1%'),
      marginBottom:hp('2%')
      },
  buttonStyle:{height:hp('8%'),
       width:'100%',
       borderBottomLeftRadius:10,
       borderBottomRightRadius:10, 
       backgroundColor:'#034d94',
        // marginTop:hp('24%'),
        //justifyContent:'flex-end',
     alignItems:'center'
  },
  requiredDot:{
    height:6,
     width:6,
      borderRadius:3,
       backgroundColor:'red',
         marginStart:wp('8%'),
         marginTop:hp('2%'), 
   },
    requiredField:{
      flexDirection:'row',
       alignItems:'center'
    },
     nameStyleWithDot:{
     marginTop:hp('2%'), 
     marginStart:wp('2%'),
 },
 signUpBtn:{
   height:hp('7%'),
    backgroundColor:'#008f2c',
     width:'84%',
      marginStart:'8%', 
     marginEnd:'8%',
  marginTop:hp('5%'),
   marginBottom:hp('4%'), 
   borderRadius:10,
    justifyContent:'center',
     alignItems:'center'
     },
     lastRow:{
           flexDirection:'row',
            marginTop:hp('1%'), 
            width:'100%', 
            height:hp('6%'),
         justifyContent:'center', 
         alignItems:'center',
         },
    logo:{
      width: '45%',
       height: hp('8%'), 
    },
    titleView:{
      justifyContent:'center',
       alignItems:'center',
        height:hp('11%')
    },
   titleText:{
     color:'#034d94',
     fontWeight:'bold',
     fontSize:20
   },
   hideShow:{
     height:hp('4%'),
      width:hp('4%'),
       marginEnd:wp('10%'),
   },
   forgotPass:{
     height:hp('3%'),
      marginStart:wp('44%'),
       marginTop:-hp('0.5%'), 
   },
   signInText:{
     fontSize:18,
      fontWeight:"bold",
       color:'white'
   },
   socialView:{
     flexDirection:'row',
      height:hp('7%'), 
      marginBottom:hp('1%'),
       justifyContent:'space-between',
       marginStart:wp('8%'),
        marginEnd:wp('8%')
   },
  facebookView:{
    height:hp('7%'),
       backgroundColor:'#034d94',
       width:wp('32%'),
      borderRadius:10,
     justifyContent:'space-between',
       alignItems:'center',
       flexDirection:'row', 
       paddingStart:wp('2%'), paddingEnd:wp('2%')
  },
 fbLogo:{
   width: hp('4%'), height: hp('4%'), 
 },
button: {
    alignItems: 'center',
    backgroundColor: '#DDDDDD',
    padding: 10,
    width: 300,
    marginTop: 30,
  },
  errorMessage:{
    fontSize:11,
    color:'red',
    marginLeft:'10%'
  }
});


