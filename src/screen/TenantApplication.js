import React, { Component } from 'react';
import { Text, View, SafeAreaView, Image, TouchableOpacity,TouchableHighlight, StyleSheet, Dimensions, Alert,
Button, FlatList } from 'react-native';
import {widthPercentageToDP as wp, heightPercentageToDP as hp} from 'react-native-responsive-screen';
 import ToolbarHomeImg from '../component/ToolbarHomeImg';
 import AsyncStorage from '@react-native-community/async-storage';
 import RNPaypal from 'react-native-paypal-lib';
 import Api from "../Api/Api";
 import Toast from 'react-native-simple-toast';
 import { 
  ProgressDialog, 
} from "react-native-simple-dialogs";
var token;
const monthNames = [" ", "Jan", "Feb", "March", "April", "May", "June",
  "July", "Aug", "Sept", "Oct", "Nov", "Dec"
];
export default class TenantApplication extends Component {
  state = {
    dialogVisible: false,
    dialogVisibleReject:false,
    paymentDialog:false,
    flatlistData:[],
    showProgress: false,
    test: true,
    date:[]
  };

  showDialog(){
    this.setState({ dialogVisible: true });
  };
 
  handleCancel = () => {
    this.setState({ dialogVisible: false,
      dialogVisibleReject:false,
      paymentDialog:false});
  };
  moveToTanentApplicationView(id,status,propertyid){
    if(status==="0"){
      this.props.navigation.navigate("TenantApplicationView",{id:id,edit:true,propertyid:propertyid})
    }
       else{
        this.props.navigation.navigate("TenantApplicationView",{id:id,edit:false,propertyid:propertyid})
       }
    }
     async componentDidMount() {
        console.log('inside fav comp');
         token = await AsyncStorage.getItem('Token');
         console.log('token', token);
          this.fetchData();
     }
      fetchData(){
         this.setState({
            showProgress: true,
          });
          Api("tenent_application_list", {
            userid:token
          // userid:'152'
          })
            .then((responseJson) => {
               this.setState({
              showProgress: false
            });
          if(responseJson.status==1){
            let dataArray =  responseJson.response.data;   
            var date = dataArray.map(function(item){
              
            if(item.created_at != null){
              let day = item.created_at.split(' ');
                   let splitDate = day[0].split('-');
               return splitDate;
             }
             else{
              return null;
             }
            })
            console.log("ppppp",responseJson.response)
             this.setState({
               flatlistData : responseJson.response.data,
               propertyid:  responseJson.response.data.propertyid,
               date:date
             })
             
            }
            else if(responseJson==false){
              alert('No internet');
            }
          else{
            Alert.alert(responseJson.message);
            
          }
        }).catch((error) => {
      console.error(error);
      this.setState({
        showProgress: false
      });
    });
  }

  acceptReject(item, id){
   
    this.setState({
      showProgress: true,
    });
    Api("tenent_acceptreject_application", {
      application_id:id, 
      action:item
    })
      .then((responseJson) => {
        this.setState({
          showProgress: false
        });
    if(responseJson.status==1){
      console.log('22222');
      console.log(responseJson.message);    
      }
      else if(responseJson==false){
        alert('No internet');
      }
    else{
      Alert.alert(responseJson.message);
    }
  }).catch((error) => {
console.error(error);
this.setState({
  showProgress: false
});
});
  }
 
  handleDelete = () => {
    // The user has pressed the "Delete" button, so here you can do your own logic.
    // ...Your logic
    this.setState({ dialogVisible: false,
      dialogVisibleReject:false,
      paymentDialog:false });
  };

  showDialogReject(){
    this.setState({ dialogVisibleReject: true });
  };

  showDialogPayment(){
    this.setState({ paymentDialog: true });
  };
  paymentGateway(amount, description, id,propertyid,pay_type_id){
    console.log("propertyidpropertyid",propertyid)
    console.log("id",id)
    console.log("pay_type_id",pay_type_id)
    console.log("user_id",token)
    RNPaypal.paymentRequest({
      clientId: 'ASV62g-yJsjYEKkbd6I-e2OG7dkKr1hB0rBrwQ7JFLrLCrRTTvXpED-wFxe8V8JZ1tJLg8kmxRNWBeyi',
      environment: RNPaypal.ENVIRONMENT.SANDBOX,
      intent: RNPaypal.INTENT.SALE,
      price: amount,
      currency: 'USD',
      description: description,
      acceptCreditCards: true
      }).then(function (payment, confirm) {
        //sendPaymentToConfirmInServer(payment, confirm);
        Toast.show('Payment Done');
        console.log('pymenttttt');
        console.log("payment",payment);
        // console.log("price",price)


        if(payment.response.id){
          Api("add_payment_data", {
            user_id: token,
            propertyid: propertyid,
            amount:amount,
            totalamount: amount,
            payment_type: "application",
            payment_date: new Date().getFullYear()+'-'+("0"+(new Date().getMonth()+1)).slice(-2)+'-'+("0"+new Date().getDate()).slice(-2),
            pay_type_id: pay_type_id,
            application_id:id
          })


       //   console.log("application_id",responseJson);
      .then((responseJson) => {
       
        
      if(responseJson.status==1){
      console.log('Done');
      console.log(responseJson.message);
      Toast.show(responseJson.message);
//this.componentWillMount()
      }
      else if(responseJson==false){
        alert('No internet');
      }
      else{
      Alert.alert(responseJson.message);
      }
      })
      .catch((error) => {
      console.error(error);
      
      });
        }


        if(description === 'Property Payment'){
          Api("tenent_acceptreject_application", {
            application_id:id, 
            action: 'accpet'
          })
            .then((responseJson) => {
              this.setState({
                showProgress: false
              });
          if(responseJson.status==1){
            console.log('22222');
            console.log(responseJson.message);   
            Toast.show(responseJson.message);
            }
            else if(responseJson==false){
              alert('No internet');
            }
          else{
            Alert.alert(responseJson.message);
          }
        }).catch((error) => {
      console.error(error);
      this.setState({
        showProgress: false
      });
      });
        }
      }).catch(err => {
       console.log(err.message);
      alert(err.message)
      })
  }
  statusView(ispaymentDone, status, totalRent, appPayment, id,propertyid,pay_type_id ){
  //  console.log('payment status', item);
   //console.log('propertyidpropertyid', propertyid); 
   //console.log("pay_type_id",pay_type_id)
  // item.payment_status, item.status, totalRent, appPayment, item.id)
    if(ispaymentDone == '0'){
      return(
      <TouchableOpacity style={styles.makePayment}
       onPress={()=> this.paymentGateway(Number(appPayment), 'Application Payment', id,propertyid,pay_type_id )}>
       <Text style={{color:'white'}}> $ MAKE PAYMENT</Text>
       </TouchableOpacity>
      );
    }
    else{
        if(status == '1'){
          return(
            <View style={styles.makePayment}>
             <Text style={{color:'white'}}>Waiting for admin </Text>
             </View>
            );
        }
        else if(status == '2'){
          return(
            <View style={styles.acceptOrReject}>
            <TouchableOpacity style={styles.acceptView}
            //  onPress={()=> this.acceptReject('accpet', )}>
            onPress={()=> this.paymentGateway(totalRent, 'Property Payment', id)}>
            <Text style={{color:'white'}}>ACCEPT</Text>
            </TouchableOpacity>
            <TouchableOpacity style={styles.rejectView}
            onPress={()=> this.acceptReject('reject', item.id)}>
            <Text style={{color:'white'}}>REJECT</Text>
            </TouchableOpacity>
             </View>
          );
        }
        else if(status == '3'){
            return(
              <View style={styles.makePayment}>
               <Text style={{color:'white'}}>Rejected by admin </Text>
               </View>
              );
        }
        else if(status == '4'){
          return(
            <TouchableOpacity style={styles.makePayment}
             >
             <Text style={{color:'white'}}> View</Text>
             </TouchableOpacity>
            );
      }
      else if(status == '5'){
        return(
          <View style={styles.makePayment}>
               <Text style={{color:'white'}}>Rejected by Tenant </Text>
               </View>
          );
    }
    }
  }

  _renderItem = ({item, index}) =>{
        var totalRent =0;
       if(item.payment.convenience_charges === undefined){
        //console.log('undefineee', item.payment.convenience_charges);
       }else{
        totalRent = Number(item.payment.convenience_charges) + Number(item.payment.amount);
       // console.log('Total  ',item.payment.convenience_charges,' + ',item.payment.amount, '= ', totalRent );
       // console.log(typeof totalRent);
       }
    return(
    <View style={styles.flatListView}>
       <Image
          style={styles.displayImage}
          source={{uri: item.image_url}}
       />
        <View style={styles.firstRow}>
    <Text style={styles.priceStyle}>{item.amount}</Text>
    <TouchableHighlight onPress={() => this.moveToTanentApplicationView(item.id,item.payment_status,item.propertyid)}>
    <Image
          style={{width: 21, height:21}}
          source={require('../image/eye.png')}
     />
     </TouchableHighlight>
    </View>
    <Text style={styles.quantityText}>4 Bed | 3 Balcony | 3 Bathrooms</Text>
     <Text style={styles.addressText} numberOfLines={1}>{item.property_address}</Text>
     <View style={styles.lastRow}>
     <Image
          style={{width: 18, height:18}}
          source={require('../image/clock.png')}
       />
       {this.state.date[index] != null ?
     <Text style={styles.applyText}>Applied, {monthNames[Number(this.state.date[index][1])]}, {Number(this.state.date[index][2])} {this.state.date[index][0]} </Text>
     : null}
     {
        item.payment_status === '0' ?
        <View style={styles.unpaidView  }> 
      <Text style={{fontSize:13}}>UNPAID</Text>
     </View>:
      <View style={styles.paidView}> 
      <Text style={{fontSize:13}}>PAID</Text>
     </View>
      }
     </View>
    
     {this.statusView(item.payment_status, item.status, totalRent, item.payment.amount, item.id,item.propertyid,item.payment.pay_type_id)}
    </View>
     );
    }
  render() {
    return (
       <SafeAreaView style={styles.container}>
      <View style={styles.container}>
     <ToolbarHomeImg title={"Tenant Application"} />
            <FlatList
        data={this.state.flatlistData}
        extraData={this.state}
      keyExtractor={(item, index) => index.toString()}
        renderItem={this._renderItem}
      />
        <ProgressDialog
          activityIndicatorColor="red"
          activityIndicatorSize="large"
          animationType="fade"
          message="Please, wait..."
          visible={this.state.showProgress}
        />
      
          </View>
           </SafeAreaView>

           
    );
  }
}
const styles = StyleSheet.create({
container: {
    flex: 1,
   backgroundColor: "#ededed"
  },
  menuIcon: {
    height: "40%",
    resizeMode: "contain",
    width: 30,
    marginStart: 5
  },
  headerView: {
      position: 'absolute',
    flexDirection: "row",
    backgroundColor: "white",
    justifyContent: "space-between",
    //paddingHorizontal: 10,
    alignItems: "center",
    height:  hp('9%'),
    width:'100%'
  },
   flatListView:{
     height:hp('32%'),
     margin:hp('3%'),
     marginTop:hp('1.5%'),
     marginBottom:hp('1.5%'),
     borderRadius:13,
     backgroundColor:'white',
       
  },
  firstRow:{
    flexDirection:'row', 
   marginStart:wp('3.7%'),
   marginTop:wp('3.7%'),
    marginEnd:wp('3.7%'),
    justifyContent: "space-between",
    },
    lastRow:{
      flexDirection:'row', 
   marginStart:wp('3.7%'),
   marginTop:-6,
    marginEnd:wp('3.7%'),
   alignItems:'center',
   flex:1
    },
    displayImage:{
       borderTopLeftRadius: 11, 
        borderTopRightRadius: 11,
        width: '100%', 
        height: hp('10%')
    },
    priceStyle:{
      color:'#034d94',
       fontWeight:'bold'
    },
    quantityText:{
      color:'#939393',
       marginStart:wp('3.7%'),
        fontSize:11.5
    },
   addressText:{
     color:'black', 
     marginStart:wp('3.7%'),
      fontWeight:'bold',
   },
   applyText:{
     color:'#939393', 
     marginStart:'2%',
      fontSize:11.5,
      width:'55%'
   },
    unpaidView:{
      justifyContent:'center',
       alignItems:'center',
        backgroundColor:'#ffcccd', 
        borderRadius:15, height:hp('3%'),
         borderColor:'#f0b0b2',
      borderWidth:2,
      marginStart:'13%',
     width:65
    },
    paidView:{
      justifyContent:'center',
       alignItems:'center',
        backgroundColor:'#cbe8d8',
         borderRadius:15, 
         height:hp('3%'),
          borderColor:'green',
      borderWidth:2,
      marginStart:'13%',
     width:65
    },
    makePayment:{
      justifyContent:'center',
       alignItems:'center',
        height:hp('6.5%'),
      backgroundColor:'#008f2c',
       borderBottomLeftRadius:11,
        borderBottomRightRadius:11
    },
    acceptOrReject:{
      height:hp('6.5%'),
       flexDirection:'row' 
    },
    acceptView:{
      borderBottomLeftRadius:11,
       width:'50%',
        backgroundColor:'#00508f',
         justifyContent:'center',
          alignItems:'center'
    },
    rejectView:{
       width:'50%',
        backgroundColor:'#eb3b48',
         borderLeftColor:'white',
          borderLeftWidth :1,
           justifyContent:'center',
            alignItems:'center',
        borderBottomRightRadius:11
    }

});
