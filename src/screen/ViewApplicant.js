import React, { Component } from "react";
import {
  Text,
  View,
  SafeAreaView,
  Image,
  TouchableOpacity,
  StyleSheet,
  Dimensions,
  Alert,
  Button,
  FlatList,Linking,
} from "react-native";
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp
} from "react-native-responsive-screen";
import ToolbarWithLeftIcon from "../component/ToolbarWithLeftIcon";
import { ProgressDialog } from "react-native-simple-dialogs";
import Communications from 'react-native-communications';
var Property_id = "";
const monthNames = [" ", "Jan", "Feb", "March", "April", "May", "June",
  "July", "Aug", "Sept", "Oct", "Nov", "Dec"
];

export default class ViewApplicant extends Component {
  constructor(props) {
    super(props);
    this.state = {
      dataSource: "",
      showProgress: false,
      date:[],
    };
  }
  componentWillMount() {
    Property_id = this.props.navigation.getParam("Property_id");
    this.fetchData();
  }

  fetchData() {
    this.setState({
      showProgress: true
    });
    fetch(
      "https://1800rentpro.com/rentpro_api/api/landlord_tenent_application_list",
      {
        method: "POST",
        headers: {
          Accept: "application/json",
          "Content-Type": "application/json"
        },
        body: JSON.stringify({
          usertype: "2",
        //  propertyid: '841'
          propertyid: Property_id
        })
      }
    )
      .then(response => response.json())
      .then(responseJson => {
        console.log("responseJsonfgdfgh", responseJson.response);
        console.log("responseJson", responseJson.message);
        let dataArray =  responseJson.response.data;
        var date = dataArray.map(function(item){
          if(item.move_in_date != null){
           let splitDate = item.move_in_date.split('-');
           return splitDate;
          }
          else{
           return null;
          }
        })
        this.setState({
          dataSource: responseJson.response.data,
          date :date
        });
        console.log(
          "responseJson.responseresponseJson.response",
          this.state.dataSource
        );
        // console.log(
        //   "this.state.dataSource.user_data",
        //   this.state.dataSource.user_data.mobileno
        // );
        if (responseJson.status == 1) {
          console.log("22222");
          console.log(responseJson.message);

          this.setState({
            showProgress: false
          });
        } else {
          Alert.alert(responseJson.message);
          this.setState({
            showProgress: false
          });
        }
      })
      .catch(error => {
        console.error(error);
        this.setState({
          showProgress: false
        });
      });
  }



  _renderItem = ({ item, index }) => (
    <View style={styles.flatListView}>
      <Image
        style={styles.displayImage}
        // source={require('../image/room.png')}
        source={{ uri: item.image_url }}
      />

      <Text style={styles.userName}>{item.user_data.firstname} {item.user_data.lastname}</Text>

      <Text style={styles.address}>{item.property_address}</Text>
      <View style={styles.dateRow}>
        <Image style={styles.icon} source={require("../image/calendar.png")} />
        {this.state.date[index] != null ?
        <Text style={styles.dateText}>
          Moved in From, {monthNames[Number(this.state.date[index][1])]} {Number(this.state.date[index][2])}, {Number(this.state.date[index][0])}
        </Text>
        :
        <Text style={styles.dateText}> Moved in From, </Text>
     }
      </View>

      <View style={styles.lastRow}>
        <TouchableOpacity style={styles.phoneView} 
        onPress={() => Communications.phonecall(item.user_data.mobileno, true)}>
          <Image style={styles.icon} source={require("../image/Phone.png")} />
          <Text style={{ color: "white" }}> Phone</Text>
    </TouchableOpacity>
        <TouchableOpacity style={styles.middleView}
        onPress={() => Communications.
          email([item.user_data.email],null,null,'My Subject','My body text')}>
       
       
          <Image style={styles.icon} source={require("../image/Email.png")} />
          <Text style={{ color: "white" }}> Email</Text>
          </TouchableOpacity>
        
        <TouchableOpacity style={styles.phoneView} 
        onPress={() => Communications.text(item.user_data.mobileno, true)}>
          <Image style={styles.icon} source={require("../image/Message.png")} />
          <Text style={{ color: "white" }}> Message</Text>
          </TouchableOpacity>
      </View>
    </View>
  );
  render() {
    return (
      <SafeAreaView style={[styles.container]}>
        <View style={styles.container}>
          <ToolbarWithLeftIcon title="My Applicants" />

          <FlatList
            data={this.state.dataSource}
            extraData={this.state}
            keyExtractor={(item, index) => index.toString()}
            renderItem={this._renderItem}
          />
          <View style={{ height: hp("1%"), width: "100%" }}></View>
          <ProgressDialog
            // title="Progress Dialog"
            activityIndicatorColor="red"
            activityIndicatorSize="large"
            animationType="fade"
            message="Please, wait..."
            visible={this.state.showProgress}
          />
        </View>
      </SafeAreaView>
    );
  }
}
const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "#ededed"
  },
  flatListView: {
    height: hp("30%"),
    margin: hp("3%"),
    marginTop: hp("3%"),
    marginBottom: hp("0%"),
    borderRadius: 11,
    backgroundColor: "white"
  },
  dateRow: {
    flexDirection: "row",
    marginStart: wp("3.7%"),
    height: hp("4"),
    // backgroundColor:'red',
    alignItems: "center",
    marginBottom: hp("1%")
  },
  lastRow: {
    flexDirection: "row",
    borderBottomRightRadius: 11,
    backgroundColor: "#034d94",
    alignItems: "center",
    flex: 1,
    height: hp("6%"),
    borderBottomLeftRadius: 11
  },
  displayImage: {
    width: "100%",
    height: hp("10%"),
    borderTopLeftRadius: 11,
    borderTopRightRadius: 11
  },
  userName: {
    color: "#034d94",
    marginStart: wp("3.7%"),
    marginTop: wp("3.7%"),
    fontWeight: "bold"
  },
  address: {
    color: "black",
    marginStart: wp("3.7%"),
    fontWeight: "bold"
  },
  phoneView: {
    flexDirection: "row",
    width: "33%",
    height: "100%",
    justifyContent: "center",
    alignItems: "center"
  },
  middleView: {
    flexDirection: "row",
    width: "33%",
    height: "100%",
    borderLeftWidth: 1,
    borderColor: "white",
    borderRightWidth: 1,
    justifyContent: "center",
    alignItems: "center"
  },
  icon: {
    height: hp("3%"),
    width: hp("3%"),
    marginEnd: wp("2%")
  },
  dateText: {
    marginStart: wp("2%"),
    color: "gray",
    fontSize: 12
  }
});
