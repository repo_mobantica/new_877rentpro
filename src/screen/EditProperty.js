import React, { Component } from 'react'
import { AppRegistry, StyleSheet, Text, View, Button, TouchableOpacity } from 'react-native'
import EditPropertyOne from '../component/EditPropertyOne';
import EditPropertyTwo from '../component/EditPropertyTwo';
import EditPropertyThree from '../component/EditPropertyThree';
import EditPropertyFour from '../component/EditPropertyFour';
import EditPropertyFive from '../component/EditPropertyFive';
import Swiper from 'react-native-swiper';
import ToolbarHomeImg from '../component/ToolbarHomeImg';
import {widthPercentageToDP as wp, heightPercentageToDP as hp} from 'react-native-responsive-screen';
import AsyncStorage from '@react-native-community/async-storage';
import Toast from 'react-native-simple-toast';
import Api from "../Api/Api";
import { 
  ProgressDialog, 
} from "react-native-simple-dialogs";

var token ='';
var photoGallery=[];
var imageArray;
const renderPagination = (index, total, context) => {

  return (
    <View >
      <Text style={{ color: 'grey' }}>
        <Text >{index + 1}</Text>/{total}
      </Text>
    </View>
  )
}

export default class EditProperty extends Component {

  constructor(props){
    super(props);
    this.params = this.props.navigation.state.params;
    this.onPressNext = this.onPressNext.bind(this);
    this.onPressPrev = this.onPressPrev.bind(this);
    this.state = {
      idxActive: 0,
      addOne:'',
      addTwo:'',
      addThree:'',
      addFour:'',
      addFive:'',
      showProgress: false, set:false
    }
 }
 async componentDidMount(){
    token = await AsyncStorage.getItem('Token');
    const id=this.params.id;
    console.log("id",id);

}
componentWillMount(){
  console.log('9999999');
   this.getData();
   
}
async getData(){
  try {
    //const value = await AsyncStorage.getItem('Token');
    this.setState({
      showProgress: true
    });
   // token=value;
    Property_id =  this.props.navigation.getParam('id');
   // Edit =  this.props.navigation.getParam('edit');
 
    Api("property_details", {
      propertyid:this.params.id,
    
    })
      .then(responseJson => {
        if (responseJson.status == 1) {
         data=responseJson.response.data;

         console.log("data",responseJson.response.data);
         this.setState({
          showProgress: false,
          set:true
        });
        }
       else if(responseJson==false)
        {
          alert("No internet");
          this.setState({
            showProgress: false,
            set:false,
          });
        }
         else {
          Alert.alert(responseJson.message);
          this.setState({
            showProgress: false,
            set:false,
          });
        }
  })
  } catch(e) {
    // error reading value
  }
}
convertImage(image, index){
  fetch('https://1800rentpro.com/rentpro_api/api/upload_image', {
    method: 'POST',
    headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json',
              },
    body: JSON.stringify({
      image: image,
      type:"property"
          })
  }).then((response) => response.json())
    .then((responseJson) => {
    
  if(responseJson.status==1){
     photoGallery.push(responseJson.response.data);
     if(imageArray.length === index+1){
      console.log(photoGallery);
     // console.log(photoGallery);
     this.fetchData();
     }
     
      }
  else{
    Alert.alert(responseJson.message);
    this.setState({
      showProgress: false
      });               
  }
}).catch((error) => {              
console.error(error);
this.setState({
  showProgress: false
  });
}); 
}
   onSave=()=>{
    let five = this.refs.editPropertyFive.nextPress();
  
    if(five.isNext){
      this.setState({
        addFive:five
      },function(){
        this.setState({
          showProgress: true
        });
        
        if(imageArray.length != 0){
          for(let i= 0 ; i<imageArray.length; i++){
            this.convertImage(imageArray[i], i);
          }
        }
        else{
          this.fetchData();
        }
        
      })
  
   }
   }
 fetchData (){
//alert(JSON.stringify(this.state.addOne)+"\n"+JSON.stringify(this.state.addTwo)+"\n"+JSON.stringify(this.state.addThree)+"\n"+JSON.stringify(this.state.addFour)+"\n"+JSON.stringify(this.state.addFive))
  fetch('https://1800rentpro.com/rentpro_api/api/update_property', {
    method: 'POST',
    headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json',
              },
    body: JSON.stringify({
      firstname:this.state.addFive.firstname,
      lastname: this.state.addFive.lastname,
      email: this.state.addFive.email,
      mobile: this.state.addFive.mobile,
      address: this.state.addOne.address,
      display_address: this.state.addOne.displayAddress,
      country:"India",
      state:"Maharashtra",
      city:"Pune",
      zip:"411021",
      latitude: this.state.addOne.latitude,
      longitude: this.state.addOne.longitute,
      unit_details: this.state.addOne.unitDetails,
      details: "Property",
      cats: this.state.addTwo.cat,
      dogs:this.state.addTwo.dog,
      bedrooms: this.state.addThree.bedrooms,
      baths:  this.state.addThree.fullBaths,
      minsquarefeet:  this.state.addThree.minsquarefeet,
      laundry_type:  this.state.addThree.laundry,
      parking_type:  this.state.addThree.parkingType,
      parkingfee:  this.state.addThree.parkingFees,
      minrent:  this.state.addThree.minrent,
      mindeposit: this.state.addThree.deposit,
      isopentolease:  this.state.addFour.availabilityStatus,
      availabledate:  this.state.addFour.availableDate,
      leaseperiod:  this.state.addFour.leasePeriod,
      youtube_url: this.state.addFour.youTubeUrl,
      usertype: "2",
      userid: token,
      property_primary_image:this.state.addFour.PrimaryPhoto,
      package:this.state.addFive.package,
      rent_collect: this.state.addFive.rent_collect,
      visibilitystatus: this.state.addFive.visibilitystatus,
      property_photo_gallery: photoGallery,
      property_types: this.state.addTwo.property_types,
      property_appliances:this.state.addTwo.property_appliances,
      property_amenities:this.state.addTwo.property_amenities,
      property_id:this.params.id
      })
  }).then((response) => response.json())
    .then((responseJson) => {
    
  if(responseJson.status==1){
    var that=this;
    Toast.show(responseJson.message);
      if(responseJson.message=="Property Successfully Updated!"){
      setTimeout(function(){
        that.props.navigation.navigate("MyProperties");
        
       }, 2000);
    }
   this.setState({
    showProgress: false
  });
    }
  else{
    Alert.alert(responseJson.message);
    this.setState({
      showProgress: false
    });
  }
}).catch((error) => {
//alert(error)
this.setState({
showProgress: false
});
});
 }
 
 onPressPrev = () => {
  const {idxActive} = this.state;
  if (idxActive > 0) {
    this.refs.swiper.scrollBy(-1)
  }
}

onPressNext = () => {
  const {idxActive} = this.state;
  // let four = this.refs.newPropertyFour.nextPress();
  // console.log('four',four);
  
  if (idxActive < 4) {
    
    switch (this.state.idxActive) {
      case 0:
        let one = this.refs.editPropertyOne.nextPress();
        if(one.isNext){
          this.setState({
            addOne:one
          },function(){
            console.log('one', this.state.addOne);
          })
       this.refs.swiper.scrollBy(1);
       }
        break;
        case 1:
          let two = this.refs.editPropertyTwo.nextPress();
          if(two.isNext){
            this.setState({
              addTwo:two
            },function(){
              console.log('two', this.state.addTwo);
            })
         this.refs.swiper.scrollBy(1);
         }
        break;
      case 2:
        let three = this.refs.editPropertyThree.nextPress();
        if(three.isNext){
          this.setState({
            addThree:three
          },function(){
            console.log('three', this.state.addThree);
          })
       this.refs.swiper.scrollBy(1);
       }
        break;
        case 3:
          let four = this.refs.editPropertyyFour.nextPress();
         imageArray = four.photoGallery;
         console.log('imageArray.length');
        // console.log(imageArray.length);
        // console.log(imageArray);
        if(four.isNext){
          this.setState({
            addFour:four
          },function(){
            console.log('four', this.state.addFour);
          })
       this.refs.swiper.scrollBy(1);
       }
          break;
          
       
    }
  }
}
onResetPress(index){
  if(index==1)
  {
    this.refs.editPropertyTwo.resetPress()
  }
    if(index==2)
    {
      this.refs.editPropertyThree.resetPress()
    }
    if(index==3)
    {
      this.refs.editPropertyyFour.resetPress()
    }
    if(index==4)
    {
      this.refs.editPropertyFive.resetPress()
    }
  }
  render() {
    return (

      <View style={{flex:1}}>
        <ToolbarHomeImg title="Edit Property" />
         
        <Swiper
          style={styles.wrapper}
          renderPagination={renderPagination}
          showsButtons={false}
           showsPagination={false}
          scrollEnabled={false}
          loop={false}
          ref={'swiper'}
          dot={true}
          onIndexChanged={idxActive => this.setState({idxActive},function(){
           // console.log('nextttt', this.state.idxActive);
          }) }
        >
              
            <EditPropertyOne  ref="editPropertyOne"  data={this.state.set?data:null}/>
            <EditPropertyTwo ref="editPropertyTwo"  data={this.state.set?data:null}/>
            <EditPropertyThree ref="editPropertyThree"  data={this.state.set?data:null}/>
            <EditPropertyFour ref="editPropertyyFour"  data={this.state.set?data:null}/>
            <EditPropertyFive ref="editPropertyFive"  data={this.state.set?data:null}/>
           </Swiper>

         <View style={styles.footer}>
          <View style={{flexDirection:'row',}}>
          <View style={styles.indicator}></View>
           <View style={this.state.idxActive >=1 ? styles.indicator : styles.inactiveIndicator}></View>
            <View style={this.state.idxActive >=2 ? styles.indicator : styles.inactiveIndicator}></View>
             <View style={this.state.idxActive >=3 ? styles.indicator : styles.inactiveIndicator}></View>
            <View style={this.state.idxActive >=4 ? styles.indicator : styles.inactiveIndicator}></View>
         </View>
            
            {this.state.idxActive >=1 ?
            <TouchableOpacity style={ styles.resetBtn}
            onPress={this.onResetPress.bind(this,this.state.idxActive)}
       >
       <Text style={[styles.nextText,{color:'black'}]}>Reset</Text>
       </TouchableOpacity> 
       :
       null
     }
           
      {this.state.idxActive ===4 ?
          <TouchableOpacity style={styles.applyBtn}
          onPress={this.onSave} 
        >
       <Text style={styles.nextText}>Save</Text>
       </TouchableOpacity> 
      :
       <TouchableOpacity style={styles.nextBtn}
       onPress={this.onPressNext} >
       <Text style={styles.nextText}>Next</Text>
       </TouchableOpacity> 
      }
      </View>
      
      <ProgressDialog
          // title="Progress Dialog"
          activityIndicatorColor="red"
          activityIndicatorSize="large"
          animationType="fade"
          message="Please, wait..."
          visible={this.state.showProgress}
        />
      </View>
    );
  }
}

const styles = StyleSheet.create({
  wrapper: {},
 
   indicator:{ 
         height:12,
          width:12,
          borderRadius:6,
          backgroundColor:'#008f2c',
         margin:wp('1%'),
       },
       inactiveIndicator:{ 
         height:12,
          width:12,
          borderRadius:6,
          backgroundColor:'white',
          borderWidth:0.5,
         margin:wp('1%'),
       },
       nextBtn:{
         height:hp('7%'),
       backgroundColor:'#034d94',
       width:wp('25%'),
      borderRadius:10,
     justifyContent:'center',
       alignItems:'center',
       flexDirection:'row', 
       paddingStart:wp('2%'),
        paddingEnd:wp('2%')
       },
       applyBtn:{
         height:hp('7%'),
       backgroundColor:'#008f2c',
       width:wp('26%'),
      borderRadius:10,
        flexDirection:'row', 
     justifyContent:'center',
       alignItems:'center',
       },
       nextText:{
         fontSize:13,
          fontWeight:'bold',
           color:'white'
           },
          footer:{
            backgroundColor:'white',
             height:'11%',
              justifyContent:'space-between', 
              flexDirection:'row',
            alignItems:'center',
            paddingStart:'7%',
           paddingEnd:'6%'
       },
         signUpBtn:{
   height:hp('7%'),
    backgroundColor:'#008f2c',
     width:'46%',
      marginStart:'4%', 
     marginEnd:'2%',
   borderRadius:10,
    justifyContent:'center',
     alignItems:'center'
     },
     resetBtn:{
        height:hp('7%'),
      // backgroundColor:'#034d94',
       borderWidth:0.5,
       borderColor:'gray',
        width:wp('24%'),
      borderRadius:10,
     justifyContent:'center',
       alignItems:'center', 
       marginStart:wp('2%'),
        marginEnd:-wp('5%')
     }
})
