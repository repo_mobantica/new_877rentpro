import React, { Component } from "react";
import {
  AppRegistry,
  StyleSheet,
  Text,
  View,Alert,
  Button,
  TouchableOpacity,BackHandler
} from "react-native";
import AddCreditReportOne from "../component/AddCreditReportOne";
import AddCreditReportTwo from "../component/AddCreditReportTwo";
import Swiper from "react-native-swiper";
import Toolbar from "../component/Toolbar";
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp
} from "react-native-responsive-screen";
import AsyncStorage from '@react-native-community/async-storage';
import Toast from 'react-native-simple-toast';
import { 
  ProgressDialog, 
} from "react-native-simple-dialogs";
import RNPaypal from 'react-native-paypal-lib';
import Api from "../Api/Api";
const renderPagination = (index, total, context) => {
  return (
    <View>
      <Text style={{ color: "grey" }}>
        <Text>{index + 1}</Text>/{total}
      </Text>
    </View>
  );
};
var token="";
export default class AddCreditReport extends Component {
  constructor(props) {
    super(props);
    this.onPressNext = this.onPressNext.bind(this);
    this.onPressPrev = this.onPressPrev.bind(this);
    this.state = {
      applyOne: "",
      applyTwo: "",
      idxActive: 0
    };
    this.back_Button_Press = this.back_Button_Press.bind(this);
  }

  onPressPrev = () => {
    const { idxActive } = this.state;
    if (idxActive > 0) {
      this.refs.swiper.scrollBy(-1);
    }
  };

  onPressNext = () => {
    const { idxActive } = this.state;
    console.log("active pageeee");
    console.log(this.state.idxActive);
    // Probably best set as a constant somewhere vs a hardcoded 5
    if (idxActive < 2) {
      let a = this.refs.addReportOne.nextPress();
    
        if(a.isNext == true){
        this.refs.swiper.scrollBy(1);
      }
      // let firstdata=this.refs.addReportOne.firstNextPress();

      console.log("a", a);
      //this.refs.swiper.scrollBy(1);
    }
  };
  async componentDidMount() {
    //this.saveDatas();
    const value = await AsyncStorage.getItem('Token');
    console.log('valueeeeeee',value);
    token = value;
   
  }
  saveDatas = () => {
    let firstdata = this.refs.addReportOne.firstNextPress();
    alert(JSON.stringify("firstdata", firstdata));
  };
  onResetPress = () => {
    let one = this.refs.addReportTwo.resetPress();
  };
  onPressSave = () => {
    let one = this.refs.addReportTwo.savedata();
    let firstdata = this.refs.addReportOne.nextPress();
    if(one==false)
    {
      alert("select at leaset one package")
    }
    else{
      var a = one.data.toString();
      var array = a.split(',');
      this.setState({
        showProgress: true
      });
    //  alert( token+"\n"+firstdata.firstName+"\n"+firstdata.middleName+"\n"+
    //   firstdata.lastName+"\n"+firstdata.address+"\n"+firstdata.birthday+"\n"+[one]+"\n"+firstdata.socialSecurity+"\n"+firstdata.imageFile)
  
    if(firstdata.imageFile != '' ){
    fetch('https://1800rentpro.com/rentpro_api/api/add_credit_report', {
    method: "POST",
    headers: {
      Accept: "application/json",
      "Content-Type": "application/json"
    },
    body: JSON.stringify({
    
      user_id: token,
      firstname: firstdata.firstName,
      middlename:firstdata.middleName,
      lastname:firstdata.lastName,
      address:firstdata.address,
      date_of_birth: firstdata.birthday,
      home_phone: "",
      amount:one.amount,
      cr_package:array,
      ssn:firstdata.socialSecurity,
      imageFile:firstdata.imageFile
      })
  })
    .then(response => response.json())
    .then(responseJson => {

      if (responseJson.status == 1) {
        this.setState({
          showProgress: false
        });
       var that=this;
        Toast.show(responseJson.message);

        if(responseJson.message=="credit report addded successfully!")
        {
          RNPaypal.paymentRequest({
            clientId: 'ASV62g-yJsjYEKkbd6I-e2OG7dkKr1hB0rBrwQ7JFLrLCrRTTvXpED-wFxe8V8JZ1tJLg8kmxRNWBeyi',
            environment: RNPaypal.ENVIRONMENT.SANDBOX,
            intent: RNPaypal.INTENT.SALE,
            price: Number(one.amount),
            currency: 'USD',
            description: `Application Payment`,
            acceptCreditCards: true
            }).then(function (payment) {
             
              console.log('pymenttttt');
              console.log(payment);
              if(payment.response.id){
                Api("add_credit_payment", {
                  user_id: token,
                  cr_id:responseJson.response,
                  amount:one.amount
                })
            .then((responseJson) => {
             
            if(responseJson.status==1){
            console.log('22222');
            console.log(responseJson.message);
            Toast.show(responseJson.message);
            setTimeout(function(){
              that.props.navigation.navigate("MyCreditReport");
              
             }, 2000);
            }
            else if(responseJson==false){
              alert('No internet');
            }
            else{
            Alert.alert(responseJson.message);
            }
            }).catch((error) => {
            console.error(error);
            
            });
              }
             
            }).catch(err => {
             console.log(err.message);
             if(err.message=="User cancelled"){
              setTimeout(function(){
                that.props.navigation.navigate("MyCreditReport");
                
               }, 2000);
             }
            
            // alert("e"+err.message)
            })
      
      
        }
        this.setState({
          showProgress: false
        });
      } else {
        Alert.alert(responseJson.message);
        this.setState({
          showProgress: false
        });
      }
    })
    .catch(error => {
     alert("e"+error);
      this.setState({
        showProgress: false
      });
    });
    }
  
  else{
    console.log('image is not available');

    Alert.alert(
      '',
      'Please Select Image',
      [
       
        {text: 'OK', onPress: () => console.log('OK Pressed')},
      ],
      {cancelable: false},
    );
   
    // if(idUrlUpload != ''){
    //   Alert.alert('id proof upload failed!!');
    // }
    // else if(incomeUrlUpload != ''){
    //   Alert.alert('income proof upload failed!!');
    // }
    this.setState({
      showProgress: false
    });
  }
   
  };

}
back_Button_Press = () => {
 
  const { idxActive } = this.state;
  
    if (idxActive == 1) {
      this.refs.swiper.scrollBy(-1);
    }
 
}
componentWillUnmount(){
  BackHandler.removeEventListener('hardwareBackPress', this.back_Button_Press);
}
componentWillMount(){
  BackHandler.addEventListener('hardwareBackPress', this.back_Button_Press);
}
  render() {
    return (
      <View style={{ flex: 1 }}>
        <Toolbar title="Add Credit Report" />

        <Swiper
          style={styles.wrapper}
          renderPagination={renderPagination}
          showsButtons={false}
          showsPagination={false}
          scrollEnabled={false}
          loop={false}
          ref={"swiper"}
          dot={true}
          onIndexChanged={idxActive =>
            this.setState({ idxActive }, function() {
              //console.log('nextttt', this.state.idxActive);
            })
          }
        >
          {/* <View style={styles.slide}> */}
          <AddCreditReportOne ref="addReportOne" />
          {/* </View> */}
          <AddCreditReportTwo ref="addReportTwo" />
        </Swiper>

        {this.state.idxActive === 0 ? (
          <View style={styles.footer}>
            <View style={{ flexDirection: "row" }}>
              <View style={styles.indicator}></View>
              <View
                style={[
                  styles.indicator,
                  { backgroundColor: "white", borderWidth: 0.5 }
                ]}
              ></View>
            </View>
            <TouchableOpacity style={styles.nextBtn} onPress={this.onPressNext}>
              <Text style={styles.nextText}>Next</Text>
            </TouchableOpacity>
          </View>
        ) : (
          <View style={styles.footer}>
            <View style={{ flexDirection: "row", width: "22%" }}>
              <View style={styles.indicator}></View>
              <View style={styles.indicator}></View>
            </View>
            <TouchableOpacity
              style={[
                styles.nextBtn,
                {
                  backgroundColor: "white",
                  borderWidth: 0.5,
                  borderColor: "gray"
                }
              ]}
              onPress={this.onResetPress}
            >
              <Text style={[styles.nextText, { color: "black" }]}>Reset</Text>
            </TouchableOpacity>
            <TouchableOpacity
              style={styles.signUpBtn}
              onPress={this.onPressSave}
            >
              <Text
                style={{ fontSize: 13, fontWeight: "bold", color: "white" }}
              >
                Save & Make Payment
              </Text>
            </TouchableOpacity>
          </View>
        )}
           <ProgressDialog
          // title="Progress Dialog"
          activityIndicatorColor="red"
          activityIndicatorSize="large"
          animationType="fade"
          message="Please, wait..."
          visible={this.state.showProgress}
        />
      </View>
    );
  }
}

const styles = StyleSheet.create({
  wrapper: {},

  indicator: {
    height: 12,
    width: 12,
    borderRadius: 6,
    backgroundColor: "#008f2c",
    margin: wp("1%")
  },
  nextBtn: {
    height: hp("7%"),
    backgroundColor: "#034d94",
    width: wp("25%"),
    borderRadius: 10,
    justifyContent: "center",
    alignItems: "center",
    flexDirection: "row",
    paddingStart: wp("2%"),
    paddingEnd: wp("2%")
  },
  nextText: {
    fontSize: 13,
    fontWeight: "bold",
    color: "white"
  },
  resetText: {
    fontSize: 13,
    fontWeight: "bold",
    color: "white"
  },
  footer: {
    backgroundColor: "white",
    height: "11%",
    justifyContent: "space-between",
    flexDirection: "row",
    alignItems: "center",
    paddingStart: "7%",
    paddingEnd: "7%"
  },
  signUpBtn: {
    height: hp("7%"),
    backgroundColor: "#008f2c",
    width: "46%",
    marginStart: "4%",
    marginEnd: "2%",
    borderRadius: 10,
    justifyContent: "center",
    alignItems: "center"
  }
});
