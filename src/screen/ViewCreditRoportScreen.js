import React, { Component } from 'react';
import AsyncStorage from "@react-native-community/async-storage";
import Toolbar from '../component/Toolbar';
import {widthPercentageToDP as wp, heightPercentageToDP as hp} from 'react-native-responsive-screen';
import { Text, View, SafeAreaView, Image, TouchableOpacity, StyleSheet, Dimensions, Alert,
    Button, FlatList, TextInput, ScrollView } from 'react-native';
    import commonStyles from "../styles/index";
    import CheckBox from "react-native-check-box";
    import Api from "../Api/Api";
    import { ProgressDialog } from "react-native-simple-dialogs";
import { thisExpression } from '@babel/types';
export default class ViewCreditReportScreen extends Component {
    constructor(props) {
        super(props);
       this.state = {
            creaditDataHolder: [],
           Total:0,
          firstName:'',
          lastName:'',
          middleName:'',
          address:'',
          birthday:'', 
          socialSecurity:'',
           isDateTimePickerVisible: false,
           firstError:false,
           lastError:false,
           middleError:false,
           addressError:false,
           birthdayError:false,
           socialError:false,
           filePath:'',
           date:'',
           maxDate:'',
           showProgress:false
        };
     }
     collapseView(item, index) {
        let rowData = this.state.creaditDataHolder;
      
        rowData[index].isExpanded = !rowData[index].isExpanded;
    
        console.log(rowData);
        this.setState(
          {
            creaditDataHolder: rowData
          }
          // alert(JSON.stringify(this.state.creaditDataHolder))
        );
      }
      componentWillMount(){
        var id =this.props.navigation.getParam('id');
       
        this.setState({
          showProgress: true
        });
        Api("view_creadit_reports", {
          credit_report_id: id,
         
        })
          .then(responseJson => {
            this.setState({
              showProgress: false
            });
         
            if (responseJson.status == 1) {
              var checkboxArray=[];
              total=0
              responseJson.response.map(item=>{
                this.setState({
                  firstName:item.firstname,
                  lastName:item.lastname,
                  middleName:item.middlename,
                  address:item.address,
                  birthday:item.date_of_birth,
                  socialSecurity:item.ssn,


                })
               item.pckage_data.map(item=>{
                total = parseInt(total) + parseInt(item.price);
               checkboxArray.push({
                 id:item.id,
                 title:item.title,
                 time_duration:item.time_duration,
                 description:item.description,
                 price:item.price,
                 is_mandatory:item.is_mandatory,
                 isExpanded:false,
                 isDuplex:true
                })
                })
                this.setState({
                   creaditDataHolder:checkboxArray,
                   Total:total
                })
              })
        
         
            }else if(responseJson==false)
            {
              alert("No internet")
            }
             else {
              Alert.alert(responseJson.message);
              this.setState({
                showProgress: false
              });
            }
          })
          .catch(error => {
            console.error(error);
            this.setState({
              showProgress: false
            });
          });
      }
    render(){
        return(
             <SafeAreaView style={styles.container}>
            <View style={styles.container}>
                 {/* <ToolbarHomeImg title="Add Credit Report" />     */}
                 <View style={{height:'9%', backgroundColor:'white', paddingLeft:'7%', paddingRight:'7%',
         flexDirection:'row', justifyContent:'space-between', alignItems:'center'}}>
          <TouchableOpacity 
          
          >
          <Image source={require("../image/left.png")} style={{width:25,height:20 }} />
          </TouchableOpacity>
          <Text style={{textAlign:"center", color:'#034d94',  fontWeight:'bold', fontSize:18}}>View Credit Report</Text>
          <View style={{height:10, width:10, backgroundColor:'white'}}></View>
        </View>
                 <View style={styles.mainView}>
                
                 <ScrollView>
                  
                    
                    <View style={commonStyles.requiredField}>
                
                   <Text style={commonStyles.nameStyle}>First Name</Text>
                   </View>
                  <TextInput
                 style={styles.textInput}
                 underlineColorAndroid = "transparent"
                // placeholder="Enter First Name"
                 value={this.state.firstName}
                 />
              <View style={styles.textinputBorder}></View>
             
      
              <Text style={styles.nameStyle}>Middle Name</Text>
                  <TextInput
                style={styles.textInput}
                underlineColorAndroid = "transparent"
                placeholder="Enter Middle Name"
             
                value={this.state.middleName}
               
                
              />
              <View style={commonStyles.textinputBorder}></View>
      
               <View style={commonStyles.requiredField}>
                 
                   <Text style={commonStyles.nameStyle}>Last Name</Text>
                   </View>
                  <TextInput
                style={styles.textInput}
                 underlineColorAndroid = "transparent"
                placeholder="Enter Last Name"
               
                value={this.state.lastName}
             
              />
              <View style={styles.textinputBorder}></View>
            
      
              <View style={commonStyles.requiredField}>
                
                   <Text style={commonStyles.nameStyle}>Address</Text>
                   </View>
                   <View style={styles.textInputView}>
                  <TextInput
               style={styles.textInput}
                 underlineColorAndroid = "transparent"
                   style = {{ flex: 1, textAlignVertical:'top' }}
                  numberOfLines={3}
                placeholder="Address here"
              
                value={this.state.address}
              
              />
              </View>
              <View style={commonStyles.textinputBorder}></View>
             
              <View style={commonStyles.requiredField}>
                
                   <Text style={commonStyles.nameStyle}>Birthday</Text>
                   </View>
                   <View style={styles.birthdayView}>
                   <TextInput
               style={styles.textInput}
                 underlineColorAndroid = "transparent"
                   style = {{ flex: 1, textAlignVertical:'top' }}
                  numberOfLines={3}
                placeholder="Address here"
              
                value={this.state.birthday}
              
              />
                    {/* <DatePicker
              style={{width: 200}}
              date={this.state.date}
              mode="date"
              placeholder="select date"
              format="DD/MM/YYYY"
             // minDate="2016-05-01"
              maxDate={this.state.maxDate}
              confirmBtnText="Confirm"
              cancelBtnText="Cancel"
              hideText='true'
              iconSource= {require('../image/CalendarBlue.png')}
              customStyles={{
               
                dateInput: {
                  borderWidth: 0,
                  },
                  dateIcon: {
                    bottom:2
                  },
              }}
              onDateChange={(date) => this.setDate(date)}
            /> */}
                
              </View>
              <View style={commonStyles.textinputBorder}></View>
            
      
               <Text style={commonStyles.nameStyle}>Social Security Nunber</Text>
                  <TextInput
                style={styles.textInput}
                keyboardType="numeric"
                 underlineColorAndroid = "transparent"
                placeholder="Enter social Security number here"
             
                value={this.state.socialSecurity}
               
              />
              <View style={commonStyles.textinputBorder}></View> 
      
                {/* <Text style={commonStyles.nameStyle}>Upload State/Goverment ID</Text> */}
              
                <Text style={commonStyles.nameStyle}>Selected Packages</Text>
                <FlatList
                  data={this.state.creaditDataHolder}
            
                  extraData={this.state}
                  renderItem={({ item, index }) => 
                    <View style={{ paddingHorizontal: 13 }}>
                      <View
                        style={{
                          flex: 1,
                          flexDirection: "row",
                          borderBottomColor: "gray",
                          borderBottomWidth: 0.5
                        }}
                      >
                        <View style={{ flex: 0.1 }}>
                          <CheckBox
                            style={{ padding: 10 }}
                            checkBoxColor={"#034d94"}
                            checkedCheckBoxColor={"#034d94"}
                            disabled={true}
                            isChecked={item.isDuplex}
                          />
                        </View>
                        <View style={{ flex: 0.9 }}>
                          <Text
                            numberOfLines={item.isExpanded ? null : 2}
                            style={{
                              padding: 10,
                              color: "gray",
                              fontSize: 15,
                              lineHeight: 20
                            }}
                          >
                            {" "}
                            {item.title}{" "}
                          </Text>
                        </View>
                      </View>
                      <View style={{ flexDirection: "row" }}>
                        <View style={{ flex: 0.1 }}></View>
                        <View
                          style={{
                            justifyContent: "space-between",
                            flexDirection: "row",
                            flex: 1
                          }}
                        >
                          <TouchableOpacity
                            onPress={() => this.collapseView(item, index)}
                          >
                            {item.isExpanded ? (
                              <Text
                                style={{
                                  fontWeight: "bold",
                                  color: "#034d94",
                                  fontSize: 16,
                                  padding: 12
                                }}
                              >
                                Show less
                              </Text>
                            ) : (
                              <Text
                                style={{
                                  fontWeight: "bold",
                                  color: "#034d94",
                                  fontSize: 16,
                                  padding: 12
                                }}
                              >
                                Show more
                              </Text>
                            )}
                          </TouchableOpacity>
                          <Text
                            style={{ padding: 10, lineHeight: 20 }}
                           
                          >
                            {" "}
                            {"$" + item.price}{" "}
                          </Text>
                        </View>
                      </View>
                    </View>
                  }
                />
                <View
                style={{
                  justifyContent: "space-between",
                  flexDirection: "row",
                  flex: 1,paddingHorizontal:10
                }}
              >
                <Text style={{ fontWeight: "bold", fontSize: 16, padding: 12 }}>
                  Total
                </Text>
                <Text
                  style={{
                    fontWeight: "bold",
                    fontSize: 20,
                    color: "#034d94",
                    padding: 10
                  }}
                >
                  ${this.state.Total}
                </Text>
              </View>
                  </ScrollView>
                 </View>
                </View>
                <ProgressDialog
            // title="Progress Dialog"
            activityIndicatorColor="red"
            activityIndicatorSize="large"
            animationType="fade"
            message="Please, wait..."
            visible={this.state.showProgress}
          />
                 </SafeAreaView>
        )
    }
}
const styles = StyleSheet.create({
    container: {
        flex: 1,
       backgroundColor: "#ededed",
        paddingBottom:'4%',
      },
      mainView:{
        backgroundColor:'white',
      // flex:1,
       height:'100%',
       margin:'5%', 
      borderRadius:10,
      },
      nameStyle:{
           marginTop:hp('5%'), 
           marginStart:wp('10%')
     },
     nameStyleWithDot:{
         marginTop:hp('2%'), 
         marginStart:wp('2%'),
     },
     textInput:{
        marginStart:wp('10%'), 
         marginTop:-hp('1.3%'),
         //backgroundColor:'yellow', 
        // height:hp('5%')
     },
     textinputBorder:{
       marginStart:wp('5%'), 
       marginEnd:wp('5%'), 
        backgroundColor:'gray',
         height:1,
          width:'85%',
          marginTop:-hp('1%')
          },
          calenderIconView:{
            width:wp('20%'), 
          marginTop:-hp('1%'),
           justifyContent:'center',
           alignItems:'center'
          },
           calenderImg:{
             height:hp('3%'),
           width:hp('3%')
          },
          birthdayView:{ 
            flexDirection:'row', 
            height:hp('6%'),
           marginStart:wp('10%'),
            justifyContent:'space-between',
            alignItems:'center',
            marginEnd:wp('5%')
           },
           indicator:{ 
             height:12,
              width:12,
              borderRadius:6,
              backgroundColor:'#008f2c',
             margin:wp('1%'),
           },
           nextBtn:{
             height:hp('7%'),
           backgroundColor:'#034d94',
           width:wp('25%'),
          borderRadius:10,
         justifyContent:'center',
           alignItems:'center',
           flexDirection:'row', 
           paddingStart:wp('2%'),
            paddingEnd:wp('2%')
           },
           nextText:{
             fontSize:13,
              fontWeight:'bold',
               color:'white'
               },
           uploadView:{
             height:hp('8%'),
              marginStart:'8%',
               marginEnd:'8%',
               marginTop:hp('1%'),
                borderRadius:10,
                 borderStyle: 'dotted',
                 borderWidth: 1,
                  borderColor:'gray',
                   flex:1,
                    flexDirection:'row', 
                    alignItems:'center', 
                    justifyContent:'space-around'
              },
              footer:{
                backgroundColor:'white',
                 height:'11%',
                  justifyContent:'space-between', 
                  flexDirection:'row',
                alignItems:'center',
                paddingStart:'7%',
               paddingEnd:'7%'
           },
           textInputView:{
            height:hp('13%'),
             marginStart:'10%',
              marginEnd:'10%',
              justifyContent: 'flex-start',
         },
    });
    