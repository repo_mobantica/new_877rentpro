import React, { Component } from 'react';
import { Text, View, SafeAreaView, Image, TouchableOpacity, StyleSheet, Dimensions, Alert,
Button, FlatList } from 'react-native';
import {widthPercentageToDP as wp, heightPercentageToDP as hp} from 'react-native-responsive-screen';
import ToolbarWithLeftIcon from '../component/ToolbarWithLeftIcon';
import AsyncStorage from '@react-native-community/async-storage';
import RNPaypal from 'react-native-paypal-lib';
import Toast from 'react-native-simple-toast';
import Api from "../Api/Api";
import { 
  ProgressDialog, 
} from "react-native-simple-dialogs";
var token='';
const monthNames = [" ", "January", "February", "March", "April", "May", "June",
  "July", "August", "September", "October", "November", "December"
];
export default class OutwardPendingPayment extends Component {
  state = {
    flatlistData:[],
    date:[],
    showProgress: false
  }
    
  async componentDidMount(){
    token = await AsyncStorage.getItem('Token');
    console.log( 'token', token);
   this.fetchData();
}

  fetchData(){
    this.setState({
      showProgress: true,
    });
    Api("outward_pending_payments", {
      userid: token
    })
      .then((responseJson) => {
        this.setState({
          showProgress: false
        });
    if(responseJson.status==1){
      console.log('22222');
      let dataArray =  responseJson.response;
      var date = dataArray.map(function(item){
        if(item.due_date != null){
         let splitDate = item.due_date.split('-');
         return splitDate;
        }
        else{
         return null;
        }
      })
      this.setState({
        flatlistData: responseJson.response,
        date:date,
      })
      }
      else if(responseJson==false){
        alert('No internet');
      }
    else{
      Alert.alert(responseJson.message);
    }
  }).catch((error) => {
  console.error(error);
  this.setState({
  showProgress: false
  });
  });
  }

    
  paymentGateway(item){
   
      RNPaypal.paymentRequest({
        clientId: 'ASV62g-yJsjYEKkbd6I-e2OG7dkKr1hB0rBrwQ7JFLrLCrRTTvXpED-wFxe8V8JZ1tJLg8kmxRNWBeyi',
        environment: RNPaypal.ENVIRONMENT.SANDBOX,
        intent: RNPaypal.INTENT.SALE,
        price: Number(item.amount),
        currency: 'USD',
        description: `Package Payment`,
        acceptCreditCards: true
        }).then(function (payment) {
          //sendPaymentToConfirmInServer(payment, confirm);
          console.log('pymenttttt');
          console.log(payment);
          if(payment.response.id){
            Api("add_payment_data", {
              user_id: token,
              propertyid:  item.id,
              amount:item.amount,
              totalamount: item.amount,
              payment_type: item.payment_type,
              payment_date: new Date().getFullYear()+'-'+("0"+(new Date().getMonth()+1)).slice(-2)+'-'+("0"+new Date().getDate()).slice(-2),
              pay_type_id: item.type
            })
        .then((responseJson) => {
         
        if(responseJson.status==1){
        console.log('22222');
        console.log(responseJson.message);
        Toast.show(responseJson.message);
        }
        else if(responseJson==false){
          alert('No internet');
        }
        else{
        Alert.alert(responseJson.message);
        }
        }).catch((error) => {
        console.error(error);
        
        });
          }
         
        }).catch(err => {
         console.log(err.message);
        // alert("e"+err.message)
        })
    
  }

  _renderItem = ({item, index}) => (
    <View style={styles.flatListView}>
   <Image
          style={{ borderTopLeftRadius: 11, 
        borderTopRightRadius: 11,width: '100%', height: hp('10%')}}
        source={{uri: item.image_url}}
       />
        <View style={styles.firstRow}>
    <Text style={{color:'#034d94', fontWeight:'bold'}}>${item.amount}</Text>
    
    <Image
          style={{width: 21, height:21, }}
          source={require('../image/eye.png')}
       />
    </View>
     <Text style={{color:'black', marginStart:wp('3.7%'), fontWeight:'bold',}}>{item.property_address}</Text>
     <View style={styles.lastRow}>
     <Image
          style={{width: 11, height:11}}
          source={require('../image/calendar.png')}
       />
    
     {this.state.date[index] != null ?
    <Text style={styles.dateText}> {monthNames[Number(this.state.date[index][1])]} {Number(this.state.date[index][2])} {Number(this.state.date[index][0])}</Text>
    :
    <Text style={styles.dateText}> Due date </Text>
        }
     </View>
     <TouchableOpacity style={{justifyContent:'center', alignItems:'center', height:hp('6.5%'),
      backgroundColor:'#008f2c', borderBottomLeftRadius:11, borderBottomRightRadius:11}}
      onPress={()=> this.paymentGateway(item)}>
     <Text style={{color:'white'}}> $ MAKE PAYMENT</Text>
     </TouchableOpacity>
    </View>
  );
  render() {
    
    return (
       <SafeAreaView style={styles.container}>
      <View style={styles.container}>
       <ToolbarWithLeftIcon title="Outward Pending Payment" />
            {/* <View style={{marginTop:hp('9%')}}> */}
            <FlatList
        data={this.state.flatlistData}
        extraData={this.state}
       keyExtractor={(item, index) => index.toString()}
        renderItem={this._renderItem}
      />
       <ProgressDialog
          // title="Progress Dialog"
          activityIndicatorColor="red"
          activityIndicatorSize="large"
          animationType="fade"
          message="Please, wait..."
          visible={this.state.showProgress}
        />
{/* </View> */}
          </View>
           </SafeAreaView>
    );
  }
}

const styles = StyleSheet.create({
container: {
    flex: 1,
   backgroundColor: "#ededed"
  },
  menuIcon: {
    height: "40%",
    resizeMode: "contain",
    width: 30,
    marginStart: 5
  },
  dateText:{
    color:'#939393',
     marginStart:wp('1.6%'),
      fontSize:12, width:'72%'
  },
   flatListView:{
     height:hp('29%'),
     margin:hp('3%'),
     marginTop:hp('3%'),
     marginBottom:hp('0%'),
     borderRadius:13,
     backgroundColor:'white',
       
  },
  firstRow:{
    flexDirection:'row', 
   marginStart:wp('3.7%'),
   marginTop:wp('3.7%'),
    marginEnd:wp('3.7%'),
    justifyContent: "space-between",
    },
    lastRow:{
      flexDirection:'row', 
   marginStart:wp('3.7%'),
   marginTop:-6,
    marginEnd:wp('3.7%'),
   alignItems:'center',
   flex:1
    },
    
});