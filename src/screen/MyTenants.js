import React, { Component } from "react";
import {
  Text,
  View,
  SafeAreaView,
  Image,
  TouchableOpacity,
  StyleSheet,
  Dimensions,
  Alert,
  Button,
  FlatList,Linking,
} from "react-native";
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp
} from "react-native-responsive-screen";
import ToolbarWithLeftIcon from "../component/ToolbarWithLeftIcon";
import { ProgressDialog } from "react-native-simple-dialogs";
import AsyncStorage from '@react-native-community/async-storage';
import Communications from 'react-native-communications';
var token ='';

export default class MyTenants extends Component {
  constructor(props) {
    super(props);
    this.state = {
      dataSource: "",
      showProgress: false
    };
  }
    async componentWillMount() {
    token = await AsyncStorage.getItem('Token');
    console.log('tokennnnn', token);
    Property_id = this.props.navigation.getParam("Property_id");
    this.fetchData();
  }

  fetchData() {
    this.setState({
      showProgress: true
    });
    fetch(
      "https://1800rentpro.com/rentpro_api/api/my_tenants_list",
      {
        method: "POST",
        headers: {
          Accept: "application/json",
          "Content-Type": "application/json"
        },
        body: JSON.stringify({
          userid: token
        })
      }
    )
      .then(response => response.json())
      .then(responseJson => {
        console.log("responseJsonfgdfgh", responseJson.response);
        console.log("responseJson", responseJson.message);
        this.setState({
          dataSource: responseJson.response.data
        });
        
        if (responseJson.status == 1) {
          console.log("22222");
          console.log(responseJson.message);

          this.setState({
            showProgress: false
          });
        } else {
          Alert.alert(responseJson.message);
          this.setState({
            showProgress: false
          });
        }
      })
      .catch(error => {
        console.error(error);
        this.setState({
          showProgress: false
        });
      });
  }



  _renderItem = ({ item }) => (
    <View style={styles.flatListView}>
      <Image
        style={styles.displayImage}
        // source={require('../image/room.png')}
        source={{ uri: item.image_url }}
      />

      <Text style={styles.userName}>{item.firstname} {item.lastname}</Text>

      <Text style={styles.address}>{item.address}</Text>
      <View style={styles.dateRow}>
        <Image style={styles.icon} source={require("../image/calendar.png")} />
        <Text style={styles.dateText}>
          Moved in From, {""}
          {item.tenant_move_in_date}{" "}
        </Text>
      </View>

      <View style={styles.lastRow}>
        <TouchableOpacity style={styles.phoneView} 
        onPress={() => Communications.phonecall(item.mobileno, true)}>
          <Image style={styles.icon} source={require("../image/Phone.png")} />
          <Text style={{ color: "white" }}> Phone</Text>
    </TouchableOpacity>
        <TouchableOpacity style={styles.middleView}
        onPress={() => Communications.
          email([item.email],null,null,'My Subject','My body text')}>
       
       
          <Image style={styles.icon} source={require("../image/Email.png")} />
          <Text style={{ color: "white" }}> Email</Text>
          </TouchableOpacity>
        
        <TouchableOpacity style={styles.phoneView} 
        onPress={() => Communications.text(item.mobileno, true)}>
          <Image style={styles.icon} source={require("../image/Message.png")} />
          <Text style={{ color: "white" }}> Message</Text>
          </TouchableOpacity>
      </View>
    </View>
  );
  render() {
    return (
      <SafeAreaView style={[styles.container]}>
        <View style={styles.container}>
          <ToolbarWithLeftIcon title="Applicants" />

          <FlatList
            data={this.state.dataSource}
            extraData={this.state}
            keyExtractor={(item, index) => index.toString()}
            renderItem={this._renderItem}
          />
          <View style={{ height: hp("1%"), width: "100%" }}></View>
          <ProgressDialog
            // title="Progress Dialog"
            activityIndicatorColor="red"
            activityIndicatorSize="large"
            animationType="fade"
            message="Please, wait..."
            visible={this.state.showProgress}
          />
        </View>
      </SafeAreaView>
    );
  }
}
const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "#ededed"
  },
  flatListView: {
    height: hp("30%"),
    margin: hp("3%"),
    marginTop: hp("3%"),
    marginBottom: hp("0%"),
    borderRadius: 11,
    backgroundColor: "white"
  },
  dateRow: {
    flexDirection: "row",
    marginStart: wp("3.7%"),
    height: hp("4"),
    // backgroundColor:'red',
    alignItems: "center",
    marginBottom: hp("1%")
  },
  lastRow: {
    flexDirection: "row",
    borderBottomRightRadius: 11,
    backgroundColor: "#034d94",
    alignItems: "center",
    flex: 1,
    height: hp("6%"),
    borderBottomLeftRadius: 11
  },
  displayImage: {
    width: "100%",
    height: hp("10%"),
    borderTopLeftRadius: 11,
    borderTopRightRadius: 11
  },
  userName: {
    color: "#034d94",
    marginStart: wp("3.7%"),
    marginTop: wp("3.7%"),
    fontWeight: "bold"
  },
  address: {
    color: "black",
    marginStart: wp("3.7%"),
    fontWeight: "bold"
  },
  phoneView: {
    flexDirection: "row",
    width: "33%",
    height: "100%",
    justifyContent: "center",
    alignItems: "center"
  },
  middleView: {
    flexDirection: "row",
    width: "33%",
    height: "100%",
    borderLeftWidth: 1,
    borderColor: "white",
    borderRightWidth: 1,
    justifyContent: "center",
    alignItems: "center"
  },
  icon: {
    height: hp("3%"),
    width: hp("3%"),
    marginEnd: wp("2%")
  },
  dateText: {
    marginStart: wp("2%"),
    color: "gray",
    fontSize: 12
  }
});
