import React, { Component } from "react";
import {
  Text,
  View,
  SafeAreaView,
  Image,
  TouchableOpacity,
  StyleSheet,
  Dimensions,
  Alert,
  AsyncStorage,
  Button,
  FlatList,
  TextInput,
  ScrollView
} from "react-native";
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp
} from "react-native-responsive-screen";
import CheckBox from "react-native-check-box";
import RadioForm, {
  RadioButton,
  RadioButtonInput,
  RadioButtonLabel
} from "react-native-simple-radio-button";
import ToolBarForProfile from "../component/ToolBarForProfile";
import commonStyles from "../styles/index";
import { ProgressDialog } from "react-native-simple-dialogs";
var radio_props = [
  { label: "Landlord", value: 0 },
  { label: "Tenant", value: 1 }
];
export default class PendingDetails extends Component {
  constructor(props) {
    super(props);
    this.params = this.props.navigation.state.params;
    this.state = {
      data: "",
      showProgress: false,
    };
  }

  async componentDidMount() {
    this.paymentDetails();
  }

  paymentDetails = () => {
    this.setState({
      showProgress: true
    });
    fetch("https://1800rentpro.com/rentpro_api/api/view_payment_details", {
      method: "POST",
      headers: {
        Accept: "application/json",
        "Content-Type": "application/json"
      },
      body: JSON.stringify({
        payment_id: this.params.id
      })
    })
      .then(response => response.json())
      .then(responseJson => {
       // console.log("userData", responseJson.response.data.userData.firstname);

        this.setState({
          data: responseJson.response.data,
          firstname: responseJson.response.data.userData.firstname,
          lastname: responseJson.response.data.userData.lastname,
          email: responseJson.response.data.userData.email,
          usertype: responseJson.response.data.userData.usertype
        });
      
        // console.log(
        //   "firstnamefirstnamefirstname",
        //   this.state.data.userData.firstname
        // );

        if (responseJson.status == 1) {
          this.setState({
            showProgress: false
          });
          console.log("22222");
          console.log(responseJson.message);
         /// Toast.show(responseJson.message);
          this.setState({
            showProgress: false
          });
        } else {
          Alert.alert(responseJson.message);
          this.setState({
            showProgress: false
          });
        }
      })
      .catch(error => {
        alert("e" + error);
        console.log("error", error);
        this.setState({
          showProgress: false
        });
      });
  };

  render() {
   
    return (
      <SafeAreaView style={styles.container}>
        <View style={styles.container}>
          <View style={styles.mainView}>
            <ScrollView>
            <View style={{ height: hp("1%"), width: "100%" }}></View>
          <ProgressDialog
            // title="Progress Dialog"
            activityIndicatorColor="red"
            activityIndicatorSize="large"
            animationType="fade"
            message="Please, wait..."
            visible={this.state.showProgress}
          />
              <View style={{ paddingStart: "7%", paddingEnd: "7%" }}>
                <View style={styles.firstRow}>
                  <Text style={styles.totalTextStyle}> Total</Text>
                  <View style={styles.topViewStyle}>
                    <Text style={styles.usdTextStyle}>{this.state.data.amount} USD</Text>
                  </View>
                </View>
                <Text style={[commonStyles.textTitle, { marginTop: hp("5%") }]}>
                  Payment ID
                </Text>
                <Text style={commonStyles.valueStyle}>
                  {" "}
                  {this.state.data.id}
                </Text>
                <View style={commonStyles.profileTextBorder}></View>

                <Text style={commonStyles.textTitle}>UserName</Text>
                <Text style={commonStyles.valueStyle}>
                  {this.state.firstname}
                  {" "}
                  {this.state.lastname}
                </Text>
                <View style={commonStyles.profileTextBorder}></View>

                <Text style={commonStyles.textTitle}>Email</Text>
                <Text style={commonStyles.valueStyle}> {this.state.email}</Text>
                <View style={commonStyles.profileTextBorder}></View>

                <Text style={commonStyles.textTitle}>Property ID</Text>
                <Text style={commonStyles.valueStyle}>
                  {" "}
                  {this.state.data.propertyid}
                </Text>
                <View style={commonStyles.profileTextBorder}></View>

                <Text style={commonStyles.textTitle}>User Type</Text>
                <Text style={commonStyles.valueStyle}> {this.state.usertype}</Text>
                <View style={commonStyles.profileTextBorder}></View>

                <Text style={commonStyles.textTitle}>Amount</Text>
                <Text style={commonStyles.valueStyle}>
                  {" "}
                  {this.state.data.amount}
                </Text>
                <View style={commonStyles.profileTextBorder}></View>

                <Text style={commonStyles.textTitle}>Payment Type</Text>
                <Text style={commonStyles.valueStyle}>
                  {this.state.data.pay_type}
                </Text>
                <View style={commonStyles.profileTextBorder}></View>

                <Text style={commonStyles.textTitle}>Status</Text>
                <Text style={commonStyles.valueStyle}> {this.state.status=='1'?'Incompleted':'Completed'}</Text>
                <View style={commonStyles.profileTextBorder}></View>
              </View>
            </ScrollView>
          </View>
        </View>
      </SafeAreaView>
    );
  }
}
const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "#ededed"
  },
  mainView: {
    height: "100%",
    width: "90%",
    margin: wp("5%"),
    backgroundColor: "white",
    borderRadius: 10,
    flex: 1
  },
  topViewStyle: {
    width: "30%",
    backgroundColor: "#5bc0de",
    height: "75%",
    borderRadius: 4,
    alignItems: "center",
    justifyContent: "center"
  },
  firstRow: {
    flexDirection: "row",
    height: "7%",
    width: "100%",
    alignItems: "center",
    justifyContent: "space-between",
    marginTop: hp("2%")
  },
  totalTextStyle: {
    fontSize: 20,
    fontWeight: "bold"
  },
  usdTextStyle: {
    color: "white",
    fontWeight: "bold"
  }
});
