import React, { Component } from 'react';
import { Text, View, SafeAreaView, Image, TouchableOpacity, StyleSheet, Dimensions, Alert,
Button, FlatList } from 'react-native';
import {widthPercentageToDP as wp, heightPercentageToDP as hp} from 'react-native-responsive-screen';
 import ToolbarHomeImg from '../component/ToolbarHomeImg';
 import AsyncStorage from '@react-native-community/async-storage';
 import Api from "../Api/Api";
 import RNPaypal from 'react-native-paypal-lib';
 import Toast from 'react-native-simple-toast';
 import { 
  ProgressDialog, 
} from "react-native-simple-dialogs";
 var token ='';
 var resultData;
export default class PendingPayment extends Component {
  state = {
    dialogVisible: false,
    flatlistData:[],
    showProgress:false
  };

  showDialog = () => {
    this.setState({ dialogVisible: true });
  };
 
  handleCancel = () => {
    this.setState({ dialogVisible: false });
  };
 
  handleDelete = () => {
    // The user has pressed the "Delete" button, so here you can do your own logic.
    // ...Your logic
    this.setState({ dialogVisible: false });
  };
  async componentDidMount(){
      token = await AsyncStorage.getItem('Token');
      console.log( 'token', token);
     this.fetchData();
  }

         paymentGateway(item){
          RNPaypal.paymentRequest({
            clientId: 'ASV62g-yJsjYEKkbd6I-e2OG7dkKr1hB0rBrwQ7JFLrLCrRTTvXpED-wFxe8V8JZ1tJLg8kmxRNWBeyi',
            environment: RNPaypal.ENVIRONMENT.SANDBOX,
            intent: RNPaypal.INTENT.SALE,
            price: Number(item.amount),
            currency: 'USD',
            description: `Application Payment`,
            acceptCreditCards: true
            }).then(function (payment) {
              //sendPaymentToConfirmInServer(payment, confirm);
              console.log('pymenttttt');
              console.log(payment);
              if(payment.response.id){
                Api("add_payment_data", {
                  user_id: token,
                  propertyid: item.id,
                  amount: item.amount,
                  totalamount: item.totalamount,
                  payment_type: "application",
                  payment_date: new Date().getFullYear()+'-'+("0"+(new Date().getMonth()+1)).slice(-2)+'-'+("0"+new Date().getDate()).slice(-2),
                  pay_type_id: item.type
                })
            .then((responseJson) => {
             
            if(responseJson.status==1){
            console.log('22222');
            console.log(responseJson.message);
            Toast.show(responseJson.message);
            }
            else if(responseJson==false){
              alert('No internet');
            }
            else{
            Alert.alert(responseJson.message);
            }
            }).catch((error) => {
            console.error(error);
            
            });
              }
             
            }).catch(err => {
             console.log(err.message);
            // alert("e"+err.message)
            })
       }

  fetchData(){
    this.setState({
      showProgress: true
    });
    Api("tenent_pending_payment_list", {
      userid:token
    })
      .then((responseJson) => {
        this.setState({
          showProgress: false
        });
    if(responseJson.status==1){
      // console.log('22222');
      // console.log(responseJson.response);
      this.setState({
        flatlistData: responseJson.response.data
      })
      }
      else if(responseJson==false)
      {
        alert("No internet");
      }
    else{
      Alert.alert(responseJson.message);
    }
  }).catch((error) => {
console.error(error);
this.setState({
  showProgress: false
});
});
  }
  _renderItem = ({item, index}) => {
   return(
    <View style={styles.flatListView}>
   <Image
          style={styles.displayImg}
          source={{uri: item.image_url}}
       />
        <View style={styles.firstRow}>
    <Text style={styles.priceText}>{item.amount}</Text>
    <TouchableOpacity 
    onPress={()=>this.props.navigation.navigate('PendingDetails')}>
    <Image
          style={styles.eyeIcon}
          source={require('../image/eye.png')}
       />
       </TouchableOpacity>
    </View>
     <Text style={styles.addressText}>{item.property_address}</Text>
     <View style={styles.lastRow}>
     <Image
          style={styles.calenderImg}
          source={require('../image/calendar.png')}
       />
     <Text style={styles.dueDateText}>Due on: Today</Text>
      <View style={styles.rentView}> 
      <Text style={{fontSize:10}}>RENT</Text>
     </View>
     </View>
     <TouchableOpacity style={styles.makePayment}
     onPress={()=> this.paymentGateway(item)}>
     <Text style={{color:'white'}}> $ MAKE PAYMENT</Text>
     </TouchableOpacity>
    </View>
  );
   }
  render() {
    
    return (
       <SafeAreaView style={styles.container}>
      <View style={styles.container}>
      <ToolbarHomeImg title="Pending Payment" />
            
            <FlatList
        data={this.state.flatlistData}
       extraData={this.state}
       keyExtractor={(item, index) => index.toString()}
        renderItem={this._renderItem}
      />
        <View style ={{height:hp('3%'), width:'100%'}}></View>
        <ProgressDialog
          activityIndicatorColor="red"
          activityIndicatorSize="large"
          animationType="fade"
          message="Please, wait..."
          visible={this.state.showProgress}
        />
          </View>
           </SafeAreaView>
    );
  }
}
const styles = StyleSheet.create({
container: {
    flex: 1,
   backgroundColor: "#ededed"
  },
  menuIcon: {
    height: "40%",
    resizeMode: "contain",
    width: 30,
    marginStart: 5
  },
   flatListView:{
     height:hp('29%'),
     margin:hp('3%'),
     marginTop:hp('3%'),
     marginBottom:hp('0%'),
     borderRadius:13,
     backgroundColor:'white',
       
  },
  firstRow:{
    flexDirection:'row', 
   marginStart:'5%',
   marginTop:wp('3.7%'),
    marginEnd:'5%',
    justifyContent: "space-between",
    },
    lastRow:{
      flexDirection:'row', 
   marginStart:'5%',
   marginTop:-6,
    marginEnd:'5%',
   alignItems:'center',
   flex:1
    },
    makePayment:{
      justifyContent:'center',
       alignItems:'center',
        height:hp('6.5%'),
      backgroundColor:'#008f2c',
       borderBottomLeftRadius:11,
        borderBottomRightRadius:11
    },
    rentView:{
      justifyContent:'center',
       alignItems:'center',
        backgroundColor:'#e7e7e7',
         borderRadius:15,
          height:hp('3%'), 
          borderColor:'black',
   borderWidth:1,
    marginStart:'45%',
     width:65
    },
    dueDateText:{
      color:'gray',
       marginStart:wp('1.6%'),
        fontSize:12 
    },
    calenderImg:{
      width: 11, height:11
    },
    addressText:{
      color:'black',
       marginStart:'5%', 
       fontWeight:'bold',
    },
    displayImg:{
       borderTopLeftRadius: 11, 
        borderTopRightRadius: 11,
        width: '100%',
         height: hp('10%')
    },
    eyeIcon:{
      width: hp('4%'), height:hp('4%'), 
    },
    priceText:{
      color:'#034d94', fontWeight:'bold'
    }
});