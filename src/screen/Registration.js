import React, { Component } from 'react';
import { Text, View, SafeAreaView, Image, TouchableOpacity, StyleSheet, Dimensions, Alert,
Button, FlatList, TextInput, ScrollView } from 'react-native';
import {widthPercentageToDP as wp, heightPercentageToDP as hp} from 'react-native-responsive-screen';

import ToolbarWithLeftIcon from '../component/ToolbarWithLeftIcon';

import DateTimePicker from "react-native-modal-datetime-picker";
import commonStyles from "../styles/index";
import RadioForm, {RadioButton, RadioButtonInput, RadioButtonLabel} from 'react-native-simple-radio-button';


import Modal from "react-native-modal";
import RNPicker from "rn-modal-picker";

var radio_props = [
  {label: 'Landlord', value: 2 },
  {label: 'Tenant', value: 3 }
];

export default class Registration extends Component {
     
      constructor(props) {
          super(props);
         this.state = {
         firstName:'',
      lastName:'',
      middleName:'',
      email:'',
      password:'',
      confirmPass:'',
      income:'',
      Birthday:'',
      birthToSend:'',
      mobileNo:'',
      selectedState:'',
      cityId:'',
      stateId:'',
      countryId:'',
      newPass:'',
      userId:2,
      isDateTimePickerVisible: false,
       value: 0,
        firstError:false,
       emailError:false,
       passError:false,
       confirmPassError:false,
       lastError:false,
       countryError:false,
       stateError:false,
       cityError:false,
      ErrorStatus:false,
      birtthdayError:false,
      incomeError:false,
      mobileError:false,
      userIdError:false,
      passError:false,
      confPassError:false,
      validAge:false,
      countryList:[],
      stateList:[],
      cityList:[],
      isModalVisible: false,
      countryPhnoeCode:'',
      placeHolderText: "Please Select Country",
      selectedCountry: "",
      selectedState:'',
      selectedCity:'',
       phoneCode:'',
    };
    
  }
  _selectedCity(id, name, value) {
    console.log('plzz set city');
            console.log('city id', id);
            console.log('city name', name);
            console.log('city value', value);
    this.setState({ selectedCity: name, stateError:false, cityId:value });
  }
 componentWillMount(){
  this.getData();
 }
  getData(){
  var data =  this.props.navigation.getParam('countryList');
 

   this.setState({
     countryList: data,
     firstName:this.props.navigation.getParam('first_name'),
     lastName:this.props.navigation.getParam('last_name'),
     middleName:this.props.navigation.getParam('middle_name'),
     email:this.props.navigation.getParam('email')
   }, function(){
        console.log('this countryList ');
     console.log(this.state.countryList);
      })
  }
  showDateTimePicker = () => {
    this.setState({ isDateTimePickerVisible: true });
  };
 
  hideDateTimePicker = () => {
    this.setState({ isDateTimePickerVisible: false });
  };

  toggleModal(){
    this.setState({ isModalVisible: !this.state.isModalVisible });
  };

  closeModel(){
    // this.setState({ 
    //   isModalVisible: !this.state.isModalVisible 
    // }, function(){
    //   this.props.navigation.navigate('Login');
    // });
    this.props.navigation.navigate('Login');
  };
 
  handleDatePicked = date => {
     let day =date.getDate();
    let month =date.getMonth() +1;
    let year =date.getFullYear();
     let fullDate = day + '/' +month +'/' +year;
     let birthToSend = year + '-' +month+ '-'+ day;
     
    this.setState({ Birthday: fullDate,
      birthToSend:birthToSend,
        birtthdayError:false });
    this.hideDateTimePicker();
     
    let age = 18;
    var setDate = new Date(year + age, month - 1, day);
    var currdate = new Date();
    
    if (currdate >= setDate) {
      // you are above 18
     // alert("above 18");
      this.setState({
        validAge: false
      })
    } else {
      this.setState({
        validAge: true
      })
    }

  };

  namevalidate(text) {    
    const reg =  /^[A-Za-z\b]+$/;
     if(text!=''){
       if (reg.test(text) === false) {
         return false;
       }
       else {
       this.setState({firstName:text,
         firstError:false})
       }
       }
       else{
       this.setState({firstName:'',
      firstError:true})
       }
    }
  
    lastNamevalidate(text) {    
      const reg =  /^[A-Za-z\b]+$/;
      //const passReg = /^(?=.*\d)(?=.*[a-z])(?=.*[A-Z])[0-9a-zA-Z]{8,}$/;
       if(text!=''){
         if (reg.test(text) === false) {
           return false;
         }
         else {
         this.setState({lastName:text,
        lastError:false})
         }
         }
         else{
         this.setState({lastName:'',
        lastError:true})
         }
      }
  
      mobilevalidate = (text3) => {
        const reg = /^[0-9\b]+$/;
        
        if(text3!=''){
        if (reg.test(text3) === false) {
          return false;
        } 
        else {
        this.setState({mobileNo:text3,
        mobileError:false})
        }
        }
        else{
        this.setState({mobileNo:'',
      mobileError:true})
        }
      }
    
      getCountry(){
        db.transaction(tx => {
          tx.executeSql('SELECT * FROM table_country', [], (tx, results) => {
            var tempCountry = [];
            for (let i = 0; i < results.rows.length; ++i) {
              tempCountry.push(results.rows.item(i));
            }

            var countryList = tempCountry.map(item =>  ({id:item.country_phonecode, name:item.country_name }) );
            console.log('this countryList ');
            console.log(countryList);
            this.setState({
              countryList: countryList,
            });
          });
        });
      }

      getState(id, name, value){
        var countryId = id +1;
        console.log('plzz set country');
            console.log('id', countryId);
            console.log('name', name);
            console.log('value', value);
           
        this.setState({
          selectedCountry: name,
          phoneCode:value,
          selectedCity:'',
          selectedState:'',
          countryId:value,
          countryError:false},function(){
            // console.log('plzz set country');
            // console.log(index+1, '   ', name);
          }); 
          var data =  this.props.navigation.getParam('state_list');
         
        //  db.transaction(tx => {
        //   tx.executeSql('SELECT * FROM table_state  WHERE state_country_id =?', [value], (tx, results) => {
        //     var tempState = [];
        //     for (let i = 0; i < results.rows.length; ++i) {
        //       tempState.push(results.rows.item(i));
        //     }
    
        var filterArray = data.filter(
          data => data.country_id === value,
        );
  
          var stateList = filterArray.map(item =>  ( item ) );
           
            this.setState({
              stateList: stateList,
            });
           
         // });
        // });
      }
     
      getCity(id, name, value){
       //alert(' state id', id+1,"\n",name);
            console.log('state name', name);
            console.log('state value', value);
        this.setState({
          selectedState: name,
          selectedCity:'',
          stateError:false,
          stateId:value
        },function(){
          // console.log('plzz set state');
          // console.log(id, '   ', name);
        })
        var data =  this.props.navigation.getParam('city_list');
         
        // db.transaction(tx => {
        //   tx.executeSql('SELECT * FROM table_city WHERE city_state_id=?', [value], (tx, results) => {
        //     var tempCity = [];
        //     for (let i = 0; i < results.rows.length; ++i) {
        //       tempCity.push(results.rows.item(i));
        //     }
       
        var filterArray = data.filter(
          data => data.state_id === value,
        );
    
      
          var cityList = filterArray.map(item =>  ({ name:item.name, id:item.id }) );
           
            this.setState({
              cityList: cityList,
            });
        //   });
        // });
      }

  onSubmit = (e) => {
    let income = this.state.income;
            const emailReg=  /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;
            const passReg = /^(?=.*\d)(?=.*[a-z])(?=.*[A-Z])[0-9a-zA-Z^!"#$%&'()*+,-.:;<=>?@[\]^_`{|}~]{8,}$/;
            var rgx = /^[0-9]*\.?[0-9]*$/;
              let mobileNo = this.state.mobileNo;
              let count =0;
        if(this.state.email == "" ||  emailReg.test(this.state.email) === false){
         this.setState({
           emailError:true
         }) 
         count++;
        }
           if(this.state.firstName== ""){
            this.setState({
      firstError:true
            })
      count++;
           }
       if(this.state.lastName== ""){
            this.setState({
         lastError:true
            })
      count++;
           }
       if(this.state.selectedCountry== ""){
              this.setState({
        countryError:true
              })
        count++;
             }
         if(this.state.selectedState== ""){
                this.setState({
                  stateError:true
                })
          count++;
               }
           if(this.state.selectedCity== ""){
                  this.setState({
            cityError:true
                  })
            count++;
                 }
             if(this.state.Birthday== "" || this.state.validAge== true){
                    this.setState({
              birtthdayError:true
                    })
              count++;
                   }
              if(this.state.income== "" || income.split('.').length >2 || income.charAt(0) === '.' || income.charAt(income.length-1) === '.' ){
                      this.setState({
                incomeError:true
                      })
                count++;
                     }
              if(this.state.mobileNo== "" || mobileNo.length < 10){
                      this.setState({
                mobileError:true
                      })
                count++;
                     }
                  if(this.state.password== "" ||  passReg.test(this.state.password) === false){
                          this.setState({
                    passError:true
                          })
                    count++;
                         }
                     if(this.state.confirmPass== "" ||  (this.state.password != this.state.confirmPass)){
                            this.setState({
                      confPassError:true
                            })
                      count++;
                         }

                      if(count === 0){

                        let today = new Date();
                             let todayDay =today.getDate();
                           let todayMonth =today.getMonth() +1;
                           let todayYear =today.getFullYear();
                           var todayDate = todayYear + '-' +todayMonth +'-' +todayDay;
                         //  console.log("today date is: ", todayDate);
                             console.log('cccccc', this.state.countryId);
                             console.log('sssss', this.state.stateId);
                             console.log('iiiiii', this.state.cityId);
                       // this.props.navigation.navigate('Login');
                       fetch('https://1800rentpro.com/rentpro_api/api/sign_up', {
            method: 'POST',
            headers: {
                    'Accept': 'application/json',
                    'Content-Type': 'application/json',
                      },
            body: JSON.stringify({
              firstname: this.state.firstName,
              middlename: this.state.middleName,
              lastname: this.state.lastName,
              user_type_id: this.state.userId,
              countryid: this.state.countryId,
              stateid: this.state.stateId,
              cityid: this.state.cityId,
              dateofbirth: this.state.birthToSend,
              annual_income: this.state.income,
              mobileno: this.state.mobileNo,
              password: this.state.password,
              email: this.state.email,
              created: todayDate,
       })
          }).then((response) => response.json())
            .then((responseJson) => {
            
          if(responseJson.status==1){
            console.log('is sign in 6666');
            console.log(responseJson.message);
           // this.props.navigation.navigate('Login');  // Signed up successfully!
           if(responseJson.message== 'Signed up successfully!'){
           this.toggleModal(); 
           }
            }
          else{
            Alert.alert(responseJson.message);
          }
        }).catch((error) => {
      console.error(error);
    });
                      }
                      
        }

    calculateDate(){
      var day = 12;
      var month = 12;
      var year = 2006;
      var age = 18;
      var setDate = new Date(year + age, month - 1, day);
      var currdate = new Date();
      
      if (currdate >= setDate) {
        // you are above 18
        alert("above 18");
      } else {
        alert("below 18");
      }
    }

  render() {
    
    return (
       <SafeAreaView style={styles.container}>
      <View style={styles.container}>
           <ToolbarWithLeftIcon title="Sign Up" />    
           <View style={styles.mainView}>
           <View style={{ height:'100%',}}>
            <ScrollView>
            <View style={commonStyles.titleView}>
            <Text style={commonStyles.titleText}>General Account Information</Text>
            </View>
            <View style={commonStyles.requiredField}>
            <View style={commonStyles.requiredDot}></View>
             <Text style={commonStyles.nameStyleWithDot}>Email</Text>
             </View>
            <TextInput
          style={styles.textInput}
           underlineColorAndroid = "transparent"
           placeholderTextColor={'gray'}
          placeholder="Enter Email"
          onChangeText={(email) => this.setState({email:email, 
          emailError:false})}
          returnKeyType = {"next"}
           autoFocus = {true}
         onSubmitEditing={(event) => { 
            this.refs.FirstN.focus(); 
          }}
          value={this.state.email}
        />
        <View style={commonStyles.textinputBorder}></View>
        { this.state.emailError == true ? (
             <Text style={commonStyles.errorMessage}>
                 * Please enter correct input to proceed.
             </Text>
            ) : null  }

       <View style={commonStyles.requiredField}>
            <View style={commonStyles.requiredDot}></View>
             <Text style={commonStyles.nameStyleWithDot}>First Name</Text>
             </View>
            <TextInput
         style={styles.textInput}
           underlineColorAndroid = "transparent"
           placeholderTextColor={'gray'}
           maxLength={20}
          placeholder="Enter First Name"
          ref='FirstN'
           returnKeyType = {"next"}
           onSubmitEditing={(event) => { 
            this.refs.MiddleN.focus(); 
          }}
        onChangeText={text => this.namevalidate(text)}  
          value={this.state.firstName}
        />
        <View style={commonStyles.textinputBorder}></View>
        { this.state.firstError == true ? (
             <Text style={commonStyles.errorMessage}>
                * Please enter correct input to proceed.
             </Text>
            ) : null  }

        <Text style={commonStyles.nameStyle}>Middle Name</Text>
            <TextInput
          style={styles.textInput}
           underlineColorAndroid = "transparent"
           placeholderTextColor={'gray'}
           maxLength={20}
          placeholder="Enter Middle Name"
          onChangeText={(middleName) => this.setState({middleName})}
          value={this.state.middleName}
          ref='MiddleN'
           returnKeyType = {"next"}
           onSubmitEditing={(event) => { 
            this.refs.LastN.focus(); 
          }}
        />
        <View style={commonStyles.textinputBorder}></View>

         <View style={commonStyles.requiredField}>
            <View style={commonStyles.requiredDot}></View>
             <Text style={commonStyles.nameStyleWithDot}>Last Name</Text>
             </View>
            <TextInput
          style={styles.textInput}
           underlineColorAndroid = "transparent"
           placeholderTextColor={'gray'}
          placeholder="Enter Last Name"
          maxLength={20}
          onChangeText={text => this.lastNamevalidate(text)}  
         value={this.state.lastName}
         ref='LastN'
          
        />
        <View style={commonStyles.textinputBorder}></View>
        { this.state.lastError == true ? (
             <Text style={commonStyles.errorMessage}>
                 * Please enter correct input to proceed.
             </Text>
            ) : null  }

         <View style={commonStyles.requiredField}>
            <View style={commonStyles.requiredDot}></View>
             <Text style={commonStyles.nameStyleWithDot}>Country</Text>
             </View>
             <View style={styles.textInput}>
             <RNPicker
          dataSource={this.state.countryList}
          dummyDataSource={this.state.countryList}
          defaultValue={false}
          pickerTitle={"Select Country "}
          showSearchBar={true}
          disablePicker={false}
          changeAnimation={"none"}
          searchBarPlaceHolder={"Search....."}
          showPickerTitle={true}
          searchBarContainerStyle={styles.searchBarContainerStyle}
          pickerStyle={styles.pickerStyle}
          selectedLabel={this.state.selectedCountry}
          placeHolderLabel={this.state.placeHolderText}
          selectLabelTextStyle={styles.selectLabelTextStyle}
          placeHolderTextStyle={styles.placeHolderTextStyle}
          dropDownImageStyle={styles.dropDownImageStyle}
          dropDownImage={require('../image/droparrow.png')}
          selectedValue={(id, name, value) => this.getState(id, name, value)}
        //  selectedValue={(item) => this._selectedValue(item)}
        />
        </View>
       <View style={commonStyles.textinputBorder}></View>
        { this.state.countryError == true ? (
             <Text style={commonStyles.errorMessage}>
                 * Please enter correct input to proceed.
             </Text>
            ) : null  }

         <View style={commonStyles.requiredField}>
            <View style={commonStyles.requiredDot}></View>
             <Text style={commonStyles.nameStyleWithDot}>State</Text>
             </View>
        <View style={styles.textInput}>
            
         <RNPicker
          dataSource={this.state.stateList}
          dummyDataSource={this.state.stateList}
          defaultValue={false}
          pickerTitle={"Select State "}
          showSearchBar={true}
          disablePicker={false}
          changeAnimation={"none"}
          searchBarPlaceHolder={"Search....."}
          showPickerTitle={true}
          searchBarContainerStyle={styles.searchBarContainerStyle}
          pickerStyle={styles.pickerStyle}
          selectedLabel={this.state.selectedState}
          placeHolderLabel={this.state.placeHolderText}
          selectLabelTextStyle={styles.selectLabelTextStyle}
          placeHolderTextStyle={styles.placeHolderTextStyle}
          dropDownImageStyle={styles.dropDownImageStyle}
          dropDownImage={require('../image/droparrow.png')}
          selectedValue={(index, name, value) => this.getCity(index, name, value)}
        //  selectedValue={(item) => this._selectedValue(item)}
        />
        </View>
        <View style={styles.textinputBorder}></View>
        { this.state.stateError == true ? (
             <Text style={commonStyles.errorMessage}>
                 * Please enter correct input to proceed.
             </Text>
            ) : null  }
        
        <View style={styles.requiredField}>
            <View style={styles.requiredDot}></View>
             <Text style={styles.nameStyleWithDot}>City</Text>
             </View>
        <View style={styles.textInput}>
             {/* <RNPickerSelect
            onValueChange={(value) => this.setState({city:value, 
              cityError:false})}
            items={this.state.cityList}
        /> */}
        <RNPicker
          dataSource={this.state.cityList}
          dummyDataSource={this.state.cityList}
          defaultValue={false}
          pickerTitle={"select City"}
          showSearchBar={true}
          disablePicker={false}
          changeAnimation={"none"}
          searchBarPlaceHolder={"Search....."}
          showPickerTitle={true}
          searchBarContainerStyle={styles.searchBarContainerStyle}
          pickerStyle={styles.pickerStyle}
          selectedLabel={this.state.selectedCity}
          placeHolderLabel={this.state.placeHolderText}
          selectLabelTextStyle={styles.selectLabelTextStyle}
          placeHolderTextStyle={styles.placeHolderTextStyle}
          dropDownImageStyle={styles.dropDownImageStyle}
          dropDownImage={require('../image/droparrow.png')}
          selectedValue={(index, name, value) => this._selectedCity(index, name, value)}
        //  selectedValue={(item) => this._selectedValue(item)}
        />
        </View>
        <View style={styles.textinputBorder}></View>
        { this.state.cityError == true ? (
             <Text style={commonStyles.errorMessage}>
                 * Please enter correct input to proceed.
             </Text>
            ) : null  }

         <View style={{height:hp('10%'), justifyContent:'center', alignItems:'center',}}>
            <Text style={{fontSize:16, fontWeight:'bold'}}>Personal Information</Text>
            </View>

            <View style={commonStyles.requiredField}>
            <View style={commonStyles.requiredDot}></View>
             <Text style={commonStyles.nameStyleWithDot}>Birthday</Text>
             </View>
             <View style={styles.birthdayView}>
             
              {this.state.Birthday === '' ?
             <Text style={{color:'gray'}}>DD/MM/YYYY</Text> :
             <Text>{this.state.Birthday}</Text>
        }
            
        <TouchableOpacity 
        style={styles.calenderIconView}
       onPress={this.showDateTimePicker}>
         <Image style={styles.calenderImg} source={require('../image/CalendarBlue.png')}/>
        </TouchableOpacity>
        </View>
        <View style={styles.textinputBorder}></View>
        { this.state.birtthdayError == true ? (
             <Text style={commonStyles.errorMessage}>
                age should be 18+
             </Text>
            ) : null  }
        
        <View style={commonStyles.requiredField}>
            <View style={commonStyles.requiredDot}></View>
             <Text style={commonStyles.nameStyleWithDot}>Annual Income</Text>
             </View>
            <TextInput
          style={styles.textInput}
          keyboardType='decimal-pad'
           underlineColorAndroid = "transparent"
           placeholderTextColor={'gray'}
          placeholder="Enter Annual Income"
          onChangeText={(income) => this.setState({income:income,
          incomeError:false})}
          value={this.state.income}
          ref='IncomeN'
           returnKeyType = {"next"}
           onSubmitEditing={(event) => { 
            this.refs.MobileN.focus(); 
          }}
        />
        <View style={commonStyles.textinputBorder}></View>
        { this.state.incomeError == true ? (
             <Text style={commonStyles.errorMessage}>
                Income should be valid number may contain decimal upto two digits only
             </Text>
            ) : null  }

        <View style={commonStyles.requiredField}>
            <View style={commonStyles.requiredDot}></View>
             <Text style={commonStyles.nameStyleWithDot}>Mobile Number</Text>
             </View>
             <View style={{flexDirection:'row'}}>
               {/* <Text  style={{width:'18%',  marginStart:wp('10%'), marginTop:'2%' }}> </Text> */}
            <TextInput
          // style={[styles.textInput, { width:'65%', marginStart:0}]}
          style={styles.textInput}
          keyboardType="numeric"
           underlineColorAndroid = "transparent"
          placeholder="Enter your Mobile Number"
          placeholderTextColor={'gray'}
          maxLength={10}
          onChangeText={text => this.mobilevalidate(text)}  
          value={this.state.mobileNo}
          ref='MobileN'
           returnKeyType = {"next"}
           onSubmitEditing={(event) => { 
            this.refs.PasswordN.focus(); 
          }}
        />
        </View>
        <View style={commonStyles.textinputBorder}></View>
        { this.state.mobileError == true ? (
             <Text style={commonStyles.errorMessage}>
                * Please enter correct input to proceed.
             </Text>
            ) : null  }

        <View style={commonStyles.requiredField}>
            <View style={commonStyles.requiredDot}></View>
             <Text style={commonStyles.nameStyleWithDot}>User Type ID</Text>
             </View>
             <View style={{height:hp('10%'), justifyContent:'center', paddingStart:wp('10%') }}>
              <RadioForm
               radio_props={radio_props}
               initial={0}
                 formHorizontal={true}
                 buttonSize={15}
                 buttonInnerColor= {'#034d94'}
                 buttonOuterColor ={'#034d94'}
                 labelStyle={{marginRight:wp('15%'),}}
                         onPress={(value) => this.setState({userId:value,
                        userIdError:false})}
                   />
             </View>
            <View style={styles.textinputBorder}></View>

        <View style={styles.requiredField}>
            <View style={styles.requiredDot}></View>
             <Text style={styles.nameStyleWithDot}>Password</Text>
             </View>
            <TextInput
          style={styles.textInput}
           underlineColorAndroid = "transparent"
           placeholderTextColor={'gray'}
        //   secureTextEntry={true}
          placeholder="Enter Password"
          onChangeText={(password) => this.setState({password:password,
          passError:false})}
          value={this.state.password}
          ref='PasswordN'
           returnKeyType = {"next"}
           onSubmitEditing={(event) => { 
            this.refs.ConfirmPassordN.focus(); 
          }}
        />
        <View style={commonStyles.textinputBorder}></View>
        { this.state.passError == true ? (
             <Text style={commonStyles.errorMessage}>
           password should contain at least one digit, one lower case, one upper case & it size must be greater than 8 character'
             </Text>
            ) : null  }

        <View style={commonStyles.requiredField}>
            <View style={commonStyles.requiredDot}></View>
             <Text style={commonStyles.nameStyleWithDot}>Confirm Password</Text>
             </View>
            <TextInput
          style={styles.textInput}
           underlineColorAndroid = "transparent"
           placeholderTextColor={'gray'}
          placeholder="Enter Confirm Password"
          onChangeText={(confirmPass) => this.setState({confirmPass:confirmPass,
          confPassError:false})}
          value={this.state.confirmPass}
          ref='ConfirmPassordN'
        />
        <View style={styles.commonStyles}></View>
        { this.state.confPassError == true ? (
             <Text style={commonStyles.errorMessage}>
                * password does not match
             </Text>
            ) : <View style={{height:hp('3%')}}></View>  }
         </ScrollView>
        </View>
       
         <DateTimePicker
          isVisible={this.state.isDateTimePickerVisible}
          onConfirm={this.handleDatePicked}
          onCancel={this.hideDateTimePicker}
        />  
      <Modal isVisible={this.state.isModalVisible}
       onBackdropPress={() => this.setState({ isVisible: false })}>
          <View style={{ height:'50%',  backgroundColor: "#ededed", margin:'8%' }}>
          <Image source={require('../image/logo.png')} style={{height:'23%', margin:'5%', alignSelf:'center'}} />
          <Text  style={{marginTop:'10%', alignSelf:'center'}}>You have succefully registerd </Text>
          <Text style={{marginBottom:'10%', alignSelf:'center'}}> with 877RentPro</Text>
            <TouchableOpacity  onPress={()=> this.closeModel()} style={{backgroundColor:'#034d94', alignSelf:'center', width:'45%', height:'15%', borderRadius:5, marginTop:'10%', margin:'5%', justifyContent:'center', alignItems:'center'}} >
              <Text style={{color:'white', fontWeight:'bold', }}>Login</Text>
              </TouchableOpacity>
          </View>
        </Modal>
        <TouchableOpacity style={styles.buttonStyle}
       onPress={(e) => this.onSubmit(e)}> 
        <Text style={styles.buttonText}>Sign Up</Text>
        </TouchableOpacity>
        </View>
          </View>
           </SafeAreaView>
    );
  }
}
const styles = StyleSheet.create({
container: {
    flex: 1,
   backgroundColor: "#ededed"
  },
  mainView:{
    height:'100%',
     width:'86%',
     margin:'7%', 
     backgroundColor:'white',
      borderRadius:10, 
      flex:1},
 nameStyle:{
    marginTop:hp('2%'), 
     marginStart:wp('10%'),
  // backgroundColor:'yellow', 
 },
 nameStyleWithDot:{
     marginTop:hp('2%'), 
     marginStart:wp('2%'),
 },
 textInput:{
    marginStart:wp('10%'), 
     marginTop:-hp('1.3%'),
     //backgroundColor:'yellow', 
    // height:hp('5%')
 },
 textinputBorder:{
   marginStart:wp('5%'), 
   marginEnd:wp('5%'), 
    backgroundColor:'gray',
     height:1,
      width:'85%',
      marginTop:-hp('1%')
      },
  buttonStyle:{height:hp('8%'),
       width:'100%',
       borderBottomLeftRadius:10,
       borderBottomRightRadius:10, 
       backgroundColor:'#008f2c',
        // marginTop:hp('24%'),
        // justifyContent:'flex-end',
     alignItems:'center'
  }, 
  requiredDot:{
    height:6,
     width:6,
      borderRadius:3,
       backgroundColor:'red',
         marginStart:wp('8%'),
         marginTop:hp('2%'), 
       },
       titleView:{
         height:hp('8%'), 
         justifyContent:'center',
          alignItems:'center'
      },
      titleText:{
        fontSize:16,
         fontWeight:'bold'
    },
    requiredField:{
      flexDirection:'row',
       alignItems:'center'
    },
    buttonText:{
      fontWeight:'bold', 
      fontSize:16, 
      color:'white', 
      marginTop:hp('1.3%')
      },
      calenderIconView:{
        width:wp('20%'), 
      marginTop:-hp('1%'),
       justifyContent:'center',
       alignItems:'center'
      },
       calenderImg:{
         height:hp('4%'),
       width:hp('4%')
      },
      birthdayView:{ 
        flexDirection:'row', 
        height:hp('6%'),
       marginStart:wp('10%'),
        justifyContent:'space-between',
        alignItems:'center',
        marginEnd:wp('5%')
       },

  searchBarContainerStyle: {
    marginBottom: 10,
    flexDirection: "row",
    height: 40,
    shadowOpacity: 1.0,
    shadowRadius: 5,
    shadowOffset: {
      width: 1,
      height: 1
    },
    backgroundColor: "rgba(255,255,255,1)",
    shadowColor: "#d3d3d3",
    borderRadius: 10,
    elevation: 3,
    marginLeft: 10,
    marginRight: 10
  },

  selectLabelTextStyle: {
    color: "#000",
    textAlign: "left",
    width: "85%",
    padding: 10,
   
    flexDirection: "row"
  },
  placeHolderTextStyle: {
    color: "#D3D3D3",
    padding: 10,
    textAlign:"left",
    width: "85%",

    flexDirection: "row",
 
  },
  dropDownImageStyle: {
   // marginLeft: "10%",
    marginRight:'20%',
    width: 20,
    height: 20,
    alignSelf: "center"
  },

  pickerStyle: {
    // marginLeft: 18,
    // elevation:3,
    // paddingRight: 25,
    // marginRight: 10,
    marginBottom: '2%',
    
    // shadowOpacity: 1.0,
    // shadowOffset: {
    //   width: 1,
    //   height: 1
    // },
    // borderWidth:1,
    // shadowRadius: 10,
    // backgroundColor: "rgba(255,255,255,1)",
    // shadowColor: "#d3d3d3",
    // borderRadius: 5,
    marginRight:10,
     flexDirection: "row",
     marginStart:wp('5%'), 
   
  }
});

//let myState1 = myState.substr(1, myState.length-2);