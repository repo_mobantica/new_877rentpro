import React, { Component } from 'react';
import { Text, View, SafeAreaView, Image, TouchableOpacity, StyleSheet, Dimensions, Alert,
Button, FlatList, TextInput } from 'react-native';
import {widthPercentageToDP as wp, heightPercentageToDP as hp} from 'react-native-responsive-screen';
import ToolbarHomeImg from '../component/ToolbarHomeImg';
import commonStyles from "../styles/index";
import Toast from 'react-native-simple-toast';
import Api from "../Api/Api";
import AsyncStorage from "@react-native-community/async-storage";
export default class ChangePassword extends Component {
    constructor(props) {
    super(props);

    this.state = {
      password:'',
      newPass:'',
      confirmPass:'',
      passwordError:false,
      PasswordValidation:false,
      newPassError:false,
      
    };    
  }

  async onSubmit(){
     this.setState({
      passwordError:false,
      newPassError:false,
      PasswordValidation:false

     }) 
      if(this.state.password== ""){
         this.setState({
             passwordError:true
         }) 
        }
    else if(this.state.newPass==""){
       this.setState({
        newPassError:true
     }) 

    }else if(this.state.newPass!=this.state.confirmPass)
    {
       this.setState({
        PasswordValidation:true
     }) 
    }
    else{
      var token = await AsyncStorage.getItem("Token");
      
      Api("change_password", {
        userid: token,
        old_password: this.state.password,
        new_password: this.state.newPass
      })
        .then(responseJson => {
          if(responseJson==false)
          {
            alert("No internet")
          }
           else {
            Alert.alert(responseJson.message);
           if(responseJson.message=="Sucessfully Password Changeed!")
           {
             this.props.navigation.navigate("MapScreen")
           }
          }
    })
  }
}
  render() {
    
    return (
       <SafeAreaView style={styles.container}>
      <View style={styles.container}>
           <ToolbarHomeImg title="Change Password" />    
           <View style={styles.mainView}>
           <View style={{ height:'98%', }}>
            
           <View style={styles.requiredField}>
            <View style={styles.requiredDot}></View>
             <Text style={styles.nameStyleWithDot}>Current Password</Text>
             </View>
            <TextInput
          style={styles.textInput}
           underlineColorAndroid = "transparent"
          placeholder="Current Password"
          secureTextEntry={true}
          onChangeText={(password) => this.setState({password})}
          value={this.state.password}
        />
        <View style={styles.textinputBorder}></View>
        { this.state.passwordError == true ? (
             <Text style={commonStyles.errorMessage}>
                 * Please enter correct input to proceed.
             </Text>
            ) : null  }

       <Text style={styles.nameStyle}>New Password</Text>
            <TextInput
          style={styles.textInput}
           underlineColorAndroid = "transparent"
          placeholder="New Password"
          secureTextEntry={true}
          onChangeText={(newPass) => this.setState({newPass})}
          value={this.state.newPass}
        />
        <View style={styles.textinputBorder}></View>
        { this.state.newPassError == true ? (
             <Text style={commonStyles.errorMessage}>
                 * Please enter new password .
             </Text>
            ) : null  }

        <Text style={styles.nameStyle}>Confirm Password</Text>
            <TextInput
          style={{ marginStart:wp('10%'),
            marginTop:-hp('1.3%')}}
           underlineColorAndroid = "transparent"
           secureTextEntry={true}
          placeholder="Confirm Password"
          onChangeText={(confirmPass) => this.setState({confirmPass})}
          value={this.state.confirmPass}
        />
        <View style={styles.textinputBorder}></View>
        { this.state.PasswordValidation == true ? (
             <Text style={commonStyles.errorMessage}>
                 * New password and confirm password both not same .
             </Text>
            ) : null  }
       </View>
        <TouchableOpacity style={styles.buttonStyle}
        onPress={()=> this.onSubmit() }>
        <Text style={{fontWeight:'bold', fontSize:16, color:'white', }}> Change Password</Text>
        </TouchableOpacity>
        </View>
          </View>
           </SafeAreaView>
    );
  }
}
const styles = StyleSheet.create({
container: {
    flex: 1,
   backgroundColor: "#ededed"
  },
  mainView:{
    height:'80%',
     width:'86%',
     margin:'7%', 
     backgroundColor:'white',
      borderRadius:10, 
      flex:1},
 nameStyle:{
    marginTop:hp('5%'), 
     marginStart:wp('10%')
 },
 textInput:{
    marginStart:wp('10%'), 
     marginTop:-hp('1.3%')
 },
 textinputBorder:{
   marginStart:wp('5%'), 
   marginEnd:wp('5%'), 
    backgroundColor:'gray',
     height:1,
      width:'85%',
      marginTop:-hp('1%')},
  buttonStyle:{height:'10%',
       width:'100%',
       borderBottomLeftRadius:10,
       borderBottomRightRadius:10, 
       backgroundColor:'#034d94',
        // marginTop:hp('24%'),
        justifyContent:'center',
     alignItems:'center'
  },
  nameStyleWithDot:{
    marginTop:hp('2%'), 
    marginStart:wp('2%'),
},
requiredDot:{
  height:6,
   width:6,
    borderRadius:3,
     backgroundColor:'red',
       marginStart:wp('8%'),
       marginTop:hp('2%'), 
     },
  requiredField:{
    flexDirection:'row',
     alignItems:'center'
  },
});


// return (
//        <SafeAreaView style={[styles.container,{backgroundColor:'red'}]}>
//       <View style={styles.container}>
//                <ToolbarHomeImg title="Change Password" />
//               <View style={{marginTop: hp('25%'), marginStart:wp('15%'), marginEnd:wp('15%'), alignItems:'center'}}>
//               <View style={styles.textInputView}> 
//                <TextInput
//               style={{height: 40}}
//               placeholder="Current Password"
//               onChangeText={(password) => this.setState({password})}
//               value={this.state.password}
//              />
//                </View>

//                 <View style={styles.textInputView}> 
//                <TextInput
//               style={{height: 40}}
//               placeholder="New Password"
//               onChangeText={(newPass) => this.setState({newPass})}
//               value={this.state.newPass}
//              />
//                </View>

//                 <View style={styles.textInputView}> 
//                <TextInput
//               style={{height: 40}}
//               underlineColorAndroid="transparent"
//               placeholder="Confirm Password"
//               onChangeText={(confirmPass) => this.setState({confirmPass})}
//               value={this.state.confirmPass}
             
//              />
//                </View>

//                 <View style={{borderRadius:30, width:wp('37%'),height:hp('5.8%'), justifyContent:'center', alignItems:'center', backgroundColor:'#034d94'}}> 
//               <Text style={{color:'white', fontWeight:'bold'}}>SUBMIT</Text>
//                </View>
        
        
//       </View>
//           </View>
//            </SafeAreaView>
//     );
//   }
// }
// const styles = StyleSheet.create({
// container: {
//     flex: 1,
//    backgroundColor: "#ededed"
//   },
//   menuIcon: {
//     height: "40%",
//     resizeMode: "contain",
//     width: 30,
//     marginStart: 5
//   },
//   textInputView:{
//      alignItems:'center',
//       justifyContent:'center',
//        width:wp('70%'), 
//      height:hp('8%'),
//       backgroundColor:'white',
//        borderColor:'black',
//         borderWidth:1,
//          borderRadius:35,
//          marginBottom:hp('5%')
//   }
    
// });