import React, { Component } from "react";
import {
  Text,
  View,
  SafeAreaView,
  Image,
  TouchableOpacity,
  StyleSheet,
  Dimensions,
  Alert,
  Button,
  PermissionsAndroid
} from "react-native";
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp
} from "react-native-responsive-screen";
import ListScreen from "./ListScreen";
import FavoriteScreen from "./FavoriteScreen";
import MapView, { Marker } from "react-native-maps";
import Geocoder from "react-native-geocoder";
import { Searchbar } from "react-native-paper";
import { NavigationEvents } from "react-navigation";
import Toolbar from "../component/Toolbar";
import Geolocation from "react-native-geolocation-service";
import AsyncStorage from "@react-native-community/async-storage";
import Api from "../Api/Api";
import { GooglePlacesAutocomplete } from "react-native-google-places-autocomplete";
const NetInfo = require("@react-native-community/netinfo");
import { ProgressDialog } from "react-native-simple-dialogs";
var dataSelected;
var halfBath = 0;
var indexSelected;
var location = "";
var token = "";
export default class Map extends Component {
  state = {
    focusedLocation: {
      latitude: 34.052235,
      longitude: -118.243683,
      latitudeDelta: 0.0922,
      longitudeDelta:
        (Dimensions.get("window").width / Dimensions.get("window").height) *
        0.0922
    },
    locationChoosen: false,
    search: "",
    selectedMarkerIndex: "",
    showDialog: false,
    flatlistData: [],
    currentLat: "",
    currentLong: "",
    locationPermission: false,
    showProgress: false,
    connection_Status: ""
  };

  picKLocationHandler = event => {
    const coords = event.nativeEvent.coordinate;
    this.map.animateToRegion({
      ...this.state.focusedLocation,
      latitude: coords.latitude,
      longitude: coords.longitude
    });
    this.setState(prevState => {
      return {
        focusedLocation: {
          ...prevState.focusedLocation,
          latitude: coords.latitude,
          longitude: coords.longitude
        },
        locationChoosen: true
      };
    });
  };

  async componentDidMount() {
 
    location = await AsyncStorage.getItem("LocationPermission");
  //  search = await AsyncStorage.getItem("Search");
    token = await AsyncStorage.getItem("Token");
    // if(search==null)
    // {
    //   search = await  AsyncStorage.setItem('Search', 'false');
    //  }
    
    console.log("hasss permission");
     console.log(token);
    
      this.getCurrentLocation();
     // this.setState({ search: search });
  }

  getCurrentLocation() {
    
    this.setState({
      selectedMarkerIndex: ""
    });
    if (location != "") {
      Geolocation.getCurrentPosition(
        position => {
          console.log(position);
          this.setState(
            {
              currentLat: position.coords.latitude,
              currentLong: position.coords.longitude
            },
            function() {
              console.log("this.state.currentLat", this.state.currentLat);
              console.log("this.state.currentLong", this.state.currentLong);
              this.fetchData();
            }
          );
        },
        error => {
          // See error code charts below.
          console.log(error.code, error.message);
        },
        { enableHighAccuracy: true, timeout: 15000, maximumAge: 10000 }
      );
    }
  }

  fetchData() {
    this.setState({
      showProgress: true
    });
    Api("property_list", {
      userid: token,
      lat: this.state.currentLat,
      long: this.state.currentLong
    })
      .then(responseJson => {
        this.setState({
          showProgress: false
        });
      
        if (responseJson.status == 1) {
          let data = responseJson.response.data;

          this.setState(
            {
              flatlistData: data
            },
            function() {
              //console.log('length',this.state.flatlistData.length);
            }

          );
         
          this.map.animateToRegion({
            ...this.state.focusedLocation,
            latitude: this.state.currentLat,
            longitude: this.state.currentLong
          });
          this.setState(prevState => {
            return {
              focusedLocation: {
                ...prevState.focusedLocation,
                latitude: this.state.currentLat,
                longitude: this.state.currentLong
              },
              locationChoosen: true
            };
          });
          this.map.fitToSuppliedMarkers(this.state.flatlistData.map(m => m.id), true);
        this.map.fitToElements(true)
        }else if(responseJson==false)
        {
          alert("No internet")
        }
         else {
          Alert.alert(responseJson.message);
          this.setState({
            showProgress: false
          });
        }
      })
      .catch(error => {
        console.error(error);
        this.setState({
          showProgress: false
        });
      });
  }
  //'lat',res.position[lat)
  updateSearch = search => {
    this.setState({ search });
  };
  searchLocation(search) {
    Geocoder.geocodeAddress(search)
      .then(res => {
        // console.log('aaaaaaaaa');
        // console.log(res );
        console.log("aaaaaaaaa");
        console.log("lng", res[0].position.lng, "lat", res[0].position.lat);
        console.log("aaaaaaaaa");

        this.map.animateToRegion({
          ...this.state.focusedLocation,
          latitude: res[0].position.lat,
          longitude: res[0].position.lng
        });
        this.setState(
          prevState => {
            return {
              focusedLocation: {
                ...prevState.focusedLocation,
                latitude: res[0].position.lat,
                longitude: res[0].position.lng
              },
              locationChoosen: false,
              currentLat: res[0].position.lat,
              currentLong: res[0].position.lng
            };
          },
          function() {
            console.log("hhhhhhh");
            console.log(this.state.currentLat, this.state.currentLong);
            this.fetchData();
          }
        );
      })
      .catch(err => console.log("in serch error ...////", err));
  }
  onPressMarker(e, index) {
    this.setState({ selectedMarkerIndex: index, showDialog: true });
    dataSelected = this.state.flatlistData[index];
    indexSelected = index;
    console.log("selected maeker", dataSelected.isfavorite);
    console.log("selected maeker", dataSelected.id);
    if (dataSelected.halfbaths != 0) {
      halfBath = "1/2";
    } else {
      halfBath = "";
    }
  }
componentWillUnmount(){
  AsyncStorage.setItem('Search', 'false');
}
  addRemoveFavorite(status, index, id) {
    let flatlistData = this.state.flatlistData;
    let isFavorite = status;
    isFavorite = !isFavorite;
    console.log("status status", status);
    console.log("status status", id);

    flatlistData[index].isfavorite = !flatlistData[index].isfavorite;
    this.setState(
      {
        flatlistData: flatlistData
      },
      function() {
        console.log("after ", this.state.flatlistData[index].isfavorite);
      }
    );
    //"userid":"91","propertyid":"863", "addremovetype":"false
    this.setState({
      showProgress: true
    });
    Api("add_remove_favorites", {
      userid: token,
      propertyid: id,
      addremovetype: isFavorite
    })
      .then(responseJson => {
        this.setState({
          showProgress: false
        });
        if (responseJson.status == 1) {
          console.log(responseJson.message);
        } else {
          Alert.alert(responseJson.message);
        }
      })
      .catch(error => {
        console.error(error);
        this.setState({
          showProgress: false
        });
      });
  }

  onCancelPress() {
    this.setState({
      showDialog: false
    });
  }
  render() {
    const { search } = this.state;
    return (
      <SafeAreaView style={styles.container}>
        <View style={styles.container}>
          <Toolbar image title={"Dashboard"} />
          <NavigationEvents onDidFocus={() => {
              this.map.fitToSuppliedMarkers(this.state.flatlistData.map(m => m.id), true);
              this.map.fitToElements(true)
             AsyncStorage.getItem('Search').then((value) =>
            {
              if(value=='false'){
                this.getCurrentLocation()
             
              }
              else{
             this.setState({
               search:value
             },function(){
              this.searchLocation(value);
             })
            
                
              }
            }
          )  
            }} />
          <MapView
            style={styles.map}
            initialRegion={this.state.focusedLocation}
            onPress={this.picKLocationHandler}
            
            ref={ref => (this.map = ref)}
          >
            {this.state.flatlistData.length != 0
              ? this.state.flatlistData.map((item, index) => (
                  <MapView.Marker
                    key={index}
                    coordinate={{
                      latitude: Number(item.latitude),
                      longitude: Number(item.longitude)
                    }}
                    identifier={item.id}
                    title={item.display_address}
                    onPress={e => {
                      e.stopPropagation();
                      this.onPressMarker(e, index);
                    }}
                  >
                    {item.isfavorite ? (
                      <Image
                        style={{ height: 30, width: 30 }}
                        source={require("../image/MapFavorite.png")}
                      />
                    ) : this.state.selectedMarkerIndex === index ? (
                      <Image
                        style={{ height: 30, width: 30 }}
                        source={require("../image/selectedLocation.png")}
                      />
                    ) : (
                      <Image
                        style={{ height: 30, width: 30 }}
                        source={require("../image/location.png")}
                      />
                    )}
                  </MapView.Marker>
                ))
              : null}
          </MapView>
          <TouchableOpacity
          activeOpacity={0.9}
          onPress={()=>this.props.navigation.navigate("LocationSearchScreen",{returnScreen:'Map'})}
            style={[
              styles.searchBox,
              {
                justifyContent: "center",
                alignItems: "center",
                backgroundColor: "white"
              }
            ]}
          >
            <Searchbar
              placeholder="Current Location"
              editable={false}
             
              onChangeText={query => {
                this.setState({ search: query }, function() {
                  this.searchLocation();
                });
              }}
              value={this.state.search}
              placeholderTextColor="#034d94"
              icon={require("../image/my_location.png")}
              style={{
                height: hp("6.8%"),
                borderRadius: 35,
                width: "100%",
                backgroundColor: "transparent"
              }}
            />

            {/* <GooglePlacesAutocomplete
      placeholder='Search'
      minLength={2} // minimum length of text to search
      autoFocus={false}
      returnKeyType={'search'} // Can be left out for default return key https://facebook.github.io/react-native/docs/textinput.html#returnkeytype
      listViewDisplayed='auto'    // true/false/undefined
      fetchDetails={true}
      renderDescription={row => row.description} // custom description render
      onPress={(data, details = null) => { // 'details' is provided when fetchDetails = true
        console.log(data, details);
      }}
      
      getDefaultValue={() => ''}
      
      query={{
        // available options: https://developers.google.com/places/web-service/autocomplete
        key: 'AIzaSyBfLzq13ziAB--ID_DJIfo7W55jyiFSPBw',
        language: 'en', // language of the results
        types: '(cities)' // default: 'geocode'
      }}
      
      styles={{
        textInputContainer: {
          width: '100%'
        },
        description: {
          fontWeight: 'bold'
        },
        predefinedPlacesDescription: {
          color: '#1faadb'
        }
      }}
      
      currentLocation={true} // Will add a 'Current location' button at the top of the predefined places list
      currentLocationLabel="Current location"
      nearbyPlacesAPI='GooglePlacesSearch' // Which API to use: GoogleReverseGeocoding or GooglePlacesSearch
      GoogleReverseGeocodingQuery={{
        // available options for GoogleReverseGeocoding API : https://developers.google.com/maps/documentation/geocoding/intro
      }}
      GooglePlacesSearchQuery={{
        // available options for GooglePlacesSearch API : https://developers.google.com/places/web-service/search
        rankby: 'distance',
        types: 'food'
      }}
 
      filterReverseGeocodingByTypes={['locality', 'administrative_area_level_3']} // filter the reverse geocoding results by types - ['locality', 'administrative_area_level_3'] if you want to display only cities
     // predefinedPlaces={[homePlace, workPlace]}
 
      debounce={200} // debounce the requests in ms. Set to 0 to remove debounce. By default 0ms.
      renderLeftButton={()  =>  <Image style={{height:20, width:20, margin:1}} source={{uri: 'http://facebook.github.io/react/assets/logo_og.png'}} />}
    /> */}
          </TouchableOpacity>
          {this.state.showDialog ? (
            <View
              style={{
                height: hp("26.2%"),
                backgroundColor: "white",
                marginTop: hp("60.5%"),
                marginBottom: 0,
                marginStart: wp("8%"),
                marginEnd: wp("8%")
              }}
            >
              <TouchableOpacity
                onPress={() =>
                  this.props.navigation.navigate("PropertyInfo", {
                    Property_id: dataSelected.id
                  })
                }
              >
                <Image
                  style={{
                    borderTopLeftRadius: 11,
                    borderTopRightRadius: 11,
                    width: "100%",
                    height: hp("11.8%")
                  }}
                  source={{ uri: dataSelected.image_url }}
                />
              </TouchableOpacity>
              <TouchableOpacity
                style={{
                  height: 36,
                  width: 36,
                  borderRadius: 18,
                  backgroundColor: "white",
                  marginTop: -hp("13.5%"),
                  marginStart: wp("77%"),
                  justifyContent: "center",
                  alignItems: "center"
                }}
                onPress={() => this.onCancelPress()}
              >
                <Image
                  style={{ width: 10, height: 10 }}
                  source={require("../image/cancel.png")}
                />
              </TouchableOpacity>

              <View style={styles.firstRow}>
                <Text style={{ color: "#034d94", fontWeight: "bold" }}>
                  ${dataSelected.minrent} - {dataSelected.maxrent}
                </Text>
                <TouchableOpacity
                  onPress={() =>
                    this.addRemoveFavorite(
                      dataSelected.isfavorite,
                      indexSelected,
                      dataSelected.id
                    )
                  }
                >
                  {dataSelected.isfavorite ? (
                    <Image
                      style={{ width: 25, height: 25 }}
                      source={require("../image/heart.png")}
                    />
                  ) : (
                    <Image
                      style={{ width: 25, height: 25 }}
                      source={require("../image/Like.png")}
                    />
                  )}
                </TouchableOpacity>
              </View>
              <TouchableOpacity
                onPress={() =>
                  this.props.navigation.navigate("PropertyInfo", {
                    Property_id: dataSelected.id
                  })
                }
              >
                <Text
                  style={{
                    color: "#939393",
                    marginStart: wp("3.7%"),
                    fontSize: 11.5
                  }}
                >
                  {dataSelected.bedrooms} Bed | {dataSelected.fullbaths}{" "}
                  {halfBath} Bathrooms
                </Text>
                <Text
                  style={{
                    color: "black",
                    marginStart: wp("3.7%"),
                    fontWeight: "bold",
                    marginBottom: hp("2%")
                  }}
                  numberOfLines={1}
                >
                  {dataSelected.display_address}
                </Text>
                <View style={styles.lastRow}>
                  <Image
                    style={{ width: 18, height: 18 }}
                    source={require("../image/clock.png")}
                  />
                  <Text
                    style={{
                      color: "#939393",
                      marginStart: wp("1.6%"),
                      fontSize: 11.5
                    }}
                  >
                    Applied, Yesterday at 12:09 PM
                  </Text>
                </View>
              </TouchableOpacity>
            </View>
          ) : (
            <View style={styles.tabView}>
              <View style={styles.subTab}>
                <Image
                  style={styles.menuIcon}
                  source={require("../image/map12.png")}
                />
                <Text style={styles.fontStyle}>MAP</Text>
              </View>
              <TouchableOpacity
                onPress={() => this.props.navigation.navigate("ListScreen")}
              >
                <View style={[styles.subTab, { backgroundColor: "#034d94" }]}>
                  <Image
                    style={styles.menuIcon}
                    source={require("../image/List.png")}
                  />
                  <Text style={[styles.fontStyle, { color: "white" }]}>
                    LIST
                  </Text>
                </View>
              </TouchableOpacity>
              <TouchableOpacity
                onPress={() => this.props.navigation.navigate("FavoriteScreen")}
              >
                <View
                  style={[
                    styles.subTab,
                    { width: wp("25.3%"), backgroundColor: "#034d94" }
                  ]}
                >
                  <Image
                    style={styles.menuIcon}
                    source={require("../image/map.png")}
                  />
                  <Text style={[styles.fontStyle, { color: "white" }]}>
                    FAVORITE
                  </Text>
                </View>
              </TouchableOpacity>
            </View>
          )}

          {/* <View style={{height:hp('25.9%'), backgroundColor:'white',marginTop:hp('70%'),
          marginBottom:0, marginStart:wp('8%'), marginEnd:wp('8%')}}>

           <Image
          style={{ borderTopLeftRadius: 11, 
        borderTopRightRadius: 11,width: '100%', height: hp('11.8%')}}
          source={require('../image/room.png')}
       />
      <TouchableOpacity 
      style={{height:34, width:34, borderRadius:17, backgroundColor:'white', marginTop:-100,
         marginStart:wp('72%'), justifyContent:'center', alignItems:'center'}}
              onPress={() => this.onCancelPress()}>
      {/* <View style={{height:32, width:32, borderRadius:16, backgroundColor:'red', marginTop:-100,
         marginStart:wp('72%'), justifyContent:'center', alignItems:'center'}}> */}
          {/* <Image
          style={{width: 10, height:10}}
          source={require('../image/cancel.png')}
       />
        
         </TouchableOpacity>

        <View style={styles.firstRow}>
    <Text style={{color:'#034d94', fontWeight:'bold'}}>$210</Text>
    <Image
          style={{width: 15, height:15}}
          source={require('../image/heart.png')}
       />
       
    </View>
    <Text style={{color:'#939393', marginStart:wp('3.7%'), fontSize:10}}>4 Bed | 3 Balcony | 3 Bathrooms</Text>
     <Text style={{color:'black', marginStart:wp('3.7%'), fontWeight:'bold',}}>chicago, IL, USA</Text>
     <View style={styles.lastRow}>
     <Image
          style={{width: 18, height:18}}
          source={require('../image/clock.png')}
       />
     <Text style={{color:'#939393', marginStart:wp('1.6%'), fontSize:10 }}>Applied, Yesterday at 12:09 PM</Text>
      </View>
       
     </View>
      */}
          <ProgressDialog
            // title="Progress Dialog"
            activityIndicatorColor="red"
            activityIndicatorSize="large"
            animationType="fade"
            message="Please, wait..."
            visible={this.state.showProgress}
          />
        </View>
      </SafeAreaView>
    );
  }
}
const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "#ededed"
  },
  menuIcon: {
    height: "40%",
    resizeMode: "contain",
    width: 30,
    marginStart: 5
  },
  headerView: {
    position: "absolute",
    flexDirection: "row",
    backgroundColor: "white",
    justifyContent: "space-between",
    //paddingHorizontal: 10,
    alignItems: "center",
    height: hp("9%"),
    width: "100%"
  },
  headerIcon: {
    height: "30%",
    resizeMode: "center",
    width: 35
    // marginEnd: 5
  },
  tabView: {
    position: "absolute",
    height: hp("9%"),
    margin: wp("10%"),
    marginBottom: hp("5%"),
    marginTop: hp("85%"),
    //justifyContent: "flex-end",
    backgroundColor: "#034d94",
    borderRadius: 35,
    flexDirection: "row"
  },
  searchBox: {
    position: "absolute",
    height: hp("7%"),
    margin: wp("10%"),
    marginBottom: hp("5%"),
    marginTop: hp("11.5%"),
    //justifyContent: "flex-end",
    backgroundColor: "white",
    borderRadius: 35,
    flexDirection: "row",
    width: "80%"
  },
  subTab: {
    width: wp("23.1%"),
    margin: wp("1.5%"),
    justifyContent: "center",
    alignItems: "center",
    backgroundColor: "white",
    borderRadius: 33
  },
  tapImage: {
    height: "20%"
  },
  fontStyle: {
    fontSize: 12
  },
  map: {
    marginTop: hp("9%"),
    position: "absolute",
    left: 0,
    right: 0,
    height: hp("100%")
  },
  firstRow: {
    flexDirection: "row",
    height: "10%",
    // width:'100%',
    marginStart: wp("3.7%"),
    marginTop: hp("10%"),
    marginEnd: wp("3.7%"),
    marginBottom: hp("0.5%"),
    justifyContent: "space-between",
    alignItems: "center"
    // backgroundColor:'yellow'
  },
  lastRow: {
    flexDirection: "row",
    marginStart: wp("3.7%"),
    marginTop: -6,
    marginEnd: wp("3.7%"),
    alignItems: "center",
    flex: 1
  }
});