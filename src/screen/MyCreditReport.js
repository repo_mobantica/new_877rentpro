import React, { Component } from 'react';
import { Text, View, SafeAreaView, Image, TouchableOpacity, StyleSheet, Dimensions, Alert,
Button, FlatList } from 'react-native';
import {widthPercentageToDP as wp, heightPercentageToDP as hp} from 'react-native-responsive-screen';
import Toolbar from '../component/Toolbar';
import Api from "../Api/Api";
import RNPaypal from 'react-native-paypal-lib';
import Toast from 'react-native-simple-toast';
import { 
  ProgressDialog, 
} from "react-native-simple-dialogs";
import AsyncStorage from '@react-native-community/async-storage';
const monthNames = [" ", "January", "February", "March", "April", "May", "June",
  "July", "August", "September", "October", "November", "December"
];
var token='';
export default class MyCreditReport extends Component {

  state = {
    flatlistData:[],
      date:[]

  }
  async componentDidMount() {
    token = await AsyncStorage.getItem('Token');
    console.log('token', token);
    this.fetchData();
  }
    
  paymentGateway(id, amount){
    var that = this;
    RNPaypal.paymentRequest({
      clientId: 'ASV62g-yJsjYEKkbd6I-e2OG7dkKr1hB0rBrwQ7JFLrLCrRTTvXpED-wFxe8V8JZ1tJLg8kmxRNWBeyi',
      environment: RNPaypal.ENVIRONMENT.SANDBOX,
      intent: RNPaypal.INTENT.SALE,
      price: Number(amount),
      currency: 'USD',
      description: `Application Payment`,
      acceptCreditCards: true
      }).then(function (payment) {
       
        console.log('pymenttttt');
        console.log(payment);
        if(payment.response.id){
          this.setState({
            showProgress: true
          });
          Api("add_credit_payment", {
            user_id: token,
            cr_id:id,
            amount:amount
          })
      .then((responseJson) => {
       
      if(responseJson.status==1){
        that.fetchData();
      console.log('22222');
      console.log(responseJson.message);
      Toast.show(responseJson.message);
      
      }
      else if(responseJson==false){
        alert('No internet');
        this.setState({
          showProgress: false
        });
      }
      else{
      Alert.alert(responseJson.message);
      this.setState({
        showProgress: false
      });
      }
      }).catch((error) => {
      console.error(error);
      this.setState({
        showProgress: false
      });
      });
        }
       
      }).catch(err => {
       console.log(err.message);
      // alert("e"+err.message)
      })
  }
  fetchData(){
    this.setState({
      showProgress: true
    });
    Api("manage_creadit_reports", {
      userid:token
    })
      .then((responseJson) => {
        this.setState({
          showProgress: false
        });
    if(responseJson.status==1){
      console.log('22222');
      console.log(responseJson.response);
      let dataArray =  responseJson.response;
      var date = dataArray.map(function(item){
        if(item.date_of_birth != null){
         let splitDate = item.date_of_birth.split('-');
         return splitDate;
        }
        else{
         return null;
        }
      })
        
     this.setState({
      showProgress: false,
      flatlistData:responseJson.response,
      date:date,
    });
      }
      else if(responseJson==false){
        alert("No internet");
      }
    else{
      Alert.alert(responseJson.message);
    }
  }).catch((error) => {
  console.error(error);
  this.setState({
  showProgress: false
  });
  });
  }
 
  _renderItem = ({item, index}) => (
    <View style={styles.flatListView}>
   {/* <Image
          style={styles.displayImage}
          source={require('../image/room.png')}
       /> */}
        <View style={styles.firstRow}>
    <Text style={styles.prizeText}>{item.firstname} {item.lastname} </Text>
    <TouchableOpacity onPress={()=>this.props.navigation.navigate("ViewCreditRoportScreen",{id:item.id})}>
    <Image
          style={styles.imageStyle}
          source={require('../image/eye.png')}
       />
          </TouchableOpacity>
    </View>

      <Text style={styles.location}>SSN: {item.ssn}</Text>
       <View style={styles.lastRow}>
       <View style={{flexDirection:'row', }}>
       <Image
           style={{width: hp('2%'), height:hp('2%'), marginEnd:wp('2%') }}
          source={require('../image/birthday_cake.png')}
       />
          {this.state.date[index] != null ?
    <Text >{monthNames[Number(this.state.date[index][1])]} {Number(this.state.date[index][2])} {Number(this.state.date[index][0])}</Text>
         : <Text>Birthday</Text>}
    </View>
     <View style={styles.viewButton}> 
     {item.status=="1"?
       <Text style={{fontSize:10}}>PENDING</Text>:null
     }
     {item.status=="2"?
       <Text style={{fontSize:10}}>PROCESSING</Text>:null
     }
     {item.status=="3"?
       <Text style={{fontSize:10}}>COMPLETED</Text>:null
     }
     {item.status=="4"?
       <Text style={{fontSize:10}}>APPROVED</Text>:null
     }
     {item.status=="5"?
       <Text style={{fontSize:10}}>REJECTED</Text>:null
     }
     
     </View>
    </View>
     {/* <View style={styles.viewStyle}>
     <Text style={{color:'white'}}> Download Report</Text>
     </View> */}
     <View style={styles.acceptOrReject}>
    <TouchableOpacity style={styles.acceptView}
    //  onPress={()=> this.acceptReject('accpet', item.id)}>
    onPress={()=>console.log('h')}>
    <Text style={{color:'white'}}>Download Report</Text>
    </TouchableOpacity>
    {item.payment_status ==='0'?
    <TouchableOpacity style={styles.rejectView}
    onPress={()=> this.paymentGateway(item.id, item.amount)}>
   <Text style={{color:'white'}}>Make Payment</Text>
   </TouchableOpacity>
   :<TouchableOpacity style={styles.rejectView}
  >
  <Text style={{color:'white'}}>Payment Done</Text>
  </TouchableOpacity>}
    
     </View>
    </View>
  );
  render() {
    
    return (
       <SafeAreaView style={styles.container}>
      <View style={styles.container}>
     <Toolbar title="My Credit Reports" />
     
            <FlatList
          data={this.state.flatlistData}
          extraData={this.state}
          keyExtractor={(item, index) => index.toString()}
          renderItem={this._renderItem}
      />
         <ProgressDialog
          // title="Progress Dialog"
          activityIndicatorColor="red"
          activityIndicatorSize="large"
          animationType="fade"
          message="Please, wait..."
          visible={this.state.showProgress}
        />
          </View>
           </SafeAreaView>
    );
  }
}
const styles = StyleSheet.create({
container: {
    flex: 1,
   backgroundColor: "#ededed"
  },
  menuIcon: {
    height: "40%",
    resizeMode: "contain",
    width: 30,
    marginStart: 5
  },
   flatListView:{
    // height:hp('29%'),
     height:hp('20%'),
     marginStart:hp('3%'),
     marginEnd:hp('3%'),
     marginTop:hp('1.5%'),
     marginBottom:hp('1.5%'),
     borderRadius:13,
     backgroundColor:'white',
       
  },
  firstRow:{
    flexDirection:'row', 
   marginStart:wp('3.7%'),
   marginTop:wp('3.7%'),
    marginEnd:wp('3.7%'),
    justifyContent: "space-between",
    },
    lastRow:{
    flexDirection:'row', 
   marginStart:wp('3.7%'),
   marginEnd:wp('3.7%'),
   height:hp('4.5'),
   // backgroundColor:'red',
     alignItems:'center',
     marginBottom:hp('1%'),
     justifyContent: "space-between",
    },
   imageStyle:{ 
     width: hp('3%'), 
     height:hp('3%'),
      },
      viewStyle:{
        justifyContent:'center', 
        alignItems:'center', 
        height:hp('6.5%'),
      backgroundColor:'#034d94', 
      borderBottomLeftRadius:11,
       borderBottomRightRadius:11, 
      flexDirection:'row'
      },
      viewButton: {
        justifyContent:'center', 
        alignItems:'center',
         backgroundColor:'#d3eff5',
          borderRadius:15, 
          height:hp('3.5%'),
      borderColor:'#034d94',
       borderWidth:2,
         width:wp('23%')
         },
         displayImage:{
            borderTopLeftRadius: 11, 
        borderTopRightRadius: 11,
        width: '100%', 
        height: hp('10%')
        },
        location:{
          color:'black',
           marginStart:wp('3.7%'),
            fontWeight:'bold'
        },
        noOfItemtext:{
          color:'gray',
           marginStart:wp('3.7%'),
            fontSize:12
            },
            prizeText:{
              color:'#034d94',
               fontWeight:'bold'
               },
               acceptOrReject:{
                height:hp('6.5%'),
                 flexDirection:'row' 
              },
              acceptView:{
                borderBottomLeftRadius:11,
                 width:'50%',
                  backgroundColor:'#00508f',
                   justifyContent:'center',
                    alignItems:'center'
              },
              rejectView:{
                 width:'50%',
                 backgroundColor:'#008f2c',
                   borderLeftColor:'white',
                    borderLeftWidth :1,
                     justifyContent:'center',
                      alignItems:'center',
                  borderBottomRightRadius:11
              }            
});