import React, { Component } from 'react'
import { AppRegistry, StyleSheet, Text, View, Button, TouchableOpacity, Image, Alert } from 'react-native'
import ApplyForRentOne from '../component/ApplyForRentOne';
import ApplyForRentTwo from '../component/ApplyForRentTwo';
import ApplyForRentThree from '../component/ApplyForRentThree';
import ApplyForRentFour from '../component/ApplyForRentFour';
import ApplyForRentFive from '../component/ApplyForRentFive';
import ApplyForRentSix from '../component/ApplyForRentSix';
import ApplyForRentSeven from '../component/ApplyForRentSeven';
import ApplyForRentEight from '../component/ApplyForRentEight';
import Swiper from 'react-native-swiper';
import ToolbarWithNavigation from '../component/ToolbarWithNavigation';
import AsyncStorage from '@react-native-community/async-storage';
import {widthPercentageToDP as wp, heightPercentageToDP as hp} from 'react-native-responsive-screen';
import Toast from 'react-native-simple-toast';
import RNPaypal from 'react-native-paypal-lib';
import Api from "../Api/Api";
import { 
  ProgressDialog, 
} from "react-native-simple-dialogs";

var token= '';
var Property_id ='';
 var idUrlUpload ='';
 var incomeUrlUpload = '';
 var resultData;
const renderPagination = (index, total, context) => {

  return (
    <View >
      <Text style={{ color: 'grey' }}>
        <Text >{index + 1}</Text>/{total}
      </Text>
    </View>
  )
}

export default class ApplyForRent extends Component {

  constructor(props){
    super(props);
    this.onPressNext = this.onPressNext.bind(this);
    this.onPressPrev = this.onPressPrev.bind(this);
    this.state = {
      idxActive: 0,
      applyOne:'',
      applyTwo:'',
      applyThree:'',
      applyFour:'',
      applyFive:'',
      applySix:'',
      applySeven:'',
      applyEight:'',
      showProgress:false
    }
 }

 
 onPressPrev = () => {
  const {idxActive} = this.state;
  if (idxActive > 0) {
    this.refs.swiper.scrollBy(-1)
  }
  else{
    //  this.props.navigation.navigate('PropertyInfo');
   }
}

componentDidMount(){
  console.log('9999999');
  this.getData();
   Property_id =  this.props.navigation.getParam('Property_id');
}
 async getData(){
    try {
      const value = await AsyncStorage.getItem('Token');
      console.log('valueeeeeee',value);
      token = value;
      // if(value !== null) {
      //  console.log('valueeeeeee',value);
      // }
    } catch(e) {
      // error reading value
    }
  }

onSubmitPress = () => {
  this.refs.swiper.scrollBy(1);
  const {idxActive} = this.state;
  console.log('active pageeee');
  console.log(this.state.idxActive);
 // Probably best set as a constant somewhere vs a hardcoded 5
 let eight = this.refs.applyRentEight.nextPress();
    if(eight.isNext){
  this.setState({
       applyEight: eight
     },function(){
       
      for(let i =0; i<2; i++){
        console.log('iiiii', i);
        if(i== 0){
         this.convertImage('idUrl');
        }
        else{
         this.convertImage('incomeUrl');
        console.log('incomeUrl');
        }
      }
    })
    }
  }
 
  convertImage(item){
          var baseImage;
          console.log('item', item);
            if(item === 'idUrl'){
              baseImage = this.state.applyEight.idUrl;
            }
            else if(item === 'incomeUrl'){
              baseImage = this.state.applyEight.incomeUrl;
            }
            this.setState({
              showProgress: true
            });
            fetch('https://1800rentpro.com/rentpro_api/api/upload_image', {
            method: 'POST',
            headers: {
                    'Accept': 'application/json',
                    'Content-Type': 'application/json',
                      },
            body: JSON.stringify({
              image: baseImage,
              type:"property"
                  })
          }).then((response) => response.json())
            .then((responseJson) => {
            
          if(responseJson.status==1){
          
          console.log('mouse');
        console.log(responseJson);
            
        if(item === 'idUrl'){
         idUrlUpload = responseJson.response.data;
         console.log('idUrl');
         console.log(idUrlUpload);
        }
        else if(item === 'incomeUrl'){
          incomeUrlUpload = responseJson.response.data;
          console.log('idUrl');
          console.log(incomeUrlUpload);
          this.saveData();
        }
            }
          else{
            Alert.alert(responseJson.message);
            this.setState({
              showProgress: false
            });                  
          }
        }).catch((error) => {              
      console.error(error);
      this.setState({
        showProgress: false
      });
    });
  
       
  }
    saveData(){
      console.log('idUrlUpload',idUrlUpload);
      console.log('incomeUrlUpload',incomeUrlUpload);
       //  if(idUrlUpload != '' && incomeUrlUpload != ''){
      fetch('https://1800rentpro.com/rentpro_api/api/update_application', {
            method: 'POST',
            headers: {
                    'Accept': 'application/json',
                    'Content-Type': 'application/json',
                      },
            body: JSON.stringify({
              application_id: "",  userid: token, propertyid: Property_id,  application_type: "property", 

              firstname: this.state.applyOne.firstName,  

             lastname: this.state.applyOne.lastName,

             email: this.state.applyOne.email,

             mobile:this.state.applyOne.mobileNo,

             dateofbirth:this.state.applyOne.birthday,

             work_phone: this.state.applyOne.workPhone,

             home_phone: "",

             guarantor: this.state.applyOne.guarantor,

               ssn: this.state.applyOne.SSN,

               id_type:this.state.applyOne.idType, 

              id_number: this.state.applyOne.idNumber, 

               present_address: this.state.applyTwo.address,

             present_state: this.state.applyTwo.state,

             present_city: this.state.applyTwo.city,

             present_zip: this.state.applyTwo.zip,

             present_manager_name:this.state.applyTwo.managerName,

             present_manager_phone: this.state.applyTwo.managerPhone,

             present_moving_reason: "abc",

             present_rent_per_month: "432543",

             present_date_in: "2019-09-30",

             present_date_out: "2019-10-30",

             bank_name: this.state.applyThree.bankName,

             acc_type: this.state.applyThree.accountType,

             acc_no: this.state.applyThree.accountNumber,

             avg_monthly_bal: this.state.applyThree.avgBalance,

              fin_obl_creditor_name_1: this.state.applyFour.financeCreditorName,

             fin_obl_address_1: this.state.applyFour.financeAddress,

             fin_obl_phone_1: this.state.applyFour.financePhone,

             fin_obl_monthly_payment_1: this.state.applyFour.finanacePayment,

             fin_obl_creditor_name_2: "",

             fin_obl_address_2: "",

             fin_obl_phone_2: "",

             fin_obl_monthly_payment_2: "",

             emergencycontact_name:this.state.applyFive.emergencyName,

             emergencycontact_address:this.state.applyFive.emergencyAddress,

             emergencycontact_relationship: this.state.applyFive.emergencyRelationship,

             emergencycontact_phone:this.state.applyFive.emergencyPhone,

              cats: this.state.applySix.cat,

             dogs:this.state.applySix.dog,

             tenant_emp_status: this.state.applySix.tenanatIs,

             occupation: this.state.applySix.occupation,

             income: this.state.applySix.income,

             frequency_1: this.state.applySix.frequency,

             other_sources: this.state.applySix.otherSource,

             amount: "",

             frequency_2: "1",

             occupants: "1",

             lease_period: "1",

             move_in_date: "2019-10-30",

             employer_name: this.state.applySix.employerName,

             employer_address:this.state.applySix.employerAdress,

             start_date: this.state.applySix.dateFrom,

             end_date: this.state.applySix.dateTo,

               proposed_occupants_name_1: this.state.applySeven.occupant[0].name,

             proposed_occupants_age_1:this.state.applySeven.occupant[0].age,

             proposed_occupants_dob_1: this.state.applySeven.occupant[0].birthday,

             proposed_occupants_mobile_no_1: this.state.applySeven.occupant[0].mobileNo,

             proposed_occupants_name_2: this.state.applySeven.occupant[1].name,

             proposed_occupants_age_2: this.state.applySeven.occupant[1].age,

             proposed_occupants_dob_2:this.state.applySeven.occupant[1].birthday,

             proposed_occupants_mobile_no_2: this.state.applySeven.occupant[1].mobileNo,

             proposed_occupants_name_3: this.state.applySeven.occupant[2].name,

             proposed_occupants_age_3: this.state.applySeven.occupant[2].age,

             proposed_occupants_dob_3: this.state.applySeven.occupant[2].birthday,

             proposed_occupants_mobile_no_3:this.state.applySeven.occupant[2].mobileNo,

             proposed_occupants_name_4: this.state.applySeven.occupant[3].name,

             proposed_occupants_age_4: this.state.applySeven.occupant[3].age,

             proposed_occupants_dob_4: this.state.applySeven.occupant[3].birthday,

             proposed_occupants_mobile_no_4:this.state.applySeven.occupant[3].mobileNo,

             proposed_occupants_name_5: this.state.applySeven.occupant[4].name,

             proposed_occupants_age_5: this.state.applySeven.occupant[4].age,

             proposed_occupants_dob_5  :this.state.applySeven.occupant[4].birthday,

             proposed_occupants_mobile_no_5:this.state.applySeven.occupant[4].mobileNo,

             proposed_occupants_name_6: this.state.applySeven.occupant[5].name,

             proposed_occupants_age_6: this.state.applySeven.occupant[5].age,

             proposed_occupants_dob_6: this.state.applySeven.occupant[5].birthday,

             proposed_occupants_mobile_no_6:this.state.applySeven.occupant[5].mobileNo,

             contact_first_name: this.state.applySeven.referanceContact[0].name,

             contact_first:this.state.applySeven.referanceContact[0].mobileNo,

             contact_second_name: this.state.applySeven.referanceContact[1].name,

             contact_second:this.state.applySeven.referanceContact[1].mobileNo,

             supervisor_contact_name: this.state.applySeven.supevisorName,

             supervisor_contact: this.state.applySeven.supervisorMobile,

             vehicle_make:this.state.applySeven.vehicalMake,

             vehicle_model: this.state.applySeven.vehicalModel,

             vehicle_year: this.state.applySeven.VehicalYear,

             vehicle_lic: this.state.applySeven.vehicalLisence,

               is_evicted: this.state.applyEight.radioOne,

             is_bankrupt: this.state.applyEight.radioThree,

             is_convicted: this.state.applyEight.radioThree,

             primary_bank: "",

             id_proof_path_first: idUrlUpload,

             id_proof_path_second: incomeUrlUpload,

             signature: this.state.applyEight.signUrl,
              
                  })
          }).then((response) => response.json())
            .then((responseJson) => {
            
          if(responseJson.status==1){
            this.setState({
              showProgress: false
            });
            console.log('22222');
            console.log(responseJson.response);
            Toast.show(responseJson.message);
            resultData= responseJson.response.data;
            if(responseJson.success){
              RNPaypal.paymentRequest({
                clientId: 'ASV62g-yJsjYEKkbd6I-e2OG7dkKr1hB0rBrwQ7JFLrLCrRTTvXpED-wFxe8V8JZ1tJLg8kmxRNWBeyi',
                environment: RNPaypal.ENVIRONMENT.SANDBOX,
                intent: RNPaypal.INTENT.SALE,
                price: Number(resultData.amount),
                currency: 'USD',
                description: `Application Payment`,
                acceptCreditCards: true
                }).then(function (payment) {
                  //sendPaymentToConfirmInServer(payment, confirm);
                  console.log('pymenttttt');
                  console.log(payment);
                  if(payment.response.id){
                    Api("add_payment_data", {
                      user_id: token,
                      propertyid: Property_id,
                      amount:resultData.amount,
                      totalamount: resultData.amount,
                      payment_type: "application",
                      payment_date: new Date().getFullYear()+'-'+("0"+(new Date().getMonth()+1)).slice(-2)+'-'+("0"+new Date().getDate()).slice(-2),
                      pay_type_id: resultData.pay_type_id
                    })
                .then((responseJson) => {
                 
                if(responseJson.status==1){
                console.log('22222');
                console.log(responseJson.message);
                Toast.show(responseJson.message);
                }
                else if(responseJson==false){
                  alert('No internet');
                }
                else{
                Alert.alert(responseJson.message);
                }
                }).catch((error) => {
                console.error(error);
                
                });
                  }
                 
                }).catch(err => {
                 console.log(err.message);
                // alert("e"+err.message)
                })
            }
           
           this.setState({
            showProgress: false
          });
            }
          else{
            Alert.alert(responseJson.message);
            this.setState({
              showProgress: false
            });
            
          }
        }).catch((error) => {
      console.error(error);
      this.setState({
        showProgress: false
      });
    });
    // }
    // else{
    //   console.log('imagw is not available');
     
    //   if(idUrlUpload != ''){
    //     Alert.alert('id proof upload failed!!');
    //   }
    //   else if(incomeUrlUpload != ''){
    //     Alert.alert('income proof upload failed!!');
    //   }
    //   this.setState({
    //     showProgress: false
    //   });
    // }
  }

      //   paymentApi(){
      //     Api("add_payment_data", {
      //       user_id: "120",
      //       propertyid: "778",
      //       amount:"1500",
      //       totalamount: "1570",
      //       payment_type: "application",
      //       payment_date: "2019-07-10",
      //       pay_type_id: "12"
      //     })
      // .then((responseJson) => {
      //   this.setState({
      //     showProgress: false
      //   });
      // if(responseJson.status==1){
      // console.log('22222');
      // console.log(responseJson.message);
      // Toast.show(responseJson.message);
      // }
      // else if(responseJson==false){
      //   alert('No internet');
      // }
      // else{
      // Alert.alert(responseJson.message);
      // }
      // }).catch((error) => {
      // console.error(error);
      // this.setState({
      // showProgress: false
      // });
      // });
      //   }
onPressNext = () => {
  const {idxActive} = this.state;
//  let eight = this.refs.applyRentEight.nextPress();
//  console.log('eight',eight);
 
 if (idxActive < 8) {
    switch (this.state.idxActive) {
      case 0:
        let one = this.refs.applyRentOne.nextPress();
        console.log('oneeee', one);
        if(one.isNext){
          this.setState({
            applyOne:one
          })
          this.refs.swiper.scrollBy(1);
        }
        break;
        case 1:
          let two = this.refs.applyRentTwo.nextPress();
          if(two.isNext){
            this.setState({
              applyTwo:two
            })
            this.refs.swiper.scrollBy(1);
          }
          break;
          case 2:
            let three = this.refs.applyRentThree.nextPress();
          if(three.isNext){
            this.setState({
              applyThree:three
            })
            this.refs.swiper.scrollBy(1);
          }
          break;
          case 3:
            let four = this.refs.applyRentFour.nextPress();
          if(four.isNext){
            this.setState({
              applyFour:four
            })
            this.refs.swiper.scrollBy(1);
          }
          break;
          case 4:
            let five = this.refs.applyRentFive.nextPress();
          if(five.isNext){
            this.setState({
              applyFive:five
            },function(){
            //   console.log('this.state.applyFive');
            // console.log(this.state.applyFive);
            })
            this.refs.swiper.scrollBy(1);
          }
          break;
          case 5:
            let six = this.refs.applyRentSix.nextPress();
          if(six.isNext){
            this.setState({
              applySix:six
            },function(){
              console.log('this.state.applySix');
            console.log(this.state.applySix);
            })
            this.refs.swiper.scrollBy(1);
          }
          break;
             case 6:
            let seven = this.refs.applyRentSeven.nextPress();
          if(seven.isNext){
            this.setState({
              applySeven:seven
            },function(){
              console.log('this.state.applySeven');
            console.log(this.state.applySeven);
            })
            this.refs.swiper.scrollBy(1);
          }
          break;
          
      //  default:
      //     //  this.refs.swiper.scrollBy(1);
     }
  }
}
  render() {
    return (

      <View style={{flex:1}}>
        {/* <ToolbarWithNavigation title="Apply For Rent" parentMethod={this.onPressNext}/> */}
        <View style={{height:'9%', backgroundColor:'white', paddingLeft:'7%', paddingRight:'7%',
         flexDirection:'row', justifyContent:'space-between', alignItems:'center'}}>
          <TouchableOpacity 
          
          onPress={this.onPressPrev}>
          <Image source={require("../image/left.png")} style={{width:25,height:20 }} />
          </TouchableOpacity>
          <Text style={{textAlign:"center", color:'#034d94',  fontWeight:'bold', fontSize:18}}>Apply For Rent</Text>
          <View style={{height:10, width:10, backgroundColor:'white'}}></View>
        </View>

        <Swiper
          style={styles.wrapper}
          renderPagination={renderPagination}
          showsButtons={false}
           showsPagination={false}
          scrollEnabled={false}
          loop={false}
          ref={'swiper'}
          dot={true}
          onIndexChanged={idxActive => this.setState({idxActive},function(){
           // console.log('nextttt', this.state.idxActive);
          }) }
        >
          {/* <View style={styles.slide}> */}
        
            <ApplyForRentOne   ref="applyRentOne"/>
          {/* </View> */}
           <ApplyForRentTwo ref="applyRentTwo" />
           <ApplyForRentThree ref="applyRentThree" />
           <ApplyForRentFour ref="applyRentFour" />
           <ApplyForRentFive ref="applyRentFive" />
           <ApplyForRentSix ref="applyRentSix"/>
           <ApplyForRentSeven ref="applyRentSeven"/>
           <ApplyForRentEight ref="applyRentEight"/>
            
           </Swiper>  

           <View style={styles.footer}>
          <View style={{flexDirection:'row',}}>
          <View style={styles.indicator}></View>
           <View style={this.state.idxActive >=1 ? styles.indicator : styles.inactiveIndicator}></View>
            <View style={this.state.idxActive >=2 ? styles.indicator : styles.inactiveIndicator}></View>
             <View style={this.state.idxActive >=3 ? styles.indicator : styles.inactiveIndicator}></View>
            <View style={this.state.idxActive >=4 ? styles.indicator : styles.inactiveIndicator}></View>
            <View style={this.state.idxActive >=5 ? styles.indicator : styles.inactiveIndicator}></View>
            <View style={this.state.idxActive >=6 ? styles.indicator : styles.inactiveIndicator}></View>
            <View style={this.state.idxActive >=7 ? styles.indicator : styles.inactiveIndicator}></View>
         </View>
           
      {this.state.idxActive >=7 ?
           <TouchableOpacity style={[styles.nextBtn,{  backgroundColor:'#008f2c',}]}
       onPress={this.onSubmitPress} >
       <Text style={styles.nextText}>SAVE</Text>
       </TouchableOpacity>
      :
       <TouchableOpacity style={styles.nextBtn}
      //  onPress={this.onPressNext} >
         // onPress={()=>this.saveData()} >
            onPress={this.onPressNext} >
         
       <Text style={styles.nextText}>Next</Text>
       </TouchableOpacity> 
      }
       <ProgressDialog
          // title="Progress Dialog"
          activityIndicatorColor="red"
          activityIndicatorSize="large"
          animationType="fade"
          message="Please, wait..."
          visible={this.state.showProgress}
        />
      </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  wrapper: {},
   indicator:{ 
         height:12,
          width:12,
          borderRadius:6,
          backgroundColor:'#008f2c',
         margin:wp('1%'),
       },
       inactiveIndicator:{ 
         height:12,
          width:12,
          borderRadius:6,
          backgroundColor:'white',
          borderWidth:0.5,
         margin:wp('1%'),
       },
       nextBtn:{
         height:hp('7%'),
       backgroundColor:'#034d94',
       width:wp('25%'),
      borderRadius:10,
     justifyContent:'center',
       alignItems:'center',
       flexDirection:'row', 
       paddingStart:wp('2%'),
        paddingEnd:wp('2%')
       },
       nextText:{
         fontSize:13,
          fontWeight:'bold',
           color:'white'
           },
          footer:{
            backgroundColor:'white',
             height:'11%',
              justifyContent:'space-between', 
              flexDirection:'row',
            alignItems:'center',
            paddingStart:'7%',
           paddingEnd:'7%'
       },
         signUpBtn:{
   height:hp('7%'),
    backgroundColor:'#008f2c',
     width:'46%',
      marginStart:'4%', 
     marginEnd:'2%',
   borderRadius:10,
    justifyContent:'center',
     alignItems:'center'
     },
})