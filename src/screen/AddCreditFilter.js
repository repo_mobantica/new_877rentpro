import React, { Component } from 'react';
import { Text, View, SafeAreaView, Image, TouchableOpacity, StyleSheet, Dimensions, Alert,
Button, FlatList, TextInput, ScrollView } from 'react-native';
import {widthPercentageToDP as wp, heightPercentageToDP as hp} from 'react-native-responsive-screen';
import ToolbarHomeImg from '../component/ToolbarHomeImg';
import DateTimePicker from "react-native-modal-datetime-picker";
import Dash from 'react-native-dash';
import commonStyles from "../styles/index";
import ToolbarForFilter from '../component/ToolbarForFilter'
import RNPickerSelect from 'react-native-picker-select';
var count;
export default class AddCreditFilter extends Component {
    constructor(props) {
        super(props);
       this.state = {
          state:'',
          toDate:'',
          fromDate:'',
    city:'',
    toFrom:''
  };
}

showDateTimePicker(item){
  count = item;
  this.setState({ isDateTimePickerVisible: true,
  toFrom:item });
};

hideDateTimePicker = () => {
  this.setState({ isDateTimePickerVisible: false });
};

handleDatePicked = date => {
  console.log("A date has been picked: ", date);
   let day =date.getDate();
  let month =date.getMonth() +1;
  let year =date.getFullYear();
   let fullDate = day + '/' +month +'/' +year;
   console.log("A date has been picked: ", fullDate);
   if(this.state.toFrom === '0'){
    this.setState({ fromDate: fullDate,
    });
   }
   else{
    this.setState({ toDate: fullDate,
    });
   }
 
  this.hideDateTimePicker();
};
  
  render() {
    
    return (
       <SafeAreaView style={styles.container}>
      <View style={styles.container}>
          <ToolbarForFilter image title={"Dashboard"} />
           <View style={styles.mainView}>
         <View style={{height:'95%',}}>
           {/* <Text style={[styles.nameStyle, {marginTop:'10%'}]}>Search Text</Text> */}
           {/* <TextInput
          style={styles.boxStyle}
          placeholder="Search Text"
        /> */}
         <Text style={commonStyles.nameStyle}>Search Text</Text>
           <TextInput
          style={styles.textInput}
           underlineColorAndroid = "transparent"
           placeholderTextColor={'gray'}
          placeholder="Search Text"
          onChangeText={(email) => this.setState({email:email, 
          emailError:false})}
          returnKeyType = {"next"}
           autoFocus = {true}
         onSubmitEditing={(event) => { 
            this.refs.FirstN.focus(); 
          }}
          value={this.state.email}
        />
        <View style={commonStyles.textinputBorder}></View>

        <View style={{flexDirection:'row', }}>
        <View style={styles.fromText}>
        <Text style={styles.nameStyle}>From</Text>
        </View>
        <View style={styles.fromText}>
        <Text style={styles.nameStyle}>To</Text>
        </View>
        </View>
        <View style={{flexDirection:'row'}}>
        <View  style={styles.dateBox}>
        <Text>{this.state.fromDate}</Text>
         <TouchableOpacity 
        style={styles.calenderIconView}
       onPress={() => this.showDateTimePicker('0')}>
         <Image
           style={styles.calenderIcon}
          source={require('../image/calendar.png')}
       />
      </TouchableOpacity>
          </View>
          <View  style={styles.dateBox}>
         <Text>{this.state.toDate}</Text>
        <TouchableOpacity 
        style={styles.calenderIconView}
       onPress={() => this.showDateTimePicker('1')}>
         <Image
            style={styles.calenderIcon}
          source={require('../image/calendar.png')}
       />
       </TouchableOpacity>
       </View>
          </View>

          <Text style={commonStyles.nameStyle}>Status</Text>
        {/* <View  style={styles.boxStyle}>
                <RNPickerSelect
            onValueChange={(value) => this.setState({city:value, 
              cityError:false})}
            items={[
                { label: 'pune', value: 'pune' },
                { label: 'Mumbai', value: 'Mumbai' },
                { label: 'Delhi', value: 'Delhi' },
            ]}
        />
          </View> */}

<View style={styles.textInput}>
             <RNPickerSelect
            onValueChange={(value) => this.setState({city:value, 
              cityError:false})}
            items={[
                { label: 'pune', value: 'pune' },
                { label: 'Mumbai', value: 'Mumbai' },
                { label: 'Delhi', value: 'Delhi' },
            ]}
        />
        </View>
        <View style={commonStyles.textinputBorder}></View>
          </View>
          <View style={styles.offerOuterView}>
                  <View style={styles.rentApplyView}>
                <Text style={styles.whiteText}>Search</Text>
               </View> 
               <View style={styles.makeOfferView}>
              <Text style={styles.whiteText}>Reset</Text>
               </View> 
               </View>
          
           </View>
           <DateTimePicker
          isVisible={this.state.isDateTimePickerVisible}
          onConfirm={this.handleDatePicked}
          onCancel={this.hideDateTimePicker}
        />  
          </View>
           </SafeAreaView>
    );
  }
}
const styles = StyleSheet.create({
container: {
    flex: 1,
   backgroundColor: "#ededed",
    paddingBottom:'4%',
  },
  mainView:{
    backgroundColor:'white',
  // flex:1,
  //marginBottom:'12%',
 // marginTop:'12%',
   height:'90%',
   margin:'5%', 
  borderRadius:10,
  },
  nameStyle:{
   marginTop:'8%', 
     marginStart:wp('10%'),
     fontWeight:'bold'
  // backgroundColor:'yellow', 
 },
 whiteText:{
    color:'white'
  },
rentApplyView:{
width:'50%',
 backgroundColor:'#034d94', 
borderBottomLeftRadius: 11,
 borderEndWidth:1,
 justifyContent:'center', 
alignItems:'center',
 borderColor:'white'
},
makeOfferView:{
justifyContent:'center',
 alignItems:'center',
borderBottomRightRadius: 11,
 width:'50%',
  backgroundColor:'#034d94',
   borderColor:'white'
},
offerOuterView:{
height:'9%', 
 flexDirection:'row'
}, 
boxStyle:{
    borderWidth: 2,  // size/width of the border
            borderColor: 'lightgrey',  // color of the border
            paddingLeft: 10,
            height: 50,
            width:'86%',
            margin:'7%',
            marginTop:'4%'
},
fromText:{
    height:35,
     justifyContent:'center', 
     width:'50%', 
},
dateBox:{
    borderWidth: 2,  
    borderColor: 'lightgrey', 
    height: 50,
    width:'40%',
    marginStart:'7%',
    flexDirection:'row',
    alignItems:'center',
    justifyContent:'space-between'
  },
  calenderIcon:{
    width: hp('2%'), 
     height:hp('2%'),
      marginEnd:wp('2%') 
  },
  textInputStyle:{
    height: 50,
    width:'70%',
  },
  textInput:{
    marginStart:wp('10%'), 
   //  marginTop:-hp('1.3%'),
     //backgroundColor:'yellow', 
    // height:hp('5%')
 },
 textinputBorder:{
   marginStart:wp('5%'), 
   marginEnd:wp('5%'), 
   marginBottom:'8%',
    backgroundColor:'gray',
     height:1,
      width:'85%',
      marginTop:-hp('1%')
      },
      nameStyle:{
        marginTop:'8%', 
         marginStart:wp('10%'),
      // backgroundColor:'yellow', 
     },
});
