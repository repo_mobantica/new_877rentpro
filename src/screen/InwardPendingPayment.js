import React, { Component } from 'react';
import { Text, View, SafeAreaView, Image, TouchableOpacity, StyleSheet, Dimensions, Alert,
Button, FlatList } from 'react-native';
import {widthPercentageToDP as wp, heightPercentageToDP as hp} from 'react-native-responsive-screen';
import ToolbarWithLeftIcon from '../component/ToolbarWithLeftIcon';
import AsyncStorage from '@react-native-community/async-storage';
import Api from "../Api/Api";
import { 
  ProgressDialog, 
} from "react-native-simple-dialogs";
const monthNames = [" ", "January", "February", "March", "April", "May", "June",
  "July", "August", "September", "October", "November", "December"
];
var token='';
export default class InwardPendingPayment extends Component {
 
  state = {
    dialogVisible: false,
    flatlistData:[],
    date:[],
    showProgress: false
  }

  async componentDidMount(){
    token = await AsyncStorage.getItem('Token');
   console.log('hasss permission');
    console.log( 'token', token);
   this.fetchData();
}

fetchData(){
  this.setState({
    showProgress: true
  });
  Api("inward_pending_payments", {
    userid:token
  })
    .then((responseJson) => {
      this.setState({
        showProgress: false
      });
  if(responseJson.status==1){
    console.log('22222');
    let dataArray =  responseJson.response;
    var date = dataArray.map(function(item){
      if(item.due_date != null){
       let splitDate = item.due_date.split('-');
       return splitDate;
      }
      else{
       return null;
      }
    })
    console.log('date', responseJson.response);
    this.setState({
      flatlistData: responseJson.response,
      date:date,
    },function(){
      console.log('flatlistData');
      console.log(this.state.flatlistData);
    })
    }
     else if(responseJson==false){
      alert("No internet");
    }
  else{
    Alert.alert(responseJson.message);
  }
}).catch((error) => {
console.error(error);
this.setState({
showProgress: false
});
});
}

  _renderItem = ({item, index}) => {
    console.log('iteem');
    console.log(item);
    return(
    <View style={styles.flatListView}>
   <Image
          style={styles.displayImg}
          source={{uri: item.image_url}}
       />
        <View style={styles.firstRow}>
    <Text style={styles.priceText}>${item.amount}</Text>
    <Image
          style={{width: 21, height:21}}
          source={require('../image/eye.png')}
       />
    </View>
     <Text style={styles.addressText} numberOfLines={1}>{item.property_address}</Text>
     <View style={styles.lastRow}>
     <Image
          style={{width: 10.5, height:10.5}}
          source={require('../image/calendar.png')}
       />

{this.state.date[index] != null ?
    <Text style={styles.dateText}> {monthNames[Number(this.state.date[index][1])]} {Number(this.state.date[index][2])} {Number(this.state.date[index][0])}</Text>
    :
    <Text style={styles.dateText}> Due date </Text>
        }
    
     <View style={styles.paidView}> 
     {item.status === '0' ?
      <Text style={styles.fontSize}>UNPAID</Text>
      : <Text style={styles.fontSize}>PAID</Text>
    }
     </View>
     </View>
    </View>
  );
 }
  render() {
    
    return (
       <SafeAreaView style={[styles.container,{backgroundColor:'red'}]}>
      <View style={styles.container}>
     <ToolbarWithLeftIcon title="Inward pending Payment" />
            <FlatList
        data={this.state.flatlistData}
        extraData={this.state}
       keyExtractor={(item, index) => index.toString()}
        renderItem={this._renderItem}
      />
      <ProgressDialog
          // title="Progress Dialog"
          activityIndicatorColor="red"
          activityIndicatorSize="large"
          animationType="fade"
          message="Please, wait..."
          visible={this.state.showProgress}
        />
          </View>
           </SafeAreaView>
    );
  }
}
const styles = StyleSheet.create({
container: {
    flex: 1,
   backgroundColor: "#ededed"
  },
  menuIcon: {
    height: "40%",
    resizeMode: "contain",
    width: 30,
    marginStart: 5
  },
   flatListView:{
     height:hp('24%'),
     margin:hp('3%'),
     marginTop:hp('3%'),
     marginBottom:hp('0%'),
     borderRadius:11,
     backgroundColor:'white',
       
  },
  firstRow:{
    flexDirection:'row', 
   marginStart:wp('3.7%'),
   marginTop:wp('3.7%'),
    marginEnd:wp('3.7%'),
    justifyContent: "space-between",
    },
    lastRow:{
      flexDirection:'row', 
   marginStart:wp('3.7%'),
   marginTop:-6,
    marginEnd:wp('3.7%'),
   alignItems:'center',
   flex:1
    },
   addressText:{
     color:'black',
     marginStart:wp('3.7%'),
     fontWeight:'bold',
   },
   paidView:{
     justifyContent:'center',
      alignItems:'center',
      backgroundColor:'#cbe8d8', 
      borderRadius:15, 
      height:hp('3%'),
       borderColor:'green',
   borderWidth:2,
  //  marginStart:'41.5%', 
    width:65
   },
   dateText:{
     color:'#939393',
      marginStart:wp('1.6%'),
       fontSize:12, width:'72%'
   },
   fontSize:{
     fontSize:13
   },
  priceText:{
    color:'#034d94', 
    fontWeight:'bold'
  },
displayImg:{
  width: '100%',
   height: hp('10%'),
     borderTopLeftRadius: 11, 
      borderTopRightRadius: 11,
}
});