import React, { Component } from "react";

import {
  Text,
  View,
  SafeAreaView,
  Image,
  TouchableOpacity,
  StyleSheet,
  Dimensions,
  Alert,
  AsyncStorage,
  Button,
  FlatList,
  TextInput,
  ScrollView
} from "react-native";
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp
} from "react-native-responsive-screen";
import CheckBox from "react-native-check-box";
import Api from "../Api/Api";
import RadioForm, {
  RadioButton,
  RadioButtonInput,
  RadioButtonLabel
} from "react-native-simple-radio-button";
import commonStyles from "../styles/index";
import RNPicker from "rn-modal-picker";
import Toast from 'react-native-simple-toast';
import { 
  ProgressDialog, 
} from "react-native-simple-dialogs";
import DateTimePicker from 'react-native-modal-datetime-picker';
import ToolBarForProfile from "../component/ToolBarForProfile";
var radio_props = [
  { label: "Landlord", value: 2 },
  { label: "Tenant", value: 3 }
];
 var country =[];
 var stateData =[];
 var cityData =[];
 var token ='';
export default class MyProfile extends Component {
  constructor(props) {
    super(props);

    this.state = {
      isDateTimePickerVisible: false,
      firstname: "",
      lastname:'',
      middleName:'',
      tenant: false,
      manager: false,
      landlord: false,
      broker: false,
      countryList:[],
      stateList:[],
      cityList:[],
      address: "",
      email: "",
      message: "",
      mobileNumber: "",
      countryId: "",
      stateId: "",
      cityId: "",
      birthday: "",
      income: "",
      userId: "2",data:"",
      selectedCountry: "",
      selectedState:'',
      selectedCity:'',
      value:'2',
      showProgress: false,
      validAge: false,
      birthToSend:'',
      user: ['Landloard', 'Tenant'],
    };
  }

  async componentDidMount() {
   token = await AsyncStorage.getItem("Token");
   this.countryData(); 
    this.getProfileData();
   
  }
   
  showDateTimePicker = () => {
    this.setState({ isDateTimePickerVisible: true });
  };
 
  hideDateTimePicker = () => {
    this.setState({ isDateTimePickerVisible: false });
  };
 
  handleDatePicked = date => {
     let day =date.getDate();
    let month =date.getMonth() +1;
    let year =date.getFullYear();
     let fullDate = day + '/' +month +'/' +year;
     let birthToSend = year + '-' +month+ '-'+ day;
     
    this.setState({ birthday: fullDate,
      birthToSend:birthToSend,
        birtthdayError:false });
    this.hideDateTimePicker();
     
    let age = 18;
    var setDate = new Date(year + age, month - 1, day);
    var currdate = new Date();
    
    if (currdate >= setDate) {
     
      this.setState({
        validAge: false
      })
    } else {
      this.setState({
        validAge: true
      })
    }

  };

  getState(id, name, value){
       console.log('plzz set country');
        console.log('id', id);
        console.log('name', name);
        console.log('value', value);
       
    this.setState({
      selectedCountry: name,
      phoneCode:value,
      selectedCity:'',
      selectedState:'',
      countryId:value,

      countryError:false},function(){
        // console.log('plzz set country');
        // console.log(index+1, '   ', name);
      }); 
      var data =  this.props.navigation.getParam('state_list');

    var filterArray = stateData.filter(
      data => data.country_id === value,
    );
      console.log('filterArray filterArray', filterArray);
      var stateList = filterArray.map(item =>  ( item ) );
      console.log('stateList stateList', stateList);
        this.setState({
          stateList: stateList,
        });
       
     // });
    // });
  }
 
  getCity(id, name, value){
    console.log('state data show ');
    console.log('state id', value);
    console.log('state name', name);
        console.log('state value', value);
    this.setState({
      selectedState: name,
      selectedCity:'',
      stateId:value
    },function(){
      // console.log('plzz set state');
      // console.log(id, '   ', name);
    })
   
      var filterArray = cityData.filter(
      data => data.state_id === value,
    );
     var cityList = filterArray.map(item =>  ({ name:item.name, id:item.id }) );
       
        this.setState({
          cityList: cityList,
        });
    
  }
  _selectedCity(id, name, value) {
    console.log('plzz set city');
            console.log('city id', id);
            console.log('city name', name);
            console.log('city value', value);
    this.setState({ selectedCity: name, cityId:value });
  }

  getProfileData(){
    this.setState({
      showProgress:true
    })
    fetch("https://1800rentpro.com/rentpro_api/api/view_profile", {
      method: "POST",
      headers: {
        Accept: "application/json",
        "Content-Type": "application/json"
      },
      body: JSON.stringify({
        userid: token
      })
    })
      .then(response => response.json())
      .then(responseJson => {
       
        if(responseJson.status==1){
          let profileData = responseJson.response;
          var bdy = profileData.dateofbirth.split('-');
          var finalDate = bdy[2] +'/'+bdy[1]+'/'+bdy[0];

          console.log('data data data', finalDate);
            this.setState({
                firstname : profileData.firstname,
                lastname: profileData.lastname,
                middleName: profileData.middlename,
                email :profileData.email,
                selectedCountry: profileData.country,
                selectedState: profileData.state,
                selectedCity: profileData.city,
                birthday :  finalDate,
                 income : profileData.annual_income,
                 mobileNumber: profileData.mobileno,
                 userId: profileData.user_type_id,
                 showProgress: false,
                 birthToSend:profileData.dateofbirth,
                 cityId:profileData.cityid,
                 countryId:profileData.countryid,
                 stateId:profileData.stateid,
            })

          }
          else{
            Alert.alert(responseJson.message);
            this.setState({
              showProgress: false
            });
         
          }
      })
      .catch(error => {
        alert("e" + error);
        console.log("error", error);
        this.setState({
          showProgress: false
        });
      });
  };

  namevalidate(text) {    
    const reg =  /^[A-Za-z\b]+$/;
     if(text!=''){
       if (reg.test(text) === false) {
         return false;
       }
       else {
       this.setState({firstname:text,
         firstError:false})
       }
       }
       else{
       this.setState({firstname:'',
      firstError:true})
       }
    }
  
    lastNamevalidate(text) {    
      const reg =  /^[A-Za-z\b]+$/;
      //const passReg = /^(?=.*\d)(?=.*[a-z])(?=.*[A-Z])[0-9a-zA-Z]{8,}$/;
       if(text!=''){
         if (reg.test(text) === false) {
           return false;
         }
         else {
         this.setState({lastname:text,
       })
         }
         }
         else{
         this.setState({lastname:'',
       })
         }
      }

  countryData(){
    console.log('inside country country ');
    this.setState({
      showProgress: true
    });     
          Api("country_state_city_list", {
            
          })
      .then((responseJson) => {
        
    if(responseJson.status==1){
      
       // countryData = responseJson.response.country_list;
        stateData = responseJson.response.state_list;
         cityData = responseJson.response.city_list;
        this.setState({
          countryList:responseJson.response.country_list,
          stateList:responseJson.response.state_list,
          cityList:responseJson.response.city_list
        },function(){
          // console.log('country data');
          // console.log(this.state.cityList);
        })
      }
      else if(responseJson==false){
        alert('No internet');
        this.setState({
          showProgress: false
        });
      }
    else{
      Alert.alert(responseJson.message);
      this.setState({
        showProgress: false
      });
    }
  }).catch((error) => {
console.error(error);
this.setState({
  showProgress: false
});
});
  }

  mobilevalidate = (text3) => {
    const reg = /^[0-9\b]+$/;
    
    if(text3!=''){
    if (reg.test(text3) === false) {
      return false;
    } 
    else {
    this.setState({mobileNumber:text3})
    }
    }
    else{
    this.setState({mobileNumber:''})
    }
  }

  profileUpdate(){
   if( this.state.validAge === false){
    this.setState({
      showProgress: true
    });
    fetch("https://1800rentpro.com/rentpro_api/api/update_profile", {
      method: "POST",
      headers: {
        Accept: "application/json",
        "Content-Type": "application/json"
      },
      body: JSON.stringify({
     
      firstname:this.state.firstname,
      middlename:this.state.middleName, 
      lastname:this.state.lastname,
      countryid:this.state.countryId,
      stateid:this.state.stateId,
      cityid:this.state.cityId,
      dateofbirth:this.state.birthToSend,
      annual_income:this.state.income,
      mobileno:this.state.mobileNumber,
      userid:token,
    
      })
    })
      .then(response => response.json())
      .then(responseJson => {
        this.setState({
          showProgress: false
        });
        if(responseJson.status==1){
          console.log('#####');
         console.log(responseJson);
         Toast.show(responseJson.message);
         this.props.navigation.navigate('MapScreen')
        }
        else if(responseJson==false){
          alert('No internet');
          this.setState({
            showProgress: false
          });
        }
      else{
        Alert.alert(responseJson.message);
        this.setState({
          showProgress: false
        });
      }
       
      })
      .catch(error => {
        alert("e" + error);
        console.log("error", error);
        this.setState({
          showProgress: false
        });
      });
    }
  };

  render() {
    return (
      <SafeAreaView style={styles.container}>
        <View style={styles.container}>
          <ToolBarForProfile title="My Profile " />
          <View style={styles.mainView}>
            <ScrollView>
              <View style={{ paddingStart: "7%", paddingEnd: "7%" }}>
                <View style={styles.titleView}>
                  <Text style={styles.titleText}>
                    General Account Information
                  </Text>
                </View>
                <View style={styles.textinputBorder}></View>

                <Text style={styles.nameStyle}>Email</Text>
                <TextInput
          style={styles.textInput}
           underlineColorAndroid = "transparent"
           placeholderTextColor={'gray'}
          placeholder="Enter Email"
          onChangeText={(email) => this.setState({email:email, 
          emailError:false})}
          value={this.state.email}
        />

                <Text style={styles.nameStyle}>First Name</Text>
                <TextInput
                 style={styles.textInput}
               underlineColorAndroid = "transparent"
               placeholderTextColor={'gray'}
               placeholder="Enter Email"
               onChangeText={text => this.namevalidate(text)}  
               value={this.state.firstname}
               />

                <Text style={styles.nameStyle}>Middle Name</Text>
                <TextInput
                 style={styles.textInput}
               underlineColorAndroid = "transparent"
               placeholderTextColor={'gray'}
               placeholder="Enter Email"
               onChangeText={(middleName) => this.setState({middleName})}
               value={this.state.middleName}
               />

                <Text style={styles.nameStyle}>Last Name</Text>
                <TextInput
                 style={styles.textInput}
               underlineColorAndroid = "transparent"
               placeholderTextColor={'gray'}
               placeholder="Enter Email"
               onChangeText={text => this.lastNamevalidate(text)}  
               value={this.state.lastname}
               />

                <Text style={styles.nameStyle}>Country</Text>
                <View style={styles.textInput}>
             <RNPicker
          dataSource={this.state.countryList}
          dummyDataSource={this.state.countryList}
          defaultValue={false}
          pickerTitle={"Select Country "}
          showSearchBar={true}
          disablePicker={false}
          changeAnimation={"none"}
          searchBarPlaceHolder={"Search....."}
          showPickerTitle={true}
          searchBarContainerStyle={this.props.searchBarContainerStyle}
          pickerStyle={styles.pickerStyle}
          selectedLabel={this.state.selectedCountry}
          placeHolderLabel={this.state.selectedCountry}
          selectLabelTextStyle={styles.selectLabelTextStyle}
          placeHolderTextStyle={styles.placeHolderTextStyle}
          dropDownImageStyle={styles.dropDownImageStyle}
          dropDownImage={require('../image/droparrow.png')}
          selectedValue={(id, name, value) => this.getState(id, name, value)}
        //  selectedValue={(item) => this._selectedValue(item)}
        />
        </View>

                <Text style={styles.nameStyle}>State</Text>
                <RNPicker
          dataSource={this.state.stateList}
          dummyDataSource={this.state.stateList}
          defaultValue={false}
          pickerTitle={"Select State "}
          showSearchBar={true}
          disablePicker={false}
          changeAnimation={"none"}
          searchBarPlaceHolder={"Search....."}
          showPickerTitle={true}
          searchBarContainerStyle={this.props.searchBarContainerStyle}
          pickerStyle={styles.pickerStyle}
          selectedLabel={this.state.selectedState}
          placeHolderLabel={this.state.selectedState}
          selectLabelTextStyle={styles.selectLabelTextStyle}
          placeHolderTextStyle={styles.placeHolderTextStyle}
          dropDownImageStyle={styles.dropDownImageStyle}
          dropDownImage={require('../image/droparrow.png')}
          selectedValue={(index, name, value) => this.getCity(index, name, value)}
        //  selectedValue={(item) => this._selectedValue(item)}
        />

                <Text style={styles.nameStyle}>City</Text>
                <RNPicker
          dataSource={this.state.cityList}
          dummyDataSource={this.state.cityList}
          defaultValue={false}
          pickerTitle={"select City"}
          showSearchBar={true}
          disablePicker={false}
          changeAnimation={"none"}
          searchBarPlaceHolder={"Search....."}
          showPickerTitle={true}
          searchBarContainerStyle={this.props.searchBarContainerStyle}
          pickerStyle={styles.pickerStyle}
          selectedLabel={this.state.selectedCity}
          placeHolderLabel={this.state.selectedCity}
          selectLabelTextStyle={styles.selectLabelTextStyle}
          placeHolderTextStyle={styles.placeHolderTextStyle}
          dropDownImageStyle={styles.dropDownImageStyle}
          dropDownImage={require('../image/droparrow.png')}
          selectedValue={(index, name, value) => this._selectedCity(index, name, value)}
        //  selectedValue={(item) => this._selectedValue(item)}
        />
                <View style={styles.titleView}>
                  <Text style={styles.titleText}>Personal Information</Text>
                </View>
                <View style={styles.textinputBorder}></View>

                <Text style={styles.nameStyle}>Birthday</Text>
                {/* <TextInput
                 style={styles.textInput}
               underlineColorAndroid = "transparent"
               placeholderTextColor={'gray'}
               placeholder="Enter Birthdate"
               onChangeText={(birthday) => this.setState({birthday:birthday})}
               value={this.state.birthday}
               /> */}
               <TouchableOpacity style={styles.birthdayView}
                onPress={this.showDateTimePicker}>
             
             {this.state.birthday === '' ?
            <Text style={{color:'gray'}}>DD/MM/YYYY</Text> :
            <Text>{this.state.birthday}</Text>
            }
            <View 
       style={styles.calenderIconView} >
        <Image style={styles.calenderImg} source={require('../image/CalendarBlue.png')}/>
       </View>
       </TouchableOpacity>
       
       { this.state.validAge == true ? (
            <Text style={commonStyles.errorMessage}>
               age should be 18+
            </Text>
           ) : null  }

                <Text style={styles.nameStyle}>Annual Income(in $)</Text>
                <TextInput
                 style={styles.textInput}
               underlineColorAndroid = "transparent"
               placeholderTextColor={'gray'}
               placeholder="Enter Annual Income"
               keyboardType={'numeric'}
               onChangeText={(email) => this.setState({income:email  })}
               value={this.state.income}
               />

                <Text style={styles.nameStyle}>Mobile Number</Text>
                <TextInput
                 style={styles.textInput}
               underlineColorAndroid = "transparent"
               placeholderTextColor={'gray'}
               placeholder="Enter Mobile Number"
               onChangeText={text => this.mobilevalidate(text)}  
               value={this.state.mobileNumber}
               />

                <Text style={styles.nameStyle}>User Type ID</Text>
                <View style={styles.radioBtnRow}>
                  
                <TouchableOpacity style= {styles.radioBtn} onPress={() =>{this.setState({userId:'2'})}}>
                {
                   this.state.userId == '2' ?
                    <Image style={styles.radioImg} source={require('../image/blueCheck.png')}/>
                    :
                    <Image style={styles.radioImg} source={require('../image/UnCheckedBlue.png')}/>
                }
                 <Text>{this.state.user[0]}</Text>
                </TouchableOpacity>

                 <TouchableOpacity style= {styles.radioBtn}   onPress={() =>{this.setState({userId:'3'})}}>
                {
                   this.state.userId == '3' ?
                    <Image style={styles.radioImg} source={require('../image/blueCheck.png')}/>
                    :
                    <Image style={styles.radioImg} source={require('../image/UnCheckedBlue.png')}/>
                }
                 <Text>{this.state.user[1]}</Text>
                </TouchableOpacity>
                  {/* <RadioForm
                    radio_props={radio_props}
                    initial={3}
                    formHorizontal={true}
                    buttonSize={15}
                    buttonInnerColor={"#034d94"}
                    buttonOuterColor={"#034d94"}
                    labelStyle={{ marginRight: wp("15%") }}
                    onPress={value => {
                      this.setState({ userId: value });
                    }}
                  /> */}
                </View>
              </View>
            </ScrollView>
            <TouchableOpacity style={styles.lastButton} onPress={()=>this.profileUpdate()}>
              <Text style={styles.sendText}> Update</Text>
            </TouchableOpacity>
            <DateTimePicker
          isVisible={this.state.isDateTimePickerVisible}
          onConfirm={this.handleDatePicked}
          onCancel={this.hideDateTimePicker}
        />  
            <ProgressDialog
          // title="Progress Dialog"
          activityIndicatorColor="red"
          activityIndicatorSize="large"
          animationType="fade"
          message="Please, wait..."
          visible={this.state.showProgress}
        />
          </View>
        </View>
      </SafeAreaView>
    );
  }
}
const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "#ededed"
  },
  mainView: {
    height: "100%",
    width: "90%",
    margin: wp("5%"),
    backgroundColor: "white",
    borderRadius: 10,
    flex: 1
  },
  searchBarContainerStyle: {
    marginBottom: 10,
    flexDirection: "row",
    height: 40,
    shadowOpacity: 1.0,
    shadowRadius: 5,
    shadowOffset: {
      width: 1,
      height: 1
    },
    backgroundColor: "rgba(255,255,255,1)",
    shadowColor: "#d3d3d3",
    borderRadius: 10,
    elevation: 3,
    marginLeft: 10,
    marginRight: 10
  },
  birthdayView:{ 
    flexDirection:'row', 
    height:hp('6%'),
   marginStart:wp('10%'),
    justifyContent:'space-between',
    alignItems:'center',
    marginEnd:wp('5%')
   },
  selectLabelTextStyle: {
    color: "#000",
    textAlign: "left",
    width: "99%",
    padding: 10,
    flexDirection: "row"
  },
  placeHolderTextStyle: {
    color: "black",
    padding: 10,
    textAlign: "left",
    width: "99%",
    flexDirection: "row"
  },
  dropDownImageStyle: {
    marginLeft:-20,
    width: 20,
    height: 20,
    alignSelf: "center"
  },
  iconRow: {
    height: hp("5%"),
    marginTop: hp("1%"),
    width: "100%",
    flexDirection: "row",
    alignItems: "center"
    //backgroundColor:'red',
  },
  checkboxText: {
    width: "42%",
    fontSize: 13,
    color: "gray"
  },
  titleView: {
    height: hp("6%"),
    justifyContent: "center",
    marginTop: hp("2%")
  },
  titleText: {
    fontSize: 16
    // fontWeight:'bold'
  },
  nameStyle: {
    marginTop: hp("1%")
    // backgroundColor:'yellow',
  },
  textInput:{
    marginTop: -hp("1.3%")
  },
     stylePicker: {
    marginStart:wp('10%'), 
     marginTop:-hp('1.3%'),
    //backgroundColor:'yellow',
    // height:hp('5%')
  },
  textinputBorder: {
    backgroundColor: "gray",
    height: 1,
    width: "100%",
    marginBottom: hp("3%")
    //marginTop:-hp('1%')
  },
  textInputView: {
    height: hp("13%"),
    marginStart: "10%",
    marginEnd: "10%",
    justifyContent: "flex-start"
  },
  lastButton: {
    width: "100%",
    marginTop: hp("2%"),
    height: "8%",
    borderBottomLeftRadius: 10,
    borderBottomRightRadius: 10,
    backgroundColor: "#008f2c",
    justifyContent: "center",
    alignItems: "center"
  },
  sendText: {
    color: "white",
    fontWeight: "bold"
  },
  pickerStyle: {
    marginBottom: '2%',
     flexDirection: "row",
    marginStart:wp('5%'), 
   
  },
  calenderIconView:{
    width:wp('20%'), 
  marginTop:-hp('1%'),
   justifyContent:'center',
   alignItems:'center'
  },
  radioImg:{
    height:hp('4%'),
    width:hp('4%'),
   marginRight:10
 },
   calenderImg:{
     height:hp('4%'),
   width:hp('4%')
  },
  radioBtn:{
    flexDirection:'row',
alignItems:'center',
width:'40%',
// backgroundColor:'red'
},
radioBtnRow:{
flexDirection:'row',
height:hp('7%'),
alignItems:'center', 
paddingStart:'5%',
paddingEnd:'5%',
justifyContent:'space-between'
},
});
