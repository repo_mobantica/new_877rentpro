import React, { Component } from 'react';
 
import { StyleSheet, Text, View, Button, TouchableOpacity, Image } from 'react-native';
 
import { createAppContainer, createBottomTabNavigator, createDrawerNavigator, createStackNavigator } from "react-navigation";
 import HomeScreen from './src/HomeScreen';

import DetailsScreen from './src/DetailsScreen';
import StudentScreen from './src/StudentScreen';
class HamburgerIcon extends Component {
 
  toggleDrawer = () => {
 
    this.props.navigationProps.toggleDrawer();
 
  }
 
  render() {
 
    return (
 
      <View style={{ flexDirection: 'row' }}>
 
        <TouchableOpacity onPress={this.toggleDrawer.bind(this)} >
 
          <Image
            source={{ uri: 'https://reactnativecode.com/wp-content/uploads/2018/04/hamburger_icon.png' }}
            style={{ width: 25, height: 25, marginLeft: 5 }}
          />
 
        </TouchableOpacity>
 
      </View>
 
    );
 
 
  }
}
 

 
export const Tab_1 = createBottomTabNavigator({
  MAP: {
    screen: HomeScreen,
  },
  // LIST: {
  //   screen: SettingsScreen,
  // },
  // FAVORITES: {
  //   screen: SettingsScreen,
  // },
  // SEARCHES: {
  //   screen: SettingsScreen,
  // }
}, {
   // tabBarPosition: 'top',
 
    swipeEnabled: true,
 
    tabBarOptions: {
      
      activeTintColor: 'skyblue',
     // pressColor: '#004D40',
      inactiveTintColor: 'black',
      style: {
        backgroundColor: '#fff',
       
       marginBottom:15
      },
 tabStyle: {
    justifyContent: 'center'
  },
      labelStyle: {
       // textAlign: "center",
        fontSize: 13,
        //fontWeight: '200'
      }
    }
 
  });
 
// export const Tab_2 = createBottomTabNavigator({
//   Third: {
//     screen: StudentScreen,
//   },
//   Forth: {
//     screen: DetailsScreen,
//   }
// }, {
//     tabBarPosition: 'top',
 
//     swipeEnabled: true,
 
//     tabBarOptions: {
 
//       activeTintColor: '#fff',
//       pressColor: '#004D40',
//       inactiveTintColor: '#fff',
//       style: {
 
//         backgroundColor: '#00B8D4'
 
//       },
 
//       labelStyle: {
//         fontSize: 16,
//         fontWeight: '200'
//       }
//     }
 
//   });
 
const First_2_Tabs = createStackNavigator({
  First: {
    screen: Tab_1,
    navigationOptions: ({ navigation }) => ({
      title: 'Current Map Area',
      headerLeft: <HamburgerIcon navigationProps={navigation} />,
      headerStyle: {
        backgroundColor: '#fff',
        elevation: 0, // remove shadow on Android
        shadowOpacity: 0, // remove shadow on iOS
      },
      headerTintColor: 'black',
    })
  },
});
 
// const Second_2_Tabs = createStackNavigator({
//   First: {
//     screen: Tab_2,
//     navigationOptions: ({ navigation }) => ({
//       title: 'Current Map Area',
//       headerLeft: <HamburgerIcon navigationProps={navigation} />,
//       headerStyle: {
//         backgroundColor: 'white',
//         elevation: 0, // remove shadow on Android
//         shadowOpacity: 0, // remove shadow on iOS
//       },
//       headerTintColor: 'black',
//     })
//   },
// });
 
const MyDrawerNavigator = createDrawerNavigator({
 
  Home_Menu_Label: {
 
    screen: First_2_Tabs,
 
  },
 
  // Student_Menu_Label: {
 
  //   screen: Second_2_Tabs,
 
  // }
 
});
 
export default createAppContainer(MyDrawerNavigator);
 
const styles = StyleSheet.create({
 
  MainContainer: {
 
    flex: 1,
    justifyContent: 'center',
    backgroundColor: '#f5fcff',
    padding: 11
 
  },
 
  text:
  {
    fontSize: 22,  
    color: '#000',
    textAlign: 'center',
    marginBottom: 10
  },
 
});

//-----------------------------------------------------------------------------------
// import React from "react";
// import {
//   Text,
//   View,
//   AsyncStorage,
//   Image,
//   StyleSheet,
//   TouchableOpacity,
//   Platform,
//   Alert,
//   Button,
//   SafeAreaView,
// } from "react-native";
// import {
//   createBottomTabNavigator,
//   createStackNavigator,
//   createAppContainer,
// } from 'react-navigation';
// import HomeScreen from './src/HomeScreen';
// import SettingsScreen from './src/SettingsScreen';
// import DetailsScreen from './src/DetailsScreen';

// const HomeStack = createStackNavigator({
//   Home: HomeScreen,
//   Details: DetailsScreen,
// },
// {
//   headerMode: 'none',
//   navigationOptions: {
//     headerVisible: false,
//   }
//  });

// const SettingsStack = createStackNavigator({
//   Settings: SettingsScreen,
//   Details: DetailsScreen,
// },
// {
//   headerMode: 'none',
//   navigationOptions: {
//     headerVisible: false,
//   }
//  });

// export default createAppContainer(createBottomTabNavigator(
//   {
//     Home: HomeStack,
//     Settings: SettingsStack,
//   },
//   {
//     /* Other configuration remains unchanged */
//   }
// ));
	

