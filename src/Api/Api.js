const NetInfo = require('@react-native-community/netinfo');
export default function PostByFetch(url, data)
{
 return NetInfo.isConnected.fetch().then(isConnected => {
  if(isConnected){
   return fetch('https://1800rentpro.com/rentpro_api/api/'+url,{

        method: 'POST',
        headers: {
          'Accept': 'application/json',
          'Content-Type': 'application/json',
        },    
        body: JSON.stringify(data),
       }).then((response) => response.json())
       .then((responseJson) => {
        
        return responseJson;
    
       }).catch((error) => {
          return error
       });  
      }
      else{
        return false
      }
     
    });
}