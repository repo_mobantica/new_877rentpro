import React, { Component } from "react";
import {
  Text,
  View,
  SafeAreaView,
  Image,
  TouchableOpacity,
  StyleSheet,
  Dimensions,
  Alert,
  Button,
  FlatList,
  TextInput,
  ScrollView
} from "react-native";
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp
} from "react-native-responsive-screen";
import ToolbarHomeImg from "../component/ToolbarHomeImg";
import DateTimePicker from "react-native-modal-datetime-picker";
import Dash from "react-native-dash";
import commonStyles from "../styles/index";
import DocumentPicker from "react-native-document-picker";
import DatePicker from "react-native-datepicker";
import ImagePicker from "react-native-image-picker";
import moment from 'moment';
var idImage='';
export default class AddCreditReportOne extends Component {
  constructor(props) {
    super(props);

    this.state = {
      firstName: "",
      lastName: "",
      middleName: "",
      address: "",
      birthday: "",
      socialSecurity: "",
      isDateTimePickerVisible: false,
      firstError: false,
      lastError: false,
      middleError: false,
      addressError: false,
      birthdayError: false,
      socialError: false,
      imageError:false,
      filePath: "",
      date: "",
      maxDate: "",
      dateToSend:'',
      validAge: false
    };
  }
  firstNextPress(){
    
    let count =0;
    if(count === 0){
      let addReportOne ={'isNext': true,
        'firstName' : this.state.firstName,
        'middleName': this.state.middleName,
        'lastName':this.state.lastName,
        'address':this.state.address,
        'birthday':this.state.birthday,
        'socialSecurity':this.state.socialSecurity,
        'imageFile':idImage
     }
       console.log('firstNextPress',addReportOne);    
     return addReportOne;
    }
    else{
      let addReportOne ={'isNext': false,
    }
    
    return addReportOne;
       }
  }

  namevalidate(text) {
    const reg = /^[A-Za-z\b]+$/;
    if (text != "") {
      if (reg.test(text) === false) {
        return false;
      } else {
        this.setState({ firstName: text, firstError: false });
      }
    } else {
      this.setState({ firstName: "", firstError: true });
    }
  }

  lastNamevalidate(text) {
    const reg = /^[A-Za-z\b]+$/;
    if (text != "") {
      if (reg.test(text) === false) {
        return false;
      } else {
        this.setState({ lastName: text, lastError: false });
      }
    } else {
      this.setState({ lastName: "", lastError: true });
    }
  }

  middleNamevalidate(text) {
    const reg = /^[A-Za-z\b]+$/;
    // /^(?=.*[a-z])[0-9a-zA-Z ]*$/;
    if (text != "") {
      if (reg.test(text) === false) {
        return false;
      } else {
        this.setState({ middleName: text, middleError: false });
      }
    } else {
      this.setState({ middleName: "", middleError: true });
    }
  }

  addressvalidate(text) {
    // const reg = /^(?=.*[a-z])[0-9a-zA-Z ]*$/;
  
   this.setState({ address: text, addressError: false });
     
  }

  showDateTimePicker = () => {
    this.setState({ isDateTimePickerVisible: true });
  };
 
  hideDateTimePicker = () => {
    this.setState({ isDateTimePickerVisible: false });
  };
 
  handleDatePicked = date => {
    
   let sample=moment(date).format('YYYY-MM-DD');
   let show=moment(date).format('DD/MM/YYYY');
  
   let day =date.getDate();
   let month =date.getMonth();
   let year =date.getFullYear();
    
   let age = 18;
   var setDate = new Date(year + age, month, day);
   var currdate = new Date()
    
    if (currdate >= setDate) {
      // you are above 18
     // alert("above 18");
      this.setState({
        validAge: false
      })
    } else {
      this.setState({
        validAge: true
      })
    }
     this.setState({ birthday: show, birthdayError:false, dateToSend:sample });
    this.hideDateTimePicker();
    
  };


  socialValidate = text3 => {
    const reg = /^[0-9\b]+$/;

    if (text3 != "") {
      if (reg.test(text3) === false) {
        return false;
      } else {
        this.setState({ socialSecurity: text3, socialError: false });
      }
    } else {
      this.setState({ socialSecurity: "", socialError: true });
    }
  };

  chooseFile(item) {
    console.log("itttttem", item);
    var options = {
      title: "Select Image",
      customButtons: [{ name: "customOptionKey", title: "Uplod PDF" }],
      storageOptions: {
        skipBackup: true,
        path: "images"
      }
    };
    ImagePicker.showImagePicker(options, response => {
      console.log("Response = ", response);

      if (response.didCancel) {
        console.log("User cancelled image picker");
      } else if (response.error) {
        console.log("ImagePicker Error: ", response.error);
      } else if (response.customButton) {
        console.log("User tapped custom button: ", response.customButton);
        //  alert(response.customButton);
      ///  this.selectMultipleFile(item);
      } else {
      //  let source = response;
        // You can also display the image using data:
         let source = { uri: 'data:image/jpeg;base64,' + response.data };
        idImage=source.uri;
        this.setState({
          filePath: source.uri
        });
       
      }
    });
  }

  componentDidMount() {
    let that = this;

    let date = new Date().getDate();
    let month = new Date().getMonth() + 1;
    let year = new Date().getFullYear();
    let current_date = date + "/" + month + "/" + year;
    that.setState(
      {
        date: current_date,
        maxDate: current_date
      },
      function() {
        console.log("datatat");
        console.log(this.state.date);
      }
    );
  }

  setDate(date) {
    this.setState({
      birthday: date,
      birthdayError: false
    });
  }
  async selectMultipleFile() {
    //Opening Document Picker for selection of multiple file
    try {
      const results = await DocumentPicker.pickMultiple({
        type: [DocumentPicker.types.pdf]
        //There can me more options as well find above
      });
      for (const res of results) {
        //Printing the log realted to the file
        console.log("res : " + JSON.stringify(res));
        console.log("URI : " + res.uri);
        console.log("Type : " + res.type);
        console.log("File Name : " + res.name);
        console.log("File Size : " + res.size);
      }

      this.setState({
        filePath: results[0]
      });
    } catch (err) {
      //Handling any exception (If any)
      if (DocumentPicker.isCancel(err)) {
        //If user canceled the document selection
        alert("Canceled from multiple doc picker");
      } else {
        //For Unknown Error
        alert("Unknown Error: " + JSON.stringify(err));
        throw err;
      }
    }
  }

  // onSubmit = (e) => {
  nextPress() {
    let data = 0;
    if (idImage == ''||idImage==null) {
      this.setState({
        imageError: true
      });
      
     data++
    }
    if (this.state.firstName == "") {
      this.setState({
        firstError: true
      });
      data++;
    }
    if (this.state.lastName == "") {
      this.setState({
        lastError: true
      });
      data++;
    }
    if (this.state.address == "") {
      this.setState({
        addressError: true
      });
      data++;
    }
    if (this.state.birthday == "" || this.state.validAge === true) {
      this.setState({
        birthdayError: true
      });
      data++;
    } 
    if (this.state.middleName == "") {
      this.setState({
        middleError: true
      });
      data++;
    }
    if (this.state.socialSecurity == "") {
      this.setState({
       socialError: true
      });
      data++;
    }
  
    
    if(data === 0){
     
      let addReportOne ={'isNext': true,
        'firstName' : this.state.firstName,
        'middleName': this.state.middleName,
        'lastName':this.state.lastName,
        'address':this.state.address,
        'birthday':this.state.dateToSend,
        'socialSecurity':this.state.socialSecurity,
        'imageFile':idImage

     }
     
     return addReportOne;
    
    }
    else{
      let addReportOne ={'isNext': false,
       }
    
        return addReportOne;
       }

   
  }
  
  render() {
    return (
      <SafeAreaView style={styles.container}>
        <View style={styles.container}>
          {/* <ToolbarHomeImg title="Add Credit Report" />     */}
          <View style={styles.mainView}>
            <ScrollView>
              <View style={commonStyles.titleView}>
                <Text style={commonStyles.titleText}>
                  Credit Report Application Form
                </Text>
              </View>

              <View style={commonStyles.requiredField}>
                <View style={commonStyles.requiredDot}></View>
                <Text style={commonStyles.nameStyleWithDot}>First Name</Text>
              </View>
              <TextInput
                style={styles.textInput}
                underlineColorAndroid="transparent"
                placeholder="Enter First Name"
                onChangeText={text => this.namevalidate(text)}
                value={this.state.firstName}
                returnKeyType={"next"}
                autoFocus={true}
                onSubmitEditing={event => {
                  this.refs.MiddleN.focus();
                }}
              />
              <View style={styles.textinputBorder}></View>
              {this.state.firstError == true ? (
                <Text style={commonStyles.errorMessage}>
                  * Please enter correct input to proceed.
                </Text>
              ) : null}

                <View style={commonStyles.requiredField}>
                <View style={commonStyles.requiredDot}></View>
                <Text style={commonStyles.nameStyleWithDot}>Middle Name</Text>
              </View>
              <TextInput
                style={styles.textInput}
                underlineColorAndroid="transparent"
                placeholder="Enter Middle Name"
                onChangeText={text => this.middleNamevalidate(text)}
                value={this.state.middleName}
                ref="MiddleN"
                returnKeyType={"next"}
                onSubmitEditing={event => {
                  this.refs.LastN.focus();
                }}
              />
              <View style={commonStyles.textinputBorder}></View>
              {this.state.middleError == true ? (
                <Text style={commonStyles.errorMessage}>
                  * Please enter correct input to proceed.
                </Text>
              ) : null}

              <View style={commonStyles.requiredField}>
                <View style={commonStyles.requiredDot}></View>
                <Text style={commonStyles.nameStyleWithDot}>Last Name</Text>
              </View>
              <TextInput
                style={styles.textInput}
                underlineColorAndroid="transparent"
                placeholder="Enter Last Name"
                onChangeText={text => this.lastNamevalidate(text)}
                value={this.state.lastName}
                ref="LastN"
                returnKeyType={"next"}
                onSubmitEditing={event => {
                  this.refs.AddressN.focus();
                }}
              />
              <View style={styles.textinputBorder}></View>
              {this.state.lastError == true ? (
                <Text style={commonStyles.errorMessage}>
                  * Please enter correct input to proceed.
                </Text>
              ) : null}

              <View style={commonStyles.requiredField}>
                <View style={commonStyles.requiredDot}></View>
                <Text style={commonStyles.nameStyleWithDot}>Address</Text>
              </View>
              <View style={styles.textInputView}>
                <TextInput
                  style={styles.textInput}
                  underlineColorAndroid="transparent"
                  style={{ flex: 1, textAlignVertical: "top" }}
                  numberOfLines={3}
                  placeholder="Address here"
                  onChangeText={text => this.addressvalidate(text)}
                  value={this.state.address}
                  multiline={true}
                  ref="AddressN"
                  returnKeyType={"next"}
                  onSubmitEditing={event => {
                    this.refs.SSNo.focus();
                  }}
                />
              </View>
              <View style={commonStyles.textinputBorder}></View>
              {this.state.addressError == true ? (
                <Text style={commonStyles.errorMessage}>
                  * Please enter correct input to proceed.
                </Text>
              ) : null}

              <View style={commonStyles.requiredField}>
                <View style={commonStyles.requiredDot}></View>
                <Text style={commonStyles.nameStyleWithDot}>Birthday</Text>
              </View>
              <TouchableOpacity style={styles.birthdayView}
               onPress={this.showDateTimePicker}>
             {this.state.birthday === '' ?
             <Text style={{color:'gray'}}>DD/MM/YYYY</Text> :
             <Text >{this.state.birthday}</Text> 
             }
             <View 
             style={styles.calenderIconView}>
             <Image style={styles.calenderImg} source={require('../image/CalendarBlue.png')}/>
            </View>
            </TouchableOpacity>
              <View style={commonStyles.textinputBorder}></View>
              {this.state.birthdayError == true ? (
                <Text style={commonStyles.errorMessage}>
                  * Please enter correct input to proceed.
                </Text>
              ) : null}

              <Text style={commonStyles.nameStyle}>Social Security Nunber</Text>
              <TextInput
                style={styles.textInput}
                keyboardType="numeric"
                underlineColorAndroid="transparent"
                placeholder="Enter social Security number here"
                onChangeText={text => this.socialValidate(text)}
                value={this.state.socialSecurity}
                ref="SSNo"
              />
              <View style={commonStyles.textinputBorder}></View>
              {this.state.socialError == true ? (
                <Text style={commonStyles.errorMessage}>
                  * Please enter correct input to proceed.
                </Text>
              ) : null}

              <View style={commonStyles.requiredField}>
                <View style={commonStyles.requiredDot}></View>
                <Text style={commonStyles.nameStyleWithDot}>Upload State/Goverment ID</Text>
              </View>
              <TouchableOpacity
                style={styles.uploadView}
                onPress={() => this.chooseFile()}
              >
                <Text style={{ color: "gray" }}>Drop/Upload file here</Text>
                <Image
                  style={styles.calenderImg}
                  source={require("../image/upload.png")}
                />
              </TouchableOpacity>
              {this.state.imageError == true ? (
                <Text style={commonStyles.errorMessage}>
                  * Please select State/Goverment ID
                </Text>
              ) : null}
               
            </ScrollView>
            <DateTimePicker
          isVisible={this.state.isDateTimePickerVisible}
          onConfirm={this.handleDatePicked}
          onCancel={this.hideDateTimePicker}
        />  
          </View>
        </View>
      </SafeAreaView>
    );
  }
}
const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "#ededed",
    paddingBottom: "4%"
  },
  mainView: {
    backgroundColor: "white",
    // flex:1,
    height: "100%",
    margin: "5%",
    borderRadius: 10
  },
  nameStyle: {
    marginTop: hp("5%"),
    marginStart: wp("10%")
  },
  nameStyleWithDot: {
    marginTop: hp("2%"),
    marginStart: wp("2%")
  },
  textInput: {
    marginStart: wp("10%"),
    marginTop: -hp("1.3%")
    //backgroundColor:'yellow',
    // height:hp('5%')
  },
  textinputBorder: {
    marginStart: wp("5%"),
    marginEnd: wp("5%"),
    backgroundColor: "gray",
    height: 1,
    width: "85%",
    marginTop: -hp("1%")
  },
  calenderIconView: {
    width: wp("20%"),
    marginTop: -hp("1%"),
    justifyContent: "center",
    alignItems: "center"
  },
  calenderImg: {
    height: hp("3%"),
    width: hp("3%")
  },
  birthdayView: {
    flexDirection: "row",
    height: hp("6%"),
    marginStart: wp("10%"),
    justifyContent: "space-between",
    alignItems: "center",
    marginEnd: wp("5%")
  },
  indicator: {
    height: 12,
    width: 12,
    borderRadius: 6,
    backgroundColor: "#008f2c",
    margin: wp("1%")
  },
  nextBtn: {
    height: hp("7%"),
    backgroundColor: "#034d94",
    width: wp("25%"),
    borderRadius: 10,
    justifyContent: "center",
    alignItems: "center",
    flexDirection: "row",
    paddingStart: wp("2%"),
    paddingEnd: wp("2%")
  },
  nextText: {
    fontSize: 13,
    fontWeight: "bold",
    color: "white"
  },
  uploadView: {
    height: hp("8%"),
    marginStart: "8%",
    marginEnd: "8%",
    marginTop: hp("1%"),
    borderRadius: 10,
    borderStyle: "dotted",
    borderWidth: 1,
    borderColor: "gray",
    flex: 1,
    flexDirection: "row",
    alignItems: "center",
    justifyContent: "space-around"
  },
  footer: {
    backgroundColor: "white",
    height: "11%",
    justifyContent: "space-between",
    flexDirection: "row",
    alignItems: "center",
    paddingStart: "7%",
    paddingEnd: "7%"
  },
  textInputView: {
    height: hp("13%"),
    marginStart: "10%",
    marginEnd: "10%",
    justifyContent: "flex-start"
  }
});
