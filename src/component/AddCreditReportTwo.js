import React, { Component } from "react";
import {
  Text,
  View,
  SafeAreaView,
  Image,
  TouchableOpacity,
  StyleSheet,
  Dimensions,
  Alert,
  Button,
  FlatList,
  TextInput,
  ScrollView,
} from "react-native";
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp
} from "react-native-responsive-screen";
import ToolbarHomeImg from "../component/ToolbarHomeImg";
import DateTimePicker from "react-native-modal-datetime-picker";
import Dash from "react-native-dash";
import CheckBox from "react-native-check-box";

export default class AddCreditReportTwo extends Component {
  constructor(props) {
    super(props);

    this.state = {
      isDuplex: false,
      isDuplex1: true,
      checked: [],
      listdata: [
        { isExpanded: false },
        { isExpanded: true },
        { isExpanded: false },
        { isExpanded: true }
      ]
    };
    this.state = {
      creaditDataHolder: [],
      OrignalData: [],
      textInput_Holder: "",
      Total: 0,
      time_duration: "",
      title: "",
      description: "",
      is_mandatory: false
    };
    this.creaditData = [
      {
        title:
          "Transunion Credit Report Card (24-48 Hours) Queries the TransUnion database to provide credit report details. Report includes FICO credit score range, current monthly debt range, past due accounts & amounts, and collection account totals.",
        price: 39,
        isExpanded: false,
        isDuplex: false,
        key: "1"
      },
      {
        title:
          "Nationwide Criminal Search (24-48 Hours) The nationwide criminal search includes FBI, DEA and other federal reports, plus 50 state coverage. Includes Sex Offender records and Inmate records. Not all counties supply complete data. (see coverage) 8.75.",

        price: 10,
        isExpanded: false,
        isDuplex: false,
        key: "2"
      },
      {
        title: "Sexual offender",
        price: 10,
        isExpanded: false,
        isDuplex: false,
        key: "3"
      },
      {
        title: "Criminal Background Search",
        price: 10,
        isExpanded: false,
        isDuplex: false,
        key: "4"
      },
      {
        title: "State / City specific",
        price: 10,
        isExpanded: false,
        isDuplex: false,
        key: "5"
      },
      {
        title:
          "SSN Verification (24-48 Hours) This search verifies the SSN was issued and when and checks for death certificates. Also provides address history for SSN.",
        price: 5,
        isExpanded: false,
        isDuplex: false,
        key: "6"
      },
      {
        title:
          "Evictions Search (24-48 Hours) Searches court records for eviction records.",
        price: 10,
        isExpanded: false,
        isDuplex: false,
        key: "6"
      },
      {
        title:
          "Validated Nationwide Sex Offender Registry (1 - 3 Business Days) Scans the nationwide sex offender registry. Positive hits are validated. For non-validated instant data (same records), use 'Nationwide Criminal Search' instead.",
        price: 15,
        isExpanded: false,
        isDuplex: false,
        key: "7"
      },
      {
        title:
          "Validated Nationwide Inmate Search (1 - 3 Business Days) Scans department of corrections database for any prison records. Positive hits are validated. For non-validated instant data (same records), use 'Nationwide Criminal Search' instead.",
        price: 15,
        isExpanded: false,
        isDuplex: false,
        key: "8"
      },
      {
        title:
          "Terrorist Links Search (24-48 Hours) Scans the federal terrorist (OFAC) database.",
        price: 5,
        isExpanded: false,
        isDuplex: false,
        key: "9"
      },
      {
        title:
          "Bankruptcy, Judgments, Liens Search (1 Business Day) Searches court records for bankruptcies, judgments, and liens.",
        price: 10,
        isExpanded: false,
        isDuplex: false,
        key: "10"
      }
    ];
  }
 
  handleChange(index) {
    // let checked = this.state.creaditDataHolder;
    let checked = this.state.creditData
    console.log("check", checked);
    // checked[index] = !checked[index];
    checked[index].isDuplex = !checked[index].isDuplex;
    
    if (checked[index].isDuplex == true) {
      total = parseInt(this.state.Total) + parseInt(checked[index].price);
    } else {
      total = parseInt(this.state.Total) -parseInt( checked[index].price);
    }
    console.log("checked", checked);
    this.setState({
      creditData: checked,
      Total: total
    });
    //alert(JSON.stringify(this.state.creaditDataHolder))
  }

  collapseView(item, index) {
    let rowData = this.state.creditData;
    console.log("before", rowData);
    // let index = item.index;
    // alert(item);

    // console.log(this.state.listdata);
    rowData[index].isExpanded = !rowData[index].isExpanded;

    console.log(rowData);
    this.setState(
      {
        creditData: rowData
      }
      // alert(JSON.stringify(this.state.creaditDataHolder))
    );
  }

  GetItem(item) {
    Alert.alert(item);
  }
  FlatListItemSeparator = () => {
    return (
      <View
        style={{
          height: 1,
          width: "100%",
          backgroundColor: "#607D8B"
        }}
      />
    );
  };

  sundayImage(i, event) {
    const alarmList = this.state.alarmList;
    alarmList[i].sunday = !alarmList[i].sunday;
    Alert.alert(alarmList[i].sunday + " " + i);

    this.setState({
      alarmList: alarmList
    });
  }

  resetPress() {
    var data=this.state.creditData;
    this.state.creditData.map((item)=>{
        data.push(item.isDuplex=false)
      
      })
    console.log('dataaaa',data);
    this.setState({
      Total: 0,
      creditData: data
    });
  }

  savedata() {
 var data=[];
  if(this.state.Total==0){
    return false
  }
 else{
  this.state.creditData.map((item)=>{
  
    if(item.isDuplex==true)
    {
       data.push(item.id)
    }
    })
   
    return {data:data,amount:this.state.Total};
 }

    
  }

  componentWillMount() {
    this.setState({ ActivityIndicator_Loading: true }, () => {
      fetch("https://1800rentpro.com/rentpro_api/api/credit_packages", {
        method: "POST",
        headers: {
          Accept: "application/json",
          "Content-Type": "application/json"
        },
        body: JSON.stringify({
          title: this.state.title,
          time_duration: this.state.time_duration,
          description: this.state.description,
          price: this.state.Total,
          is_mandatory: this.state.is_mandatory
        })
      })
        .then(response => response.json())
        .then(responseJsonFromServer => {
          //  alert(responseJsonFromServer);
          console.log("responseJsonFromServer", responseJsonFromServer);

          // this.setState({
          //   ActivityIndicator_Loading: false,
          //   data: responseJsonFromServer.response
          // });
          // alert(JSON.stringify(this.state.data))
          let checkboxArray=[];
    
          responseJsonFromServer.response.map((item, index)=>{
            if(index==0)
            {   checkboxArray.push({
              id:item.id,
              title:item.title,
              time_duration:item.time_duration,
              description:item.description,
              price:item.price,
              is_mandatory:item.is_mandatory,
              isExpanded:false,
              isDuplex:true,
              })
              this.setState({Total:item.price})

            }
            else{
              checkboxArray.push({
                id:item.id,
                title:item.title,
                time_duration:item.time_duration,
                description:item.description,
                price:item.price,
                is_mandatory:item.is_mandatory,
                isExpanded:false,
                isDuplex:false
    
    
               })
            }
        
          })
         console.log('checkboxArray', checkboxArray);

         this.setState({
          ActivityIndicator_Loading: false,
          data: responseJsonFromServer.response,
         creditData: checkboxArray
        });

        console.log("ghfghgf",this.state.creditData)

        })
       
        .catch(error => {
          console.log("error", error);
          this.setState({ ActivityIndicator_Loading: false });
        });
    });
  }
  FlatListItemSeparator = () => {
    return (
      <View
        style={{
          height: 0.5,
          width: "100%",
          backgroundColor: "rgba(0,0,0,0.5)"
        }}
      />
    );
  };

  render() {
    return (
      <SafeAreaView style={styles.container}>
        <View style={styles.container}>
          <View style={styles.mainView}>
            <View style={{ height: "90%" }}>
              <ScrollView>
                <View style={styles.titleView}>
                  <Text style={styles.titleText}>Select Packages/Product</Text>
                  <TouchableOpacity
                    onPress={() => {
                      Alert.alert("hi");
                    }}
                  >
                    <Image
                      style={styles.questionPic}
                      source={require("../image/Info-Help.png")}
                    />
                  </TouchableOpacity>
                </View>

                <FlatList
                  // data={this.state.creaditDataHolder}
                  data={this.state.creditData}
                  width="100%"
                  extraData={this.state}
                  // keyExtractor={(index) => index.toString()}

                  // ItemSeparatorComponent={this.FlatListItemSeparator}
                  keyExtractor={(item, index) => index.toString()}
                  renderItem={({ item, index }) => (
                    <View style={{ paddingHorizontal: 13 }}>
                      <View
                        style={{
                          flex: 1,
                          flexDirection: "row",
                          borderBottomColor: "gray",
                          borderBottomWidth: 0.5
                        }}
                      >
                        <View style={{ flex: 0.1 }}>
                          <CheckBox
                            style={{ padding: 10 }}
                            checkBoxColor={"#034d94"}
                            checkedCheckBoxColor={"#034d94"}
                           onClick={() => this.handleChange(index)}
                          //  value = { this.state.isDuplex }
                          //   onClick = {() => this.checkBox_Test(item.isDuplex) }
                            isChecked={item.isDuplex}
                          />
                        </View>
                        <View style={{ flex: 0.9 }}>
                          <Text
                            numberOfLines={item.isExpanded ? null : 2}
                            style={{
                              padding: 10,
                              color: "gray",
                              fontSize: 15,
                              lineHeight: 20
                            }}
                          >
                            {" "}
                            {item.title}{""}{item.time_duration}{""}{item.description}
                          </Text>
                        </View>
                      </View>
                      <View style={{ flexDirection: "row" }}>
                        <View style={{ flex: 0.1 }}></View>
                        <View
                          style={{
                            justifyContent: "space-between",
                            flexDirection: "row",
                            flex: 1
                          }}
                        >
                          <TouchableOpacity
                            onPress={() => this.collapseView(item, index)}
                          >
                            {item.isExpanded ? (
                              <Text
                                style={{
                                  fontWeight: "bold",
                                  color: "#034d94",
                                  fontSize: 16,
                                  padding: 12
                                }}
                              >
                                Show less
                              </Text>
                            ) : (
                              <Text
                                style={{
                                  fontWeight: "bold",
                                  color: "#034d94",
                                  fontSize: 16,
                                  padding: 12
                                }}
                              >
                                Show more
                              </Text>
                            )}
                          </TouchableOpacity>
                          <Text
                            style={{ padding: 10, lineHeight: 20 }}
                            onPress={this.GetItem.bind(this, item.price)}
                          >
                            {" "}
                            {"$" + item.price}{" "}
                          </Text>
                        </View>
                      </View>
                    </View>

                   
                  )}
                />
              
              </ScrollView>
            </View>
            {/* <View
              style={{
                paddingEnd: "10%",
                paddingStart: "10%",
                borderTopWidth: 1,
                borderColor: "#ededed",
                justifyContent: "space-between",
                alignItems: "center",
                flexDirection: "row",
                height: "14%",
                width: "100%",padding:10
              }}
            > */}
            <View
              style={{
                flexDirection: "row",
                paddingHorizontal: 13,
                borderTopColor: "gary",
                borderTopWidth: 0.5
              }}
            >
              <View style={{ flex: 0.1 }}></View>
              <View
                style={{
                  justifyContent: "space-between",
                  flexDirection: "row",
                  flex: 1
                }}
              >
                <Text style={{ fontWeight: "bold", fontSize: 16, padding: 12 }}>
                  Total
                </Text>
                <Text
                  style={{
                    fontWeight: "bold",
                    fontSize: 20,
                    color: "#034d94",
                    padding: 10
                  }}
                >
                  ${this.state.Total}
                </Text>
              </View>
            </View>
          </View>
        </View>
      </SafeAreaView>
    );
  }
}
const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "#ededed",
    paddingBottom: "4%"
  },
  mainView: {
    backgroundColor: "white",
    // flex:1,
    height: "100%",
    margin: "5%",
    borderRadius: 10
  },
  textView: {
    // flex: 1,

    flexDirection: "column"
  },
  titleView: {
    height: hp("8%"),
    justifyContent: "space-between",
    alignItems: "center",
    flexDirection: "row",
    marginStart: "25%",
    marginEnd: "3%"
    // backgroundColor:'red'
  },
  titleText: {
    fontSize: 16,
    fontWeight: "bold"
  },
  indicator: {
    height: 12,
    width: 12,
    borderRadius: 6,
    backgroundColor: "#008f2c",
    margin: wp("1%")
  },
  nextBtn: {
    height: hp("7%"),
    borderWidth: 2,
    borderColor: "#ededed",
    width: "27%",
    borderRadius: 10,
    justifyContent: "center",
    alignItems: "center",
    flexDirection: "row",
    paddingStart: wp("2%"),
    paddingEnd: wp("2%")
  },
  signUpBtn: {
    height: hp("7%"),
    backgroundColor: "#008f2c",
    width: "46%",
    marginStart: "4%",
    marginEnd: "2%",
    borderRadius: 10,
    justifyContent: "center",
    alignItems: "center"
  },
  nextText: {
    fontSize: 13,
    fontWeight: "bold"
  },
  footer: {
    backgroundColor: "white",
    height: "11%",
    //justifyContent:'space-between',
    flexDirection: "row",
    alignItems: "center",
    paddingStart: "7%",
    paddingEnd: "7%"
  },
  questionPic: {
    height: hp("4%"),
    width: hp("4%")
  }
});
