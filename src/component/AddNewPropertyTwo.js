import React, { Component } from 'react';
import { Text, View, SafeAreaView, Image, TouchableOpacity, StyleSheet, Dimensions, Alert,
Button, FlatList, TextInput, ScrollView } from 'react-native';
import {widthPercentageToDP as wp, heightPercentageToDP as hp} from 'react-native-responsive-screen';
import ToolbarHomeImg from '../component/ToolbarHomeImg';
import DateTimePicker from "react-native-modal-datetime-picker";
import Dash from 'react-native-dash';
import CheckBox from 'react-native-check-box';

var propertyAmen = ['Pool', 'Exercise Facility', 'Furnished', 'Unfurnished', 'Elevator', 'Parking'];
var propertyType =['Apartments', 'House', 'Condo', 'Flat', 'Duplex', 'Townhomes', 'Single Family', 'Roommate',
         'Office', 'Special Purpose', 'Land', 'Retail', 'Warehouse', 'Industrial' ];

var appliances = ['Air Conditioning', 'Dish And washer', 'Garbage Disposal', 'Microwave', 'Refrigerator', 'Oven', 'Washer Dryer']; 
export default class AddNewPropertyOne extends Component {
    constructor(props) {
    super(props);
    this.state = {
     
      parking :false,
     pool  :false,
     exerciseFacility  :false,
     furnished  :false,
     unfurnished  :false,
     elevator  :false,
     apartments  :false,
     house  :false,
     condo  :false,
     flat  :false,
     duplex  :false,
     townhomes  :false,
     singleFamily  :false,
     roommate  :false,
     office  :false,
     land  :false,
     retail  :false,
     warehouse  :false,
      industrial  :false,
     specialPurpose  :false,
     airConditioning :false,
     Dishwasher  :false,
     garbageDisposal  :false,
     microwave  :false,
     refrigerator  :false,
     oven  :false,
     washerDryer  :false,
      cat  :false,
     dog  :false,
    };
 }
 resetPress(){
    this.setState({
      parking :false,
      pool  :false,
      exerciseFacility  :false,
      furnished  :false,
      unfurnished  :false,
      elevator  :false,
      apartments  :false,
      house  :false,
      condo  :false,
      flat  :false,
      duplex  :false,
      townhomes  :false,
      singleFamily  :false,
      roommate  :false,
      office  :false,
      land  :false,
      retail  :false,
      warehouse  :false,
       industrial  :false,
      specialPurpose  :false,
      airConditioning :false,
      Dishwasher  :false,
      garbageDisposal  :false,
      microwave  :false,
      refrigerator  :false,
      oven  :false,
      washerDryer  :false,
       cat  :false,
      dog  :false,
    })
 }
 nextPress(){
   var amenities = [this.state.pool, this.state.exerciseFacility, this.state.furnished, this.state.unfurnished,
       this.state.elevator, this.state.parking];     

   var propertyTypeState =[this.state.apartments, this.state.house, this.state.condo,this.state.flat, this.state.duplex,
       this.state.townhomes, this.state.singleFamily, this.state.roommate, this.state.office, this.state.specialPurpose,
        this.state.land, this.state.retail, this.state.warehouse, this.state.industrial ];

 var appliancesState = [this.state.airConditioning, this.state.Dishwasher, this.state.garbageDisposal, this.state.microwave, 
   this.state.refrigerator, this.state.oven, this.state.washerDryer]; 

   let Amenities_index =[];
     let type_index =[];
     let appliances_index =[];
     let property_amenities =[];
     let property_types =[];
     let property_appliances =[];

   for(let i= 0; i<amenities.length; i++ ){
      if(amenities[i] == true){
          Amenities_index.push(i);
      }
   }
  
   property_amenities = Amenities_index.map(function(item, index){
          return propertyAmen[item];
   })

   for(let i= 0; i<propertyTypeState.length; i++ ){
      if(propertyTypeState[i] == true){
          type_index.push(i);
      }
   }
  
    property_types = type_index.map(function(item, index){
          return propertyType[item];
   })

   for(let i= 0; i<appliancesState.length; i++ ){
      if(appliancesState[i] == true){
         appliances_index.push(i);
      }
   }
  
     property_appliances = appliances_index.map(function(item, index){
          return appliances[item];
   })
   let cat = '0';
   let dog= '0';
    if(this.state.cat){
       cat = '1';
    }
    if(this.state.dog){
      dog = '1';
   }
          let addTwo ={'isNext': true,
                        'cat':cat,
                         'dog': dog,
                         'property_appliances': property_appliances,
                         'property_types' : property_types,
                         'property_amenities': property_amenities,
                  }
   
      return addTwo;
 }
    
   
  render() {
    
    return (
       <SafeAreaView style={styles.container}>
      <View style={styles.container}>
           <View style={styles.mainView}>
           <ScrollView>

          <View style={[styles.flatListView,{ height:hp('24%'),}]}>
      <View style={styles.listTitle}>
      <Text>Amenities </Text>
      <Image
          style={styles.iconStyle}
          source={require('../image/droparrow.png')}
       />
      </View>

     <View style={styles.listRow}>
    <View style={styles.checkboxView}>
       <CheckBox
        style={styles.checkboxStyle}
        checkBoxColor={'#034d94'}
        checkedCheckBoxColor={'#034d94'}
         onClick={()=>{
         this.setState({
             pool:!this.state.pool
          })
        }}
       isChecked={this.state.pool}
        />
         <Text>Pool</Text>
      </View>
      <View style={styles.checkboxView}>
       <CheckBox
        style={styles.checkboxStyle}
        checkBoxColor={'#034d94'}
        checkedCheckBoxColor={'#034d94'}
         onClick={()=>{
         this.setState({
             exerciseFacility:!this.state.exerciseFacility
          })
        }}
       isChecked={this.state.exerciseFacility}
        />
         <Text>Exercise Facility</Text>
      </View>
       </View>

        <View style={styles.listRow}>
    <View style={styles.checkboxView}>
       <CheckBox
        style={styles.checkboxStyle}
        checkBoxColor={'#034d94'}
        checkedCheckBoxColor={'#034d94'}
         onClick={()=>{
         this.setState({
             furnished:!this.state.furnished
          })
        }}
       isChecked={this.state.furnished}
        />
         <Text>Furnished</Text>
      </View>
      <View style={styles.checkboxView}>
       <CheckBox
        style={styles.checkboxStyle}
        checkBoxColor={'#034d94'}
        checkedCheckBoxColor={'#034d94'}
         onClick={()=>{
         this.setState({
             unfurnished:!this.state.unfurnished
          })
        }}
       isChecked={this.state.unfurnished}
        />
         <Text>UnFurnished</Text>
      </View>
       </View>

        <View style={styles.listRow}>
    <View style={styles.checkboxView}>
       <CheckBox
        style={styles.checkboxStyle}
        checkBoxColor={'#034d94'}
        checkedCheckBoxColor={'#034d94'}
         onClick={()=>{
         this.setState({
             elevator:!this.state.elevator
          })
        }}
       isChecked={this.state.elevator}
        />
         <Text>Elevator</Text>
      </View>
      <View style={styles.checkboxView}>
       <CheckBox
        style={styles.checkboxStyle}
        checkBoxColor={'#034d94'}
        checkedCheckBoxColor={'#034d94'}
         onClick={()=>{
         this.setState({
             parking:!this.state.parking
          })
        }}
       isChecked={this.state.parking}
        />
         <Text>Parking </Text>
      </View>
       </View>

    </View> 

           <View style={styles.flatListView}>
      <View style={styles.listTitle}>
      <Text>Property Types - Housing </Text>
      <Image
          style={styles.iconStyle}
          source={require('../image/droparrow.png')}
       />
      </View>

     <View style={styles.listRow}>
    <View style={styles.checkboxView}>
       <CheckBox
        style={styles.checkboxStyle}
        checkBoxColor={'#034d94'}
        checkedCheckBoxColor={'#034d94'}
         onClick={()=>{
         this.setState({
             apartments:!this.state.apartments
          })
        }}
       isChecked={this.state.apartments}
        />
         <Text>Apartments </Text>
      </View>
      <View style={styles.checkboxView}>
       <CheckBox
        style={styles.checkboxStyle}
        checkBoxColor={'#034d94'}
        checkedCheckBoxColor={'#034d94'}
         onClick={()=>{
         this.setState({
             house :!this.state.house 
          })
        }}
       isChecked={this.state.house }
        />
         <Text>House </Text>
      </View>
       </View>

        <View style={styles.listRow}>
    <View style={styles.checkboxView}>
       <CheckBox
        style={styles.checkboxStyle}
        checkBoxColor={'#034d94'}
        checkedCheckBoxColor={'#034d94'}
         onClick={()=>{
         this.setState({
             condo:!this.state.condo
          })
        }}
       isChecked={this.state.condo}
        />
         <Text>Condo </Text>
      </View>
      <View style={styles.checkboxView}>
       <CheckBox
        style={styles.checkboxStyle}
        checkBoxColor={'#034d94'}
        checkedCheckBoxColor={'#034d94'}
         onClick={()=>{
         this.setState({
             flat:!this.state.flat
          })
        }}
       isChecked={this.state.flat}
        />
         <Text>Flat </Text>
      </View>
       </View>

        <View style={styles.listRow}>
    <View style={styles.checkboxView}>
       <CheckBox
        style={styles.checkboxStyle}
        checkBoxColor={'#034d94'}
        checkedCheckBoxColor={'#034d94'}
         onClick={()=>{
         this.setState({
             duplex:!this.state.duplex
          })
        }}
       isChecked={this.state.duplex}
        />
         <Text>Duplex </Text>
      </View>
      <View style={styles.checkboxView}>
       <CheckBox
        style={styles.checkboxStyle}
        checkBoxColor={'#034d94'}
        checkedCheckBoxColor={'#034d94'}
         onClick={()=>{
         this.setState({
             townhomes:!this.state.townhomes
          })
        }}
       isChecked={this.state.townhomes}
        />
         <Text>Townhomes  </Text>
      </View>
       </View>
        <View style={styles.listRow}>
    <View style={styles.checkboxView}>
       <CheckBox
        style={styles.checkboxStyle}
        checkBoxColor={'#034d94'}
        checkedCheckBoxColor={'#034d94'}
         onClick={()=>{
         this.setState({
             singleFamily:!this.state.singleFamily
          })
        }}
       isChecked={this.state.singleFamily}
        />
         <Text>Single Family  </Text>
      </View>
      <View style={styles.checkboxView}>
       <CheckBox
        style={styles.checkboxStyle}
        checkBoxColor={'#034d94'}
        checkedCheckBoxColor={'#034d94'}
         onClick={()=>{
         this.setState({
             roommate:!this.state.roommate
          })
        }}
       isChecked={this.state.roommate}
        />
         <Text>Roommate   </Text>
      </View>
       </View>
    </View> 

      <View style={[styles.flatListView,{ height:hp('24%'),}]}>
      <View style={styles.listTitle}>
      <Text>Property Types - Commercial </Text>
      <Image
          style={styles.iconStyle}
          source={require('../image/droparrow.png')}
       />
      </View>

     <View style={styles.listRow}>
    <View style={styles.checkboxView}>
       <CheckBox
        style={styles.checkboxStyle}
        checkBoxColor={'#034d94'}
        checkedCheckBoxColor={'#034d94'}
         onClick={()=>{
         this.setState({
             office:!this.state.office
          })
        }}
       isChecked={this.state.office}
        />
         <Text>Office </Text>
      </View>
      <View style={styles.checkboxView}>
       <CheckBox
        style={styles.checkboxStyle}
        checkBoxColor={'#034d94'}
        checkedCheckBoxColor={'#034d94'}
         onClick={()=>{
         this.setState({
             specialPurpose:!this.state.specialPurpose
          })
        }}
       isChecked={this.state.specialPurpose}
        />
         <Text>Special Purpose </Text>
      </View>
       </View>

        <View style={styles.listRow}>
    <View style={styles.checkboxView}>
       <CheckBox
        style={styles.checkboxStyle}
        checkBoxColor={'#034d94'}
        checkedCheckBoxColor={'#034d94'}
         onClick={()=>{
         this.setState({
             land:!this.state.land
          })
        }}
       isChecked={this.state.land}
        />
         <Text>Land </Text>
      </View>
      <View style={styles.checkboxView}>
       <CheckBox
        style={styles.checkboxStyle}
        checkBoxColor={'#034d94'}
        checkedCheckBoxColor={'#034d94'}
         onClick={()=>{
         this.setState({
             retail:!this.state.retail
          })
        }}
       isChecked={this.state.retail}
        />
         <Text>Retail </Text>
      </View>
       </View>

        <View style={styles.listRow}>
    <View style={styles.checkboxView}>
       <CheckBox
        style={styles.checkboxStyle}
        checkBoxColor={'#034d94'}
        checkedCheckBoxColor={'#034d94'}
         onClick={()=>{
         this.setState({
             warehouse:!this.state.warehouse
          })
        }}
       isChecked={this.state.warehouse}
        />
         <Text>Warehouse </Text>
      </View>
      <View style={styles.checkboxView}>
       <CheckBox
        style={styles.checkboxStyle}
        checkBoxColor={'#034d94'}
        checkedCheckBoxColor={'#034d94'}
         onClick={()=>{
         this.setState({
             industrial:!this.state.industrial
          })
        }}
       isChecked={this.state.industrial}
        />
         <Text>Industrial  </Text>
      </View>
       </View>

    </View> 

     <View style={styles.flatListView}>
      <View style={styles.listTitle}>
      <Text>Appliances </Text>
      <Image
          style={styles.iconStyle}
          source={require('../image/droparrow.png')}
       />
      </View>

     <View style={styles.listRow}>
    <View style={styles.checkboxView}>
       <CheckBox
        style={styles.checkboxStyle}
        checkBoxColor={'#034d94'}
        checkedCheckBoxColor={'#034d94'}
         onClick={()=>{
         this.setState({
             airConditioning:!this.state.airConditioning
          })
        }}
       isChecked={this.state.airConditioning}
        />
         <Text>Air Conditioning </Text>
      </View>
      <View style={styles.checkboxView}>
       <CheckBox
        style={styles.checkboxStyle}
        checkBoxColor={'#034d94'}
        checkedCheckBoxColor={'#034d94'}
         onClick={()=>{
         this.setState({
             Dishwasher :!this.state.Dishwasher 
          })
        }}
       isChecked={this.state.Dishwasher }
        />
         <Text>Dishwasher  </Text>
      </View>
       </View>

        <View style={styles.listRow}>
    <View style={styles.checkboxView}>
       <CheckBox
        style={styles.checkboxStyle}
        checkBoxColor={'#034d94'}
        checkedCheckBoxColor={'#034d94'}
         onClick={()=>{
         this.setState({
             garbageDisposal:!this.state.garbageDisposal
          })
        }}
       isChecked={this.state.garbageDisposal}
        />
         <Text>Garbage Disposal  </Text>
      </View>
      <View style={styles.checkboxView}>
       <CheckBox
        style={styles.checkboxStyle}
        checkBoxColor={'#034d94'}
        checkedCheckBoxColor={'#034d94'}
         onClick={()=>{
         this.setState({
             microwave:!this.state.microwave
          })
        }}
       isChecked={this.state.microwave}
        />
         <Text>Microwave  </Text>
      </View>
       </View>

        <View style={styles.listRow}>
    <View style={styles.checkboxView}>
       <CheckBox
        style={styles.checkboxStyle}
        checkBoxColor={'#034d94'}
        checkedCheckBoxColor={'#034d94'}
         onClick={()=>{
         this.setState({
             refrigerator:!this.state.refrigerator
          })
        }}
       isChecked={this.state.refrigerator}
        />
         <Text>Refrigerator  </Text>
      </View>
      <View style={styles.checkboxView}>
       <CheckBox
        style={styles.checkboxStyle}
        checkBoxColor={'#034d94'}
        checkedCheckBoxColor={'#034d94'}
         onClick={()=>{
         this.setState({
             oven:!this.state.oven
          })
        }}
       isChecked={this.state.oven}
        />
         <Text>Oven   </Text>
      </View>
       </View>
        <View style={styles.listRow}>
    <View style={styles.checkboxView}>
       <CheckBox
        style={styles.checkboxStyle}
        checkBoxColor={'#034d94'}
        checkedCheckBoxColor={'#034d94'}
         onClick={()=>{
         this.setState({
             washerDryer:!this.state.washerDryer
          })
        }}
       isChecked={this.state.washerDryer}
        />
         <Text>Washer and Dryer </Text>
      </View>
      
       </View>
    </View> 

    <View style={[styles.flatListView,{height:hp('14%')}]}>
      <View style={styles.listTitle}>
      <Text>Pets </Text>
      <Image
          style={styles.iconStyle}
          source={require('../image/droparrow.png')}
       />
      </View>

     <View style={styles.listRow}>
    <View style={styles.checkboxView}>
       <CheckBox
        style={styles.checkboxStyle}
        checkBoxColor={'#034d94'}
        checkedCheckBoxColor={'#034d94'}
         onClick={()=>{
         this.setState({
             cat:!this.state.cat
          })
        }}
       isChecked={this.state.cat}
        />
         <Text>Cat  </Text>
      </View>
      <View style={styles.checkboxView}>
       <CheckBox
        style={styles.checkboxStyle}
        checkBoxColor={'#034d94'}
        checkedCheckBoxColor={'#034d94'}
         onClick={()=>{
         this.setState({
             dog :!this.state.dog 
          })
        }}
       isChecked={this.state.dog }
        />
         <Text>Dog   </Text>
      </View>
       </View>
    </View> 

         </ScrollView>
           </View>
          </View>
           </SafeAreaView>
    );
  }
}
const styles = StyleSheet.create({
container: {
    flex: 1,
   backgroundColor: "#ededed",
   paddingBottom:'2%'
  },
   flatListView:{
     height:hp('29%'),
     margin:hp('3%'),
     marginTop:hp('3%'),
     marginBottom:hp('0%'),
     borderRadius:11,
     backgroundColor:'white',
       
  },
   mainView:{
   // backgroundColor:'white',
     height:'100%',
  
  },
   listTitle:{
     height:hp('7%'),
      flexDirection:'row',
       marginStart:'5%',
    marginEnd:'5%',
     borderBottomWidth:0.5, 
      justifyContent:'space-between', 
       alignItems:'center',
  },
  iconStyle:{
    height:hp('4%'),
     width:hp('4%')
  },
   listRow:{
     height:hp('5.5%'),
      flexDirection:'row',
       marginStart:'5%',
        marginEnd:'5%', 
         alignItems:'center',
    },
    checkboxView:{
       flexDirection:'row', 
        alignItems:'center',
         width:'50%', 
  }, 
  checkboxStyle:{ 
     paddingEnd:'4%'
     },
});