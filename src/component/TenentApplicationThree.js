import React, { Component } from 'react';
import { Text, View, SafeAreaView, Image, TouchableOpacity, StyleSheet, Dimensions, Alert,TextInput,
Button, FlatList, ScrollView } from 'react-native';
import {widthPercentageToDP as wp, heightPercentageToDP as hp} from 'react-native-responsive-screen';
import ToolbarWithLeftIcon from '../component/ToolbarWithLeftIcon';
import DateTimePicker from "react-native-modal-datetime-picker";
import DatePicker from 'react-native-datepicker'
import commonStyles from "../styles/index";
export default class TenantApplicationThree extends Component {

       constructor(props) {
    super(props);

    this.state = {
     firstName:'',
      lastName:'',
      middleName:'',
      address:'',
      birthday:'', 
      socialSecurity:'',
      isDateTimePickerVisible: false,
       occupant:[],
       referanceContact:[],
       vehicalMake:'',
       vehicalModel:'',
       VehicalYear:'',
       vehicalLisence:'',      
         date:'',
        maxDate:'',
        supevisorName:'',
        supervisorMobile:'',
    };
 }

 componentDidMount() {
  let that = this;

  let date = new Date().getDate();
  let month = new Date().getMonth() + 1;
  let year = new Date().getFullYear();
  let current_date = date + '/' + month + '/' + year;
  that.setState({
    date:current_date,
    maxDate: current_date
  }, function(){
    console.log('datatat');
    console.log(this.state.date);
  });
}
componentWillReceiveProps(props){
  var data=props.data;
   let that = this;
   var array=[];
   var contact=[];
     if(data!=null){
     
     if(data.proposed_occupants_name_1=="" &&
      data.proposed_occupants_age_1==null&&
      data.proposed_occupants_dob_1==null&&
      data.proposed_occupants_mobile_no_1=="")
      {
        var obj={
          name :'',
          birthday: '', 
          age: '', 
          mobileNo:''
         }
         array.push(obj);
      }
      else{
        var obj={
          name : data.proposed_occupants_name_1,
          birthday: data.proposed_occupants_dob_1, 
          age: data.proposed_occupants_age_1, 
          mobileNo:  data.proposed_occupants_mobile_no_1
         }
         array.push(obj);
      }
       if(data.proposed_occupants_name_2=="" &&
      data.proposed_occupants_age_2==null&&
      data.proposed_occupants_dob_2==null&&
      data.proposed_occupants_mobile_no_2=="")
      {
       
      }
      else{
        var obj={
          name : data.proposed_occupants_name_2,
          birthday: data.proposed_occupants_dob_2, 
          age: data.proposed_occupants_age_2, 
          mobileNo:  data.proposed_occupants_mobile_no_2
         }
         array.push(obj);
      }
      if(data.proposed_occupants_name_3=="" &&
      data.proposed_occupants_age_3==null&&
      data.proposed_occupants_dob_3==null&&
      data.proposed_occupants_mobile_no_3==""){
      
      }
      else{
        var obj={
          name : data.proposed_occupants_name_3,
          birthday: data.proposed_occupants_dob_3, 
          age: data.proposed_occupants_age_3, 
          mobileNo:  data.proposed_occupants_mobile_no_3
         }
         array.push(obj);
      }
      if(data.proposed_occupants_name_4=="" &&
      data.proposed_occupants_age_4==null&&
      data.proposed_occupants_dob_4==null&&
      data.proposed_occupants_mobile_no_4==""){
       
      }
      else{
        var obj={
          name : data.proposed_occupants_name_4,
          birthday: data.proposed_occupants_dob_4, 
          age: data.proposed_occupants_age_4, 
          mobileNo:  data.proposed_occupants_mobile_no_4
         }
         array.push(obj);
      }
       if(data.proposed_occupants_name_5=="" &&
      data.proposed_occupants_age_5==null&&
      data.proposed_occupants_dob_5==null&&
      data.proposed_occupants_mobile_no_5==""){
      
      }
      else{
        var obj={
          name : data.proposed_occupants_name_5,
          birthday: data.proposed_occupants_dob_5, 
          age: data.proposed_occupants_age_5, 
          mobileNo:  data.proposed_occupants_mobile_no_5
         }
         array.push(obj);
      }
       if(data.proposed_occupants_name_6=="" &&
      data.proposed_occupants_age_6==null&&
      data.proposed_occupants_dob_6==null&&
      data.proposed_occupants_mobile_no_6==""){
       
      }
      else{
        var obj={
          name : data.proposed_occupants_name_6,
          birthday: data.proposed_occupants_dob_6, 
          age: data.proposed_occupants_age_6, 
          mobileNo:  data.proposed_occupants_mobile_no_6
         }
         array.push(obj);
      }
      if(data.contact_first_name==""&&data.contact_first=="")
      {
        var obj={name : '', mobileNo:''}
        contact.push(obj)
      }
      else{
        var obj={name :data.contact_first_name , mobileNo:data.contact_first}
        contact.push(obj)
      }
      if(
      data.contact_second_name==""&&
      data.contact_second=="")
      {

      }
      else{
        var obj={name :data.contact_second_name , mobileNo:data.contact_second}
        contact.push(obj)
      }
     

      that.setState({
        vehicalMake:data.vehicle_make,
        vehicalModel:data.vehicle_model,
        VehicalYear:data.vehicle_year,
        vehicalLisence:data.vehicle_lic,
        occupant:array,
        supevisorName:data.supervisor_contact_name,
        supervisorMobile:data.supervisor_contact,
        referanceContact:contact
       });
   }
 }
 showDateTimePicker = () => {
    this.setState({ isDateTimePickerVisible: true });
  };
 
  hideDateTimePicker = () => {
    this.setState({ isDateTimePickerVisible: false });
  };
 
  handleDatePicked = date => {
    console.log("A date has been picked: ", date);
     let day =date.getDate();
    let month =date.getMonth();
    let year =date.getFullYear();
     let fullDate = day + '/' +month +'/' +year;
     console.log("A date has been picked: ", fullDate);
    this.setState({ birthday: fullDate });
    this.hideDateTimePicker();
   // Alert.alert(date)
  };

    addOccupant(){
      let data = this.state.occupant;
      if(data.length < 6){
        data.push({name : '', birthday:'', age:'', mobileNo:''});
      this.setState({
        occupant:data
      }, function(){
        console.log('this.state.occupant');
        console.log(this.state.occupant);
      });
      }
       }

       removeOccupant(index){
        let data = this.state.occupant;
          data.splice(index, 1)
        this.setState({
          occupant:data
        }, function(){
          console.log('this.state.occupant in remove');
          console.log(this.state.occupant);
        });
        }
         

     addReferanceContact(){
      let data = this.state.referanceContact;
      if(data.length <2){
        data.push({name : '', mobileNo:''});
        this.setState({
          referanceContact:data
        });
      }    
    }

    removeReferance(index){
      let data = this.state.referanceContact;
        data.splice(index, 1)
      this.setState({
        referanceContact:data
      }, function(){
        console.log('this.state.occupant in remove');
        console.log(this.state.occupant);
      });
      }
     
    occupantNameSave(text, index){
      let occupant = this.state.occupant;
        occupant[index].name =text;
      if(text!=''){
        this.setState({occupant:occupant,
       })
     }
  }

  occupantBirthdate(text, index){
    let occupant = this.state.occupant;
        occupant[index].birthday =text;
      if(text!=''){
        this.setState({occupant:occupant,
       })
     }
  }

  occupantAge(text, index){
    let occupant = this.state.occupant;
      occupant[index].age =text;
    if(text!=''){
      this.setState({occupant:occupant,
     })
   }
}

occupantMobile(text, index){
  let occupant = this.state.occupant;
    occupant[index].mobileNo =text;
  if(text!=''){
    this.setState({occupant:occupant,
   })
 }
}

refContactName(text, index){
  let referanceContact = this.state.referanceContact;
  referanceContact[index].name =text;
  if(text!=''){
    this.setState({referanceContact:referanceContact,
   })
 }
}
refContactMobile(text, index){
  let referanceContact = this.state.referanceContact;
  referanceContact[index].mobileNo =text;
  if(text!=''){
    this.setState({referanceContact:referanceContact,
   })
 }
}

nextPress(){
   let data = this.state.occupant;
      if(data.length < 6){
       // let ll = data.length;
     for(let i= data.length; i <6 ;i++){
        data.push({name : '', birthday:'', age:'', mobileNo:''});
     }
    }

    let ref = this.state.referanceContact;
      if(ref.length <2){
        ref.push({name : '', mobileNo:''});
      }

  let rentSeven ={'isNext': true,
  'occupant':data,
   'referanceContact': ref,
   'supevisorName': this.state.supevisorName,
   'supervisorMobile' : this.state.supervisorMobile,
   'vehicalMake': this.state.vehicalMake,
   'vehicalModel':this.state.vehicalModel,
   'VehicalYear':this.state.VehicalYear,
   'vehicalLisence':this.state.vehicalLisence,
  }
    console.log('rentSeven', rentSeven);
  return rentSeven;
}

  _renderItemOccupant = ({item, index}) => (
    <View style={styles.flatListView}>
         <View style={styles.titleView}>
            <Text style={styles.titleText}>Proposed Occupants</Text>
        </View>

       
         {index === 0 ?
          <TouchableOpacity 
          style={{height:hp('5%'),marginTop:-hp('2.5%'), marginBottom:-hp('2.5%'), marginStart:'80%', width:hp('5%'), justifyContent:'center', alignItems:'center'}}
         onPress={()=>this.addOccupant()}
         >
            <Image style={styles.addImg} source={require('../image/add.png')}/>
            </TouchableOpacity>
          :
          <TouchableOpacity 
          style={{height:hp('5%'),marginTop:-hp('2.5%'), marginBottom:-hp('2.5%'), marginStart:'80%', width:hp('5%'), justifyContent:'center', alignItems:'center'}}
         onPress={()=>this.removeOccupant(index)}
         >
          <Image style={styles.addImg} source={require('../image/remove.png')}/>
          </TouchableOpacity>}
       
        <View style={styles.requiredField}>
            <View style={styles.requiredDot}></View>
             <Text style={styles.nameStyleWithDot}>Name</Text>
             </View>
            <TextInput
          style={styles.textInput}
           underlineColorAndroid = "transparent"
          placeholder="Enter your name"
          onChangeText={(name) => this.occupantNameSave(name, index)}
          value={this.state.occupant[index].name}
        />
         <View style={styles.textinputBorder}></View>

         <View style={commonStyles.requiredField}>
            <View style={commonStyles.requiredDot}></View>
             <Text style={commonStyles.nameStyleWithDot}>Birthday</Text>
             </View>
             <View style={styles.birthdayView}>
             {this.state.occupant[index].birthday === '' ?
             <Text style={{color:'gray', marginRight:'20%'}}>DD/MM/YYYY</Text> :
             <Text style={{ marginRight:'25%'}}>{this.state.occupant[index].birthday}</Text> 
             }
              <DatePicker
        style={{width: 200}}
        date={this.state.date}
        mode="date"
        placeholder="select date"
        format="DD/MM/YYYY"
       // minDate="2016-05-01"
        maxDate={this.state.maxDate}
        confirmBtnText="Confirm"
        cancelBtnText="Cancel"
        hideText='true'
        iconSource= {require('../image/CalendarBlue.png')}
        customStyles={{
         
          dateInput: {
            borderWidth: 0,
            },
            dateIcon: {
              bottom:2
            },
        }}
        onDateChange={(date) => this.occupantBirthdate(date, index)}
      />
          
        </View>
        <View style={commonStyles.textinputBorder}></View>

        <View style={styles.requiredField}>
            <View style={styles.requiredDot}></View>
             <Text style={styles.nameStyleWithDot}>Age</Text>
             </View>
            <TextInput
          style={styles.textInput}
           underlineColorAndroid = "transparent"
          placeholder="Enter Your age here"
          onChangeText={(age) => this.occupantAge(age, index)}
          value={this.state.occupant[index].age}
          maxLength={3}
          keyboardType= {"numeric"}
        />
         <View style={styles.textinputBorder}></View>
         <View style={styles.requiredField}>
            <View style={styles.requiredDot}></View>
             <Text style={styles.nameStyleWithDot}>Mobile Number</Text>
             </View>
            <TextInput
          style={styles.textInput}
           underlineColorAndroid = "transparent"
          placeholder="Enter Mobile number here"
          onChangeText={(mobile) => this.occupantMobile(mobile, index)}
          value={this.state.occupant[index].mobileNo}
          maxLength={10}
          keyboardType= {"numeric"}
        />
         <View style={styles.textinputBorder}></View>
         
  
    </View>
  );

    _renderItemRefContact = ({item, index}) => (
    <View style={[styles.flatListView,{height:hp('30%')}]}>
         <View style={styles.titleView}>
            <Text style={styles.titleText}>Referance Contact</Text>
        </View>
        {index === 0 ?
         <TouchableOpacity 
         style={{height:hp('5%'),  marginTop:-hp('2.5%'), marginBottom:-hp('2.5%'), marginStart:'80%', width:hp('5%'), justifyContent:'center', alignItems:'center'}}
        onPress={()=>this.addReferanceContact()}
        >
          <Image style={styles.addImg} source={require('../image/add.png')}/>
         </TouchableOpacity>
         :
         <TouchableOpacity 
         style={{height:hp('5%'),  marginTop:-hp('2.5%'), marginBottom:-hp('2.5%'), marginStart:'80%', width:hp('5%'), justifyContent:'center', alignItems:'center'}}
        onPress={()=>this.removeReferance(index)}
        >
          <Image style={styles.addImg} source={require('../image/remove.png')}/>
         </TouchableOpacity>
        }
        

        <View style={styles.requiredField}>
            <View style={styles.requiredDot}></View>
             <Text style={styles.nameStyleWithDot}>Name</Text>
             </View>
            <TextInput
          style={styles.textInput}
           underlineColorAndroid = "transparent"
          placeholder="Enter your name"
          onChangeText={(name) => this.refContactName(name, index)}
          value={this.state.referanceContact[index].name}
        />
         <View style={styles.textinputBorder}></View>
         <View style={styles.requiredField}>
            <View style={styles.requiredDot}></View>
             <Text style={styles.nameStyleWithDot}>Mobile Number</Text>
             </View>
            <TextInput
          style={styles.textInput}
           underlineColorAndroid = "transparent"
          placeholder="Enter Mobile number here"
          keyboardType={'numeric'}
          onChangeText={(mobile) => this.refContactMobile(mobile, index)}
          value={this.state.referanceContact[index].mobileNo}
        />
         <View style={styles.textinputBorder}></View>
         
  
    </View>
  );

 
  render() {
    
    return (
       <SafeAreaView style={[styles.container,{backgroundColor:'red'}]}>
      <View style={styles.container}>
      
             <ScrollView>
       <FlatList
        data={this.state.occupant}
        extraData={this.state}
      keyExtractor={(item, index) => index.toString()}
        renderItem={this._renderItemOccupant}
      />
   <View style={{height:hp('1%'), width:'100%'}}></View>

   <FlatList
        data={this.state.referanceContact}
        extraData={this.state}
      keyExtractor={(item, index) => index.toString()}
        renderItem={this._renderItemRefContact}
      />
   <View style={{height:hp('1%'), width:'100%'}}></View>

   <View style={[styles.flatListView,{height:hp('30%')}]}>
         <View style={styles.titleView}>
            <Text style={styles.titleText}>Referance Supervisor Contact</Text>
        </View>

        <View style={styles.requiredField}>
            <View style={styles.requiredDot}></View>
             <Text style={styles.nameStyleWithDot}>Name</Text>
             </View>
            <TextInput
          style={styles.textInput}
           underlineColorAndroid = "transparent"
          placeholder="Enter Name here"
          onChangeText={(supevisorName) => this.setState({supevisorName})}
          value={this.state.supevisorName}
        />
         <View style={styles.textinputBorder}></View>
         <View style={styles.requiredField}>
            <View style={styles.requiredDot}></View>
             <Text style={styles.nameStyleWithDot}>Mobile Number</Text>
             </View>
            <TextInput
          style={styles.textInput}
           underlineColorAndroid = "transparent"
          placeholder="Enter Mobile number here"
          onChangeText={(supervisorMobile) => this.setState({supervisorMobile})}
          value={this.state.supervisorMobile}
          keyboardType={'numeric'}
        />
         <View style={styles.textinputBorder}></View>
       </View>
         <View style={{height:hp('1%'), width:'100%'}}></View>
         <View style={styles.flatListView}>
         <View style={styles.titleView}>
            <Text style={styles.titleText}>Vehicle Details</Text>
        </View>

        <View style={styles.requiredField}>
            <View style={styles.requiredDot}></View>
             <Text style={styles.nameStyleWithDot}>Make</Text>
             </View>
            <TextInput
          style={styles.textInput}
           underlineColorAndroid = "transparent"
          placeholder="Enter Make here"
          onChangeText={(vehicalMake) => this.setState({vehicalMake})}
          value={this.state.vehicalMake}
        />
         <View style={styles.textinputBorder}></View>

        <View style={styles.requiredField}>
            <View style={styles.requiredDot}></View>
             <Text style={styles.nameStyleWithDot}>Model</Text>
             </View>
            <TextInput
          style={styles.textInput}
           underlineColorAndroid = "transparent"
          placeholder="Enter Model here"
          onChangeText={(vehicalModel) => this.setState({vehicalModel})}
          value={this.state.vehicalModel}
        />
         <View style={styles.textinputBorder}></View>

         <View style={styles.requiredField}>
            <View style={styles.requiredDot}></View>
             <Text style={styles.nameStyleWithDot}>Year</Text>
             </View>
            <TextInput
          style={styles.textInput}
           underlineColorAndroid = "transparent"
          placeholder="Enter year here"
          onChangeText={(VehicalYear) => this.setState({VehicalYear})}
          value={this.state.VehicalYear}
          keyboardType={'numeric'}
        />
         <View style={styles.textinputBorder}></View>

         <View style={styles.requiredField}>
            <View style={styles.requiredDot}></View>
             <Text style={styles.nameStyleWithDot}>Lisence</Text>
             </View>
            <TextInput
          style={styles.textInput}
           underlineColorAndroid = "transparent"
          placeholder="Enter Lisence Number here"
          onChangeText={(vehicalLisence) => this.setState({vehicalLisence})}
          value={this.state.vehicalLisence}
        />
         <View style={styles.textinputBorder}></View>
    </View>
           
   <View style={{height:hp('1%'), width:'100%'}}></View>
     </ScrollView>
          </View>

           </SafeAreaView>
    );
  }
}
const styles = StyleSheet.create({
container: {
    flex: 1,
   backgroundColor: "#ededed"
  },
   flatListView:{
     height:hp('52%'),
     margin:hp('3%'),
     marginTop:hp('3%'),
     marginBottom:hp('0%'),
     borderRadius:11,
     backgroundColor:'white',
       
  },
   nameStyleWithDot:{
     marginTop:hp('2%'), 
     marginStart:wp('2%'),
 },
  calenderImg:{
         height:hp('4%'),
       width:hp('4%')
      },
       addImg:{
         height:hp('5%'),
       width:hp('5%')
      },
  nameStyle:{
    marginTop:hp('2%'), 
     marginStart:wp('10%'),
  // backgroundColor:'yellow', 
 },
 textInput:{
    marginStart:wp('10%'), 
      marginEnd:wp('10%'), 
     marginTop:-hp('1.3%'),
     //backgroundColor:'yellow', 
    // height:hp('5%')
 },
       uploadView:{
         height:hp('8%'),
          marginStart:'8%',
           marginEnd:'8%',
           marginTop:hp('1%'),
            borderRadius:10,
             borderStyle: 'dotted',
             borderWidth: 1,
              borderColor:'gray',
               flex:1,
                flexDirection:'row', 
                alignItems:'center', 
                justifyContent:'space-around'
          },
      calenderIconView:{
        width:wp('20%'), 
      marginTop:-hp('1%'),
       justifyContent:'center',
       alignItems:'center'
      },
 textinputBorder:{
   marginStart:wp('5%'), 
   marginEnd:wp('5%'), 
    backgroundColor:'gray',
     height:1,
      width:'85%',
      marginTop:-hp('1%')
      },
  requiredDot:{
    height:6,
     width:6,
      borderRadius:3,
       backgroundColor:'red',
         marginStart:wp('8%'),
         marginTop:hp('2%'), 
       },
       titleView:{
         height:hp('8%'), 
         justifyContent:'center',
          alignItems:'center'
      },
      titleText:{
        fontSize:16,
         fontWeight:'bold'
    },
    requiredField:{
      flexDirection:'row',
       alignItems:'center'
    },
     birthdayView:{ 
        flexDirection:'row', 
        height:hp('6%'),
       marginStart:wp('10%'),
        justifyContent:'space-between',
        alignItems:'center',
        marginEnd:wp('5%')
       },
       indicator:{ 
         height:12,
          width:12,
          borderRadius:6,
          backgroundColor:'#008f2c',
         margin:wp('1%'),
       },
});