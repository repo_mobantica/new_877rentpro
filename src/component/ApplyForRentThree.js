import React, { Component } from 'react';
import { Text, View, SafeAreaView, Image, TouchableOpacity, StyleSheet, Dimensions, Alert,
Button, FlatList, TextInput, ScrollView } from 'react-native';
import {widthPercentageToDP as wp, heightPercentageToDP as hp} from 'react-native-responsive-screen';
import RNPickerSelect from 'react-native-picker-select';
export default class ApplyForRentThree extends Component {
    constructor(props) {
    super(props);

    this.state = {
     bankName:'',
     accountType:'',
     accountNumber:'',
     avgBalance:'',
     bankNameError:false,
     accountTypeError:false,
     accountNumberError:false,
     avgBalanceError:false
    };
 }

 nameValidate(text) {    
  const reg =  /^[A-Za-z\b]+$/;
   if(text!=''){
     if (reg.test(text) === false) {
       return false;
     }
     else {
     this.setState({bankName:text,
    bankNameError:false})
     }
     }
     else{
     this.setState({bankName:'',
     bankNameError:true})
     }
  }

  balanceValidate = (text3) => {
    const reg = /^[0-9.\b]+$/;
    
    if(text3!=''){
    if (reg.test(text3) === false) {
      return false;
    } 
    else {
   this.setState({avgBalance:text3,
    avgBalanceError:false})
    }
    }
    else{
    this.setState({avgBalance:'',
    avgBalanceError:true})
    }
  }

  accountNoValidate = (text3) => {
    const reg = /^[0-9\b]+$/;
    
    if(text3!=''){
    if (reg.test(text3) === false) {
      return false;
    } 
    else {
    this.setState({accountNumber:text3,
    accountNumberError:false})
    }
    }
    else{
    this.setState({accountNumber:'',
    accountNumberError:true})
    }
  }  

  nextPress(){
    let count =0;
     let avgBalance = this.state.avgBalance;

       if(this.state.bankName== ""){
      this.setState({
       bankNameError:true
      })
 count++;
     }
if(this.state.accountType== ""){
      this.setState({
accountTypeError:true
      })
count++;
     }
   if(this.state.accountNumber== ""){
          this.setState({
    accountNumberError:true
          })
    count++;
         }
       if(this.state.avgBalance== "" || avgBalance.split('.').length >2 || avgBalance.charAt(0) === '.' || avgBalance.charAt(avgBalance.length-1) === '.'){
        //this.state.income== "" || income.split('.').length >2 || income.charAt(0) === '.' || income.charAt(income.length-1) === '.' 
              this.setState({
        avgBalanceError:true
              })
        count++;
             }
        if(count === 0){
          let rentThree ={'isNext': true,
                    'bankName':this.state.bankName,
                     'accountType': this.state.accountType,
                     'accountNumber': this.state.accountNumber,
                     'avgBalance' : this.state.avgBalance,
              }
               console.log('rentThree', rentThree);
         return rentThree;
        }
       else{
        let rentThree ={'isNext': false,
              }
               console.log('renrentThreetOne', rentThree);
         return rentThree;
       } 
    }

  render() {
    
    return (
       <SafeAreaView style={styles.container}>
      <View style={styles.container}>
           <View style={styles.mainView}>
           <ScrollView>
             <View style={styles.titleView}>
            <Text style={styles.titleText}>Bank Information</Text>
            </View>

            <View style={styles.requiredField}>
            <View style={styles.requiredDot}></View>
             <Text style={styles.nameStyleWithDot}>Bank Name</Text>
             </View>
            <TextInput
          style={styles.textInput}
           underlineColorAndroid = "transparent"
          placeholder="Enter Bank Name Please"
          onChangeText={text => this.nameValidate(text)} 
          value={this.state.bankName}
          returnKeyType = {"next"}
          autoFocus = {true}
        onSubmitEditing={(event) => { 
           this.refs.AccontNoNext.focus(); 
         }}
        />
         <View style={styles.textinputBorder}></View>
         { this.state.bankNameError == true ? (
             <Text style={styles.errorMessage}>
                * Please enter correct input to proceed.
             </Text>
            ) : null  }
          

         <View style={styles.requiredField}>
            <View style={styles.requiredDot}></View>
             <Text style={styles.nameStyleWithDot}>Type of Account </Text>
             </View>
             <TextInput
          style={styles.textInput}
           underlineColorAndroid = "transparent"
          placeholder="Your Account here"
          onChangeText={text => this.setState({accountType:text, accountTypeError:false})} 
          value={this.state.accountType}
          maxLength={14}
          ref='AccontNoNext'
           returnKeyType = {"next"}
           onSubmitEditing={(event) => { 
            this.refs.avgBalanceNext.focus(); 
          }}
        />
        <View style={styles.textinputBorder}></View>
        { this.state.accountTypeError == true ? (
             <Text style={styles.errorMessage}>
                * Please enter correct input to proceed.
             </Text>
            ) : null  }

         <View style={styles.requiredField}>
            <View style={styles.requiredDot}></View>
             <Text style={styles.nameStyleWithDot}>Account Number</Text>
             </View>
            <TextInput
          style={styles.textInput}
           underlineColorAndroid = "transparent"
          placeholder="Enter Account number here"
          onChangeText={text => this.accountNoValidate(text)} 
          value={this.state.accountNumber}
          maxLength={14}
          keyboardType="numeric"
          ref='AccontNoNext'
           returnKeyType = {"next"}
           onSubmitEditing={(event) => { 
            this.refs.avgBalanceNext.focus(); 
          }}
        />
         <View style={styles.textinputBorder}></View>
         { this.state.accountNumberError == true ? (
             <Text style={styles.errorMessage}>
                * Please enter correct input to proceed.
             </Text>
            ) : null  }

          <View style={styles.requiredField}>
            <View style={styles.requiredDot}></View>
             <Text style={styles.nameStyleWithDot}>Average Montly Balance</Text>
             </View>
            <TextInput
          style={styles.textInput}
           underlineColorAndroid = "transparent"
          placeholder="Enter Average Montly Balance"
          onChangeText={text => this.balanceValidate(text)} 
          maxLength={15}
          value={this.state.avgBalance}
          keyboardType="numeric"
          ref='avgBalanceNext'
        />
         <View style={styles.textinputBorder}></View>
         { this.state.avgBalanceError == true ? (
             <Text style={styles.errorMessage}>
                * Please enter correct input to proceed.
             </Text>
            ) : null  }
           </ScrollView>
           </View>
           </View>
           </SafeAreaView>
    );
  }
}
const styles = StyleSheet.create({
container: {
    flex: 1,
   backgroundColor: "#ededed",
   paddingBottom:'3%'
  },
  mainView:{
    backgroundColor:'white',
     height:'100%',
   margin:'5%', 
  borderRadius:10,
  },
 nameStyleWithDot:{
     marginTop:hp('2%'), 
     marginStart:wp('2%'),
 },
 textInput:{
    marginStart:wp('10%'), 
     marginTop:-hp('1.3%'),
     paddingEnd:'8%'
     //backgroundColor:'yellow', 
    // height:hp('5%')
 },
   textInputView:{
           height:hp('13%'),
            marginStart:'10%',
             marginEnd:'10%',
             justifyContent: 'flex-start',
        },
 textinputBorder:{
   marginStart:wp('5%'), 
   marginEnd:wp('5%'), 
    backgroundColor:'gray',
     height:1,
      width:'85%',
      marginTop:-hp('1%'), 
      marginBottom:hp('1.2%')
      },
  requiredDot:{
    height:6,
     width:6,
      borderRadius:3,
       backgroundColor:'red',
         marginStart:wp('8%'),
         marginTop:hp('2%'), 
       },
       titleView:{
         height:hp('8%'), 
         justifyContent:'center',
          alignItems:'center'
      },
      titleText:{
        fontSize:16,
         fontWeight:'bold'
    },
    requiredField:{
      flexDirection:'row',
       alignItems:'center'
    },
    errorMessage:{
      fontSize:11,
      color:'red',
      marginLeft:'10%'
    }
});
