import React, { Component } from 'react';
import { Text, View, SafeAreaView, Image, TouchableOpacity, StyleSheet, Dimensions, Alert,
Button, FlatList, TextInput, ScrollView } from 'react-native';
import {widthPercentageToDP as wp, heightPercentageToDP as hp} from 'react-native-responsive-screen';
import ToolbarHomeImg from '../component/ToolbarHomeImg';
import DateTimePicker from "react-native-modal-datetime-picker";
import Dash from 'react-native-dash';
import CheckBox from 'react-native-check-box';
import { GooglePlacesAutocomplete } from 'react-native-google-places-autocomplete';

export default class AddNewPropertyOne extends Component {
    constructor(props) {
    super(props);

    this.state = {
     address:'',
     displayAddress:'',
     unitDetails:'',
     addressError:false,
     disAddressError:false,
     unitDetailsError:false,
     isSame:false,
     lat:null,
     lng:null,
    };
 }
 getAdd(data){
  console.log("add",data);

  this.setState(
      {
        
        address: data.formatted_address, // selected address
        lat: data.geometry.location.lat,//  selected coordinates latitude
        lng:data.geometry.location.lng, //  selected coordinates longitute

      }
     
    );


}
 showDateTimePicker = () => {
    this.setState({ isDateTimePickerVisible: true });
  };
 
  hideDateTimePicker = () => {
    this.setState({ isDateTimePickerVisible: false });
  };
 
  handleDatePicked = date => {
    console.log("A date has been picked: ", date);
     let day =date.getDate();
    let month =date.getMonth();
    let year =date.getFullYear();
     let fullDate = day + '/' +month +'/' +year;
     console.log("A date has been picked: ", fullDate);
    this.setState({ birthday: fullDate });
    this.hideDateTimePicker();
   // Alert.alert(date)
  };  
  addressvalidate(text) {    
    // const reg = /^(?=.*[a-z])[0-9a-zA-Z ]*$/;
    const reg =  /^[A-Za-z 0-9.,\b]+$/;
     if(text!=''){
       if (reg.test(text) === false) {
         return false;
       }
       else {
         if(this.state.isSame){
          this.setState({address:text,
              displayAddress:text,
            addressError:false})
         }else{
          this.setState({address:text,
            addressError:false})
         }
       }
       }
       else{
       this.setState({address:'',
      addressError:true})
       }
    }  

       DisAddressvalidate(text) {    
      // const reg = /^(?=.*[a-z])[0-9a-zA-Z ]*$/;
   
       if(text!=''){
         this.setState({displayAddress:text,
         disAddressError:false})
       
         }
         else{
         this.setState({displayAddress:'',
        disAddressError:true})
         }
      }  

      unitvalidate(text) {    
        // const reg = /^(?=.*[a-z])[0-9a-zA-Z ]*$/;
        const reg =  /^[A-Za-z 0-9.,\b]+$/;
         if(text!=''){
           if (reg.test(text) === false) {
             return false;
           }
           else {
           this.setState({unitDetails:text,
          unitDetailsError:false})
           }
           }
           else{
           this.setState({unitDetails:'',
          unitDetailsError:true})
           }
        }
         
       checkBoxPress(){
        this.setState({
          isSame:!this.state.isSame
       }, function(){
         if(this.state.isSame === true && this.state.address != ''){
           this.setState({
             disAddressError:false,
             displayAddress:this.state.address
           })
         }
       }
       );

       }

        nextPress(){
          let data =0;
         if(this.state.address== "" ){
               this.setState({
                     addressError:true
               }) 
               data++;
              }
             if(this.state.displayAddress== "" && this.state.isSame === false){
                  this.setState({
                    disAddressError:true
                  })
            data++;
                 }
             if(this.state.unitDetails== ""){
                    this.setState({
                      unitDetailsError:true
                    })
              data++;
                   }
              

                if(data === 0){
                  let addOne ={'isNext': true,
                      'address':this.state.address,
                       'displayAddress': this.state.displayAddress,
                       'unitDetails': this.state.unitDetails,
                       'latitude':this.state.lat,
                       'longitute':this.state.lng
                }
             
           return addOne;
                }
            else{
              let addOne ={'isNext': false,
                 }
            return addOne;
              }
        
      }
  render() {
    
    return (
       <SafeAreaView style={styles.container}>
      <View style={styles.container}>
          
           <View style={styles.mainView}>
             <ScrollView>
            <View style={styles.titleView}>
            <Text style={styles.titleText}>Present Address</Text>
            </View>

            <View style={styles.requiredField}>
            <View style={styles.requiredDot}></View>
             <Text  style={styles.nameStyleWithDot}>Actual Address</Text>
             </View>
             <GooglePlacesAutocomplete
            placeholder='Address here'
            minLength={2} // minimum length of text to search
            autoFocus={true}
            fetchDetails={true}
            returnKeyType={'default'}
           // renderDescription={row => row.description}
            onPress={(data, details = null) => { // 'details' is provided when fetchDetails = true
             var data = details;
             this.getAdd(data);
             this.setState({ showPlacesList: false })
            }}
            query={{
              // available options: https://developers.google.com/places/web-service/autocomplete
              key: 'AIzaSyAGF8cAOPFPIKCZYqxuibF9xx5XD4JBb84',
              language: 'en',
              types: 'geocode', // default: 'geocode'
            }}

            listViewDisplayed={this.state.showPlacesList}
            // textInputProps={{
            //   onFocus: () => this.setState({ showPlacesList: true }),
            //   onBlur: () => this.setState({ showPlacesList: false }),
            // }}
            styles={{
              textInputContainer: {
                backgroundColor: 'transparent',
                height: null,
                borderTopColor: 'transparent',
                borderBottomColor: 'transparent',
                borderTopWidth: 0,
                borderBottomWidth: 0,
                marginStart:'10%',
                marginEnd:'10%',
                },
                textInput: {
                color: '#000',
                paddingRight: 5,
                paddingLeft: 5,
                fontSize: 14,
                lineHeight: 23,
                height: 60,
                flex: 4,
                borderWidth: 0,
                borderBottomColor: 'transparent',
                marginTop: 2,
                marginLeft: 0
            },
              }}

            //currentLocation={true} // Will add a 'Current location' button at the top of the predefined places list
            //currentLocationLabel="Current location"
            nearbyPlacesAPI='GooglePlacesSearch' // Which API to use: GoogleReverseGeocoding or GooglePlacesSearch

            filterReverseGeocodingByTypes={['locality', 'administrative_area_level_3']} // filter the reverse geocoding results by types - ['locality', 'administrative_area_level_3'] if you want to display only cities
            // predefinedPlaces={[]}
            debounce={200}
            //predefinedPlacesAlwaysVisible={true}
          />
{/*           
             <View style={styles.textInputView}>
             <TextInput
          style={styles.textInput}
           underlineColorAndroid = "transparent"
             style = {{ flex: 1, textAlignVertical:'top' }}
           // numberOfLines={3}
          placeholder="Address here"
          onChangeText={text => this.addressvalidate(text)}
          value={this.state.address}
          multiline={true}
          returnKeyType = {"next"}
          autoFocus = {true}
        onSubmitEditing={(event) => { 
           this.refs.DisplayAddressN.focus(); 
         }}
        />
        </View> */}
        <View style={styles.textinputBorder}></View>
        { this.state.addressError == true ? (
             <Text style={styles.errorMessage}>
                 * Please enter correct input to proceed.
             </Text>
            ) : null  }

        <View style={styles.requiredField}>
            <View style={styles.requiredDot}></View>
             <Text style={styles.nameStyleWithDot}>Display Address</Text>
             </View>
             <View style={styles.textInputView}>
             {this.state.isSame ? 
                <Text style={styles.sameAddress} >{this.state.address}</Text>
              :
              <TextInput
         style={styles.textInput}
           underlineColorAndroid = "transparent"
           style = {{ flex: 1, textAlignVertical:'top' }}
            numberOfLines={3}
          placeholder="Same Address appear here"
          onChangeText={text => this.DisAddressvalidate(text)}
          value={this.state.displayAddress}
          multiline={true}
          ref='DisplayAddressN'
          returnKeyType = {"next"}
          onSubmitEditing={(event) => { 
          this.refs.UnitNext.focus(); 
         }}
        />
              }
                 
        </View>
        <View style={styles.textinputBorder}></View>
        { this.state.disAddressError == true ? (
             <Text style={styles.errorMessage}>
                 * Please enter correct input to proceed.
             </Text>
            ) : null  }
        <View style={styles.searchBox}>
       <CheckBox
        style={{ paddingEnd:'2%'}}
        checkBoxColor={'#034d94'}
        checkedCheckBoxColor={'#034d94'}
         onClick={()=>{this.checkBoxPress()  }}
        isChecked={this.state.isSame}
        />
        <Text>Same as Actual Address</Text>
        </View>

        <View style={styles.requiredField}>
            <View style={styles.requiredDot}></View>
             <Text style={styles.nameStyleWithDot}>Unit Details</Text>
             </View>
             <View style={styles.textInputView}>
            <TextInput
         style={styles.textInput}
           underlineColorAndroid = "transparent"
             style = {{ flex: 1, textAlignVertical:'top' }}
            numberOfLines={3}
          placeholder="unit details"
          onChangeText={text => this.unitvalidate(text)}
          value={this.state.unitDetails}
          multiline={true}
          ref='UnitNext'
        />
        </View>
        <View style={styles.textinputBorder}></View>
        { this.state.unitDetailsError == true ? (
             <Text style={styles.errorMessage}>
                 * Please enter correct input to proceed.
             </Text>
            ) : null  }
           </ScrollView>
           </View>
            {/* <View style={styles.footer}>
          <View style={{flexDirection:'row'}}>
          <View style={styles.indicator}></View>
          <View style={[styles.indicator,{backgroundColor:'white', borderWidth:0.5}]}></View>
         </View>
            <TouchableOpacity style={styles.nextBtn}
    //  onPress={() => {this.facebookLogin()}}
       >
       <Text style={styles.nextText}>Next</Text>
       </TouchableOpacity>
            </View> */}
          </View>
           </SafeAreaView>
    );
  }
}
const styles = StyleSheet.create({
container: {
    flex: 1,
   backgroundColor: "#ededed",
   paddingBottom:'3%'
  },
  mainView:{
    backgroundColor:'white',
     height:'100%',
   margin:'5%', 
  borderRadius:10,
  },
 nameStyleWithDot:{
     marginTop:hp('2%'), 
     marginStart:wp('2%'),
     fontWeight:'bold',
 },
 textInput:{
    marginStart:wp('10%'), 
     marginTop:-hp('1.3%'),
     //backgroundColor:'yellow', 
    // height:hp('5%')
 },
 sameAddress:{
  marginTop:hp('1%'), 
  
},
 textinputBorder:{
   marginStart:wp('5%'), 
   marginEnd:wp('5%'), 
    backgroundColor:'gray',
     height:1,
      width:'85%',
      marginTop:-hp('1%'), 
      marginBottom:hp('1.2%')
      },
  requiredDot:{
    height:6,
     width:6,
      borderRadius:3,
       backgroundColor:'red',
         marginStart:wp('8%'),
         marginTop:hp('2%'), 
       },
       titleView:{
         height:hp('8%'), 
         justifyContent:'center',
          alignItems:'center'
      },
      titleText:{
        fontSize:16,
         fontWeight:'bold'
    },
    requiredField:{
      flexDirection:'row',
       alignItems:'center'
    },
       indicator:{ 
         height:12,
          width:12,
          borderRadius:6,
          backgroundColor:'#008f2c',
         margin:wp('1%'),
       },
       nextBtn:{
         height:hp('7%'),
       backgroundColor:'#034d94',
       width:wp('25%'),
      borderRadius:10,
     justifyContent:'center',
       alignItems:'center',
       flexDirection:'row', 
       paddingStart:wp('2%'),
        paddingEnd:wp('2%')
       },
       nextText:{
         fontSize:13,
          fontWeight:'bold',
           color:'white'
           },
          footer:{
            backgroundColor:'white',
             height:'11%',
              justifyContent:'space-between', 
              flexDirection:'row',
            alignItems:'center',
            paddingStart:'7%',
           paddingEnd:'7%'
       },
       textInputView:{
           height:hp('13%'),
            marginStart:'10%',
             marginEnd:'10%',
             justifyContent: 'flex-start',
        },
        searchBox:{
            height:hp('5%'),
             flexDirection:'row',
              alignItems:'flex-start',
               paddingStart:'10%'
        },
        errorMessage:{
          fontSize:11,
          color:'red',
          marginLeft:'10%'
        }
});
