import React, { Component } from 'react';
import { Text, View, SafeAreaView, Image, TouchableOpacity, StyleSheet, Dimensions, Alert,
Button, FlatList, TextInput, ScrollView } from 'react-native';
import {widthPercentageToDP as wp, heightPercentageToDP as hp} from 'react-native-responsive-screen';
import CheckBox from 'react-native-check-box';
import DateTimePicker from "react-native-modal-datetime-picker";
import commonStyles from "../styles/index";
import DatePicker from 'react-native-datepicker'
import RNPickerSelect from 'react-native-picker-select';
export default class ApplyForRentOne extends Component {
    constructor(props) {
    super(props);

    this.state = {
      firstName:'',
      lastName:'',
      email:'',
      birthday:'',
      income:'',
      mobileNo:'',
      workPhone:'',
      isDateTimePickerVisible: false,
       value: 0,
       phoneError:false,
       firstError:false,
       emailError:false,
       mobileError:false,
       phoneError:false,
       birthError:false,
       validAge:false,
       workPhoneError:false,
       date:'',
       maxDate:'',
       guarantor:false,
       SSN:'',
       idType:'1',
       idNumber:'',
       SSNError:false,
       idTypeError:false,
       idNumberError:false,
    };
 }

 componentDidMount() {
  let that = this;

  let date = new Date().getDate();
  let month = new Date().getMonth() + 1;
  let year = new Date().getFullYear();
  let current_date = date + '/' + month + '/' + year;
  that.setState({
    date:current_date,
    maxDate: current_date
  }, function(){
    console.log('datatat');
    console.log(this.state.date);
  });
}

     namevalidate(text) {    
      const reg =  /^[A-Za-z\b]+$/;
       if(text!=''){
         if (reg.test(text) === false) {
           return false;
         }
         else {
         this.setState({firstName:text,
           firstError:false})
         }
         }
         else{
         this.setState({firstName:'',
        firstError:true})
         }
      }
    
      lastNamevalidate(text) {    
        const reg =  /^[A-Za-z\b]+$/;
        //const passReg = /^(?=.*\d)(?=.*[a-z])(?=.*[A-Z])[0-9a-zA-Z]{8,}$/;
         if(text!=''){
           if (reg.test(text) === false) {
             return false;
           }
           else {
           this.setState({lastName:text,
          lastError:false})
           }
           }
           else{
           this.setState({lastName:'',
          lastError:true})
           }
        }
    
        mobilevalidate = (text3) => {
          const reg = /^[0-9\b]+$/;
          
          if(text3!=''){
          if (reg.test(text3) === false) {
            return false;
          } 
          else {
          this.setState({mobileNo:text3,
          mobileError:false})
          }
          }
          else{
          this.setState({mobileNo:'',
        mobileError:true})
          }
        }

        phoneValidate = (text3) => {
          const reg = /^[0-9\b]+$/;
          
          if(text3!=''){
          if (reg.test(text3) === false) {
            return false;
          } 
          else {
          this.setState({workPhone:text3,
          workPhoneError:false})
          }
          }
          else{
          this.setState({workPhone:'',
          workPhoneError:true})
          }
        }  

        setDate(date){
          this.setState({
            birthday:date,
            birthdayError:false
          })
        }

        nextPress(){
                    let mobileNo = this.state.mobileNo;
                    let workPhone = this.state.workPhone;
                    let count =0;
                    const emailReg=  /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;
              if(this.state.email != "" && emailReg.test(this.state.email) === false){
               this.setState({
                 emailError:true
               }) 
               count++;
              }
                 if(this.state.firstName== ""){
                  this.setState({
            firstError:true
                  })
            count++;
                 }
             if(this.state.lastName== ""){
                  this.setState({
               lastError:true
                  })
            count++;
                 }
                   if(this.state.birthday== "" || this.state.validAge== true){
                          this.setState({
                      birthError:true
                          })
                    count++;
                         }
                    if(this.state.mobileNo== "" || mobileNo.length < 10){
                            this.setState({
                      mobileError:true
                            })
                      count++;
                           }
                       if(this.state.workPhone  != "" && workPhone.length < 10){
                              this.setState({
                          workPhoneError:true
                              })
                        count++;
                             }
                         if(this.state.SSN  == ""){
                              this.setState({
                          SSNError:true
                              })
                        count++;
                             }
                        
                         if(this.state.idType  == "" ){
                              this.setState({
                          idTypeError:true
                              })
                        count++;
                             }

                         if(this.state.idNumber == "" ){
                              this.setState({
                          idNumberError:true
                              })
                        count++;
                             }


                        
                        
                            if(count === 0){
                              let guarantor;
                              if(this.state.guarantor){
                                guarantor = '1';
                              }
                              else{
                                      guarantor = '0';
                              }

                              let rentOne ={'isNext': true,
                                        'firstName':this.state.firstName,
                                         'lastName': this.state.lastName,
                                         'email': this.state.email,
                                         'birthday' : this.state.birthday,
                                         'mobileNo': this.state.mobileNo,
                                         'workPhone':this.state.workPhone,
                                         'SSN':this.state.SSN,
                                         'idType':this.state.idType,
                                         'idNumber':this.state.idNumber,
                                           'guarantor': guarantor,
                                  }
                                  console.log('rentOne',rentOne);
                             return rentOne;
                            }
                           else{
                            let rentOne ={'isNext': false,
                                  }
                                console.log('rentOne',rentOne);   
                             return rentOne;
                           } 
              }
  render() {
    
    return (
       <SafeAreaView style={styles.container}>
      <View style={styles.container}>
          
           <View style={styles.mainView}>
           <ScrollView>
             <View style={styles.titleView}>
            <Text style={styles.titleText}>General Account Information</Text>
            </View>

            <View style={styles.requiredField}>
            <View style={styles.requiredDot}></View>
             <Text style={styles.nameStyleWithDot}>First Name</Text>
             </View>
            <TextInput
          style={styles.textInput}
           underlineColorAndroid = "transparent"
          placeholder="Enter First Name"
          onChangeText={text => this.namevalidate(text)}  
          value={this.state.firstName}
          returnKeyType = {"next"}
          autoFocus = {true}
        onSubmitEditing={(event) => { 
           this.refs.lastNext.focus(); 
         }}
        />
         <View style={styles.textinputBorder}></View>
         { this.state.firstError == true ? (
             <Text style={styles.errorMessage}>
                * Please enter correct input to proceed.
             </Text>
            ) : null  }

          <View style={styles.requiredField}>
            <View style={styles.requiredDot}></View>
             <Text style={styles.nameStyleWithDot}>Last Name</Text>
             </View>
            <TextInput
          style={styles.textInput}
           underlineColorAndroid = "transparent"
          placeholder="Enter Last Name"
          onChangeText={text => this.lastNamevalidate(text)}  
          value={this.state.lastName}
          ref='lastNext'
           returnKeyType = {"next"}
           onSubmitEditing={(event) => { 
            this.refs.emailNext.focus(); 
          }}
        />
         <View style={styles.textinputBorder}></View>
         { this.state.lastError == true ? (
             <Text style={styles.errorMessage}>
                * Please enter correct input to proceed.
             </Text>
            ) : null  }

         <Text style={styles.nameStyle}>Email</Text>
            <TextInput
          style={styles.textInput}
           underlineColorAndroid = "transparent"
          placeholder="Enter Email"
          onChangeText={(email) => this.setState({email:email, emailError:false})}
          value={this.state.email}
          ref='emailNext'
          returnKeyType = {"next"}
          onSubmitEditing={(event) => { 
           this.refs.MobileNext.focus(); 
         }}
        />
        <View style={styles.textinputBorder}></View>
        { this.state.emailError == true ? (
             <Text style={styles.errorMessage}>
                * Please enter correct input to proceed.
             </Text>
            ) : null  }

         {/* <View style={styles.requiredField}>
            <View style={styles.requiredDot}></View>
             <Text style={styles.nameStyleWithDot}>Date of Birth</Text>
             </View>
             <View style={styles.birthdayView}>
             {this.state.birthday === '' ?
             <Text style={{color:'gray'}}>DD/MM/YYYY</Text> :
             <Text >{this.state.birthday}</Text> 
             }
             <TouchableOpacity 
        style={styles.calenderIconView}
       onPress={this.showDateTimePicker}>
         <Image style={styles.calenderImg} source={require('../image/CalendarBlue.png')}/>
        </TouchableOpacity>
        </View>
        <View style={styles.textinputBorder}></View>
        { this.state.birthError == true ? (
             <Text style={styles.errorMessage}>
                user's age should be 18+
             </Text>
            ) : null  }
            */}

<View style={commonStyles.requiredField}>
            <View style={commonStyles.requiredDot}></View>
             <Text style={commonStyles.nameStyleWithDot}>Birthday</Text>
             </View>
             <View style={styles.birthdayView}>
             {this.state.birthday === '' ?
             <Text style={{color:'gray', marginRight:'20%'}}>DD/MM/YYYY</Text> :
             <Text style={{ marginRight:'25%'}}>{this.state.birthday}</Text> 
             }
              <DatePicker
        style={{width: 200}}
        date={this.state.date}
        mode="date"
        placeholder="select date"
        format="DD/MM/YYYY"
       // minDate="2016-05-01"
        maxDate={this.state.maxDate}
        confirmBtnText="Confirm"
        cancelBtnText="Cancel"
        hideText='true'
        iconSource= {require('../image/CalendarBlue.png')}
        customStyles={{
         
          dateInput: {
            borderWidth: 0,
            },
            dateIcon: {
              bottom:2
            },
        }}
        onDateChange={(date) => this.setDate(date)}
      />
          
        </View>
        <View style={commonStyles.textinputBorder}></View>
        { this.state.birthError == true ? (
             <Text style={commonStyles.errorMessage}>
               user's age should be 18+
             </Text>
            ) : null  }

            <View style={styles.requiredField}>
            <View style={styles.requiredDot}></View>
             <Text style={styles.nameStyleWithDot}>Mobile Number</Text>
             </View>
            <TextInput
          style={styles.textInput}
          maxLength={10}
           underlineColorAndroid = "transparent"
          placeholder="Enter Mobile Number"
          keyboardType="numeric"
          onChangeText={text => this.mobilevalidate(text)}
          value={this.state.mobileNo}
          ref='MobileNext'
          returnKeyType = {"next"}
          onSubmitEditing={(event) => { 
           this.refs.phoneNext.focus(); 
         }}
        />
         <View style={styles.textinputBorder}></View>
         { this.state.mobileError == true ? (
             <Text style={styles.errorMessage}>
                * Please enter correct input to proceed.
             </Text>
            ) : null  }

         <Text style={styles.nameStyle}>Work Phone Number</Text>
            <TextInput
          style={styles.textInput}
          keyboardType={'numeric'}
           underlineColorAndroid = "transparent"
          placeholder="Enter Work Phone Number"
          onChangeText={text => this.phoneValidate(text)}  
          value={this.state.workPhone}
          maxLength={10}
          ref='phoneNext'
        />
        <View style={styles.textinputBorder}></View>
        { this.state.workPhoneError == true ? (
             <Text style={styles.errorMessage}>
                * Please enter correct input to proceed.
             </Text>
            ) : null  }

            <View style={styles.requiredField}>
            <View style={styles.requiredDot}></View>
             <Text style={styles.nameStyleWithDot}>Social Security Number</Text>
             </View>
            <TextInput
          style={styles.textInput}
          maxLength={10}
           underlineColorAndroid = "transparent"
          placeholder="Social Security Number"
          keyboardType="numeric"
          onChangeText={text => this.setState({SSN:text, SSNError:false})}
          value={this.state.SSN}
        //   ref='MobileNext'
        //   returnKeyType = {"next"}
        //   onSubmitEditing={(event) => { 
        //    this.refs.phoneNext.focus(); 
        //  }}
        />
         <View style={styles.textinputBorder}></View>
         { this.state.SSNError == true ? (
             <Text style={styles.errorMessage}>
                * Please enter correct input to proceed.
             </Text>
            ) : null  }

            <View style={styles.requiredField}>
            <View style={styles.requiredDot}></View>
             <Text style={styles.nameStyleWithDot}>ID Type </Text>
             </View>
        <View style={styles.textInput}>
             <RNPickerSelect
            onValueChange={(value) => this.setState({idType:value, 
              idTypeError : false})}
            items={[
                { label: 'State Issued Identification', value: '1' },
                { label: 'Passport', value: '2' },
                 { label: 'Any other', value: '3' },
            ]}
        />
        </View>
        <View style={styles.textinputBorder}></View>
        { this.state.idTypeError == true ? (
             <Text style={styles.errorMessage}>
                * Please enter correct input to proceed.
             </Text>
            ) : null  }

            <View style={styles.requiredField}>
            <View style={styles.requiredDot}></View>
             <Text style={styles.nameStyleWithDot}>ID Number</Text>
             </View>
            <TextInput
          style={styles.textInput}
          maxLength={10}
           underlineColorAndroid = "transparent"
          placeholder="Enter ID Number"
          keyboardType="numeric"
          onChangeText={text => this.setState({idNumber:text, idNumberError:false})}
          value={this.state.idNumber}
        //   ref='MobileNext'
        //   returnKeyType = {"next"}
        //   onSubmitEditing={(event) => { 
        //    this.refs.phoneNext.focus(); 
        //  }}
        />
         <View style={styles.textinputBorder}></View>
         { this.state.idNumberError == true ? (
             <Text style={styles.errorMessage}>
                * Please enter correct input to proceed.
             </Text>
            ) : null  }

         <View style={styles.checkBoxView}>
        <CheckBox
        style={styles.checkBoxStyle}
        checkBoxColor={'#034d94'}
        checkedCheckBoxColor={'#034d94'}
         onClick={()=>{
         this.setState({
             guarantor:!this.state.guarantor
          })
        }}
       isChecked={this.state.guarantor}
        />
        <Text style={{color:'gray'}}>Guarantor</Text>
        </View>
         <DateTimePicker
          isVisible={this.state.isDateTimePickerVisible}
          onConfirm={this.handleDatePicked}
          onCancel={this.hideDateTimePicker}
        />  
           </ScrollView>
           </View>
           
          </View>
           </SafeAreaView>
    );
  }
}
const styles = StyleSheet.create({
container: {
    flex: 1,
   backgroundColor: "#ededed",
   paddingBottom:'3%'
  },
  mainView:{
    backgroundColor:'white',
     height:'100%',
   margin:'5%', 
  borderRadius:10,
  },
 nameStyleWithDot:{
     marginTop:hp('2%'), 
     marginStart:wp('2%'),
 },
 textInput:{
    marginStart:wp('10%'), 
     marginTop:-hp('1.3%'),
     //backgroundColor:'yellow', 
    // height:hp('5%')
 },
 textinputBorder:{
   marginStart:wp('5%'), 
   marginEnd:wp('5%'), 
    backgroundColor:'gray',
     height:1,
      width:'85%',
      marginTop:-hp('1%'), 
      marginBottom:hp('1.2%')
      },
  requiredDot:{
    height:6,
     width:6,
      borderRadius:3,
       backgroundColor:'red',
         marginStart:wp('8%'),
         marginTop:hp('2%'), 
       },
       titleView:{
         height:hp('8%'), 
         justifyContent:'center',
          alignItems:'center'
      },
      titleText:{
        fontSize:16,
         fontWeight:'bold'
    },
    requiredField:{
      flexDirection:'row',
       alignItems:'center'
    },
     calenderIconView:{
        width:wp('20%'), 
      marginTop:-hp('1%'),
       justifyContent:'center',
       alignItems:'center'
      },
        calenderImg:{
         height:hp('4%'),
       width:hp('4%')
      },  
      nameStyle:{
    marginTop:hp('2%'), 
     marginStart:wp('10%'),
  // backgroundColor:'yellow', 
 },
  birthdayView:{ 
        flexDirection:'row', 
        height:hp('6%'),
       marginStart:wp('10%'),
        justifyContent:'space-between',
        alignItems:'center',
        marginEnd:wp('5%')
   },
    checkBoxView:{
            height:hp('5%'),
             flexDirection:'row',
              marginStart:'10%', 
              alignItems:'center', 
    },
    checkBoxStyle:{ 
        paddingEnd: 10
  },
  errorMessage:{
    fontSize:11,
    color:'red',
    marginLeft:'10%'
  }
});
