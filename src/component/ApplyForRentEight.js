import React, { Component } from 'react';
import { Text, View, SafeAreaView, Image, TouchableOpacity, StyleSheet, Dimensions, Alert,
Button, FlatList, TextInput, ScrollView, TouchableHighlight } from 'react-native';
import {widthPercentageToDP as wp, heightPercentageToDP as hp} from 'react-native-responsive-screen';
import DocumentPicker from 'react-native-document-picker';
import ImagePicker from 'react-native-image-picker';
import RadioForm, {RadioButton, RadioButtonInput, RadioButtonLabel} from 'react-native-simple-radio-button';
import SignatureCapture from 'react-native-signature-capture';
import CheckBox from "react-native-check-box";

import { 
  ProgressDialog, 
} from "react-native-simple-dialogs";
var radio_props = [
  {label: 'Yes', value: 1 },
  {label: 'No', value: 0 }
];
    var incomeImage='';
    var idImage='';
      var imageData ='';
   
export default class ApplyForRentEight extends Component {
    constructor(props) {
    super(props);

    this.state = {
      isDuplex:false,
        isExpanded:false,
        isSigning:false,
        singleFile: '',
      multipleFile: [],
      filePath: '',
      filePathIncome: '',
      imageData:'',
      idImage:'',
      incomeImage:'',
         showProgress: false,
         radioOne:1,
         radioTwo:1,
         radioThree:1,
         idUrl:'',
         incomeUrl:'',
         signUrl:'',
         idError:false,
         incomeError:false,
         signerror:false,
         isDuplexError:false
      };
    }
   collapseView(){
      this.setState({
       isExpanded: !this.state.isExpanded
     });
    }

    saveSign() {
        this.refs["sign"].saveImage();
     // this.baseConverted(data);
      
     this.setState({
        isSigning:false
      })
  }
    
   _onSaveEvent=(result) => {
      
         let baseImage =  '"'+'data:image/jpeg;base64,' + result.encoded +'"';
         imageData=baseImage;
       this.setState({
            imageData:baseImage,
            signerror:false
          },function(){
            console.log('imageData', this.state.imageData);
          })
       }
    _onDragEvent=() => {
         // This callback will be called when the user enters signature
  
    }

  resetSign() {
      this.refs["sign"].resetImage();
      this.setState({
        isSigning:false
      })
  }
   
  showHideView(){
    this.setState({
      isSigning:true
    })
  }

  async selectMultipleFile(item) {
    //Opening Document Picker for selection of multiple file
    try {
      const results = await DocumentPicker.pickMultiple({
        type: [DocumentPicker.types.pdf],
        readContent:true
        //There can me more options as well find above
      });
      for (const res of results) {
        //Printing the log realted to the file
       
      }
      //Setting the state to show multiple file attributes
      // this.setState({ multipleFile: results }, function(){
    
      // });
      if(item === 'ID'){
        this.setState({
          filePath: results[0],
        });
      }
      else{
        this.setState({
          filePathIncome:  results[0],
        });
      }
    } catch (err) {
      //Handling any exception (If any)
      if (DocumentPicker.isCancel(err)) {
        //If user canceled the document selection
        alert('Canceled from multiple doc picker');
      } else {
        //For Unknown Error
        alert('Unknown Error: ' + JSON.stringify(err));
        throw err;
      }
    }
  }
  
 
   nextPress(){
      let count = 0;
     if(idImage== "" ){
           this.setState({
                 idError:true
           }) 
      count++;
          }
           if(incomeImage== ""){
              this.setState({
                incomeError:true
              })
        count++;
             }

         if(imageData== ""){
              this.setState({
                signerror:true
              })
        count++;
             }
        if(this.state.isDuplex == ""){
                this.setState({
            isDuplexError:true
                })
          count++;
               }
       
        console.log('inside count');
           console.log('inside count', count);
          
           if(count === 0){
       console.log('getImageUrl in if');
       
                           let rentEight ={'isNext': true,
                          'radioOne':this.state.radioOne,
                           'radioTwo': this.state.radioTwo,
                           'radioThree': this.state.radioThree,
                           'idUrl' : idImage,
                           'incomeUrl': incomeImage,
                           'signUrl':imageData,
                           }
                           console.log('rentEight',rentEight);
                    
                     return rentEight;
              }
            else{
               console.log('getImageUrl in else');
                let rentEight ={'isNext': false,
                           }
                           console.log('rentEight',rentEight);
                    
                     return rentEight;
     }
            
    //     if(noFlag === 0){
    //       console.log('inside if');
    //         fetch('https://1800rentpro.com/rentpro_api/api/upload_image', {
    //         method: 'POST',
    //         headers: {
    //                 'Accept': 'application/json',
    //                 'Content-Type': 'application/json',
    //                   },
    //         body: JSON.stringify({
    //           image: [idImage, incomeImage],
    //           type:"property"
    //               })
    //       }).then((response) => response.json())
    //         .then((responseJson) => {
            
    //       if(responseJson.status==1){
    //         this.setState({
    //           showProgress: false
    //       });
    //               idUrl= responseJson.response.data[0][0];
    //               incomeUrl=responseJson.response.data[1][0];
    //               console.log('mouse');
    //              returnJson= this.getImageUrl(true);
    //              console.log('pratima',returnJson);
    //   console.log(returnJson);
    // return returnJson;

    //         }
    //       else{
    //         Alert.alert(responseJson.message);
    //         returnJson= this.getImageUrl(false);
    //           return returnJson;              
    //       }
    //     }).catch((error) => {
    //        returnJson= this.getImageUrl(false);
                 
    //   console.error(error);
    //   this.setState({
    //     showProgress: false
    //   });
    //   return returnJson;    
    // });
  
    //     }
    //     else{
    //         returnJson= this.getImageUrl(false);
    //          console.log('returnJson in else');
    //   console.log(returnJson);
    //         return returnJson;
    //     }
                     
      }

  // chooseFile = () => {
  //   var options = {
  //     title: 'Select Image',
  //     customButtons: [
  //       { name: 'customOptionKey', title: 'Choose Photo from Custom Option' },
  //     ],
  //     storageOptions: {
  //       skipBackup: true,
  //       path: 'images',
  //     },
  //   };
  //   ImagePicker.showImagePicker(options, response => {
  //     console.log('options = ', options);
  //     console.log('Response = ', response);
 
  //     if (response.didCancel) {
  //       console.log('User cancelled image picker');
  //     } else if (response.error) {
  //       console.log('ImagePicker Error: ', response.error);
  //     } else if (response.customButton) {
  //       console.log('User tapped custom button: ', response.customButton);
  //       alert(response.customButton);
  //     } else {
  //       let source = response;
  //       console.log('response', response);
  //       // You can also display the image using data:
  //       // let source = { uri: 'data:image/jpeg;base64,' + response.data };
  //       this.setState({
  //         filePath: source,
  //       });
  //     }
  //   });
  // };
     
 chooseFile(item){
   
    var options = {
      title: 'Select Image',
      customButtons: [
        { name: 'customOptionKey', title: 'Uplod PDF' },
      ],
      storageOptions: {
        skipBackup: true,
        path: 'images',
      },
    };
    ImagePicker.showImagePicker(options, response => {

      if (response.didCancel) {
        console.log('User cancelled image picker');
      } else if (response.error) {
        console.log('ImagePicker Error: ');
        console.log('ImagePicker Error: ', response.error);
      } else if (response.customButton) {
        console.log('User tapped custom button: ');
       console.log('User tapped custom button: ', response.customButton);
       alert(response.customButton);
      this.selectMultipleFile(item);
      } else {
        let source = response;
        // console.log('what happen??');
        // console.log(response);
      let baseImage = { uri: '"'+'data:image/jpeg;base64,' + response.data +'"'}
      // let baseImage =  '"'+response.data+'"';
      
        // You can also display the image using data:
        // let source = { uri: 'data:image/jpeg;base64,' + response.data };
      if(item === 'ID'){
        idImage = baseImage.uri;
        this.setState({
          filePath: source,
          idImage: baseImage.uri,
          idError:false
        },function(){
            console.log('idImage', this.state.idImage);
          });
      }
   else{
     incomeImage= baseImage.uri;
        this.setState({
          filePathIncome: source,
          incomeImage: baseImage.uri,
          incomeError:false
        },function(){
            console.log('incomeImage', this.state.incomeImage);
          });
      }
        
      }
    });
  };  

  handleChange() {
    // let checked = this.state.creaditDataHolder;
    let checked = this.state.isDuplex
    console.log("check", checked);
 
    this.setState({
      isDuplex:!this.state.isDuplex
  }
  ,
  function(){
    if(this.state.isDuplex){
      this.setState({  isDuplexError :false })
  }
  })
    
  }
  render() {
    
    return (
       <SafeAreaView style={styles.container}>
      <View style={styles.container}>
          
           <View style={styles.mainView}>
            <ScrollView>
             <View style={styles.titleView}>
            <Text style={styles.titleText}>Important and Disclaimer</Text>
            </View>

               <Text style={styles.nameStyle}>Have you ever been evicted/asked to move?</Text>
                 <View style={styles.radioBtn}>
              <RadioForm
               radio_props={radio_props}
              // initial={0}
                 formHorizontal={true}
                 buttonSize={15}
                 buttonInnerColor= {'#034d94'}
                 buttonOuterColor ={'#034d94'}
                 labelStyle={{marginRight:wp('15%'),}}
                 onPress={(value) => {this.setState({radioOne:value}, function(){
                   console.log('radioOne');
                   console.log(this.state.radioOne);
                 })}}
                   />
             </View>

              <Text style={styles.nameStyle}>Have you ever filed bankrucpy?</Text>
                <View style={styles.radioBtn}>
              <RadioForm
               radio_props={radio_props}
              // initial={0}
                 formHorizontal={true}
                 buttonSize={15}
                 buttonInnerColor= {'#034d94'}
                 buttonOuterColor ={'#034d94'}
                 labelStyle={{marginRight:wp('15%'),}}
                         onPress={(value) => {this.setState({radioTwo:value}, function(){
                          console.log('radioTwo');
                          console.log(this.state.radioTwo);
                        })}}
                   />
             </View>

              <Text style={styles.nameStyle}>Have you ever been convicted of selling distributing or manufacturing illegal drugs?</Text>
                <View style={styles.radioBtn}>
              <RadioForm
               radio_props={radio_props}
              // initial={0}
                 formHorizontal={true}
                 buttonSize={15}
                 buttonInnerColor= {'#034d94'}
                 buttonOuterColor ={'#034d94'}
                 labelStyle={{marginRight:wp('15%'),}}
                         onPress={(value) => {this.setState({radioThree:value}, function(){
                          console.log('radioThree');
                          console.log(this.state.radioThree);
                        })}}
                   />
             </View>

              <Text style={styles.uploadTextStyle}>Uplod Id</Text>
               <View style={styles.uploadView}>
               {this.state.filePath === '' ?
            <Text style={styles.uploadText}>Drop/Upload file here</Text>:
            <Text style={styles.uploadText}>{this.state.filePath.type}</Text>
          }
               <TouchableOpacity style={{height:hp('6%'), width:hp('6%'), }}
               onPress={()=>this.chooseFile('ID')}>
            
                   <Image style={styles.calenderImg} source={require('../image/upload.png')}/>
                   </TouchableOpacity>
              </View>
               { this.state.idError == true ? (
             <Text style={styles.errorMessage}>
                * Please enter correct input to proceed.
             </Text>
            ) : null  }

              <Text style={styles.uploadTextStyle}>Income Proof</Text>
               <View style={styles.uploadView}>
               {this.state.filePathIncome === '' ?
            <Text style={styles.uploadText}>Drop/Upload file here</Text>:
            <Text style={styles.uploadText}>{this.state.filePathIncome.type}</Text>
          }
                   <TouchableOpacity style={{height:hp('6%'), width:hp('6%'), }}
               onPress={()=>this.chooseFile('Income')}>
                   <Image style={styles.calenderImg} source={require('../image/upload.png')}/>
                   </TouchableOpacity>
             </View>
              { this.state.incomeError == true ? (
             <Text style={styles.errorMessage}>
                * Please enter correct input to proceed.
             </Text>
            ) : null  }

             <Text style={styles.uploadTextStyle}>Sign Here</Text>
              <View style={styles.signatureView}>
               <SignatureCapture
                    style={{height:'100%', width:'100%'}}
                    ref="sign"
                    onSaveEvent={this._onSaveEvent}
                    onDragEvent={()=>{this._onDragEvent; this.showHideView()}}
                    saveImageFileInExtStorage={false}
                    showNativeButtons={false}
                    showTitleLabel={false}
                    viewMode={"portrait"}/>
                   </View> 
                    { this.state.signerror == true ? (
             <Text style={styles.errorMessage}>
                * Please enter correct input to proceed.
             </Text>
            ) : null  }
 
                    {
                      this.state.isSigning === true?
                      <View style={{ flex: 1, flexDirection: "row" }}>
                      <TouchableHighlight style={styles.buttonStyle}
                          onPress={() => { this.saveSign() } } >
                          <Text>Save</Text>
                      </TouchableHighlight>
   
                      <TouchableHighlight style={styles.buttonStyle}
                          onPress={() => { this.resetSign() } } >
                          <Text>Reset</Text>
                      </TouchableHighlight>
                      </View>
                      :null
                    }
      
<View style={{justifyContent:'space-between',flexDirection:'row'}}>
<Text style={styles.uploadTextStyle}>Disclaimer</Text>
             <CheckBox
                            style={{ padding: 10 }}
                            checkBoxColor={"#034d94"}
                            checkedCheckBoxColor={"#034d94"}
                            isChecked={this.state.isDuplex}
                           onClick={() => this.handleChange()}
                          
                          //  isChecked = { this.state.isDuplex }
                         // checked={this.state.isDuplex}
                         // onClick={() => this.setState({isDuplex: !this.state.isDuplex})}
                          //   onClick = {() => this.checkBox_Test(item.isDuplex) }
                         //   isChecked={item.isDuplex}
                          />
</View>
{ this.state.isDuplexError == true ? (
             <Text style={styles.errorMessage}>
                * Please enter correct input to proceed.
             </Text>
            ) : null  }
                
                  { this.state.isExpanded ?
                   <View style={styles.disclaimerView}>
               <Text style={styles.disclaimerText} > Applicant represents that all the above statements are true and correct, authorizes
                verification of the above items and agrees to furnish additional credit references upon request. Applicant
                 authorizes the Owner/Agent to obtain reports that may include credit reports, unlawful detainer (eviction) reports,
                  bad check searches, social security number verification, fraud warnings, previous tenant history and employment history.
                   Applicant consents to allow 877RENTPRO.com to disclose tenancy information to previous or subsequent Owners/Agents</Text>
               <TouchableOpacity 
                onPress ={() =>this.collapseView()}>
                <Text style={styles.moreOrLess}>Read less</Text>
               </TouchableOpacity>
               </View>
               :
                <View style={[styles.disclaimerView,{height:hp('20%')}]}>
             <Text style={styles.disclaimerText} numberOfLines={5}> Applicant represents that all the above statements are true and correct, authorizes
                verification of the above items and agrees to furnish additional credit references upon request. Applicant
                 authorizes the Owner/Agent to obtain reports that may include credit reports, unlawful detainer (eviction) reports,
                  bad check searches, social security number verification, fraud warnings, previous tenant history and employment history.
                   Applicant consents to allow 877RENTPRO.com to disclose tenancy information to previous or subsequent Owners/Agents</Text>
              <TouchableOpacity 
               onPress ={() =>this.collapseView()}>
              <Text style={styles.moreOrLess}>Read more</Text>
            </TouchableOpacity>
          </View>}
           </ScrollView>
            <ProgressDialog
          // title="Progress Dialog"
          activityIndicatorColor="red"
          activityIndicatorSize="large"
          animationType="fade"
          message="Please, wait..."
          visible={this.state.showProgress}
        />
           </View>
          </View>
           </SafeAreaView>
    );
  }
}
const styles = StyleSheet.create({
container: {
    flex: 1,
   backgroundColor: "#ededed",
   paddingBottom:'3%'
  },
  mainView:{
    backgroundColor:'white',
     height:'100%',
   margin:'5%', 
  borderRadius:10,
  },
 nameStyleWithDot:{
     marginTop:hp('2%'), 
     marginStart:wp('2%'),
 },
 textInput:{
    marginStart:wp('10%'), 
     marginTop:-hp('1.3%'),
     //backgroundColor:'yellow', 
    // height:hp('5%')
 },
 textinputBorder:{
   marginStart:wp('5%'), 
   marginEnd:wp('5%'), 
    backgroundColor:'gray',
     height:1,
      width:'85%',
      marginTop:-hp('1%'), 
      marginBottom:hp('1.2%')
      },
  requiredDot:{
    height:6,
     width:6,
      borderRadius:3,
       backgroundColor:'red',
         marginStart:wp('8%'),
         marginTop:hp('2%'), 
       },
       titleView:{
         height:hp('8%'), 
         justifyContent:'center',
          alignItems:'center'
      },
      titleText:{
        fontSize:16,
         fontWeight:'bold'
    },
    requiredField:{
      flexDirection:'row',
       alignItems:'center'
    },
      nameStyle:{
    marginTop:hp('2%'), 
     marginStart:wp('10%'),
     marginEnd:wp('10%'),
     fontSize:14,
     color:'gray'
  // backgroundColor:'yellow', 
 },  
 uploadTextStyle:{
    marginTop:hp('2%'), 
     marginStart:wp('10%'),
  // backgroundColor:'yellow', 
 },
   uploadView:{
         height:hp('8%'),
          marginStart:'8%',
           marginEnd:'8%',
           marginTop:hp('1%'),
            borderRadius:10,
             borderStyle: 'dotted',
             borderWidth: 1,
              borderColor:'gray',
               flex:1,
                flexDirection:'row', 
                alignItems:'center', 
                justifyContent:'space-around'
 },
   calenderImg:{
         height:hp('3%'),
       width:hp('3%')
  },
  signatureView:{
    marginStart:'10%', 
    marginEnd:'10%',
   borderRadius:10,
  borderWidth:1,
   height:hp('16%'),
  marginTop:hp('2%'),
   marginBottom:hp('2%'),
   padding:3,
  // backgroundColor:'#d3e6ef',
   borderColor:'#034d94'
  },
  disclaimerView:{
     paddingEnd:'10%',
     // height:hp('30%'),
       paddingStart:'10%',
       flex:1,
        marginTop:hp('2%')
  },
  disclaimerText:{
     lineHeight: 20, 
     color:'#737373',
  },
  moreOrLess:{
    fontWeight:'bold',
     color:'#034d94', 
    fontSize:16, 
    textAlign:'right'
  },
  radioBtn:{
    height:hp('7%'),
     justifyContent:'center',
      paddingStart:wp('10%') 
  },
  buttonStyle: {
    flex: 1, justifyContent: "center", alignItems: "center", height: 50,
    backgroundColor: "#eeeeee",
    margin: 10
},
  errorMessage:{
    fontSize:11,
    color:'red',
    marginLeft:'10%'
  }
});


