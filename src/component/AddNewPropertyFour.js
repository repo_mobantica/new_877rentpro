import React, { Component } from 'react';
import { Text, View, SafeAreaView, Image, TouchableOpacity, StyleSheet, Dimensions, Alert,
Button, FlatList, TextInput, ScrollView } from 'react-native';
import {widthPercentageToDP as wp, heightPercentageToDP as hp} from 'react-native-responsive-screen';
import ToolbarHomeImg from '../component/ToolbarHomeImg';
import DateTimePicker from "react-native-modal-datetime-picker";
import Dash from 'react-native-dash';
import RNPickerSelect from 'react-native-picker-select';
import ImagePicker1 from 'react-native-image-picker';
import DocumentPicker from 'react-native-document-picker';
import ImagePicker from 'react-native-image-crop-picker';
import moment from "moment"
import Moment from 'react-moment';
import RadioForm, {RadioButton, RadioButtonInput, RadioButtonLabel} from 'react-native-simple-radio-button';
var radio_props = [
  {label: 'Available Now', value: 0 },
  {label: 'On Lease', value: 1 }
];
var primaryphoto ='';
var imageArray =[];

export default class AddNewPropertyFour extends Component {
    constructor(props) {
    super(props);

    this.state = {
     leasePeriod:'',
      birthday:'',
       isDateTimePickerVisible: false,
       birthdayError:false,
       filePath: '',
       filePathPrimary: '',
       youTubeUrl:'',
       youTubeError: false,
      multipleFile: [],
      primaryphotoError:false,
      availabilityStatus:0,
      dateToSend:''
    };
 }
 resetPress(){
   this.setState({
    leasePeriod:'',
    birthday:'',
     isDateTimePickerVisible: false,
     birthdayError:false,
     filePath: '',
     filePathPrimary: '',
     youTubeUrl:'',
     youTubeError: false,
    multipleFile: [],
    primaryphotoError:false,
    availabilityStatus:0,
    
   })
 }
 showDateTimePicker = () => {
    this.setState({ isDateTimePickerVisible: true });
  };
 
  hideDateTimePicker = () => {
    this.setState({ isDateTimePickerVisible: false });
  };
 
  handleDatePicked = date => {
    
   let sample=moment(date).format('YYYY-MM-DD');
   let show=moment(date).format('DD/MM/YYYY');
     this.setState({ birthday: show, birthdayError:false, dateToSend:sample });
    this.hideDateTimePicker();
    
  };

  nextPress(){
  
    var videoid = this.state.youTubeUrl.match(/(?:https?:\/{2})?(?:w{3}\.)?youtu(?:be)?\.(?:com|be)(?:\/watch\?v=|\/)([^\s&]+)/);
    let count =0;
     if(this.state.birthday== ""  ){
       this.setState({
           birthdayError:true
     }) 
      count++;
    }

    if(primaryphoto== ""  ){
             this.setState({
                 primaryphotoError:true
           }) 
            count++;
          }

      if(videoid === null && this.state.youTubeUrl != '') {
        this.setState({
          youTubeError:true
     }) 
     count++;
     }
     
     
      if(count === 0){
        var date=moment(this.state.birthday).format('YYYY-MM-DD');
        let addFour ={'isNext': true,
          'availabilityStatus' : this.state.availabilityStatus,
          'availableDate':this.state.dateToSend,
          'leasePeriod':this.state.leasePeriod,
          'photoGallery':imageArray,
          'PrimaryPhoto':primaryphoto,
          'youTubeUrl':this.state.youTubeUrl,
       }
         console.log('4444',addFour);    
       return addFour;
      }
      else{
        let addFour ={'isNext': false,
      }
      
      return addFour;
         }
    
    }

    selectImages(){
      ImagePicker.openPicker({
        multiple: true,
        includeBase64: true
      }).then(images => {
        console.log(images);
       
     console.log('sss 1');
     let image;
           for(let i= 0 ; i<images.length; i++){
              image =  'data:'+images[i].mime+';base64,'+images[i].data;
            imageArray.push(image);     
           }
           //this.convertImage(imageArray[0]);
        //    for(let i= 0 ; i<imageArray.length; i++){
        //    this.convertImage(imageArray[i]);
        //  }
        
      });
    };
    
chooseFile(item){
 
  var options = {
    title: 'Select Image',
    // customButtons: [
    //   { name: 'customOptionKey', title: 'Uplod PDF' },
    // ],
    storageOptions: {
      skipBackup: true,
      path: 'images',
    },
  };
  ImagePicker1.showImagePicker(options, response => {
   // console.log('Response = ', response);

    if (response.didCancel) {
      console.log('User cancelled image picker');
    } else if (response.error) {
      console.log('ImagePicker Error: ', response.error);
    } else if (response.customButton) {
      console.log('User tapped custom button: ', response.customButton);
    //  alert(response.customButton);
    this.selectMultipleFile();
    } else {
      let source = response;
    
      let baseImage = { uri: '"'+'data:image/jpeg;base64,' + response.data +'"'}
    
     // if(item === 'ID'){
        idImage = baseImage.uri;
    if(item === 'photo'){
      this.setState({
        filePath: source,
      });
    }
    else{
      primaryphoto = baseImage.uri;
      this.setState({
        filePathPrimary: baseImage.uri,
        primaryphotoError:false
      }, function(){
       // console.log('filePathPrimary....', primaryphoto);
      });
    }
      
    }
  });
};  

convertImage(image){
  fetch('https://1800rentpro.com/rentpro_api/api/upload_image', {
    method: 'POST',
    headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json',
              },
    body: JSON.stringify({
      image: image,
      type:"property"
          })
  }).then((response) => response.json())
    .then((responseJson) => {
    
  if(responseJson.status==1){
   
  console.log('mouse');
console.log(responseJson);
      }
  else{
    Alert.alert(responseJson.message);
                     
  }
}).catch((error) => {              
console.error(error);

}); 
}

  render() {
    
    return (
       <SafeAreaView style={styles.container}>
      <View style={styles.container}>
           <View style={styles.mainView}>
              <ScrollView>
            <Text style={styles.nameStyle}>Availability</Text>
            <View style={{height:hp('7%'), justifyContent:'center', paddingStart:wp('10%') }}>
              <RadioForm
               radio_props={radio_props}
              // initial={0}
                 formHorizontal={true}
                 buttonSize={15}
                 buttonInnerColor= {'#034d94'}
                 buttonOuterColor ={'#034d94'}
                 labelStyle={{marginRight:wp('15%'),}}
                         onPress={(value) => {this.setState({availabilityStatus:value})}}
                   />
             </View>

               <View style={styles.requiredField}>
            <View style={styles.requiredDot}></View>
             <Text style={styles.nameStyleWithDot}>Available Date</Text>
             </View>
             <View style={styles.birthdayView}>
             {this.state.birthday === '' ?
             <Text style={{color:'gray'}}>DD/MM/YYYY</Text> :
             <Text >{this.state.birthday}</Text> 
             }
             <TouchableOpacity 
        style={styles.calenderIconView}
       onPress={this.showDateTimePicker}>
         <Image style={styles.calenderImg} source={require('../image/CalendarBlue.png')}/>
        </TouchableOpacity>
        </View>
        <View style={styles.textinputBorder}></View>
        { this.state.birthdayError == true ? (
             <Text style={styles.errorMessage}>
                 * Please select date
             </Text>
            ) : null  }

         <Text style={styles.nameStyle}>Lease Period</Text>
         <View style={styles.textInput}>
             <RNPickerSelect
            onValueChange={(value) => this.setState({})}
            items={[
                { label: "2 Months", value: "2" },
                { label: "4 Months", value: "4" },
                { label: "6 Months", value: "6" },
                { label: '1 Year', value: '12' },
                { label: '2 Years', value: '24' },
            ]}
        />
        </View>
        <View style={styles.textinputBorder}></View>

        <Text style={styles.nameStyle}>Photo Gallery</Text>
         <TouchableOpacity  onPress={()=>this.selectImages()} style={styles.uploadView}>
           {this.state.filePath === '' ?
            <Text style={styles.uploadText}>Drop/Upload file here</Text>:
            <Text style={styles.uploadText}>{this.state.filePath.type}</Text>
          }
    <TouchableOpacity style={{height:hp('6%'), width:hp('6%'), }}
    onPress={()=>this.selectImages()}>
     <Image style={styles.calenderImg} source={require('../image/upload.png')}/>
     </TouchableOpacity>
    </TouchableOpacity> 

    <View style={styles.requiredField}>
            <View style={styles.requiredDot}></View>
             <Text style={styles.nameStyleWithDot}>Primary photo</Text>
             </View>
    <TouchableOpacity  onPress={()=>this.chooseFile('primary')} style={styles.uploadView}>
           {this.state.filePathPrimary === '' ?
            <Text style={styles.uploadText}>Drop/Upload file here</Text>:
             <Text style={styles.uploadText}>{this.state.filePathPrimary.type}</Text>
          }
    <TouchableOpacity style={{height:hp('6%'), width:hp('6%'), }}
     onPress={()=>this.chooseFile('primary')}>
     <Image style={styles.calenderImg} source={require('../image/upload.png')}/>
     </TouchableOpacity>
    </TouchableOpacity>  
    { this.state.primaryphotoError == true ? (
             <Text style={styles.errorMessage}>
                 * Please insert image
             </Text>
            ) : null  }

    <Text style={styles.nameStyle}>Youtube URL</Text>
            <TextInput
          style={styles.textInput}
           underlineColorAndroid = "transparent"
          placeholder="Enter Middle Name"
          onChangeText={(youTubeUrl) => this.setState({youTubeUrl})}
          value={this.state.youTubeUrl}
        />
        <View style={styles.youTubeError}></View>
        { this.state.youTubeError == true ? (
             <Text style={styles.errorMessage}>
                 Invalid Url
             </Text>
            ) : null  }
           </ScrollView>
           </View>
           
           <DateTimePicker
          isVisible={this.state.isDateTimePickerVisible}
          onConfirm={this.handleDatePicked}
          onCancel={this.hideDateTimePicker}
        />  
          </View>
           </SafeAreaView>
    );
  }
}
const styles = StyleSheet.create({
container: {
    flex: 1,
   backgroundColor: "#ededed",
    paddingBottom:'4%',
  },
  mainView:{
    backgroundColor:'white',
  // flex:1,
   height:'100%',
   margin:'5%', 
  borderRadius:10,
  },
 nameStyleWithDot:{
     marginTop:hp('2%'), 
     marginStart:wp('2%'),
 },
  calenderImg:{
         height:hp('4%'),
       width:hp('4%')
      },
  nameStyle:{
    marginTop:hp('2%'), 
     marginStart:wp('10%'),
  // backgroundColor:'yellow', 
 },
 textInput:{
    marginStart:wp('10%'), 
      marginEnd:wp('10%'), 
     marginTop:-hp('1.3%'),
     //backgroundColor:'yellow', 
    // height:hp('5%')
 },
       uploadView:{
         height:hp('8%'),
          marginStart:'8%',
           marginEnd:'8%',
           marginTop:hp('1%'),
            borderRadius:10,
             borderStyle: 'dotted',
             borderWidth: 1,
              borderColor:'gray',
               flex:1,
                flexDirection:'row', 
                alignItems:'center', 
                justifyContent:'space-around'
          },
      calenderIconView:{
        width:wp('20%'), 
      marginTop:-hp('1%'),
       justifyContent:'center',
       alignItems:'center'
      },
 textinputBorder:{
   marginStart:wp('5%'), 
   marginEnd:wp('5%'), 
    backgroundColor:'gray',
     height:1,
      width:'85%',
      marginTop:-hp('1%')
      },
  requiredDot:{
    height:6,
     width:6,
      borderRadius:3,
       backgroundColor:'red',
         marginStart:wp('8%'),
         marginTop:hp('2%'), 
       },
       titleView:{
         height:hp('8%'), 
         justifyContent:'center',
          alignItems:'center'
      },
      titleText:{
        fontSize:16,
         fontWeight:'bold'
    },
    requiredField:{
      flexDirection:'row',
       alignItems:'center'
    },
     birthdayView:{ 
        flexDirection:'row', 
        height:hp('6%'),
       marginStart:wp('10%'),
        justifyContent:'space-between',
        alignItems:'center',
        marginEnd:wp('5%')
       },
       indicator:{ 
         height:12,
          width:12,
          borderRadius:6,
          backgroundColor:'#008f2c',
         margin:wp('1%'),
       },
       nextBtn:{
         height:hp('7%'),
       backgroundColor:'#034d94',
       width:wp('25%'),
      borderRadius:10,
     justifyContent:'center',
       alignItems:'center',
       flexDirection:'row', 
       paddingStart:wp('2%'),
        paddingEnd:wp('2%')
       },
       nextText:{
         fontSize:13,
          fontWeight:'bold',
           color:'white'
           },
          footer:{
            backgroundColor:'white',
             height:'11%',
              justifyContent:'space-between', 
              flexDirection:'row',
            alignItems:'center',
            paddingStart:'7%',
           paddingEnd:'7%'
       },
       errorMessage:{
        fontSize:11,
        color:'red',
        marginLeft:'10%'
      },
      uploadText:{
        color:'gray', width:'60%'
      }
});
