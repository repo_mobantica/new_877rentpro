import React, { Component } from 'react';
import { Text, View, SafeAreaView, Image, TouchableOpacity, StyleSheet, Dimensions, Alert,
Button, FlatList, TextInput, ScrollView } from 'react-native';
import {widthPercentageToDP as wp, heightPercentageToDP as hp} from 'react-native-responsive-screen';
import DateTimePicker from "react-native-modal-datetime-picker";
import Dash from 'react-native-dash';
import RNPickerSelect from 'react-native-picker-select';

export default class AddNewPropertyThree extends Component {
    constructor(props) {
    super(props);

    this.state = {
    bedrooms:'',
    fullBaths:'',
    minsquarefeet:'',
    laundry:'',
    parkingType:'',
    parkingFees:'',
    minrent:'',
    deposit:'',
    bedroomsError:false,
    fullBathError:false,
    minFeetError:false,
    laundryError:false,
    parktypeError:false,
    parkfeesError:false,
    minRentError:false,
    depositeError:false,
    };
 }

minFeetValidate = (text3) => {
  const reg = /^[0-9\b]+$/;
  
  if(text3!=''){
  if (reg.test(text3) === false) {
    return false;
  } 
  else {
  this.setState({minsquarefeet:text3,
  minFeetError:false})
  }
  }
  else{
  this.setState({minsquarefeet:'',
minFeetError:true})
  }
}

parkFeeValidation = (text3) => {
  const reg = /^[0-9\b]+$/;
  
  if(text3!=''){
  if (reg.test(text3) === false) {
    return false;
  } 
  else {
  this.setState({parkingFees:text3,
  parkfeesError:false})
  }
  }
  else{
  this.setState({parkingFees:'',
  parkfeesError:true})
  }
}

minRentValidation = (text3) => {
  const reg = /^[0-9\b]+$/;
  
  if(text3!=''){
  if (reg.test(text3) === false) {
    return false;
  } 
  else {
  this.setState({minrent:text3,
  minRentError:false})
  }
  }
  else{
  this.setState({minrent:'',
minRentError:true})
  }
}

depositeValidation = (text3) => {
  const reg = /^[0-9\b]+$/;
  
  if(text3!=''){
  if (reg.test(text3) === false) {
    return false;
  } 
  else {
  this.setState({deposit:text3,
  depositeError:false})
  }
  }
  else{
  this.setState({deposit:'',
depositeError:true})
  }
}

     nextPress(){
  
      let data =0;
       if(this.state.bedrooms== "" ){
         this.setState({
             bedroomsError:true
       }) 
       data++;
      }
     if(this.state.fullBaths== ""){
          this.setState({
            fullBathError:true
          })
    data++;
         }
     if(this.state.laundry== ""){
            this.setState({
              laundryError:true
            })
      data++;
           }
      if(this.state.parkingType== ""){
              this.setState({
                parktypeError:true
              })
        data++;
             }
        if(this.state.minimumRent== ""){
                this.setState({
                  minRentError:true
                })
          data++;
               }
          if(this.state.deposit== ""){
                  this.setState({
                    depositeError:true
                  })
            data++;
                 }
       

        if(data === 0){
          let addThree ={'isNext': true,
          'bedrooms' : this.state.bedrooms,
          'fullBaths': this.state.fullBaths,
          'minsquarefeet':this.state.minsquarefeet,
          'laundry':this.state.laundry,
          'parkingType':this.state.parkingType,
          'parkingFees':this.state.parkingFees,
          'minrent':this.state.minrent,
          'deposit':this.state.deposit,
                 }
             
              return addThree;
            }
       else{
      let addThree ={'isNext': false,
                 }
            return addThree;
      }

}
componentWillReceiveProps(props) {
  var data = props.data;
  console.log("data123",data)
  let that = this;
  if (data != null) {
    var guarantor = false;
    // if (data.guarantor == "1") {
    //   guarantor = true;
    // }


    that.setState({
      bedrooms: data.bedrooms,
      fullBaths: data.fullbaths,
     appliancesState: data.appliancesState,
     minsquarefeet:data.minsquarefeet,
     laundry:data.laundry_type,
     parkingType:data.parking_type,
     parkingfee:data.parkingfee,
     minrent:data.minrent,
     deposit:data.mindeposit,
     minsquarefeet:data.minsquarefeet
     
    });
    console.log("bedrooms",this.state.bedrooms)
  }
}
  render() {
    
    return (
       <SafeAreaView style={styles.container}>
      <View style={styles.container}>
       
           <View style={styles.mainView}>
           <ScrollView>
           <View style={styles.requiredField}>
            <View style={styles.requiredDot}></View>
             <Text style={styles.nameStyleWithDot}>Bedrooms</Text>
             </View>
        <View style={styles.textInput}>
             <RNPickerSelect
           placeholder={{ }}
          value={this.state.bedrooms}
             selectedValue={this.state.bedrooms}
             onValueChange={(value) => this.setState({bedrooms:value, 
              bedroomsError:false})}
            items={[
                { label: '1', value: '1' },
                { label: '2', value: '2' },
                { label: '3', value: '3' },
                { label: '4', value: '4' },
                { label: '5', value: '5' },
                { label: '6', value: '6' },
                { label: '7', value: '7' },
            ]}
          
        />
        </View>
        <View style={styles.textinputBorder}></View>
        { this.state.bedroomsError == true ? (
             <Text style={styles.errorMessage}>
                 * Please enter correct input to proceed.
             </Text>
            ) : null  }


        <View style={styles.requiredField}>
            <View style={styles.requiredDot}></View>
             <Text style={styles.nameStyleWithDot}>Full Baths</Text>
             </View>
        <View style={styles.textInput}>
             <RNPickerSelect
              placeholder={{ }}
              value={this.state.fullBaths}
              onValueChange={(value) => this.setState({fullBaths:value, 
              fullBathError:false})}
              items={[
                { label: '1', value: '1' },
                { label: '1 1/2', value: '1 1/2' },
                { label: '2', value: '2' },
                { label: '2 1/2', value: '2 1/2' },
                { label: '3', value: '3' },
                { label: '3 1/2', value: '3 1/2' },
                { label: '4', value: '4' },
                { label: '4 1/2', value: '4 1/2' },
                { label: '5', value: '5' },
                { label: '5 1/2', value: '5 1/2' },
                { label: '6', value: '6' },
                { label: '6 1/2', value: '6 1/2' },
                { label: '7', value: '7' },
                { label: '7 1/2', value: '7 1/2' },

            ]}
        />
        </View>
        <View style={styles.textinputBorder}></View>
        { this.state.fullBathError == true ? (
             <Text style={styles.errorMessage}>
                 * Please enter correct input to proceed.
             </Text>
            ) : null  }


        <Text style={styles.nameStyle}>Mininun Square Feet</Text>
            <TextInput
          style={styles.textInput}
          keyboardType="number-pad"
           underlineColorAndroid = "transparent"
          placeholder="Enter Mininun Square Feet"
          onChangeText={text => this.minFeetValidate(text)} 
          value={this.state.minsquarefeet}
        />
        <View style={styles.textinputBorder}></View>
        { this.state.minFeetError == true ? (
             <Text style={styles.errorMessage}>
                 * Please enter correct input to proceed.
             </Text>
            ) : null  }

        <View style={styles.requiredField}>
            <View style={styles.requiredDot}></View>
             <Text style={styles.nameStyleWithDot}>Laundry Type</Text>
             </View>
        <View style={styles.textInput}>
             <RNPickerSelect
                placeholder={{ }}
                value={this.state.laundry}
           onValueChange={(value) => this.setState({laundry:value, 
            laundryError:false})}
            items={[
                { label: 'Washer/Dryer', value: '1' },
                { label: 'Washer/Dryer Hookup', value: '2' },
                { label: 'Laundery Facilities', value: '3' },
            ]}
        />
        </View>
        <View style={styles.textinputBorder}></View>
        { this.state.laundryError == true ? (
             <Text style={styles.errorMessage}>
                 * Please enter correct input to proceed.
             </Text>
            ) : null  }


        <View style={styles.requiredField}>
            <View style={styles.requiredDot}></View>
             <Text style={styles.nameStyleWithDot}>Parking Type</Text>
             </View>
        <View style={styles.textInput}>
             <RNPickerSelect
               placeholder={{ }}
               value={this.state.parkingType}
             onValueChange={(value) => this.setState({parkingType:value, 
              parktypeError:false})}
            items={[
                { label: 'Surface Lot', value: '0' },
                { label: 'Covered', value: '1' },
                { label: 'Street', value: '2' },
                { label: 'Garage', value: '3' },
            ]}
        />
        </View>
        <View style={styles.textinputBorder}></View>
        { this.state.parktypeError == true ? (
             <Text style={styles.errorMessage}>
                 * Please enter correct input to proceed.
             </Text>
            ) : null  }


         <Text style={styles.nameStyle}>Parking Fees</Text>
            <TextInput
          style={styles.textInput}
          keyboardType="numeric"
           underlineColorAndroid = "transparent"
          placeholder="Parking Fees/Month"
          onChangeText={text => this.parkFeeValidation(text)} 
          value={this.state.parkingfee}
          returnKeyType = {"next"}
          autoFocus = {true}
        onSubmitEditing={(event) => { 
           this.refs.minRent.focus(); 
         }}
        />
        <View style={styles.textinputBorder}></View>


         <View style={styles.requiredField}>
            <View style={styles.requiredDot}></View>
             <Text style={styles.nameStyleWithDot}>Minimum Rent</Text>
             </View>
            <TextInput
          style={styles.textInput}
          keyboardType="numeric"
           underlineColorAndroid = "transparent"
          placeholder="Enter Minimum Rent"
          onChangeText={text => this.minRentValidation(text)} 
          value={this.state.minrent}
          ref='minRent'
           returnKeyType = {"next"}
           onSubmitEditing={(event) => { 
            this.refs.minDeposite.focus(); 
          }}
        />
         <View style={styles.textinputBorder}></View>
         { this.state.minRentError == true ? (
             <Text style={styles.errorMessage}>
                 * Please enter correct input to proceed.
             </Text>
            ) : null  }


         <View style={styles.requiredField}>
            <View style={styles.requiredDot}></View>
             <Text style={styles.nameStyleWithDot}>Minimum Deposite</Text>
             </View>
            <TextInput
          style={styles.textInput}
          keyboardType="numeric"
           underlineColorAndroid = "transparent"
          placeholder="Enter Minimum Deposite here"
          onChangeText={text => this.depositeValidation(text)} 
          value={this.state.deposit}
          ref='minDeposite'
        />
         <View style={styles.textinputBorder}></View>
         { this.state.depositeError == true ? (
             <Text style={styles.errorMessage}>
                 * Please enter correct input to proceed.
             </Text>
            ) : null  }

        
          </ScrollView>
          </View>
            
          </View>
           </SafeAreaView>
    );
  }
}
const styles = StyleSheet.create({
container: {
    flex: 1,
   backgroundColor: "#ededed",
    paddingBottom:'4%',
  },
  mainView:{
    backgroundColor:'white',
  // flex:1,
   height:'100%',
   margin:'5%', 
  borderRadius:10,
  },
 nameStyleWithDot:{
     marginTop:hp('2%'), 
     marginStart:wp('2%'),
 },
 textInput:{
    marginStart:wp('5%'), 
    marginEnd:wp('5%'),
     marginTop:-hp('1.3%'),
     paddingStart:wp('5%'),
    //  borderBottomWidth:1,
    // borderColor:'gray',
    //  textAlignVertical:"bottom"
     //backgroundColor:'yellow', 
    // height:hp('5%')
 },
 textinputBorder:{
   marginStart:wp('5%'), 
   marginEnd:wp('5%'), 
    backgroundColor:'gray',
     height:1,
      width:'85%',
      marginTop:-hp('1%')
      },
  requiredDot:{
    height:6,
     width:6,
      borderRadius:3,
       backgroundColor:'red',
         marginStart:wp('8%'),
         marginTop:hp('2%'), 
       },
       titleView:{
         height:hp('8%'), 
         justifyContent:'center',
          alignItems:'center'
      },
      titleText:{
        fontSize:16,
         fontWeight:'bold'
    },
    requiredField:{
      flexDirection:'row',
       alignItems:'center'
    },
       indicator:{ 
         height:12,
          width:12,
          borderRadius:6,
          backgroundColor:'#008f2c',
         margin:wp('1%'),
       },
       nextBtn:{
         height:hp('7%'),
       backgroundColor:'#034d94',
       width:wp('25%'),
      borderRadius:10,
     justifyContent:'center',
       alignItems:'center',
       flexDirection:'row', 
       paddingStart:wp('2%'),
        paddingEnd:wp('2%')
       },
       nextText:{
         fontSize:13,
          fontWeight:'bold',
           color:'white'
           },
          footer:{
            backgroundColor:'white',
             height:'11%',
              justifyContent:'space-between', 
              flexDirection:'row',
            alignItems:'center',
            paddingStart:'7%',
           paddingEnd:'7%'
       },
       nameStyle:{
    marginTop:hp('2%'), 
     marginStart:wp('10%'),
  // backgroundColor:'yellow', 
 },
 errorMessage:{
  fontSize:11,
  color:'red',
  marginLeft:'10%'
}
});
