import React, { Component } from 'react';
import {
  Text, View, SafeAreaView, Image, TouchableOpacity, StyleSheet, Dimensions, Alert,
  Button, FlatList, TextInput, ScrollView
} from 'react-native';
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';
import RNPickerSelect from 'react-native-picker-select';
export default class ApplyForRentFour extends Component {
  constructor(props) {
    super(props);

    this.state = {
      creditorName: '',
      address: '',
      payment: '',
      phone: '',
      creditorNameError: false,
      addressError: false,
      paymenterror: false,
      phoneError: false
    };
  }

  addressvalidate(text) {
    const reg = /^[A-Za-z 0-9.,\b]+$/;
    if (text != '') {
      if (reg.test(text) === false) {
        return false;
      }
      else {
        this.setState({
          address: text,
          addressError: false
        })
      }
    }
    else {
      this.setState({
        address: '',
        addressError: true
      })
    }
  }

  namevalidate(text) {
    const reg = /^[A-Z a-z\b]+$/;
    if (text != '') {
      if (reg.test(text) === false) {
        return false;
      }
      else {
        this.setState({
          creditorName: text,
          creditorNameError: false
        })
      }
    }
    else {
      this.setState({
        creditorName: '',
        creditorNameError: true
      })
    }
  }

  phoneValidate = (text3) => {
    const reg = /^[0-9\b]+$/;

    if (text3 != '') {
      if (reg.test(text3) === false) {
        return false;
      }
      else {
        this.setState({
          phone: text3,
          phoneError: false
        })
      }
    }
    else {
      this.setState({
        phone: '',
        phoneError: true
      })
    }
  }

  paymentValidate = (text3) => {
    const reg = /^[0-9.\b]+$/;

    if (text3 != '') {
      if (reg.test(text3) === false) {
        return false;
      }
      else {
        this.setState({
          payment: text3,
          paymenterror: false
        })
      }
    }
    else {
      this.setState({
        payment: '',
        paymenterror: true
      })
    }
  }

  nextPress() {
    let count = 0;
    let phone = this.state.phone;
    let payment = this.state.payment;

    if (this.state.creditorName == "") {
      this.setState({
        creditorNameError: true
      })
      count++;
    }
    if (this.state.address == "") {
      this.setState({
        addressError: true
      })
      count++;
    }
    if (this.state.phone == "" || phone.length < 10) {
      this.setState({
        phoneError: true
      })
      count++;
    }
    if (this.state.payment == "" || payment.split('.').length > 2 || payment.charAt(0) === '.' || payment.charAt(payment.length - 1) === '.') {
      this.setState({
        paymenterror: true
      })
      count++;
    }
    if (count === 0) {
      let rentFour = {
        'isNext': true,
        'financeCreditorName': this.state.creditorName,
        'financeAddress': this.state.address,
        'financePhone': this.state.phone,
        'finanacePayment': this.state.payment,
      }
      console.log('rentFour', rentFour);
      return rentFour;
    }
    else {
      let rentFour = {
        'isNext': false,
      }
      console.log('rentFour', rentFour);
      return rentFour;
    }

  }

  render() {
    //August, 09 2019
    return (
      <SafeAreaView style={styles.container}>
        <View style={styles.container}>
          <View style={styles.mainView}>
            <ScrollView>
              <View style={styles.titleView}>
                <Text style={styles.titleText}>Financial Obligations</Text>
              </View>

              <View style={styles.requiredField}>
                <View style={styles.requiredDot}></View>
                <Text style={styles.nameStyleWithDot}>Name of Creditor</Text>
              </View>
              <TextInput
                style={styles.textInput}
                underlineColorAndroid="transparent"
                placeholder="Enter Name of Creditor"
                onChangeText={text => this.namevalidate(text)}
                value={this.state.creditorName}
                returnKeyType={"next"}
                autoFocus={true}
                onSubmitEditing={(event) => {
                  this.refs.addressNext.focus();
                }}
              />
              <View style={styles.textinputBorder}></View>
              {this.state.creditorNameError == true ? (
                <Text style={styles.errorMessage}>
                  * Please enter correct input to proceed.
             </Text>
              ) : null}

              <View style={styles.requiredField}>
                <View style={styles.requiredDot}></View>
                <Text style={styles.nameStyleWithDot}>Address</Text>
              </View>
              <View style={styles.textInputView}>
                <TextInput
                  style={styles.textInput}
                  underlineColorAndroid="transparent"
                  style={{ flex: 1, textAlignVertical: 'top' }}
                  numberOfLines={3}
                  placeholder="Address here"
                  onChangeText={text => this.addressvalidate(text)}
                  value={this.state.address}
                  multiline={true}
                  ref='addressNext'
                  returnKeyType={"next"}
                  onSubmitEditing={(event) => {
                    this.refs.phoneNext.focus();
                  }}
                />
              </View>
              <View style={styles.textinputBorder}></View>
              {this.state.addressError == true ? (
                <Text style={styles.errorMessage}>
                  * Please enter correct input to proceed.
             </Text>
              ) : null}

              <View style={styles.requiredField}>
                <View style={styles.requiredDot}></View>
                <Text style={styles.nameStyleWithDot}>Phone Number</Text>
              </View>
              <TextInput
                style={styles.textInput}
                maxLength={10}
                keyboardType={'numeric'}
                underlineColorAndroid="transparent"
                placeholder="Enter Phone Number"
                onChangeText={text => this.phoneValidate(text)}
                value={this.state.phone}
                ref='phoneNext'
                returnKeyType={"next"}
                onSubmitEditing={(event) => {
                  this.refs.paymentNext.focus();
                }}
              />
              <View style={styles.textinputBorder}></View>
              {this.state.phoneError == true ? (
                <Text style={styles.errorMessage}>
                  * Please enter correct input to proceed.
             </Text>
              ) : null}

              <View style={styles.requiredField}>
                <View style={styles.requiredDot}></View>
                <Text style={styles.nameStyleWithDot}>Montly Payment</Text>
              </View>
              <TextInput
                style={styles.textInput}
                underlineColorAndroid="transparent"
                placeholder="Enter Montly Payment here"
                keyboardType={'numeric'}
                onChangeText={text => this.paymentValidate(text)}
                value={this.state.payment}
                ref='paymentNext'
              />
              <View style={styles.textinputBorder}></View>
              {this.state.paymenterror == true ? (
                <Text style={styles.errorMessage}>
                  * Please enter correct input to proceed.
             </Text>
              ) : null}
            </ScrollView>
          </View>
        </View>
      </SafeAreaView>
    );
  }
}
const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "#ededed",
    paddingBottom: '3%'
  },
  mainView: {
    backgroundColor: 'white',
    height: '100%',
    margin: '5%',
    borderRadius: 10,
  },
  nameStyleWithDot: {
    marginTop: hp('2%'),
    marginStart: wp('2%'),
  },
  textInput: {
    marginStart: wp('10%'),
    marginTop: -hp('1.3%'),
    //backgroundColor:'yellow', 
    // height:hp('5%')
  },
  textInputView: {
    height: hp('13%'),
    marginStart: '10%',
    marginEnd: '10%',
    justifyContent: 'flex-start',
  },
  textinputBorder: {
    marginStart: wp('5%'),
    marginEnd: wp('5%'),
    backgroundColor: 'gray',
    height: 1,
    width: '85%',
    marginTop: -hp('1%'),
    marginBottom: hp('1.2%')
  },
  requiredDot: {
    height: 6,
    width: 6,
    borderRadius: 3,
    backgroundColor: 'red',
    marginStart: wp('8%'),
    marginTop: hp('2%'),
  },
  titleView: {
    height: hp('8%'),
    justifyContent: 'center',
    alignItems: 'center'
  },
  titleText: {
    fontSize: 16,
    fontWeight: 'bold'
  },
  requiredField: {
    flexDirection: 'row',
    alignItems: 'center'
  },
  errorMessage: {
    fontSize: 11,
    color: 'red',
    marginLeft: '10%'
  }
});
