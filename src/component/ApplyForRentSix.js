import React, { Component } from 'react';
import { Text, View, SafeAreaView, Image, TouchableOpacity, StyleSheet, Dimensions, Alert,
Button, FlatList, TextInput, ScrollView } from 'react-native';
import {widthPercentageToDP as wp, heightPercentageToDP as hp} from 'react-native-responsive-screen';
import CheckBox from 'react-native-check-box';
import Dash from 'react-native-dash';
import RNPickerSelect from 'react-native-picker-select';
import DatePicker from 'react-native-datepicker'
import commonStyles from "../styles/index";

export default class ApplyForRentSix extends Component {
    constructor(props) {
    super(props);

    this.state = {
     checked:'2',
     data:['Student', 'Employed', 'Self Employed  '],
      catCheck:false,
      dogCheck:false,
      occupation:'',
      income:'',
      frequency:'1',
      otherSource:'',
      occupationError:false,
      incomeError: false,
      frequencyError: false, 
      otherSourceError: false,
      employerName:'',
      employerAdress:'',
      dateTo:'',
      dateFrom:'',
      employerNameError:false,
      employerAdressError:false,
      dateToError: false,
      dateFromError:false,
      date:"",
      maxDate:''
    };
 }

 componentDidMount() {
  let that = this;

  let date = new Date().getDate();
  let month = new Date().getMonth() + 1;
  let year = new Date().getFullYear();
  let current_date = date + '/' + month + '/' + year;
  that.setState({
    date:current_date,
    maxDate: current_date
  }, function(){
    console.log('datatat');
    console.log(this.state.date);
  });
}

addressvalidate(text) {    
  const reg =  /^[A-Za-z 0-9.,\b]+$/;
   if(text!=''){
     if (reg.test(text) === false) {
       return false;
     }
     else {
     this.setState({employerAdress:text,
    employerAdressError:false})
     }
     }
     else{
     this.setState({employerAdress:'',
    employerAdressError:true})
     }
  }  

  namevalidate(text) {    
    const reg =  /^[A-Za-z\b]+$/;
     if(text!=''){
       if (reg.test(text) === false) {
         return false;
       }
       else {
       this.setState({employerName:text,
         employerNameError:false})
       }
       }
       else{
       this.setState({employerName:'',
      employerNameError:true})
       }
    }

 occupationValidation(text) {    
  const reg =  /^[A-Za-z 0-9.,\b]+$/;
   if(text!=''){
     if (reg.test(text) === false) {
       return false;
     }
     else {
     this.setState({occupation:text,
    occupationError:false})
     }
     }
     else{
     this.setState({occupation:'',
    occupationError:true})
     }
  }  

    sourceValidate(text) {    
    const reg =  /^[A-Za-z 0-9.,\b]+$/;
     if(text!=''){
       if (reg.test(text) === false) {
         return false;
       }
       else {
       this.setState({otherSource:text,
        otherSourceError:false})
       }
       }
       else{
       this.setState({otherSource:'',
      otherSourceError:true})
       }
    }  

    incomeValidate = (text3) => {
      const reg = /^[0-9.\b]+$/;
      
      if(text3!=''){
      if (reg.test(text3) === false) {
        return false;
      } 
      else {
     this.setState({income:text3,
      incomeError:false})
      }
      }
      else{
      this.setState({income:'',
      incomeError:true})
      }
    }

    nextPress(){
      let count =0;
       let income= this.state.income;
     
         if(this.state.occupation== ""){
        this.setState({
         occupationError:true
        })
   count++;
       }
  if(this.state.income== "" || income.split('.').length >2 || income.charAt(0) === '.' || income.charAt(income.length-1) === '.' ){
        this.setState({
  incomeError:true
        })
  count++;
       }
     if(this.state.frequency== ""){
            this.setState({
      frequencyError:true
            })
      count++;
           }

      if(this.state.employerName === "" && this.state.checked=== '1'){
              this.setState({
        employerNameError:true
              })
        count++;
             }

        if(this.state.employerAdress== ""  && this.state.checked=== '1'){
                this.setState({
          employerAdressError:true
                })
          count++;
               }

          if(this.state.dateTo== ""  && this.state.checked=== '1'){
                  this.setState({
            dateToError:true
                  })
            count++;
                 }

            if(this.state.dateFrom== ""  && this.state.checked=== '1'){
                    this.setState({
              dateFromError:true
                    })
              count++;
                   }
         
              if(count === 0){
               let cat = 0;
                let dog= 0;
                   if(this.state.catCheck){
                     cat=1;
                   }
                   else{
                     cat =0;
                   }

                   if(this.state.dogCheck){
                     dog=1;
                   }
                   else{
                     dog =0;
                   }
                let rentSix ={'isNext': true,
                          'tenanatIs':this.state.checked,
                           'cat': cat,
                           'dog': dog,
                           'employerName' : this.state.employerName,
                           'employerAdress': this.state.employerAdress,
                           'dateFrom':this.state.dateFrom,
                           'dateTo':this.state.dateTo,
                           'occupation':this.state.occupation,
                           'income':this.state.income,
                           'frequency':this.state.frequency,
                           'otherSource':this.state.otherSource,
                    }
                     console.log('rentSix', rentSix);
               return rentSix;
              }
             else{
              let rentSix ={'isNext': false,
                    }
                     console.log('rentSix', rentSix);
               return rentSix;
             } 
      }


  render() {
    
    return (
       <SafeAreaView style={styles.container}>
      <View style={styles.container}>
          
           <View style={styles.mainView}>
           <ScrollView>
             <View style={styles.titleView}>
            <Text style={styles.titleText}>Employment Details</Text>
            </View>

            <Text style={styles.nameStyle}>I am a,</Text>

              <View style={styles.radioBtnRow}>
             <TouchableOpacity style= {styles.radioBtn} onPress={() =>{this.setState({checked:'2',
              dateFromError:false, dateToError:false,
                   employerAdressError:false, employerNameError:false})}}>
                {
                   this.state.checked == '2' ?
                    <Image style={styles.radioImg} source={require('../image/blueCheck.png')}/>
                    :
                    <Image style={styles.radioImg} source={require('../image/UnCheckedBlue.png')}/>
                }
                 <Text>{this.state.data[0]}</Text>
                </TouchableOpacity>

                 <TouchableOpacity style= {styles.radioBtn}   onPress={() =>{this.setState({checked:'1'})}}>
                {
                   this.state.checked == '1' ?
                    <Image style={styles.radioImg} source={require('../image/blueCheck.png')}/>
                    :
                    <Image style={styles.radioImg} source={require('../image/UnCheckedBlue.png')}/>
                }
                 <Text>{this.state.data[1]}</Text>
                </TouchableOpacity>
                </View>
                 <View style={styles.radioBtnRow}>
                <TouchableOpacity style= {styles.radioBtn}  onPress={() =>{this.setState({checked:'0', dateFromError:false, dateToError:false,
                   employerAdressError:false, employerNameError:false})}}>
                {
                   this.state.checked == '0' ?
                    <Image style={styles.radioImg} source={require('../image/blueCheck.png')}/>
                    :
                    <Image style={styles.radioImg} source={require('../image/UnCheckedBlue.png')}/>
                }
                 <Text>{this.state.data[2]}</Text>
                </TouchableOpacity>
                </View>
           
                   <Text style={styles.nameStyle}>Pets allowed?</Text>
                   <View style={styles.radioBtnRow}>
                 <CheckBox
                   style={styles.checkBoxStyle}
                     checkBoxColor={'#034d94'}
                    checkedCheckBoxColor={'#034d94'}
                     onClick={()=>{
                     this.setState({
                      catCheck:!this.state.catCheck
                      })
                       }}
                   isChecked={this.state.catCheck}
                   />
                    <Text style={styles.checkBoxText}>Cat</Text>

                    <CheckBox
                   style={styles.checkBoxStyle}
                     checkBoxColor={'#034d94'}
                    checkedCheckBoxColor={'#034d94'}
                     onClick={()=>{
                     this.setState({
                      dogCheck:!this.state.dogCheck
                      })
                       }}
                   isChecked={this.state.dogCheck}
                   />
                    <Text style={styles.checkBoxText}>Dog</Text>
            </View>

               {this.state.checked === '1' ? 
               <View>       
            <View style={styles.requiredField}>
            <View style={styles.requiredDot}></View>
             <Text style={styles.nameStyleWithDot}>Employer Name</Text>
             </View>
            <TextInput
          style={styles.textInput}
           underlineColorAndroid = "transparent"
          placeholder="Enter Employer Name here"
          onChangeText={text => this.namevalidate(text)}  
          value={this.state.employerName}
          returnKeyType = {"next"}
          autoFocus = {true}
        onSubmitEditing={(event) => { 
           this.refs.incomeNext.focus(); 
         }}
        />
         <View style={styles.textinputBorder}></View>
         </View>
         : null}

         { this.state.employerNameError == true ? 
             <Text style={styles.errorMessage}>
                * Please enter correct input to proceed.
             </Text>
            : <View style={{height:hp('3%')}}></View>  }
         
         {this.state.checked === '1' ? 
               <View> 
            <View style={styles.requiredField}>
            <View style={styles.requiredDot}></View>
             <Text style={styles.nameStyleWithDot}>Employer Address</Text>
             </View>
            <TextInput
          style={styles.textInput}
           underlineColorAndroid = "transparent"
          placeholder="Enter Employer Address here"
          onChangeText={text => this.addressvalidate(text)}  
          value={this.state.employerAdress}
          returnKeyType = {"next"}
          autoFocus = {true}
        onSubmitEditing={(event) => { 
           this.refs.incomeNext.focus(); 
         }}
        />
        <View style={styles.textinputBorder}></View>
        </View>
        : null}
         
         { this.state.employerAdressError == true ? (
             <Text style={styles.errorMessage}>
                * Please enter correct input to proceed.
             </Text>
            ) : <View style={{height:hp('3%')}}></View>  }

          {this.state.checked === '1' ? 
               <View> 
       <View style={commonStyles.requiredField}>
            <View style={commonStyles.requiredDot}></View>
             <Text style={commonStyles.nameStyleWithDot}>From</Text>
             </View>
             <View style={styles.birthdayView}>
             {this.state.dateFrom === '' ?
             <Text style={{color:'gray', marginRight:'20%'}}>DD/MM/YYYY</Text> :
             <Text style={{ marginRight:'25%'}}>{this.state.dateFrom}</Text> 
             }
              <DatePicker
        style={{width: 200}}
        date={this.state.date}
        mode="date"
        placeholder="select date"
        format="DD/MM/YYYY"
       // minDate="2016-05-01"
        maxDate={this.state.maxDate}
        confirmBtnText="Confirm"
        cancelBtnText="Cancel"
        hideText='true'
        iconSource= {require('../image/CalendarBlue.png')}
        customStyles={{
         
          dateInput: {
            borderWidth: 0,
            },
            dateIcon: {
              bottom:2
            },
        }}
        onDateChange={(date) => {this.setState({dateFrom:date, dateFromError:false})}}
      />
          </View>
          <View style={commonStyles.textinputBorder}></View>
          </View>: null}
        
        { this.state.dateFromError == true ? (
             <Text style={commonStyles.errorMessage}>
              Please select date
             </Text>
            ) : null  }
      
      {this.state.checked === '1' ? 
               <View> 
      <View style={commonStyles.requiredField}>
            <View style={commonStyles.requiredDot}></View>
             <Text style={commonStyles.nameStyleWithDot}>To</Text>
             </View>
             <View style={styles.birthdayView}>
             {this.state.dateTo === '' ?
             <Text style={{color:'gray', marginRight:'20%'}}>DD/MM/YYYY</Text> :
             <Text style={{ marginRight:'25%'}}>{this.state.dateTo}</Text> 
             }
              <DatePicker
        style={{width: 200}}
        date={this.state.date}
        mode="date"
        placeholder="select date"
        format="DD/MM/YYYY"
       // minDate="2016-05-01"
        maxDate={this.state.maxDate}
        confirmBtnText="Confirm"
        cancelBtnText="Cancel"
        hideText='true'
        iconSource= {require('../image/CalendarBlue.png')}
        customStyles={{
         
          dateInput: {
            borderWidth: 0,
            },
            dateIcon: {
              bottom:2
            },
        }}
        onDateChange={(date) => {this.setState({dateTo:date, dateToError:false})}}
      />
       </View>
       <View style={commonStyles.textinputBorder}></View>
      </View>
      : null}
          
        { this.state.dateToError == true ? (
             <Text style={commonStyles.errorMessage}>
               please select date
             </Text>
            ) : null  }

             <View style={styles.requiredField}>
            <View style={styles.requiredDot}></View>
             <Text style={styles.nameStyleWithDot}>Present Occupation</Text>
             </View>
            <TextInput
          style={styles.textInput}
           underlineColorAndroid = "transparent"
          placeholder="Enter Present Occupation here"
          onChangeText={text => this.occupationValidation(text)}  
          value={this.state.occupation}
          returnKeyType = {"next"}
          autoFocus = {true}
        onSubmitEditing={(event) => { 
           this.refs.incomeNext.focus(); 
         }}
        />
         <View style={styles.textinputBorder}></View>
         { this.state.occupationError == true ? (
             <Text style={styles.errorMessage}>
                * Please enter correct input to proceed.
             </Text>
            ) : <View style={{height:hp('3%')}}></View>  }

            <View style={styles.requiredField}>
            <View style={styles.requiredDot}></View>
             <Text style={styles.nameStyleWithDot}>Current Gross Income</Text>
             </View>
            <TextInput
          style={styles.textInput}
           underlineColorAndroid = "transparent"
          placeholder="Enter Current Gross Income"
          onChangeText={text => this.incomeValidate(text)}  
          value={this.state.income}
          keyboardType='number-pad'
          ref='incomeNext'
          returnKeyType = {"next"}
          onSubmitEditing={(event) => { 
           this.refs.SourceNext.focus(); 
         }}
        />
         <View style={styles.textinputBorder}></View>
         { this.state.incomeError == true ? (
             <Text style={styles.errorMessage}>
                * Please enter correct input to proceed.
             </Text>
            ) : <View style={{height:hp('3%')}}></View>  }

          <View style={styles.requiredField}>
            <View style={styles.requiredDot}></View>
             <Text style={styles.nameStyleWithDot}>Frequency </Text>
             </View>
        <View style={styles.textInput}>
             <RNPickerSelect
            onValueChange={(value) => this.setState({frequency:value, 
              frequencyError:false})}
            items={[
                { label: 'Yearly', value: '1' },
                { label: 'Monthly', value: '2' },
                { label: 'Weekly', value: '3' },
            ]}
        />
        </View>
        <View style={styles.textinputBorder}></View>
        { this.state.frequencyError == true ? (
             <Text style={styles.errorMessage}>
                * Please enter correct input to proceed.
             </Text>
            ) : <View style={{height:hp('3%')}}></View>  }

             <Text style={styles.nameStyleNoDot}>Other Source</Text>
            <TextInput
          style={styles.textInput}
           underlineColorAndroid = "transparent"
          placeholder="Enter Other Source here"
          onChangeText={text => this.sourceValidate(text)}  
          value={this.state.otherSource}
          ref='SourceNext'
        />
         <View style={styles.textinputBorder}></View>
         { this.state.otherSourceError == true ? (
             <Text style={styles.errorMessage}>
                * Please enter correct input to proceed.
             </Text>
            ) : <View style={{height:hp('3%')}}></View>  }

            </ScrollView>       
           </View>  
          </View>
           </SafeAreaView>
    );
  }
}
const styles = StyleSheet.create({
container: {
    flex: 1,
   backgroundColor: "#ededed",
   paddingBottom:'3%'
  },
  mainView:{
    backgroundColor:'white',
     height:'100%',
   margin:'5%', 
  borderRadius:10,
  },
 nameStyleWithDot:{
     marginTop:hp('2%'), 
     marginStart:wp('2%'),
 },
 textInput:{
    marginStart:wp('10%'), 
     marginTop:-hp('1.3%'),
     //backgroundColor:'yellow', 
    // height:hp('5%')
 },
 textinputBorder:{
   marginStart:wp('5%'), 
   marginEnd:wp('5%'), 
    backgroundColor:'gray',
     height:1,
      width:'85%',
      marginTop:-hp('1%'), 
      marginBottom:hp('1.2%')
      },
  requiredDot:{
    height:6,
     width:6,
      borderRadius:3,
       backgroundColor:'red',
         marginStart:wp('8%'),
         marginTop:hp('2%'), 
       },
       titleView:{
         height:hp('8%'), 
         justifyContent:'center',
          alignItems:'center'
      },
      titleText:{
        fontSize:16,
         fontWeight:'bold'
    },
    requiredField:{
      flexDirection:'row',
       alignItems:'center'
    },
     radioImg:{
       height:hp('4%'),
       width:hp('4%'),
      marginRight:10
    },
     radioBtn:{
          flexDirection:'row',
     alignItems:'center',
     width:'40%',
     // backgroundColor:'red'
 },
 radioBtnRow:{
   flexDirection:'row',
    height:hp('7%'),
     alignItems:'center', 
     paddingStart:'10%',
     paddingEnd:'10%'
},
 nameStyle:{
    marginTop:hp('2%'), 
     marginStart:wp('10%'),
     fontSize:16,
     color:'gray'
  // backgroundColor:'yellow', 
 }, 
 nameStyleNoDot:{
  marginTop:hp('5%'), 
   marginStart:wp('10%')
},
  checkBoxStyle:{ 
        paddingEnd: 10
  },
  checkBoxText:{
    width:'40%' 
    },
    errorMessage:{
      fontSize:11,
      color:'red',
      marginLeft:'10%'
    },
    birthdayView:{ 
      flexDirection:'row', 
      height:hp('6%'),
     marginStart:wp('10%'),
      justifyContent:'space-between',
      alignItems:'center',
      marginEnd:wp('5%')
 },
});
