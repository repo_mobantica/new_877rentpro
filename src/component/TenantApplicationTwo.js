import React, { Component } from 'react';
import { Text, View, SafeAreaView, Image, TouchableOpacity, StyleSheet, Dimensions, Alert,
Button, FlatList, TextInput, ScrollView } from 'react-native';
import {widthPercentageToDP as wp, heightPercentageToDP as hp} from 'react-native-responsive-screen';
import RNPickerSelect from 'react-native-picker-select';
export default class TenentApplicationTwo extends Component {
    constructor(props) {
    super(props);
    this.state = {
     bankName:'',
     accountType:'',
     accountNumber:'',
     avgBalance:'',
     bankNameError:false,
     accountTypeError:false,
     accountNumberError:false,
     avgBalanceError:false,
     creditorName:'',
     address:'',
     payment:'',
     phone:'',
     creditorNameError:false,
     addressError:false,
     paymenterror:false,
     phoneError:false,
     name:'',
     emergenctaddress:'',
     relationship:'',
     Emegrencyphone:'',
     nameError:false,
     emergenctaddressError:false,
     relationshipError:false,
     EmegrencyphoneError:false
    };
 }
 componentWillReceiveProps(props){
  var data=props.data;
   let that = this;
     if(data!=null){

      that.setState({
       bankName:data.bank_name,
       accountType:data.acc_type,
       accountNumber:data.acc_no,
       avgBalance:data.avg_monthly_bal,
       creditorName:data.fin_obl_creditor_name_1,
 address:data.fin_obl_address_1,
 payment:data.fin_obl_monthly_payment_1,
 phone:data.fin_obl_phone_1,
 name:data.emergencycontact_name,
 emergenctaddress:data.emergencycontact_address,
 Emegrencyphone:data.emergencycontact_phone,
 relationship:data.emergencycontact_relationship
       });
   }
 }
 nameValidate(text) {    
  const reg =  /^[A-Za-z\b]+$/;
   if(text!=''){
     if (reg.test(text) === false) {
       return false;
     }
     else {
     this.setState({bankName:text,
    bankNameError:false})
     }
     }
     else{
     this.setState({bankName:'',
     bankNameError:true})
     }
  }

  balanceValidate = (text3) => {
    const reg = /^[0-9.\b]+$/;
    
    if(text3!=''){
    if (reg.test(text3) === false) {
      return false;
    } 
    else {
   this.setState({avgBalance:text3,
    avgBalanceError:false})
    }
    }
    else{
    this.setState({avgBalance:'',
    avgBalanceError:true})
    }
  }

  accountNoValidate = (text3) => {
    const reg = /^[0-9\b]+$/;
    
    if(text3!=''){
    if (reg.test(text3) === false) {
      return false;
    } 
    else {
    this.setState({accountNumber:text3,
    accountNumberError:false})
    }
    }
    else{
    this.setState({accountNumber:'',
    accountNumberError:true})
    }
  }  

  nextPress(){
    let count =0;
     let avgBalance = this.state.avgBalance;

       if(this.state.bankName== ""){
      this.setState({
       bankNameError:true
      })
 count++;
     }
if(this.state.accountType== ""){
      this.setState({
accountTypeError:true
      })
count++;
     }
   if(this.state.accountNumber== ""){
          this.setState({
    accountNumberError:true
          })
    count++;
         }
       if(this.state.avgBalance== "" || avgBalance.split('.').length >2 || avgBalance.charAt(0) === '.' || avgBalance.charAt(avgBalance.length-1) === '.'){
        //this.state.income== "" || income.split('.').length >2 || income.charAt(0) === '.' || income.charAt(income.length-1) === '.' 
              this.setState({
        avgBalanceError:true
              })
        count++;
             }
        if(count === 0){
          let rentThree ={'isNext': true,
                        'bankName':this.state.bankName,
                        'accountType': this.state.accountType,
                        'accountNumber': this.state.accountNumber,
                        'avgBalance' : this.state.avgBalance,
                        'financeCreditorName':this.state.creditorName,
                        'financeAddress': this.state.address,
                        'financePhone': this.state.phone,
                        'finanacePayment' : this.state.payment,
                        'emergencyName':this.state.name,
                        'emergencyAddress': this.state.address,
                        'emergencyRelationship': this.state.relationship,
                        'emergencyPhone' : this.state.phone,
              }
               console.log('rentThree', rentThree);
         return rentThree;
        }
       else{
        let rentThree ={'isNext': false,
              }
               console.log('renrentThreetOne', rentThree);
         return rentThree;
       } 
    }

  render() {
    
    return (
       <SafeAreaView style={styles.container}>
      <View style={styles.container}>
           <View style={styles.mainView}>
           <ScrollView>
             <View style={styles.titleView}>
            <Text style={styles.titleText}>Bank Information</Text>
            </View>

            <View style={styles.requiredField}>
            <View style={styles.requiredDot}></View>
             <Text style={styles.nameStyleWithDot}>Bank Name</Text>
             </View>
            <TextInput
          style={styles.textInput}
           underlineColorAndroid = "transparent"
          placeholder="Enter Bank Name Please"
          onChangeText={text => this.nameValidate(text)} 
          value={this.state.bankName}
          returnKeyType = {"next"}
          autoFocus = {true}
        onSubmitEditing={(event) => { 
           this.refs.AccontNoNext.focus(); 
         }}
        />
         <View style={styles.textinputBorder}></View>
         { this.state.bankNameError == true ? (
             <Text style={styles.errorMessage}>
                * Please enter correct input to proceed.
             </Text>
            ) : null  }
          

         <View style={styles.requiredField}>
            <View style={styles.requiredDot}></View>
             <Text style={styles.nameStyleWithDot}>Type of Account </Text>
             </View>
             <TextInput
          style={styles.textInput}
           underlineColorAndroid = "transparent"
          placeholder="Your Account here"
          onChangeText={text => this.setState({accountType:text, accountTypeError:false})} 
          value={this.state.accountNumber}
          maxLength={14}
          ref='AccontNoNext'
           returnKeyType = {"next"}
           onSubmitEditing={(event) => { 
            this.refs.avgBalanceNext.focus(); 
          }}
        />
        <View style={styles.textinputBorder}></View>
        { this.state.accountTypeError == true ? (
             <Text style={styles.errorMessage}>
                * Please enter correct input to proceed.
             </Text>
            ) : null  }

         <View style={styles.requiredField}>
            <View style={styles.requiredDot}></View>
             <Text style={styles.nameStyleWithDot}>Account Number</Text>
             </View>
            <TextInput
          style={styles.textInput}
           underlineColorAndroid = "transparent"
          placeholder="Enter Account number here"
          onChangeText={text => this.accountNoValidate(text)} 
          value={this.state.accountNumber}
          maxLength={14}
          keyboardType="numeric"
          ref='AccontNoNext'
           returnKeyType = {"next"}
           onSubmitEditing={(event) => { 
            this.refs.avgBalanceNext.focus(); 
          }}
        />
         <View style={styles.textinputBorder}></View>
         { this.state.accountNumberError == true ? (
             <Text style={styles.errorMessage}>
                * Please enter correct input to proceed.
             </Text>
            ) : null  }

          <View style={styles.requiredField}>
            <View style={styles.requiredDot}></View>
             <Text style={styles.nameStyleWithDot}>Average Montly Balance</Text>
             </View>
            <TextInput
          style={styles.textInput}
           underlineColorAndroid = "transparent"
          placeholder="Enter Average Montly Balance"
          onChangeText={text => this.balanceValidate(text)} 
          maxLength={15}
          value={this.state.avgBalance}
          keyboardType="numeric"
          ref='avgBalanceNext'
        />
         <View style={styles.textinputBorder}></View>
         { this.state.avgBalanceError == true ? (
             <Text style={styles.errorMessage}>
                * Please enter correct input to proceed.
             </Text>
            ) : null  }
              <View style={styles.titleView}>
            <Text style={styles.titleText}>Financial Obligations</Text>
            </View>

            <View style={styles.requiredField}>
            <View style={styles.requiredDot}></View>
             <Text style={styles.nameStyleWithDot}>Name of Creditor</Text>
             </View>
            <TextInput
          style={styles.textInput}
           underlineColorAndroid = "transparent"
          placeholder="Enter Name of Creditor"
          onChangeText={text => this.namevalidate(text)} 
          value={this.state.creditorName}
          returnKeyType = {"next"}
           autoFocus = {true}
         onSubmitEditing={(event) => { 
            this.refs.addressNext.focus(); 
          }}
        />
         <View style={styles.textinputBorder}></View>
         { this.state.creditorNameError == true ? (
             <Text style={styles.errorMessage}>
                * Please enter correct input to proceed.
             </Text>
            ) : null  }

          <View style={styles.requiredField}>
            <View style={styles.requiredDot}></View>
             <Text style={styles.nameStyleWithDot}>Address</Text>
             </View>
             <View style={styles.textInputView}>
            <TextInput
         style={styles.textInput}
           underlineColorAndroid = "transparent"
             style = {{ flex: 1, textAlignVertical:'top' }}
            numberOfLines={3}
          placeholder="Address here"
          onChangeText={text => this.addressvalidate(text)} 
          value={this.state.address}
          multiline={true}
          ref='addressNext'
          returnKeyType = {"next"}
          onSubmitEditing={(event) => { 
           this.refs.phoneNext.focus(); 
         }}
        />
        </View>
        <View style={styles.textinputBorder}></View>
        { this.state.addressError == true ? (
             <Text style={styles.errorMessage}>
                * Please enter correct input to proceed.
             </Text>
            ) : null  }

         <View style={styles.requiredField}>
            <View style={styles.requiredDot}></View>
             <Text style={styles.nameStyleWithDot}>Phone Number</Text>
             </View>
            <TextInput
          style={styles.textInput}
          maxLength={10}
          keyboardType={'numeric'}
           underlineColorAndroid = "transparent"
          placeholder="Enter Phone Number"
          onChangeText={text => this.phoneValidate(text)} 
          value={this.state.phone}
          ref='phoneNext'
          returnKeyType = {"next"}
          onSubmitEditing={(event) => { 
           this.refs.paymentNext.focus(); 
         }}
        />
         <View style={styles.textinputBorder}></View>
         { this.state.phoneError == true ? (
             <Text style={styles.errorMessage}>
                * Please enter correct input to proceed.
             </Text>
            ) : null  }

         <View style={styles.requiredField}>
            <View style={styles.requiredDot}></View>
             <Text style={styles.nameStyleWithDot}>Montly Payment</Text>
             </View>
            <TextInput
          style={styles.textInput}
           underlineColorAndroid = "transparent"
          placeholder="Enter Montly Payment here"
          keyboardType={'numeric'}
          onChangeText={text => this.paymentValidate(text)} 
          value={this.state.payment}
          ref='paymentNext'
        />
         <View style={styles.textinputBorder}></View>
         { this.state.paymenterror == true ? (
             <Text style={styles.errorMessage}>
                * Please enter correct input to proceed.
             </Text>
            ) : null  }
                 <View style={styles.titleView}>
            <Text style={styles.titleText}>Emergency Contact</Text>
            </View>

            <View style={styles.requiredField}>
            <View style={styles.requiredDot}></View>
             <Text style={styles.nameStyleWithDot}>Name</Text>
             </View>
            <TextInput
          style={styles.textInput}
           underlineColorAndroid = "transparent"
          placeholder="Enter Name Please"
          onChangeText={text => this.namevalidate(text)}  
          value={this.state.name}
          returnKeyType = {"next"}
          autoFocus = {true}
        onSubmitEditing={(event) => { 
           this.refs.addressNext.focus(); 
         }}
        />
         <View style={styles.textinputBorder}></View>
         { this.state.nameError == true ? (
             <Text style={styles.errorMessage}>
               * Please enter correct input to proceed.
             </Text>
            ) : null  }

          <View style={styles.requiredField}>
            <View style={styles.requiredDot}></View>
             <Text style={styles.nameStyleWithDot}>Address</Text>
             </View>
             <View style={styles.textInputView}>
            <TextInput
         style={styles.textInput}
           underlineColorAndroid = "transparent"
             style = {{ flex: 1, textAlignVertical:'top' }}
            numberOfLines={3}
          placeholder="Address here"
          onChangeText={text => this.addressvalidate(text)}  
          value={this.state.emergenctaddress}
          multiline={true}
          ref='addressNext'
          returnKeyType = {"next"}
          onSubmitEditing={(event) => { 
           this.refs.relationNext.focus(); 
         }}
        />
        </View>
        <View style={styles.textinputBorder}></View>
        { this.state.emergenctaddressError == true ? (
             <Text style={styles.errorMessage}>
               * Please enter correct input to proceed.
             </Text>
            ) : null  }

         <View style={styles.requiredField}>
            <View style={styles.requiredDot}></View>
             <Text style={styles.nameStyleWithDot}>RelationShip</Text>
             </View>
            <TextInput
         style={styles.textInput}
           underlineColorAndroid = "transparent"
             style = {{ flex: 1, textAlignVertical:'top' }}
            numberOfLines={3}
          placeholder="relation here"
          onChangeText={text => this.setState({relationship:text})}  
          value={this.state.relationship}
          multiline={true}
          ref='relationNext'
          returnKeyType = {"next"}
          onSubmitEditing={(event) => { 
           this.refs.PhoneNext.focus(); 
         }}
        />
        <View style={styles.textinputBorder}></View>
       

         <View style={styles.requiredField}>
            <View style={styles.requiredDot}></View>
             <Text style={styles.nameStyleWithDot}>Phone</Text>
             </View>
            <TextInput
          style={styles.textInput}
          maxLength={10}
          keyboardType={'numeric'}
           underlineColorAndroid = "transparent"
          placeholder="Enter Phone number here"
          onChangeText={text => this.phoneValidate(text)}  
          value={this.state.Emegrencyphone}
          ref='PhoneNext'
        />
         <View style={styles.textinputBorder}></View>
         { this.state.Emegrencyphone == true ? (
             <Text style={styles.errorMessage}>
               * Please enter correct input to proceed.
             </Text>
            ) : null  }
           </ScrollView>
           </View>
           </View>
           </SafeAreaView>
    );
  }
}
const styles = StyleSheet.create({
container: {
    flex: 1,
   backgroundColor: "#ededed",
   paddingBottom:'3%'
  },
  mainView:{
    backgroundColor:'white',
     height:'100%',
   margin:'5%', 
  borderRadius:10,
  },
 nameStyleWithDot:{
     marginTop:hp('2%'), 
     marginStart:wp('2%'),
 },
 textInput:{
    marginStart:wp('10%'), 
     marginTop:-hp('1.3%'),
     paddingEnd:'8%'
     //backgroundColor:'yellow', 
    // height:hp('5%')
 },
   textInputView:{
           height:hp('13%'),
            marginStart:'10%',
             marginEnd:'10%',
             justifyContent: 'flex-start',
        },
 textinputBorder:{
   marginStart:wp('5%'), 
   marginEnd:wp('5%'), 
    backgroundColor:'gray',
     height:1,
      width:'85%',
      marginTop:-hp('1%'), 
      marginBottom:hp('1.2%')
      },
  requiredDot:{
    height:6,
     width:6,
      borderRadius:3,
       backgroundColor:'red',
         marginStart:wp('8%'),
         marginTop:hp('2%'), 
       },
       titleView:{
         height:hp('8%'), 
         justifyContent:'center',
          alignItems:'center'
      },
      titleText:{
        fontSize:16,
         fontWeight:'bold'
    },
    requiredField:{
      flexDirection:'row',
       alignItems:'center'
    },
    errorMessage:{
      fontSize:11,
      color:'red',
      marginLeft:'10%'
    }
});
