import React, { Component } from 'react';
import { Text, View, SafeAreaView, Image, TouchableOpacity, StyleSheet, Dimensions, Alert,
Button, FlatList, TextInput, ScrollView } from 'react-native';
import {widthPercentageToDP as wp, heightPercentageToDP as hp} from 'react-native-responsive-screen';
import RNPickerSelect from 'react-native-picker-select';
export default class ApplyForRentFive extends Component {
    constructor(props) {
    super(props);

    this.state = {
     name:'',
     address:'',
     relationship:'',
     phone:'',
     nameError:false,
     addressError:false,
     relationshipError:false,
     phoneError:false
    };
 }

 addressvalidate(text) {    
  const reg =  /^[A-Za-z 0-9.,\b]+$/;
   if(text!=''){
     if (reg.test(text) === false) {
       return false;
     }
     else {
     this.setState({address:text,
    addressError:false})
     }
     }
     else{
     this.setState({address:'',
    addressError:true})
     }
  }  

  namevalidate(text) {    
    const reg =  /^[A-Z a-z\b]+$/;
     if(text!=''){
       if (reg.test(text) === false) {
         return false;
       }
       else {
       this.setState({name:text,
         nameError:false})
       }
       }
       else{
       this.setState({name:'',
      nameError:true})
       }
    }

    phoneValidate = (text3) => {
      const reg = /^[0-9\b]+$/;
      
      if(text3!=''){
      if (reg.test(text3) === false) {
        return false;
      } 
      else {
     this.setState({phone:text3,
      phoneError:false})
      }
      }
      else{
      this.setState({phone:'',
      phoneError:true})
      }
    }

    nextPress(){
      let count =0;
      let phone = this.state.phone;
     
         if(this.state.name== ""){
        this.setState({
         nameError:true
        })
   count++;
       }
  if(this.state.address== ""){
        this.setState({
  addressError:true
        })
  count++;
       }
     if(this.state.phone== "" || phone.length < 10){
            this.setState({
      phoneError:true
            })
      count++;
           }
         if(this.state.relationship== ""){
                this.setState({
          relationshipError:true
                })
          count++;
               }
          if(count === 0){
            let rentFive ={'isNext': true,
                      'emergencyName':this.state.name,
                       'emergencyAddress': this.state.address,
                       'emergencyRelationship': this.state.relationship,
                       'emergencyPhone' : this.state.phone,
                }
                 console.log('rentFive', rentFive);
           return rentFive;
          }
         else{
          let rentFive ={'isNext': false,
                }
                 console.log('rentFive', rentFive);
           return rentFive;
         }   
      }

  render() {
    
    return (
       <SafeAreaView style={styles.container}>
      <View style={styles.container}>
           <View style={styles.mainView}>
           <ScrollView>
             <View style={styles.titleView}>
            <Text style={styles.titleText}>Emergency Contact</Text>
            </View>

            <View style={styles.requiredField}>
            <View style={styles.requiredDot}></View>
             <Text style={styles.nameStyleWithDot}>Name</Text>
             </View>
            <TextInput
          style={styles.textInput}
           underlineColorAndroid = "transparent"
          placeholder="Enter Name Please"
          onChangeText={text => this.namevalidate(text)}  
          value={this.state.name}
          returnKeyType = {"next"}
          autoFocus = {true}
        onSubmitEditing={(event) => { 
           this.refs.addressNext.focus(); 
         }}
        />
         <View style={styles.textinputBorder}></View>
         { this.state.nameError == true ? (
             <Text style={styles.errorMessage}>
               * Please enter correct input to proceed.
             </Text>
            ) : null  }

          <View style={styles.requiredField}>
            <View style={styles.requiredDot}></View>
             <Text style={styles.nameStyleWithDot}>Address</Text>
             </View>
             <View style={styles.textInputView}>
            <TextInput
         style={styles.textInput}
           underlineColorAndroid = "transparent"
             style = {{ flex: 1, textAlignVertical:'top' }}
            numberOfLines={3}
          placeholder="Address here"
          onChangeText={text => this.addressvalidate(text)}  
          value={this.state.address}
          multiline={true}
          ref='addressNext'
          returnKeyType = {"next"}
          onSubmitEditing={(event) => { 
           this.refs.relationNext.focus(); 
         }}
        />
        </View>
        <View style={styles.textinputBorder}></View>
        { this.state.addressError == true ? (
             <Text style={styles.errorMessage}>
               * Please enter correct input to proceed.
             </Text>
            ) : null  }

         <View style={styles.requiredField}>
            <View style={styles.requiredDot}></View>
             <Text style={styles.nameStyleWithDot}>RelationShip</Text>
             </View>
            <TextInput
         style={styles.textInput}
           underlineColorAndroid = "transparent"
             style = {{ flex: 1, textAlignVertical:'top' }}
            numberOfLines={3}
          placeholder="relation here"
          onChangeText={text => this.setState({relationship:text})}  
          value={this.state.relationship}
          multiline={true}
          ref='relationNext'
          returnKeyType = {"next"}
          onSubmitEditing={(event) => { 
           this.refs.PhoneNext.focus(); 
         }}
        />
        <View style={styles.textinputBorder}></View>
       

         <View style={styles.requiredField}>
            <View style={styles.requiredDot}></View>
             <Text style={styles.nameStyleWithDot}>Phone</Text>
             </View>
            <TextInput
          style={styles.textInput}
          maxLength={10}
          keyboardType={'numeric'}
           underlineColorAndroid = "transparent"
          placeholder="Enter Phone number here"
          onChangeText={text => this.phoneValidate(text)}  
          value={this.state.phone}
          ref='PhoneNext'
        />
         <View style={styles.textinputBorder}></View>
         { this.state.phoneError == true ? (
             <Text style={styles.errorMessage}>
               * Please enter correct input to proceed.
             </Text>
            ) : null  }
           </ScrollView>
           </View>
           </View>
           </SafeAreaView>
    );
  }
}
const styles = StyleSheet.create({
container: {
    flex: 1,
   backgroundColor: "#ededed",
   paddingBottom:'3%'
  },
  mainView:{
    backgroundColor:'white',
     height:'100%',
   margin:'5%', 
  borderRadius:10,
  },
 nameStyleWithDot:{
     marginTop:hp('2%'), 
     marginStart:wp('2%'),
 },
 textInput:{
    marginStart:wp('10%'), 
     marginTop:-hp('1.3%'),
       paddingEnd:'8%'
     //backgroundColor:'yellow', 
    // height:hp('5%')
 },
   textInputView:{
           height:hp('13%'),
            marginStart:'10%',
             marginEnd:'10%',
             justifyContent: 'flex-start',
        },
 textinputBorder:{
   marginStart:wp('5%'), 
   marginEnd:wp('5%'), 
    backgroundColor:'gray',
     height:1,
      width:'85%',
      marginTop:-hp('1%'), 
      marginBottom:hp('1.2%')
      },
  requiredDot:{
    height:6,
     width:6,
      borderRadius:3,
       backgroundColor:'red',
         marginStart:wp('8%'),
         marginTop:hp('2%'), 
       },
       titleView:{
         height:hp('8%'), 
         justifyContent:'center',
          alignItems:'center'
      },
      titleText:{
        fontSize:16,
         fontWeight:'bold'
    },
    requiredField:{
      flexDirection:'row',
       alignItems:'center'
    },
    errorMessage:{
      fontSize:11,
      color:'red',
      marginLeft:'10%'
    } 
});
