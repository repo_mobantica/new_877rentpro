import React, { Component } from 'react';
import { Text, View, SafeAreaView, Image, TouchableOpacity, StyleSheet, Dimensions, Alert,
Button, FlatList, TextInput, ScrollView } from 'react-native';
import {widthPercentageToDP as wp, heightPercentageToDP as hp} from 'react-native-responsive-screen';
import ToolbarHomeImg from '../component/ToolbarHomeImg';
import DateTimePicker from "react-native-modal-datetime-picker";
import Dash from 'react-native-dash';
import RNPickerSelect from 'react-native-picker-select';
import CheckBox from 'react-native-check-box'
import Modal from "react-native-modal";
import Icon from 'react-native-vector-icons/Ionicons'
export default class AddNewPropertyFive extends Component {
    constructor(props) {
    super(props);

    this.state = {
     firstName:'',
      lastName:'',
      mobileNo:'',
      email:'',
      firstCheck:false,
      secondCheck:false,
      thirdCheck:false,
      emailError:false,
      data: ['Free', 'Gold', 'Platinum', 'Diamond'],
      packageName:'Free Package (Non-exclusive)',
    checked:'4', 
    thirdCheckError:false,
    firstNameError:false,
    lastNameError:false,
    emailError:false,
    mobileError:false,
    isModalVisible: false,
    package:[{ title: 'Unlimited prospective renter phone and email leads'},
  {title: 'Advertisement Published on 88+ of the most popular rental listing sites'},
  {title: 'Photos and video upload'},
  {title: 'Partial Display of address for nonpaying renter members'},
  {title: '30 Day Runtime'},
  {title:"$FREE"}
]
    };
 }
 toggleModal(){
  this.setState({ isModalVisible: !this.state.isModalVisible });
};
 resetPress(){
   this.setState({
    firstName:'',
    lastName:'',
    mobileNo:'',
    email:'',
    firstCheck:false,
    secondCheck:false,
    thirdCheck:false,
    emailError:false,
    data: ['Free', 'Gold', 'Platinum', 'Diamond'],
  checked:'4', 
  thirdCheckError:false,
  firstNameError:false,
  lastNameError:false,
  emailError:false,
  mobileError:false,
  packageName:'Free Package (Non-exclusive)',
  package:[{ title: 'Unlimited prospective renter phone and email leads'},
  {title: 'Advertisement Published on 88+ of the most popular rental listing sites'},
  {title: 'Photos and video upload'},
  {title: 'Partial Display of address for nonpaying renter members'},
  {title: '30 Day Runtime'},
  {title:"$FREE"}
]
   })
 }
 namevalidate(text) {    
  const reg =  /^[A-Za-z\b]+$/;
   if(text!=''){
     if (reg.test(text) === false) {
       return false;
     }
     else {
     this.setState({firstName:text,
    firstNameError:false})
     }
     }
     else{
     this.setState({firstName:'',
    firstNameError:true})
     }
  }

  lastNamevalidate(text) {    
    const reg =  /^[A-Za-z\b]+$/;
     if(text!=''){
       if (reg.test(text) === false) {
         return false;
       }
       else {
       this.setState({lastName:text,
      lastNameError:false})
       }
       }
       else{
       this.setState({lastName:'',
      lastNameError:true})
       }
    }

    mobileValidate(text) {    
      const reg =  /^[0-9\b]+$/;
       if(text!=''){
         if (reg.test(text) === false) {
           return false;
         }
         else {
         this.setState({mobileNo:text,
        mobileError:false})
         }
         }
         else{
         this.setState({mobileNo:'',
        mobileError:true})
         }
      }

      nextPress(){
        let count = 0;
        let rent_collect= '0';
        let visibilitystatus ='0';

        const emailReg=  /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;
            if(emailReg.test(this.state.email) === false || this.state.email==''){
             this.setState({
               emailError:true
             }) 
           count++;
            }

        if(this.state.thirdCheck== false ){
                 this.setState({
                     thirdCheckError:true
               }) 
              count++; 
              }

              if(this.state.firstName=='' ){
                       this.setState({
                           firstNameError:true
                     }) 
                    count++; 
                    }

                    if(this.state.lastName=='' ){
                             this.setState({
                                 lastNameError:true
                           }) 
                          count++; 
                          }

                        if(this.state.mobileNo=='' || this.state.mobileNo.length<10 ){
                                   this.setState({
                                       mobileError:true
                                 }) 
                                count++; 
                                }

                                if(this.state.firstCheck){
                                     rent_collect ='1';
                                }

                                if(this.state.secondCheck){
                                  visibilitystatus ='1';
                             }
                      
                                if(count === 0){
                                  let addFive ={'isNext': true,
                                    'firstName' : this.state.firstName,
                                    'lastName': this.state.lastName,
                                    'email':this.state.email,
                                    'mobileNo':this.state.mobileNo,
                                    'package':this.state.checked,
                                    'rent_collect':rent_collect,
                                    'visibilitystatus':visibilitystatus,
                                 }
                                       
                                 return addFive;
                                }
                                else{
                                  let addFive ={'isNext': false,
                                }
                                return addFive;
                                   }
        }

 
    
  render() {
    
    return (
       <SafeAreaView style={styles.container}>
      <View style={styles.container}>
         
           <View style={styles.mainView}>
              <ScrollView>
              <Text style={styles.nameStyle}>First Name</Text>
            <TextInput
          style={styles.textInput}
           underlineColorAndroid = "transparent"
          placeholder="Enter First Name"
          onChangeText={text => this.namevalidate(text)}  
          value={this.state.firstName}
          returnKeyType = {"next"}
          autoFocus = {true}
        onSubmitEditing={(event) => { 
           this.refs.LastN.focus(); 
         }}
        />
        <View style={styles.textinputBorder}></View>
        { this.state.firstNameError == true ? (
             <Text style={styles.errorMessage}>
                 * Please enter correct input to proceed.
             </Text>
            ) : null  }

         <Text style={styles.nameStyle}>Last Name</Text>
            <TextInput
          style={styles.textInput}
           underlineColorAndroid = "transparent"
          placeholder="Enter Last Name"
          onChangeText={text => this.lastNamevalidate(text)}  
          value={this.state.lastName}
          ref='LastN'
           returnKeyType = {"next"}
           onSubmitEditing={(event) => { 
            this.refs.EmailN.focus(); 
          }}
        />
        <View style={styles.textinputBorder}></View>
        { this.state.lastNameError == true ? (
             <Text style={styles.errorMessage}>
                 * Please enter correct input to proceed.
             </Text>
            ) : null  }

         <Text style={styles.nameStyle}>Email</Text>
            <TextInput
          style={styles.textInput}
           underlineColorAndroid = "transparent"
          placeholder="Enter Email "
          onChangeText={(email) => this.setState({email:email, emailError:false})}
          value={this.state.email}
          ref='EmailN'
           returnKeyType = {"next"}
           onSubmitEditing={(event) => { 
            this.refs.MobileN.focus(); 
          }}
        />
        <View style={styles.textinputBorder}></View>
        { this.state.emailError == true ? (
             <Text style={styles.errorMessage}>
                 * Please enter correct input to proceed.
             </Text>
            ) : null  }

         <Text style={styles.nameStyle}>Mobile</Text>
            <TextInput
          style={styles.textInput}
          keyboardType="numeric"
           underlineColorAndroid = "transparent"
          placeholder="Enter Mobile Number"
          maxLength={10}
          onChangeText={text => this.mobileValidate(text)}  
          value={this.state.mobileNo}
          ref='MobileN'
        />
        <View style={styles.textinputBorder}></View>
        { this.state.mobileError == true ? (
             <Text style={styles.errorMessage}>
                 * Please enter correct input to proceed.
             </Text>
            ) : null  }

        <Text style={styles.nameStyle}>Select Package</Text>

              <View style={styles.radioBtnRow}>
             <TouchableOpacity style= {styles.radioBtn} onPress={() =>{this.setState({checked:'4',
             packageName:'Free Package (Non-exclusive)',
             package:[{ title: 'Unlimited prospective renter phone and email leads'},
             {title: 'Advertisement Published on 88+ of the most popular rental listing sites'},
             {title: 'Photos and video upload'},
             {title: 'Partial Display of address for nonpaying renter members'},
             {title: '30 Day Runtime'},
             {title:"$FREE"}
           ]
            
            })}}>
                {
                   this.state.checked == '4' ?
                    <Image style={styles.radioImg} source={require('../image/blueCheck.png')}/>
                    :
                    <Image style={styles.radioImg} source={require('../image/UnCheckedBlue.png')}/>
                }
                 <Text>{this.state.data[0]}</Text>
                </TouchableOpacity>

                 <TouchableOpacity style= {styles.radioBtn}   onPress={() =>{this.setState({checked:'3',
                  packageName:'Gold Package(Non-exclusive)',
                  package:[{ title: 'Displayed in Featured Listings'},
                  {title: 'Unlimited prospective renter Phone and email leads'},
                  {title: 'Advertisement Published on 88+ of the most popular rental listing sites'},
                  {title: 'Photos and video upload'},
                  {title: 'FULL display of rental address for nonpaying renter members'},
                  {title:'30 Day Runtime'},
                  {title: 'Tenant Prescreening'},
                  {title:"$14.95 per 30 days-Pay Now"}
]
                })}}>
                {
                   this.state.checked == '3' ?
                    <Image style={styles.radioImg} source={require('../image/blueCheck.png')}/>
                    :
                    <Image style={styles.radioImg} source={require('../image/UnCheckedBlue.png')}/>
                }
                 <Text>{this.state.data[1]}</Text>
                </TouchableOpacity>
                </View>
                 <View style={styles.radioBtnRow}>
                <TouchableOpacity style= {styles.radioBtn}  onPress={() =>{this.setState({checked:'2',
                packageName:'Platinum Package (Exclusive with Licensed Agent)',
                package:[{ title: 'Desired Tenancy: Short-term 1-11 month'},
                {title: 'Displayed in Featured Listings'},
                {title: 'Unlimited prospective renter Phone and email leads'},
                {title: 'Advertisement Published on 88+ of the most popular rental listing sites'},
                {title: 'Professional Photography and videography-Unlimited Media Upload'},
                {title:"FULL display of rental address for nonpaying renter members"},
                {title: '30 Day Runtime'},
                {title: 'Tenant Prescreening'},
                {title:"Full Tenant’s Credit and background check"},
                {title:"50% of first month’s rent-Pay Later"}
              ]
                })}}>
                {
                   this.state.checked == '2' ?
                    <Image style={styles.radioImg} source={require('../image/blueCheck.png')}/>
                    :
                    <Image style={styles.radioImg} source={require('../image/UnCheckedBlue.png')}/>
                }
                 <Text>{this.state.data[2]}</Text>
                </TouchableOpacity>

                <TouchableOpacity style= {styles.radioBtn}  onPress={() =>{this.setState({checked:'1',
                packageName:'Diamond Package (Exclusive with Licensed Agent)',
                package:[{ title: 'Displayed in Featured Listings'},
                {title: 'Unlimited prospective renter Phone and email leads'},
                {title: 'Advertisement Published on 88+ of the most popular rental listing sites'},
                {title: 'Professional Photography and videography-Unlimited Media Upload'},
                {title: 'FULL display of rental address for nonpaying renter members'},
                {title:"30 Day Runtime"},
                {title:"Tenant Prescreening"},
                {title:"Full Tenant’s Credit and background check"},
                {title:"4% of Lease contract_Pay Later"}
              ]
                })}}>
                {
                   this.state.checked == '1' ?
                    <Image style={styles.radioImg} source={require('../image/blueCheck.png')}/>
                    :
                    <Image style={styles.radioImg} source={require('../image/UnCheckedBlue.png')}/>
                }
                 <Text>{this.state.data[3]}</Text>
                </TouchableOpacity>
                 </View>

         <TouchableOpacity onPress={()=>this.toggleModal()} style={styles.signUpBtn}>
          <Image
              style={{height:hp('4%'), width:hp('4%')}}
              source={require("../image/calendar.png")}
            />
        <Text style={styles.packageDetailText}>View Package Details</Text>
        </TouchableOpacity>

         <View style={styles.checkBoxView}>
        <CheckBox
        style={styles.checkBoxStyle}
        checkBoxColor={'#034d94'}
        checkedCheckBoxColor={'#034d94'}
         onClick={()=>{
         this.setState({
             firstCheck:!this.state.firstCheck
          })
        }}
       isChecked={this.state.firstCheck}
        />
        <Text style={{color:'gray'}}>Rent Collect Add On</Text>
        </View>

         <View style={styles.checkBoxView}>
        <CheckBox
        style={styles.checkBoxStyle}
        checkBoxColor={'#034d94'}
        checkedCheckBoxColor={'#034d94'}
         onClick={()=>{
         this.setState({
             secondCheck:!this.state.secondCheck
          })
        }}
       isChecked={this.state.secondCheck}
        />
        <Text style={{color:'gray'}}>Hide Property</Text>
        </View>

        <View style={styles.checkBoxView}>
        <CheckBox
        style={styles.checkBoxStyle}
        checkBoxColor={'#034d94'}
        checkedCheckBoxColor={'#034d94'}
         onClick={()=>{
         this.setState({
             thirdCheck:!this.state.thirdCheck
          })
        }}
       isChecked={this.state.thirdCheck}
        />
        <Text style={{color:'gray'}}>I Accept All Term and Condition</Text>
        </View>
        { this.state.thirdCheckError == true ? (
             <Text style={styles.errorMessage}>
                 * Please accept term and condition.
             </Text>
            ) : null  }
           </ScrollView>
           </View>
           
             
          </View>
          <Modal  isVisible={this.state.isModalVisible}
           onBackdropPress={() => this.setState({ isModalVisible: false })}>
          <View style={{backgroundColor: "#ededed",borderRadius:10 }}>
        <View style={{flexDirection:'row',justifyContent:'space-between',borderBottomColor:'#333333',borderBottomWidth:0.8,paddingVertical:10}}>
          <Text style={{fontSize:18,color:'#333333',flex:1,textAlign:'center'}}>Terms and Conditions</Text>
          <Icon onPress={()=>this.setState({ isModalVisible: false })} name="ios-close" style={{fontSize:20,color:'#000',paddingRight:8}}></Icon>
        </View>
<Text style={{paddingVertical:10,color:'#333333',paddingLeft:10,fontWeight:'bold',fontSize:14}}>{this.state.packageName}</Text>
   <FlatList
       extraData={this.state}
        data={this.state.package}
        renderItem={({ item }) =><View style={{flexDirection:'row',paddingLeft:15,marginTop:2}}>
        <View style={{height:5,width:5,borderRadius:5/2,backgroundColor:'#333333',marginTop:8}}></View>
        <Text style={{fontSize:14,color:'#333333'}}> {item.title}</Text>
        </View> }
        
      />
     <TouchableOpacity onPress={()=>this.setState({ isModalVisible: false })} style={{alignSelf:'center',backgroundColor:'#5bc0de',borderRadius:4,paddingHorizontal:15,paddingVertical:10,marginBottom:10}}>
<Text style={{fontSize:14,color:'#fff'}}>Accept</Text>
     </TouchableOpacity>

          </View>
        </Modal>
           </SafeAreaView>
    );
  }
}
const styles = StyleSheet.create({
container: {
    flex: 1,
   backgroundColor: "#ededed",
    paddingBottom:'4%',
  },
  mainView:{
    backgroundColor:'white',
  // flex:1,
   height:'100%',
   margin:'5%', 
  borderRadius:10,
  },
 nameStyleWithDot:{
     marginTop:hp('2%'), 
     marginStart:wp('2%'),
 },
  nameStyle:{
    marginTop:hp('2%'), 
     marginStart:wp('10%'),
  // backgroundColor:'yellow', 
 },
 textInput:{
    marginStart:wp('10%'), 
      marginEnd:wp('10%'), 
     marginTop:-hp('1.3%'),
     //backgroundColor:'yellow', 
    // height:hp('5%')
 },
          signUpBtn:{
   height:hp('7%'),
    backgroundColor:'#034d94',
     width:'70%',
      marginStart:'18%', 
     marginEnd:'18%',
  marginTop:hp('2%'),
   marginBottom:hp('4%'), 
   borderRadius:10,
   flexDirection:'row',
    justifyContent:'space-between',
     alignItems:'center',
     paddingEnd:'6%',
     paddingStart:'6%'
     },
 textinputBorder:{
   marginStart:wp('5%'), 
   marginEnd:wp('5%'), 
    backgroundColor:'gray',
     height:1,
      width:'85%',
      marginTop:-hp('1%')
      },
  requiredDot:{
    height:6,
     width:6,
      borderRadius:3,
       backgroundColor:'red',
         marginStart:wp('8%'),
         marginTop:hp('2%'), 
       },
    requiredField:{
      flexDirection:'row',
       alignItems:'center'
    },
    indicator:{ 
         height:12,
          width:12,
          borderRadius:6,
          backgroundColor:'#008f2c',
         margin:wp('1%'),
       },
       nextBtn:{
         height:hp('7%'),
       backgroundColor:'#034d94',
       width:wp('25%'),
      borderRadius:10,
     justifyContent:'center',
       alignItems:'center',
       flexDirection:'row', 
       paddingStart:wp('2%'),
        paddingEnd:wp('2%')
       },
       nextText:{
         fontSize:13,
          fontWeight:'bold',
           color:'white'
           },
          footer:{
            backgroundColor:'white',
             height:'11%',
              justifyContent:'space-between', 
              flexDirection:'row',
            alignItems:'center',
            paddingStart:'7%',
           paddingEnd:'7%'
       },
        checkBoxView:{
            height:hp('5%'),
             flexDirection:'row',
              marginStart:'10%', 
              alignItems:'center', 
    },
    checkBoxStyle:{ 
        paddingEnd: 10
  },
  packageDetailText:{
      fontSize:14, 
      fontWeight:"bold",
       color:'white'
    },
    radioImg:{
       height:hp('4%'),
       width:hp('4%'),
      marginRight:10
    },
     radioBtn:{
          flexDirection:'row',
     alignItems:'center',
     width:'40%',
     // backgroundColor:'red'
 },
 radioBtnRow:{
   flexDirection:'row',
    height:hp('7%'),
     alignItems:'center', 
     paddingStart:'10%'
},
errorMessage:{
  fontSize:11,
  color:'red',
  marginLeft:'10%'
}
});