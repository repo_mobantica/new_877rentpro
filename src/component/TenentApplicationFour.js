import React, { Component } from "react";
import {
  Text,
  View,
  SafeAreaView,
  Image,
  TouchableOpacity,
  TouchableHighlight,
  StyleSheet,
  Dimensions,
  Alert,
  Button,
  FlatList,
  TextInput,
  ScrollView
} from "react-native";
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp
} from "react-native-responsive-screen";
import CheckBox from "react-native-check-box";
import Dash from "react-native-dash";
import RNPickerSelect from "react-native-picker-select";
import DatePicker from "react-native-datepicker";
import commonStyles from "../styles/index";
import DocumentPicker from "react-native-document-picker";
import ImagePicker from "react-native-image-picker";
import RadioForm, {
  RadioButton,
  RadioButtonInput,
  RadioButtonLabel
} from "react-native-simple-radio-button";
import SignatureCapture from "react-native-signature-capture";
import Api from "../Api/Api";

import { ProgressDialog } from "react-native-simple-dialogs";
var radio_props = [
  { label: "Yes", value: 1 },
  { label: "No", value: 0 }
];
var incomeImage = "";
var idImage = "";
var imageData = "";
export default class TenantApplicationFour extends Component {
  constructor(props) {
    super(props);

    this.state = {
      isDuplex:true,
      checked:'2',
      data1: ["Student", "Employed", "Self Employed  "],
      data: ["Self Employed  ", "Employed", "Student"],
      catCheck: false,
      dogCheck: false,
      occupation: "",
      income: "",
      frequency: "1",
      otherSource: "",
      occupationError: false,
      incomeError: false,
      frequencyError: false,
      otherSourceError: false,
      employerName: "",
      employerAdress: "",
      dateTo: "",
      dateFrom: "",
      employerNameError: false,
      isDuplexError:false,
      employerAdressError: false,
      dateToError: false,
      dateFromError: false,
      date: "",
      maxDate: "",
      isExpanded: false,
      isSigning: false,
      singleFile: "",
      multipleFile: [],
      filePath: "",
      filePathIncome: "",
      imageData: "",
      idImage: "",
      incomeImage: "",
      showProgress: false,
      radioOne: 1,
      radioTwo: 1,
      radioThree: 1,
      idUrl: "",
      incomeUrl: "",
      signUrl: "",
      idError: false,
      incomeError: false,
      signerror: false,
      imageurl: null,
      signData: null
    };
  }

  componentDidMount() {
    let that = this;

    let date = new Date().getDate();
    let month = new Date().getMonth() + 1;
    let year = new Date().getFullYear();
    let current_date = date + "/" + month + "/" + year;
    that.setState(
      {
        date: current_date,
        maxDate: current_date
      },
      function() {
        console.log("datatat");
        console.log(this.state.date);
      }
    );
  }

  addressvalidate(text) {
    const reg = /^[A-Za-z 0-9.,\b]+$/;
    if (text != "") {
      if (reg.test(text) === false) {
        return false;
      } else {
        this.setState({ employerAdress: text, employerAdressError: false });
      }
    } else {
      this.setState({ employerAdress: "", employerAdressError: true });
    }
  }
  componentWillReceiveProps(props) {
    var data = props.data;
    let that = this;
    var imageurl = null;
    if (data != null) {
      Api("upload_image", {
        image: data.signature,
        type: "property"
      }).then(responseJson => {
        if (responseJson.status == 1) {
          imageurl = responseJson.response.data;
          that.setState({
            tenant_emp_status: data.tenant_emp_status,
            occupation: data.occupation,
            income: data.income,
            frequency: data.frequency_1,
            otherSource: data.other_sources,
            employerName: data.employer_name,
            employerAdress: data.employer_address,
            dateTo: data.end_date,
            dateFrom: data.start_date,
            radioOne: Number(data.is_evicted),
            radioTwo: Number(data.is_bankrupt),
            radioThree: Number(data.is_convicted),
            imageurl: imageurl,
            idImage: data.id_proof_path_first,
            incomeImage: data.id_proof_path_second,
            //signData:data.signature
            imageData: data.signature
          });
          console.log("signDatasignData", this.state.imageData);
        } else if (responseJson == false) {
          alert("No internet");
        } else {
        }
      });
    }
  }
  namevalidate(text) {
    const reg = /^[A-Za-z\b]+$/;
    if (text != "") {
      if (reg.test(text) === false) {
        return false;
      } else {
        this.setState({ employerName: text, employerNameError: false });
      }
    } else {
      this.setState({ employerName: "", employerNameError: true });
    }
  }

  occupationValidation(text) {
    const reg = /^[A-Za-z 0-9.,\b]+$/;
    if (text != "") {
      if (reg.test(text) === false) {
        return false;
      } else {
        this.setState({ occupation: text, occupationError: false });
      }
    } else {
      this.setState({ occupation: "", occupationError: true });
    }
  }

  sourceValidate(text) {
    const reg = /^[A-Za-z 0-9.,\b]+$/;
    if (text != "") {
      if (reg.test(text) === false) {
        return false;
      } else {
        this.setState({ otherSource: text, otherSourceError: false });
      }
    } else {
      this.setState({ otherSource: "", otherSourceError: true });
    }
  }

  incomeValidate = text3 => {
    const reg = /^[0-9.\b]+$/;

    if (text3 != "") {
      if (reg.test(text3) === false) {
        return false;
      } else {
        this.setState({ income: text3, incomeError: false });
      }
    } else {
      this.setState({ income: "", incomeError: true });
    }
  };
  collapseView() {
    this.setState({
      isExpanded: !this.state.isExpanded
    });
  }

  saveSign() {
    this.refs["sign"].saveImage();
    // this.baseConverted(data);

    this.setState({
      isSigning: false
    });
  }

  _onSaveEvent = result => {
    let baseImage = '"' + "data:image/jpeg;base64," + result.encoded + '"';
    imageData = baseImage;
    this.setState(
      {
        imageData: baseImage,
        signerror: false
      },
      function() {
        console.log("imageData1234567", this.state.imageData);
      }
    );
  };
  _onDragEvent = () => {
    // This callback will be called when the user enters signature
  };

  resetSign() {
    this.refs["sign"].resetImage();
    this.setState({
      isSigning: false
    });
  }

  showHideView() {
    this.setState({
      isSigning: true
    });
  }

  async selectMultipleFile(item) {
    //Opening Document Picker for selection of multiple file
    try {
      const results = await DocumentPicker.pickMultiple({
        type: [DocumentPicker.types.pdf],
        readContent: true
        //There can me more options as well find above
      });
      for (const res of results) {
        //Printing the log realted to the file
      }
      //Setting the state to show multiple file attributes
      // this.setState({ multipleFile: results }, function(){

      // });
      if (item === "ID") {
        this.setState({
          filePath: results[0]
        });
      } else {
        this.setState({
          filePathIncome: results[0]
        });
      }
    } catch (err) {
      //Handling any exception (If any)
      if (DocumentPicker.isCancel(err)) {
        //If user canceled the document selection
        alert("Canceled from multiple doc picker");
      } else {
        //For Unknown Error
        alert("Unknown Error: " + JSON.stringify(err));
        throw err;
      }
    }
  }
  nextPress() {
    let count = 0;

    let income = this.state.income;
    if (this.state.occupation == "") {
      this.setState({
        occupationError: true
      });
      count++;
    }
    if (idImage == "") {
      this.setState({
        idError: true
      });
      count++;
    }
    if (
      this.state.income == "" ||
      income.split(".").length > 2 ||
      income.charAt(0) === "." ||
      income.charAt(income.length - 1) === "."
    ) {
      this.setState({
        incomeError: true
      });
      count++;
    }
    if (imageData == "") {
      this.setState({
        signerror: true
      });
      count++;
    }
    if (this.state.frequency == "") {
      this.setState({
        frequencyError: true
      });
      count++;
    }
    if (this.state.isDuplex == "") {
      this.setState({
        isDuplexError: true
      });
      count++;
    }

    if (this.state.employerName === "" && this.state.checked === '1') {
      this.setState({
        employerNameError: true
      });
      count++;
    }

    if (this.state.employerAdress == "" && this.state.checked === '1') {
      this.setState({
        employerAdressError: true
      });
      count++;
    }

    if (this.state.dateTo == "" && this.state.checked === '1') {
      this.setState({
        dateToError: true
      });
      count++;
    }

    if (this.state.dateFrom == "" && this.state.checked === '1') {
      this.setState({
        dateFromError: true
      });
      count++;
    }
    if (count === 0) {
      let cat = 0;
      let dog = 0;
      if (this.state.catCheck) {
        cat = 1;
      } else {
        cat = 0;
      }

      if (this.state.dogCheck) {
        dog = 1;
      } else {
        dog = 0;
      }
      let rentSix = {
        isNext: true,
        tenanatIs: this.state.checked,
        cat: cat,
        dog: dog,
        employerName: this.state.employerName,
        employerAdress: this.state.employerAdress,
        dateFrom: this.state.dateFrom,
        dateTo: this.state.dateTo,
        occupation: this.state.occupation,
        income: this.state.income,
        frequency: this.state.frequency,
        otherSource: this.state.otherSource,
        radioOne: this.state.radioOne,
        radioTwo: this.state.radioTwo,
        radioThree: this.state.radioThree,
        idUrl: this.state.idImage,
        incomeUrl: this.state.incomeImage,
        //  'signUrl':this.state.signData,
        signUrl: this.state.imageData
      };
      console.log("rentSix123", rentSix);
      return rentSix;
    } else {
      let rentSix = { isNext: false };
      console.log("rentSix", rentSix);
      return rentSix;
    }
  }

  chooseFile(item) {
    var options = {
      title: "Select Image",
      customButtons: [{ name: "customOptionKey", title: "Uplod PDF" }],
      storageOptions: {
        skipBackup: true,
        path: "images"
      }
    };
    ImagePicker.showImagePicker(options, response => {
      if (response.didCancel) {
        console.log("User cancelled image picker");
      } else if (response.error) {
        console.log("ImagePicker Error: ");
        console.log("ImagePicker Error: ", response.error);
      } else if (response.customButton) {
        console.log("User tapped custom button: ");
        console.log("User tapped custom button: ", response.customButton);
        alert(response.customButton);
        this.selectMultipleFile(item);
      } else {
        let source = response;
        // console.log('what happen??');
        // console.log(response);
        let baseImage = {
          uri: '"' + "data:image/jpeg;base64," + response.data + '"'
        };
        // let baseImage =  '"'+response.data+'"';

        // You can also display the image using data:
        // let source = { uri: 'data:image/jpeg;base64,' + response.data };
        if (item === "ID") {
          idImage = baseImage.uri;
          this.setState(
            {
              filePath: source,
              idImage: baseImage.uri,
              idError: false
              // idError:true
            },
            function() {
              console.log("idImage", this.state.idImage);
            }
          );
        } else {
          incomeImage = baseImage.uri;
          this.setState(
            {
              filePathIncome: source,
              incomeImage: baseImage.uri,
              incomeError: false
            },
            function() {
              console.log("incomeImage", this.state.incomeImage);
            }
          );
        }
      }
    });
  }
  handleChange() {
    // let checked = this.state.creaditDataHolder;
    let checked = this.state.isDuplex
    console.log("check", checked);
 
    this.setState({
      isDuplex:!this.state.isDuplex
  },
  function(){
    if(this.state.isDuplex){
      this.setState({  isDuplexError :false })
  }
  })


    
  }
  render() {
    return (
      <SafeAreaView style={styles.container}>
        <View style={styles.container}>
          <View style={styles.mainView}>
            <ScrollView>
              <View style={styles.titleView}>
                <Text style={styles.titleText}>Employment Details</Text>
              </View>

              <Text style={styles.nameStyle}>I am a,</Text>

              <View style={styles.radioBtnRow}>
                <TouchableOpacity
                  style={styles.radioBtn}
                  onPress={() => {
                    this.setState({
                      checked: '2',
                      dateFromError: false,
                      dateToError: false,
                      employerAdressError: false,
                      employerNameError: false
                    });
                  }}
                >
                  {this.state.checked == '2' ? (
                    <Image
                      style={styles.radioImg}
                      source={require("../image/blueCheck.png")}
                    />
                  ) : (
                    <Image
                      style={styles.radioImg}
                      source={require("../image/UnCheckedBlue.png")}
                    />
                  )}
                  <Text>{this.state.data[0]}</Text>
                </TouchableOpacity>

                <TouchableOpacity
                  style={styles.radioBtn}
                  onPress={() => {
                    this.setState({ checked: '1' });
                  }}
                >
                  {this.state.checked == '1' ? (
                    <Image
                      style={styles.radioImg}
                      source={require("../image/blueCheck.png")}
                    />
                  ) : (
                    <Image
                      style={styles.radioImg}
                      source={require("../image/UnCheckedBlue.png")}
                    />
                  )}
                  <Text>{this.state.data[1]}</Text>
                </TouchableOpacity>
              </View>
              <View style={styles.radioBtnRow}>
                <TouchableOpacity
                  style={styles.radioBtn}
                  onPress={() => {
                    this.setState({
                      checked: '0',
                      dateFromError: false,
                      dateToError: false,
                      employerAdressError: false,
                      employerNameError: false
                    });
                  }}
                >
                  {this.state.checked == '0' ? (
                    <Image
                      style={styles.radioImg}
                      source={require("../image/blueCheck.png")}
                    />
                  ) : (
                    <Image
                      style={styles.radioImg}
                      source={require("../image/UnCheckedBlue.png")}
                    />
                  )}
                  <Text>{this.state.data[2]}</Text>
                </TouchableOpacity>
              </View>

              <Text style={styles.nameStyle}>Pets allowed?</Text>
              <View style={styles.radioBtnRow}>
                <CheckBox
                  style={styles.checkBoxStyle}
                  checkBoxColor={"#034d94"}
                  checkedCheckBoxColor={"#034d94"}
                  onClick={() => {
                    this.setState({
                      catCheck: !this.state.catCheck
                    });
                  }}
                  isChecked={this.state.catCheck}
                />
                <Text style={styles.checkBoxText}>Cat</Text>

                <CheckBox
                  style={styles.checkBoxStyle}
                  checkBoxColor={"#034d94"}
                  checkedCheckBoxColor={"#034d94"}
                  onClick={() => {
                    this.setState({
                      dogCheck: !this.state.dogCheck
                    });
                  }}
                  isChecked={this.state.dogCheck}
                />
                <Text style={styles.checkBoxText}>Dog</Text>
              </View>

              {this.state.checked === '1' ? (
                <View>
                  <View style={styles.requiredField}>
                    <View style={styles.requiredDot}></View>
                    <Text style={styles.nameStyleWithDot}>Employer Name</Text>
                  </View>
                  <TextInput
                    style={styles.textInput}
                    underlineColorAndroid="transparent"
                    placeholder="Enter Employer Name here"
                    onChangeText={text => this.namevalidate(text)}
                    value={this.state.employerName}
                    returnKeyType={"next"}
                    autoFocus={true}
                    onSubmitEditing={event => {
                      this.refs.incomeNext.focus();
                    }}
                  />
                  <View style={styles.textinputBorder}></View>
                </View>
              ) : null}

              {this.state.employerNameError == true ? (
                <Text style={styles.errorMessage}>
                  * Please enter correct input to proceed.
                </Text>
              ) : (
                <View style={{ height: hp("3%") }}></View>
              )}

              {this.state.checked === '1' ? (
                <View>
                  <View style={styles.requiredField}>
                    <View style={styles.requiredDot}></View>
                    <Text style={styles.nameStyleWithDot}>
                      Employer Address
                    </Text>
                  </View>
                  <TextInput
                    style={styles.textInput}
                    underlineColorAndroid="transparent"
                    placeholder="Enter Employer Address here"
                    onChangeText={text => this.addressvalidate(text)}
                    value={this.state.employerAdress}
                    returnKeyType={"next"}
                    autoFocus={true}
                    onSubmitEditing={event => {
                      this.refs.incomeNext.focus();
                    }}
                  />
                  <View style={styles.textinputBorder}></View>
                </View>
              ) : null}

              {this.state.employerAdressError == true ? (
                <Text style={styles.errorMessage}>
                  * Please enter correct input to proceed.
                </Text>
              ) : (
                <View style={{ height: hp("3%") }}></View>
              )}

              {this.state.checked === '1' ? (
                <View>
                  <View style={commonStyles.requiredField}>
                    <View style={commonStyles.requiredDot}></View>
                    <Text style={commonStyles.nameStyleWithDot}>From</Text>
                  </View>
                  <View style={styles.birthdayView}>
                    {this.state.dateFrom === "" ? (
                      <Text style={{ color: "gray", marginRight: "20%" }}>
                        DD/MM/YYYY
                      </Text>
                    ) : (
                      <Text style={{ marginRight: "25%" }}>
                        {this.state.dateFrom}
                      </Text>
                    )}
                    <DatePicker
                      style={{ width: 200 }}
                      date={this.state.date}
                      mode="date"
                      placeholder="select date"
                      format="DD/MM/YYYY"
                      // minDate="2016-05-01"
                      maxDate={this.state.maxDate}
                      confirmBtnText="Confirm"
                      cancelBtnText="Cancel"
                      hideText="true"
                      iconSource={require("../image/CalendarBlue.png")}
                      customStyles={{
                        dateInput: {
                          borderWidth: 0
                        },
                        dateIcon: {
                          bottom: 2
                        }
                      }}
                      onDateChange={date => {
                        this.setState({ dateFrom: date, dateFromError: false });
                      }}
                    />
                  </View>
                  <View style={commonStyles.textinputBorder}></View>
                </View>
              ) : null}

              {this.state.dateFromError == true ? (
                <Text style={commonStyles.errorMessage}>
                  Please select date
                </Text>
              ) : null}

              {this.state.checked === '1' ? (
                <View>
                  <View style={commonStyles.requiredField}>
                    <View style={commonStyles.requiredDot}></View>
                    <Text style={commonStyles.nameStyleWithDot}>To</Text>
                  </View>
                  <View style={styles.birthdayView}>
                    {this.state.dateTo === "" ? (
                      <Text style={{ color: "gray", marginRight: "20%" }}>
                        DD/MM/YYYY
                      </Text>
                    ) : (
                      <Text style={{ marginRight: "25%" }}>
                        {this.state.dateTo}
                      </Text>
                    )}
                    <DatePicker
                      style={{ width: 200 }}
                      date={this.state.date}
                      mode="date"
                      placeholder="select date"
                      format="DD/MM/YYYY"
                      // minDate="2016-05-01"
                      maxDate={this.state.maxDate}
                      confirmBtnText="Confirm"
                      cancelBtnText="Cancel"
                      hideText="true"
                      iconSource={require("../image/CalendarBlue.png")}
                      customStyles={{
                        dateInput: {
                          borderWidth: 0
                        },
                        dateIcon: {
                          bottom: 2
                        }
                      }}
                      onDateChange={date => {
                        this.setState({ dateTo: date, dateToError: false });
                      }}
                    />
                  </View>
                  <View style={commonStyles.textinputBorder}></View>
                </View>
              ) : null}

              {this.state.dateToError == true ? (
                <Text style={commonStyles.errorMessage}>
                  please select date
                </Text>
              ) : null}

              <View style={styles.requiredField}>
                <View style={styles.requiredDot}></View>
                <Text style={styles.nameStyleWithDot}>Present Occupation</Text>
              </View>
              <TextInput
                style={styles.textInput}
                underlineColorAndroid="transparent"
                placeholder="Enter Present Occupation here"
                onChangeText={text => this.occupationValidation(text)}
                value={this.state.occupation}
                returnKeyType={"next"}
                autoFocus={true}
                onSubmitEditing={event => {
                  this.refs.incomeNext.focus();
                }}
              />
              <View style={styles.textinputBorder}></View>
              {this.state.occupationError == true ? (
                <Text style={styles.errorMessage}>
                  * Please enter correct input to proceed.
                </Text>
              ) : (
                <View style={{ height: hp("3%") }}></View>
              )}

              <View style={styles.requiredField}>
                <View style={styles.requiredDot}></View>
                <Text style={styles.nameStyleWithDot}>
                  Current Gross Income
                </Text>
              </View>
              <TextInput
                style={styles.textInput}
                underlineColorAndroid="transparent"
                placeholder="Enter Current Gross Income"
                onChangeText={text => this.incomeValidate(text)}
                value={this.state.income}
                keyboardType="number-pad"
                ref="incomeNext"
                returnKeyType={"next"}
                onSubmitEditing={event => {
                  this.refs.SourceNext.focus();
                }}
              />
              <View style={styles.textinputBorder}></View>
              {this.state.incomeError == true ? (
                <Text style={styles.errorMessage}>
                  * Please enter correct input to proceed.
                </Text>
              ) : (
                <View style={{ height: hp("3%") }}></View>
              )}

              <View style={styles.requiredField}>
                <View style={styles.requiredDot}></View>
                <Text style={styles.nameStyleWithDot}>Frequency </Text>
              </View>
              <View style={styles.textInput}>
                <RNPickerSelect
                placeholder={{}}
                value={this.state.frequency}
                  onValueChange={value =>
                    this.setState({ frequency: value, frequencyError: false })
                  }
                  items={[
                    { label: "Yearly", value: "1" },
                    { label: "Monthly", value: "2" },
                    { label: "Weekly", value: "3" }
                  ]}
                />
              </View>
              <View style={styles.textinputBorder}></View>
              {this.state.frequencyError == true ? (
                <Text style={styles.errorMessage}>
                  * Please enter correct input to proceed.
                </Text>
              ) : (
                <View style={{ height: hp("3%") }}></View>
              )}

              <Text style={styles.nameStyleNoDot}>Other Source</Text>
              <TextInput
                style={styles.textInput}
                underlineColorAndroid="transparent"
                placeholder="Enter Other Source here"
                onChangeText={text => this.sourceValidate(text)}
                value={this.state.otherSource}
                ref="SourceNext"
              />
              <View style={styles.textinputBorder}></View>
              {this.state.otherSourceError == true ? (
                <Text style={styles.errorMessage}>
                  * Please enter correct input to proceed.
                </Text>
              ) : (
                <View style={{ height: hp("3%") }}></View>
              )}
              <View style={styles.titleView}>
                <Text style={styles.titleText}>Important and Disclaimer</Text>
              </View>

              <Text style={styles.nameStyle}>
                Have you ever been evicted/asked to move?
              </Text>
              <View style={styles.radioBtn}>
                <RadioForm
                  radio_props={radio_props}
                  initial={this.state.radioOne}
                  formHorizontal={true}
                  buttonSize={15}
                  buttonInnerColor={"#034d94"}
                  buttonOuterColor={"#034d94"}
                  labelStyle={{ marginRight: wp("15%") }}
                  onPress={value => {
                    this.setState({ radioOne: value }, function() {
                      console.log("radioOne");
                      console.log(this.state.radioOne);
                    });
                  }}
                />
              </View>

              <Text style={styles.nameStyle}>
                Have you ever filed bankrucpy?
              </Text>
              <View style={styles.radioBtn}>
                <RadioForm
                  radio_props={radio_props}
                  initial={this.state.radioTwo}
                  formHorizontal={true}
                  buttonSize={15}
                  buttonInnerColor={"#034d94"}
                  buttonOuterColor={"#034d94"}
                  labelStyle={{ marginRight: wp("15%") }}
                  onPress={value => {
                    this.setState({ radioTwo: value }, function() {
                      console.log("radioTwo");
                      console.log(this.state.radioTwo);
                    });
                  }}
                />
              </View>

              <Text style={styles.nameStyle}>
                Have you ever been convicted of selling distributing or
                manufacturing illegal drugs?
              </Text>
              <View style={styles.radioBtn}>
                <RadioForm
                  radio_props={radio_props}
                  initial={this.state.radioThree}
                  formHorizontal={true}
                  buttonSize={15}
                  buttonInnerColor={"#034d94"}
                  buttonOuterColor={"#034d94"}
                  labelStyle={{ marginRight: wp("15%") }}
                  onPress={value => {
                    this.setState({ radioThree: value }, function() {
                      console.log("radioThree");
                      console.log(this.state.radioThree);
                    });
                  }}
                />
              </View>

              <Text style={styles.uploadTextStyle}>Uplod Id</Text>

              <TouchableOpacity
                style={styles.uploadView}
                onPress={() => this.chooseFile("ID")}
              >
                {this.state.filePath === "" ? (
                  <Text style={styles.uploadText}>Drop/Upload file here</Text>
                ) : (
                  <Text style={styles.uploadText}>
                    {this.state.filePath.type}
                  </Text>
                )}
                <View style={{ height: hp("6%"), width: hp("6%") }}>
                  <Image
                    style={styles.calenderImg}
                    source={require("../image/upload.png")}
                  />
                </View>
              </TouchableOpacity>

              {this.state.idError == true ? (
                <Text style={styles.errorMessage}>
                  * Please enter correct input to proceed.
                </Text>
              ) : null}

              <Text style={styles.uploadTextStyle}>Income Proof</Text>

              <TouchableOpacity
                style={styles.uploadView}
                onPress={() => this.chooseFile("Income")}
              >
                {this.state.filePathIncome === "" ? (
                  <Text style={styles.uploadText}>Drop/Upload file here</Text>
                ) : (
                  <Text style={styles.uploadText}>
                    {this.state.filePathIncome.type}
                  </Text>
                )}
                <View style={{ height: hp("6%"), width: hp("6%") }}>
                  <Image
                    style={styles.calenderImg}
                    source={require("../image/upload.png")}
                  />
                </View>
              </TouchableOpacity>

              {this.state.incomeError == true ? (
                <Text style={styles.errorMessage}>
                  * Please enter correct input to proceed.
                </Text>
              ) : null}

              {this.state.imageurl != null ? (
                <View>
                  <Text style={styles.uploadTextStyle}>Uploaded sign</Text>
                  <Image
                    resizeMode="contain"
                    style={{
                      height: 200,
                      width: Dimensions.get("window").width - 50,
                      alignSelf: "center"
                    }}
                    source={{ uri: this.state.imageurl }}
                  ></Image>
                </View>
              ) : null}
              <Text style={styles.uploadTextStyle}>Sign Here</Text>
              <View style={styles.signatureView}>
                <SignatureCapture
                  style={{ height: "100%", width: "100%" }}
                  ref="sign"
                  onSaveEvent={this._onSaveEvent}
                  onDragEvent={() => {
                    this._onDragEvent;
                    this.showHideView();
                  }}
                  saveImageFileInExtStorage={false}
                  showNativeButtons={false}
                  showTitleLabel={false}
                  viewMode={"portrait"}
                />
              </View>
              {this.state.signerror == true ? (
                <Text style={styles.errorMessage}>
                  * Please enter correct input to proceed.
                </Text>
              ) : null}

              {this.state.isSigning === true ? (
                <View style={{ flex: 1, flexDirection: "row" }}>
                  <TouchableHighlight
                    style={styles.buttonStyle}
                    onPress={() => {
                      this.saveSign();
                    }}
                  >
                    <Text>Save</Text>
                  </TouchableHighlight>

                  <TouchableHighlight
                    style={styles.buttonStyle}
                    onPress={() => {
                      this.resetSign();
                    }}
                  >
                    <Text>Reset</Text>
                  </TouchableHighlight>
                </View>
              ) : null}

              <View
                style={{
                  justifyContent: "space-between",
                  flexDirection: "row"
                }}
              >
                <Text style={styles.uploadTextStyle}>Disclaimer</Text>
                <CheckBox
                  style={{ padding: 10 }}
                  checkBoxColor={"#034d94"}
                  checkedCheckBoxColor={"#034d94"}
                  isChecked={this.state.isDuplex}
                  onClick={() => this.handleChange()}
                  />
              </View>

{this.state.isDuplexError == true ? (
                <Text style={styles.errorMessage}>
                  * Please enter correct input to proceed.
                </Text>
              ) : null}

              {this.state.isExpanded ? (
                <View style={styles.disclaimerView}>
                  <Text style={styles.disclaimerText}>
                    {" "}
                    Applicant represents that all the above statements are true
                    and correct, authorizes verification of the above items and
                    agrees to furnish additional credit references upon request.
                    Applicant authorizes the Owner/Agent to obtain reports that
                    may include credit reports, unlawful detainer (eviction)
                    reports, bad check searches, social security number
                    verification, fraud warnings, previous tenant history and
                    employment history. Applicant consents to allow
                    877RENTPRO.com to disclose tenancy information to previous
                    or subsequent Owners/Agents
                  </Text>
                  <TouchableOpacity onPress={() => this.collapseView()}>
                    <Text style={styles.moreOrLess}>Read less</Text>
                  </TouchableOpacity>
                </View>
              ) : (
                <View style={[styles.disclaimerView, { height: hp("20%") }]}>
                  <Text style={styles.disclaimerText} numberOfLines={5}>
                    {" "}
                    Applicant represents that all the above statements are true
                    and correct, authorizes verification of the above items and
                    agrees to furnish additional credit references upon request.
                    Applicant authorizes the Owner/Agent to obtain reports that
                    may include credit reports, unlawful detainer (eviction)
                    reports, bad check searches, social security number
                    verification, fraud warnings, previous tenant history and
                    employment history. Applicant consents to allow
                    877RENTPRO.com to disclose tenancy information to previous
                    or subsequent Owners/Agents
                  </Text>
                  <TouchableOpacity onPress={() => this.collapseView()}>
                    <Text style={styles.moreOrLess}>Read more</Text>
                  </TouchableOpacity>
                </View>
              )}
            </ScrollView>
          </View>
        </View>
      </SafeAreaView>
    );
  }
}
const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "#ededed",
    paddingBottom: "3%"
  },
  mainView: {
    backgroundColor: "white",
    height: "100%",
    margin: "5%",
    borderRadius: 10
  },
  nameStyleWithDot: {
    marginTop: hp("2%"),
    marginStart: wp("2%")
  },
  textInput: {
    marginStart: wp("10%"),
    marginTop: -hp("1.3%")
    //backgroundColor:'yellow',
    // height:hp('5%')
  },
  textinputBorder: {
    marginStart: wp("5%"),
    marginEnd: wp("5%"),
    backgroundColor: "gray",
    height: 1,
    width: "85%",
    marginTop: -hp("1%"),
    marginBottom: hp("1.2%")
  },
  requiredDot: {
    height: 6,
    width: 6,
    borderRadius: 3,
    backgroundColor: "red",
    marginStart: wp("8%"),
    marginTop: hp("2%")
  },
  titleView: {
    height: hp("8%"),
    justifyContent: "center",
    alignItems: "center"
  },
  titleText: {
    fontSize: 16,
    fontWeight: "bold"
  },
  requiredField: {
    flexDirection: "row",
    alignItems: "center"
  },
  radioImg: {
    height: hp("4%"),
    width: hp("4%"),
    marginRight: 10
  },
  radioBtn: {
    flexDirection: "row",
    alignItems: "center",
    width: "40%"
    // backgroundColor:'red'
  },
  radioBtnRow: {
    flexDirection: "row",
    height: hp("7%"),
    alignItems: "center",
    paddingStart: "10%",
    paddingEnd: "10%"
  },
  nameStyle: {
    marginTop: hp("2%"),
    marginStart: wp("10%"),
    fontSize: 16,
    color: "gray"
    // backgroundColor:'yellow',
  },
  nameStyleNoDot: {
    marginTop: hp("5%"),
    marginStart: wp("10%")
  },
  checkBoxStyle: {
    paddingEnd: 10
  },
  checkBoxText: {
    width: "40%"
  },
  errorMessage: {
    fontSize: 11,
    color: "red",
    marginLeft: "10%"
  },
  birthdayView: {
    flexDirection: "row",
    height: hp("6%"),
    marginStart: wp("10%"),
    justifyContent: "space-between",
    alignItems: "center",
    marginEnd: wp("5%")
  },
  uploadTextStyle: {
    marginTop: hp("2%"),
    marginStart: wp("10%")
    // backgroundColor:'yellow',
  },
  uploadView: {
    height: hp("8%"),
    marginStart: "8%",
    marginEnd: "8%",
    marginTop: hp("1%"),
    borderRadius: 10,
    borderStyle: "dotted",
    borderWidth: 1,
    borderColor: "gray",
    flex: 1,
    flexDirection: "row",
    alignItems: "center",
    justifyContent: "space-around"
  },
  calenderImg: {
    height: hp("3%"),
    width: hp("3%")
  },
  signatureView: {
    marginStart: "10%",
    marginEnd: "10%",
    borderRadius: 10,
    borderWidth: 1,
    height: hp("16%"),
    marginTop: hp("2%"),
    marginBottom: hp("2%"),
    padding: 3,
    // backgroundColor:'#d3e6ef',
    borderColor: "#034d94"
  },
  disclaimerView: {
    paddingEnd: "10%",
    // height:hp('30%'),
    paddingStart: "10%",
    flex: 1,
    marginTop: hp("2%")
  },
  disclaimerText: {
    lineHeight: 20,
    color: "#737373"
  },
  moreOrLess: {
    fontWeight: "bold",
    color: "#034d94",
    fontSize: 16,
    textAlign: "right"
  },
  radioBtn: {
    height: hp("7%"),
    justifyContent: "center",
    paddingStart: wp("10%")
  },
  buttonStyle: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center",
    height: 50,
    backgroundColor: "#eeeeee",
    margin: 10
  },
  errorMessage: {
    fontSize: 11,
    color: "red",
    marginLeft: "10%"
  }
});
