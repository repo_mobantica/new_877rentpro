import React, { Component } from 'react'
import { Image,Alert, View } from 'react-native'
import {Header,Left,Right,Body,Title,Button,Icon} from 'native-base'
import { withNavigation } from 'react-navigation';
import Drawer from 'react-native-drawer';
import {widthPercentageToDP as wp, heightPercentageToDP as hp} from 'react-native-responsive-screen';
class ToolbarWithLeftIcon extends Component {

  render() {
    return (
        <Header style={{backgroundColor:'white'}}>
        <Left style={{marginStart:-30 }}>
        {this.props.title =="Sign Up" ?
            <Button transparent onPress={()=>this.props.navigation.navigate('Login')}>
            <Icon><Image source={require("../image/left.png")} style={{width:25,height:20 }} /></Icon>
          </Button>
          :
          <Button transparent onPress={()=>this.props.navigation.openDrawer()}>
            <Icon><Image source={require("../image/menu.png")} style={{width:25,height:20 }} /></Icon>
          </Button>
        }
        </Left>
        <View style={{justifyContent:'center', alignItems:'center', width:'75%'}}>
          {
            this.props.image ? <Image source={require('../image/logo.png')} style={{height:30,width:40,alignSelf:"center", }} /> : <Title style={{textAlign:"center", color:'#034d94', fontSize:15, fontWeight:'bold', fontWeight:'bold', fontSize:18}}>{this.props.title || ""}</Title>
          }
        </View>
      </Header>
    )
  }
}

export default withNavigation(ToolbarWithLeftIcon);