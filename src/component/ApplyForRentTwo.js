import React, { Component } from 'react';
import { Text, View, SafeAreaView, Image, TouchableOpacity, StyleSheet, Dimensions, Alert,
Button, FlatList, TextInput, ScrollView, ImageBackground } from 'react-native';
import {widthPercentageToDP as wp, heightPercentageToDP as hp} from 'react-native-responsive-screen';
import RNPickerSelect from 'react-native-picker-select';
export default class ApplyForRentTwo extends Component {
    constructor(props) {
    super(props);

    this.state = {
     address:'',
     myState:'',
     state:'',
     city:'',
     zip:'',
     managerName:'',
     managerPhone:'',
     addressError:false,
     stateError:false,
     cityError:false,
     zipError:false,
     managerNameError:false,
     managerPhoneError:false,
    };
 }
 addressvalidate(text) {    
  const reg =  /^[A-Za-z 0-9.,\b]+$/;
   if(text!=''){
     if (reg.test(text) === false) {
       return false;
     }
     else {
     this.setState({address:text,
    addressError:false})
     }
     }
     else{
     this.setState({address:'',
    addressError:true})
     }
  }  

  nameValidate(text) {    
    const reg =  /^[A-Za-z\b]+$/;
     if(text!=''){
       if (reg.test(text) === false) {
         return false;
       }
       else {
       this.setState({managerName:text,
      managerNameError:false})
       }
       }
       else{
       this.setState({managerName:'',
       managerNameError:true})
       }
    }

    phoneValidate = (text3) => {
      const reg = /^[0-9\b]+$/;
      
      if(text3!=''){
      if (reg.test(text3) === false) {
        return false;
      } 
      else {
     this.setState({managerPhone:text3,
      managerPhoneError:false})
      }
      }
      else{
      this.setState({managerPhone:'',
      managerPhoneError:true})
      }
    }

    zipValidate = (text3) => {
      const reg = /^[0-9\b]+$/;
      
      if(text3!=''){
      if (reg.test(text3) === false) {
        return false;
      } 
      else {
      this.setState({zip:text3,
      zipError:false})
      }
      }
      else{
      this.setState({zip:'',
    zipError:true})
      }
    }  

    nextPress(){
      let mobileNo = this.state.managerPhone;
      let zip = this.state.zip;
      let count =0;
   
       if(this.state.address== ""){
      this.setState({
         addressError:true
       })
            count++;
     }
         if(this.state.state== ""){
      this.setState({
            stateError:true
         })
       count++;
     }
     if(this.state.city== ""){
            this.setState({
      cityError:true
            })
      count++;
           }
      if(this.state.managerPhone== "" || mobileNo.length < 10){
              this.setState({
        managerPhoneError:true
              })
        count++;
             }
         if(this.state.managerName== ""){
                this.setState({
          managerNameError:true
                })
          count++;
               }
           if(this.state.zip== "" || zip.length <4){
                  this.setState({
            zipError:true
                  })
            count++;
                 }
            if(count === 0){
              let rentTwo ={'isNext': true,
                        'address':this.state.address,
                         'state': this.state.state,
                         'city': this.state.city,
                         'zip' : this.state.zip,
                         'managerName': this.state.managerName,
                         'managerPhone':this.state.managerPhone,

                  }
                  console.log('rentTwo', rentTwo);
             return rentTwo;
            }
           else{
            let rentTwo ={'isNext': false,
                  }
             return rentTwo;
             console.log('rentTwo', rentTwo);
           } 
    }

  render() {
    
    return (
       <SafeAreaView style={styles.container}>
      <View style={styles.container}>
        
           <View style={styles.mainView}>
           <ScrollView>
             <View style={styles.titleView}>
            <Text style={styles.titleText}>Present Address</Text>
            </View>

          <View style={styles.requiredField}>
            <View style={styles.requiredDot}></View>
             <Text style={styles.nameStyleWithDot}>Address</Text>
             </View>
             <View style={styles.textInputView}>
            <TextInput
         style={styles.textInput}
           underlineColorAndroid = "transparent"
             style = {{ flex: 1, textAlignVertical:'top' }}
            numberOfLines={3}
          placeholder="Address here"
          onChangeText={text => this.addressvalidate(text)}  
          value={this.state.address}
          multiline={true}
          returnKeyType = {"next"}
          autoFocus = {true}
           onSubmitEditing={(event) => { 
           this.refs.stateNext.focus(); 
         }}
          />
        </View>
        <View style={styles.textinputBorder}></View>
        { this.state.addressError == true ? (
             <Text style={styles.errorMessage}>
                * Please enter correct input to proceed.
             </Text>
            ) : null  }

           <View style={styles.requiredField}>
            <View style={styles.requiredDot}></View>
             <Text style={styles.nameStyleWithDot}>State</Text>
             </View>
            <TextInput
         style={styles.textInput}
           underlineColorAndroid = "transparent"
            //  style = {{ flex: 1, textAlignVertical:'top' }}
          placeholder="state here"
          onChangeText={text => this.setState({state:text, stateError:false})}  
          value={this.state.state}
          ref='stateNext'
           returnKeyType = {"next"}
           onSubmitEditing={(event) => { 
            this.refs.cityNext.focus(); 
          }}
          />
        <View style={styles.textinputBorder}></View>
        { this.state.stateError == true ? (
             <Text style={styles.errorMessage}>
                * Please enter correct input to proceed.
             </Text>
            ) : null  }

          <View style={styles.requiredField}>
            <View style={styles.requiredDot}></View>
             <Text style={styles.nameStyleWithDot}>City</Text>
             </View>
            
            <TextInput
         style={styles.textInput}
           underlineColorAndroid = "transparent"
            //  style = {{ flex: 1, textAlignVertical:'top' }}
          placeholder="city here"
          onChangeText={text => this.setState({city:text, cityError:false})}  
          value={this.state.city}
          ref='cityNext'
           returnKeyType = {"next"}
           onSubmitEditing={(event) => { 
            this.refs.zipNext.focus(); 
          }}
          />
      
        <View style={styles.textinputBorder}></View>
        { this.state.cityError == true ? (
             <Text style={styles.errorMessage}>
                * Please enter correct input to proceed.
             </Text>
            ) : null  }

         <View style={styles.requiredField}>
            <View style={styles.requiredDot}></View>
             <Text style={styles.nameStyleWithDot}>ZIP</Text>
             </View>
            <TextInput
          style={styles.textInput}
           underlineColorAndroid = "transparent"
          placeholder="Enter Zip here"
          maxLength={6}
          onChangeText={text => this.zipValidate(text)}  
          value={this.state.zip}
          keyboardType="numeric"
          ref='zipNext'
           returnKeyType = {"next"}
           onSubmitEditing={(event) => { 
            this.refs.nameNext.focus(); 
          }}
        />
         <View style={styles.textinputBorder}></View>
         { this.state.zipError == true ? (
             <Text style={styles.errorMessage}>
                * Please enter correct input to proceed.
             </Text>
            ) : null  }

         <View style={styles.requiredField}>
            <View style={styles.requiredDot}></View>
             <Text style={styles.nameStyleWithDot}>Manager Name</Text>
             </View>
            <TextInput
          style={styles.textInput}
           underlineColorAndroid = "transparent"
          placeholder="Enter Manager Name here"
          onChangeText={text => this.nameValidate(text)}  
          value={this.state.managerName}
          ref='nameNext'
           returnKeyType = {"next"}
           onSubmitEditing={(event) => { 
            this.refs.PhoneNext.focus(); 
          }}
        />
         <View style={styles.textinputBorder}></View>
         { this.state.managerNameError == true ? (
             <Text style={styles.errorMessage}>
                * Please enter correct input to proceed.
             </Text>
            ) : null  }

         <View style={styles.requiredField}>
            <View style={styles.requiredDot}></View>
             <Text style={styles.nameStyleWithDot}>Manager Phone Number</Text>
             </View>
            <TextInput
          style={styles.textInput}
          maxLength={10}
           underlineColorAndroid = "transparent"
          placeholder="Enter Manager Phone Numbe here"
          onChangeText={text => this.phoneValidate(text)}  
          value={this.state.managerPhone}
          keyboardType="numeric"
          ref='PhoneNext'
        />
         <View style={styles.textinputBorder}></View>
         { this.state.managerPhoneError == true ? (
             <Text style={styles.errorMessage}>
                * Please enter correct input to proceed.
             </Text>
            ) : null  }
           </ScrollView>
           </View>
           </View>
           </SafeAreaView>
    );
  }
}
const styles = StyleSheet.create({
container: {
    flex: 1,
   backgroundColor: "#ededed",
   paddingBottom:'3%'
  },
  mainView:{
    backgroundColor:'white',
     height:'100%',
   margin:'5%', 
  borderRadius:10,
  },
 nameStyleWithDot:{
     marginTop:hp('2%'), 
     marginStart:wp('2%'),
 },
 textInput:{
    marginStart:wp('10%'), 
     marginTop:-hp('1.3%'),
       paddingEnd:'8%'
     //backgroundColor:'yellow', 
    // height:hp('5%')
 },
   textInputView:{
           height:hp('13%'),
            marginStart:'10%',
             marginEnd:'10%',
             justifyContent: 'flex-start',
        },
 textinputBorder:{
   marginStart:wp('5%'), 
   marginEnd:wp('5%'), 
    backgroundColor:'gray',
     height:1,
      width:'85%',
      marginTop:-hp('1%'), 
      marginBottom:hp('1.2%')
      },
  requiredDot:{
    height:6,
     width:6,
      borderRadius:3,
       backgroundColor:'red',
         marginStart:wp('8%'),
         marginTop:hp('2%'), 
       },
       titleView:{
         height:hp('8%'), 
         justifyContent:'center',
          alignItems:'center'
      },
      titleText:{
        fontSize:16,
         fontWeight:'bold'
    },
    requiredField:{
      flexDirection:'row',
       alignItems:'center'
    },
    errorMessage:{
      fontSize:11,
      color:'red',
      marginLeft:'10%'
    }
});
