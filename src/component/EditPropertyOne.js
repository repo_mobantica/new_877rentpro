import React, { Component } from "react";
import {
  Text,
  View,
  SafeAreaView,
  Image,
  TouchableOpacity,
  StyleSheet,
  Dimensions,
  Alert,
  Button,
  FlatList,
  TextInput,
  ScrollView
} from "react-native";
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp
} from "react-native-responsive-screen";
import ToolbarHomeImg from "../component/ToolbarHomeImg";
import DateTimePicker from "react-native-modal-datetime-picker";
import Dash from "react-native-dash";
import CheckBox from "react-native-check-box";
import { GooglePlacesAutocomplete } from 'react-native-google-places-autocomplete';
export default class AddNewPropertyOne extends Component {
  constructor(props) {
    super(props);

    this.state = {
      address: "",
      displayAddress: "",
      unitDetails: "",
      addressError: false,
      disAddressError: false,
      unitDetailsError: false,
      isSame: false,
      lat:null,
      lng:null,
    };
  }

  showDateTimePicker = () => {
    this.setState({ isDateTimePickerVisible: true });
  };

  hideDateTimePicker = () => {
    this.setState({ isDateTimePickerVisible: false });
  };

  handleDatePicked = date => {
    console.log("A date has been picked: ", date);
    let day = date.getDate();
    let month = date.getMonth();
    let year = date.getFullYear();
    let fullDate = day + "/" + month + "/" + year;
    console.log("A date has been picked: ", fullDate);
    this.setState({ birthday: fullDate });
    this.hideDateTimePicker();
    // Alert.alert(date)
  };
  addressvalidate(text) {
    // const reg = /^(?=.*[a-z])[0-9a-zA-Z ]*$/;
    const reg = /^[A-Za-z 0-9.,\b]+$/;
    if (text != "") {
      if (reg.test(text) === false) {
        return false;
      } else {
        if (this.state.isSame) {
          this.setState({
            address: text,
            displayAddress: text,
            addressError: false
          });
        } else {
          this.setState({ address: text, addressError: false });
        }
      }
    } else {
      this.setState({ address: "", addressError: true });
    }
  }

  DisAddressvalidate(text) {
    // const reg = /^(?=.*[a-z])[0-9a-zA-Z ]*$/;
    const reg = /^[A-Za-z 0-9.,\b]+$/;
    if (text != "") {
      if (reg.test(text) === false) {
        return false;
      } else {
        this.setState({ displayAddress: text, disAddressError: false });
      }
    } else {
      this.setState({ displayAddress: "", disAddressError: true });
    }
  }

  unitvalidate(text) {
    // const reg = /^(?=.*[a-z])[0-9a-zA-Z ]*$/;
    const reg = /^[A-Za-z 0-9.,\b]+$/;
    if (text != "") {
      if (reg.test(text) === false) {
        return false;
      } else {
        this.setState({ unitDetails: text, unitDetailsError: false });
      }
    } else {
      this.setState({ unitDetails: "", unitDetailsError: true });
    }
  }

  checkBoxPress() {
    this.setState(
      {
        isSame: !this.state.isSame
      },
      function() {
        if (this.state.isSame === true && this.state.address != "") {
          this.setState({
            disAddressError: false,
            displayAddress: this.state.address
          });
        }
      }
    );
  }

  componentWillReceiveProps(props) {
    var data = props.data;
    console.log("data123",data)
    let that = this;
    if (data != null) {
      var guarantor = false;
      // if (data.guarantor == "1") {
      //   guarantor = true;
      // }
      that.setState({
        address: data.address,
        displayAddress: data.display_address,
        // firstName: data.contact_details.firstname,
        // lastName: data.contact_details.lastname,
        // email: data.contact_details.email,
        unitDetails:data.unit_details,
        lat:data.latitude,
        lng:data.longitude
        // mobileNo: data.contact_details.mobileno,

        // workPhone: data.work_phone,
        // SSN: data.ssn,
        // idType: data.id_type,
        // idNumber: data.id_number,
        // guarantor: guarantor,
        // address: data.present_address,
        // state: data.present_state,
        // city: data.present_city,
        // zip: data.present_zip,
        // managerName: data.present_manager_name,
        // managerPhone: data.present_manager_phone
      });
     // console.log("address",this.state.address)
    }
  }

  nextPress() {
    let data = 0;
    if (this.state.address == "") {
      this.setState({
        addressError: true
      });
      data++;
    }
    if (this.state.displayAddress == "" && this.state.isSame === false) {
      this.setState({
        disAddressError: true
      });
      data++;
    }
    if (this.state.unitDetails == "") {
      this.setState({
        unitDetailsError: true
      });
      data++;
    }

    if (data === 0) {
      let addOne = {
        isNext: true,
        address: this.state.address,
        displayAddress: this.state.displayAddress,
        unitDetails: this.state.unitDetails,
        latitude:this.state.lat,
        longitute:this.state.lng
      };

      return addOne;
    } else {
      let addOne = { isNext: false };
      return addOne;
    }
  }
  render() {
    return (
      <SafeAreaView style={styles.container}>
        <View style={styles.container}>
          <View style={styles.mainView}>
            <ScrollView>
              <View style={styles.titleView}>
                <Text style={styles.titleText}>Present Address</Text>
              </View>

              <View style={styles.requiredField}>
                <View style={styles.requiredDot}></View>
                <Text style={styles.nameStyleWithDot}>Actual Address</Text>
              </View>
              <View style={styles.textInputView}>
                <TextInput
                  style={styles.textInput}
                  underlineColorAndroid="transparent"
                  style={{ flex: 1, textAlignVertical: "top" }}
                  // numberOfLines={3}
                  placeholder="Address here"
                  onChangeText={text => this.addressvalidate(text)}
                  value={this.state.address}
                  multiline={true}
                  returnKeyType={"next"}
                  autoFocus={true}
                  onSubmitEditing={event => {
                    this.refs.DisplayAddressN.focus();
                  }}
                />
              </View>
              <View style={styles.textinputBorder}></View>
              {this.state.addressError == true ? (
                <Text style={styles.errorMessage}>
                  * Please enter correct input to proceed.
                </Text>
              ) : null}

              <View style={styles.requiredField}>
                <View style={styles.requiredDot}></View>
                <Text style={styles.nameStyleWithDot}>Display Address</Text>
              </View>
              <View style={styles.textInputView}>
                {this.state.isSame ? (
                  <Text style={styles.sameAddress}>{this.state.address}</Text>
                ) : (
                  <TextInput
                    style={styles.textInput}
                    underlineColorAndroid="transparent"
                    style={{ flex: 1, textAlignVertical: "top" }}
                    numberOfLines={3}
                    placeholder="Same Address appear here"
                    onChangeText={text => this.DisAddressvalidate(text)}
                    value={this.state.displayAddress}
                    multiline={true}
                    ref="DisplayAddressN"
                    returnKeyType={"next"}
                    onSubmitEditing={event => {
                      this.refs.UnitNext.focus();
                    }}
                  />
                )}
              </View>
              <View style={styles.textinputBorder}></View>
              {this.state.disAddressError == true ? (
                <Text style={styles.errorMessage}>
                  * Please enter correct input to proceed.
                </Text>
              ) : null}
              <View style={styles.searchBox}>
                <CheckBox
                  style={{ paddingEnd: "2%" }}
                  checkBoxColor={"#034d94"}
                  checkedCheckBoxColor={"#034d94"}
                  onClick={() => {
                    this.checkBoxPress();
                  }}
                  isChecked={this.state.isSame}
                />
                <Text>Same as Actual Address</Text>
              </View>

              <View style={styles.requiredField}>
                <View style={styles.requiredDot}></View>
                <Text style={styles.nameStyleWithDot}>Unit Details</Text>
              </View>
              <View style={styles.textInputView}>
                <TextInput
                  style={styles.textInput}
                  underlineColorAndroid="transparent"
                  style={{ flex: 1, textAlignVertical: "top" }}
                  numberOfLines={3}
                  placeholder="unit details"
                  onChangeText={text => this.unitvalidate(text)}
                  value={this.state.unitDetails}
                  multiline={true}
                  ref="UnitNext"
                />
              </View>
              <View style={styles.textinputBorder}></View>
              {this.state.unitDetailsError == true ? (
                <Text style={styles.errorMessage}>
                  * Please enter correct input to proceed.
                </Text>
              ) : null}
            </ScrollView>
          </View>
          {/* <View style={styles.footer}>
          <View style={{flexDirection:'row'}}>
          <View style={styles.indicator}></View>
          <View style={[styles.indicator,{backgroundColor:'white', borderWidth:0.5}]}></View>
         </View>
            <TouchableOpacity style={styles.nextBtn}
    //  onPress={() => {this.facebookLogin()}}
       >
       <Text style={styles.nextText}>Next</Text>
       </TouchableOpacity>
            </View> */}
        </View>
      </SafeAreaView>
    );
  }
}
const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "#ededed",
    paddingBottom: "3%"
  },
  mainView: {
    backgroundColor: "white",
    height: "100%",
    margin: "5%",
    borderRadius: 10
  },
  nameStyleWithDot: {
    marginTop: hp("2%"),
    marginStart: wp("2%"),
    fontWeight: "bold"
  },
  textInput: {
    marginStart: wp("10%"),
    marginTop: -hp("1.3%")
    //backgroundColor:'yellow',
    // height:hp('5%')
  },
  sameAddress: {
    marginTop: hp("1%")
  },
  textinputBorder: {
    marginStart: wp("5%"),
    marginEnd: wp("5%"),
    backgroundColor: "gray",
    height: 1,
    width: "85%",
    marginTop: -hp("1%"),
    marginBottom: hp("1.2%")
  },
  requiredDot: {
    height: 6,
    width: 6,
    borderRadius: 3,
    backgroundColor: "red",
    marginStart: wp("8%"),
    marginTop: hp("2%")
  },
  titleView: {
    height: hp("8%"),
    justifyContent: "center",
    alignItems: "center"
  },
  titleText: {
    fontSize: 16,
    fontWeight: "bold"
  },
  requiredField: {
    flexDirection: "row",
    alignItems: "center"
  },
  indicator: {
    height: 12,
    width: 12,
    borderRadius: 6,
    backgroundColor: "#008f2c",
    margin: wp("1%")
  },
  nextBtn: {
    height: hp("7%"),
    backgroundColor: "#034d94",
    width: wp("25%"),
    borderRadius: 10,
    justifyContent: "center",
    alignItems: "center",
    flexDirection: "row",
    paddingStart: wp("2%"),
    paddingEnd: wp("2%")
  },
  nextText: {
    fontSize: 13,
    fontWeight: "bold",
    color: "white"
  },
  footer: {
    backgroundColor: "white",
    height: "11%",
    justifyContent: "space-between",
    flexDirection: "row",
    alignItems: "center",
    paddingStart: "7%",
    paddingEnd: "7%"
  },
  textInputView: {
    height: hp("13%"),
    marginStart: "10%",
    marginEnd: "10%",
    justifyContent: "flex-start"
  },
  searchBox: {
    height: hp("5%"),
    flexDirection: "row",
    alignItems: "flex-start",
    paddingStart: "10%"
  },
  errorMessage: {
    fontSize: 11,
    color: "red",
    marginLeft: "10%"
  }
});
